<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>中队分数排名列表--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel = "shortcut icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all" />
    <style>
        .detail-body{
            margin: 20px 0 0;
            color: #333;
            word-wrap: break-word;
        }
    </style>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>中队分数排名列表</legend>
    <div class="layui-field-box" >
<#--        <a class="layui-btn layui-btn-danger" lay-event="settlement">结算</a>-->
<#--        <a class="layui-btn layui-btn-primary" lay-event="exportData">导出数据</a>-->
<#--        <button class="layui-btn layui-btn-danger" id="settlement" lay-filter="settlement">结算</button>-->
<#--        <button class="layui-btn layui-btn-primary" id="exportData" lay-filter="exportData">导出数据</button>-->
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>

    <script type="text/html" id="userStatus">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.delFlag == false){ }}
        <span class="layui-badge layui-bg-green">正常</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-gray">停用</span>
        {{#  } }}
    </script>
    <script type="text/html" id="barDemo">
        {{# if(d.description == null || d.description == "" || d.description == undefined || d.description.indexOf('系统')<0){ }}
<#--        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="addType" >新增一个TYPE值</a>-->
<#--        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>-->
        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="editPoint_ldpy" >编辑</a>
<#--        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>-->
        {{# } }}
    </script>
</div>
<div id="page"></div>

<div id="changePoint_ldpyDiv" style="display: none" class="detail-body">
    <form  id="changePoint_ldpyForm" class="layui-form" style="width: 500px">
        <div class="layui-form-item">
            <label class="layui-form-label">队伍</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input layui-disabled" name="team" disabled lay-verify="required" placeholder="队伍名">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">原分数</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input layui-disabled" name="oldPoint_ldpy" disabled lay-verify="required" placeholder="原领导评议分数">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">新分数</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" name="newPoint_ldpy" lay-verify="required" placeholder="请输入新领导评议分数">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="changePoint_ldpy">立即提交</button>
            </div>
        </div>
    </form>
</div>
<#--<script type="text/html" id="toolbarDemo">-->
<#--    <div class="layui-btn-container">-->
<#--        <button class="layui-btn layui-btn-danger layui-btn-lg" lay-event="settlement">结算</button>-->
<#--        <button class="layui-btn layui-btn-primary layui-border-red layui-btn-lg" lay-event="resetScore">重置分数</button>-->
<#--    </div>-->
<#--</script>-->
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script type="text/javascript" src="${base}/static/layui/lay/modules/table.js"></script>
<script>
    layui.use(['layer','form','table'], function() {
        var layer = layui.layer,
                $ = layui.jquery,
                form = layui.form,
                table = layui.table,
                t,changeIndex;                  //表格数据变量

        t = {
            id: 'test',
            elem: '#test',
            url:'${base}/teamPoint/jck_totalPoint',
            // toolbar:'#toolbarDemo',
            toolbar:'true',
            defaultToolbar: ['filter', 'exports'],
            title:'中队分数排名表',
            method:'post',
            // page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            //     layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
            //     //,curr: 5 //设定初始在第 5 页
            //     groups: 2, //只显示 1 个连续页码
            //     first: "首页", //显示首页
            //     last: "尾页", //显示尾页
            //     limits:[3,10, 20, 30]
            // },
            width: $(parent.window).width()-223,
            cols: [[
                // {type:'checkbox'},
                // {field:'type',title: '评价项目'},
                // {field:'type2',title: '类别'},
                {field:'rank',title: '排名',width:'5%',type:'numbers',rowspan:3,fixed:'left'},
                {field:'team',title: '中队',width:'8%',rowspan:3,fixed:'left'},
                {field:'point_jck_srzx',title: '市容秩序',width:'6%',rowspan:3},
                {field:'point_zwb',title: '违法建设',width:'6%',rowspan:3},
                {field:'point_ggb',title: '广告牌匾',width:'6%',rowspan:3},
                {align: 'center', title: '市容环卫执法', colspan: 4}, //colspan即横跨的单元格数，这种情况下不用设置field和width
                {align: 'center', title: '市容公用执法', colspan: 3},
                {align: 'center', title: '园林与环境执法', colspan: 3},
                {align: 'center', title: '物业执法', colspan: 3},
                {field:'point_jck_qtaj',title: '其他案件',width:'6%',rowspan:3},
                {field:'point_jck_ywld',title: '亮点工作',width:'6%',rowspan:3},
                {align: 'center', title: '非现场执法', colspan: 2},
                {field:'point_fzk', title: '执法监督', width:'6%',rowspan:3},
                {field:'point_dsjk',title: '作风督察',width:'6%',rowspan:3},
                {field:'point_xfk',title: '信访诉求',width:'6%',rowspan:3},
                {field:'point_xck',title: '宣传报道',width:'6%',rowspan:3},
                {field:'point_zgk',title: '队伍建设',width:'6%',rowspan:3},
                {field:'point_ldpy',title: '领导评议',width:'6%',rowspan:3},
                {field:'point_total',title: '总分',rowspan:3},
                {fixed: 'right', title: '操作',   align: 'center',toolbar: '#barDemo',unresize: true,rowspan:3}
            ],[
                {field: 'point_jck_hjws_zljy', title: '占路经营', width: '6%',rowspan:2}
                ,{field: 'point_jck_hjws_xgg', title: '小广告', width: '6%',rowspan:2}
                ,{field: 'point_jck_hjws_ws', title: '卫生责任区', width: '8%',rowspan:2}
                ,{field: 'point_jck_hjws_ljfl', title: '垃圾分类', width: '6%',rowspan:2}
                ,{field: 'point_jck_szss_rq', title: '燃气', width: '5%',rowspan:2}
                ,{field: 'point_jck_szss_pw', title: '排污', width: '5%',rowspan:2}
                ,{field: 'point_jck_szss_jl', title: '掘路', width: '5%',rowspan:2}
                ,{field: 'point_jck_yllh_lh', title: '绿化', width: '5%',rowspan:2}
                ,{field: 'point_jck_yllh_yy', title: '油烟噪声扬尘', width: '8%',rowspan:2}
                ,{field: 'point_jck_yllh_ztc', title: '渣土车', width: '6%',rowspan:2}
                ,{field: 'point_jck_wyzf_wgqy', title: '物管处罚', width: '6%',rowspan:2}
                ,{align: 'center', title: '物业小区环境整治', colspan: 2}
                ,{field: 'point_dcs_ajcz', title: '案件处置', width: '6%',rowspan:2}
                ,{field: 'point_dcs_hs', title: '慧商安装使用', width: '8%',rowspan:2}
            ],[
                {field: 'point_jck_wyzf_qlhl', title: '圈绿毁绿', width: '6%'}
                ,{field: 'point_jck_wyzf_ldlf2', title: '地装地锁乱放', width: '8%'},
                // {field:'description',sort:true,title: '评分标准',width: '22%'},
                // {field:'createDate', sort:true ,width: '11%',title: '创建时间',templet:'<div>{{ layui.laytpl.toDateString(d.createDate) }}</div>',unresize: true}, //单元格内容水平居中
            ]],
            done: function (res) {
                exportData = res.data;
            }
        };
        console.log(layui.table);
        table.render(t);

        table.on('tool(demo)', function(obj){
            var data = obj.data;
            if(obj.event === "editPoint_ldpy"){
                $("#changePoint_ldpyForm")[0].reset();
                $("input[name='team']").val(data.team);
                $("input[name='oldPoint_ldpy']").val(data.point_ldpy);
                changeIndex = layer.open({
                    type: 1,
                    title: '编辑',
                    closeBtn: 0,
                    area: '516px',
                    shadeClose: true,
                    content: $('#changePoint_ldpyDiv')
                });
            }
        });

        <#--table.on('toolbar(demo)', function(obj){-->
        <#--    if(obj.event === "settlement"){-->
        <#--        layer.confirm("结算操作不可逆，确定要结算么？",{btn:['是的,我确定','我再想想']},-->
        <#--            function(){-->
        <#--                table.exportFile('test', exportData, 'xls');-->
        <#--                $.post("${base}/teamPoint/settlement",function (res){-->
        <#--                    if(res.success){-->
        <#--                        layer.msg("结算成功",{time: 2000},function(){-->
        <#--                            table.reload('test', t);-->
        <#--                        });-->
        <#--                    }else{-->
        <#--                        layer.msg("结算出错 "+res.message,{time:10000});-->
        <#--                    }-->

        <#--                });-->
        <#--            }-->
        <#--        )-->
        <#--    }-->
        <#--    if(obj.event === "resetScore"){-->
        <#--        layer.confirm("重置分数操作不可逆，确定要重置么？",{btn:['是的,我确定','我再想想']},-->
        <#--            function(){-->
        <#--                $.post("${base}/teamPoint/resetPoint",function (res){-->
        <#--                    if(res.success){-->
        <#--                        layer.msg("重置成功",{time: 2000},function(){-->
        <#--                            table.reload('test', t);-->
        <#--                        });-->
        <#--                    }else{-->
        <#--                        layer.msg("重置出错 "+res.message,{time:5000});-->
        <#--                    }-->

        <#--                });-->
        <#--            }-->
        <#--        )-->
        <#--    }-->

        <#--});-->

        //搜索
        form.on("submit(searchForm)",function(data){
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

        form.on("submit(changePoint_ldpy)",function (data) {
            if(data.field.oldPoint_ldpy == null || data.field.oldPoint_ldpy === ""){
                layer.msg("操作失败！原分数不能为空！");
                return false;
            }
            $.post("${base}/teamPoint/editPoint_ldpy",data.field,function(res){
                if(!res.success){
                    layer.msg(res.message);
                }else{
                    layer.msg("修改成功",{time:1000},function () {
                        table.reload('test', t);
                    })
                }
                layer.close(changeIndex);
            });
            return false;
        });

    });
</script>
</body>
</html>