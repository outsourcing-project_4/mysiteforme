package com.mysiteforme.admin.interceptor;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lsd
 * @title: AddHeaderResponseInterceptor
 * @projectName demo
 * @description: 添加响应头缺失信息
 * @date 2022/12/1 19:34
 */
@Component
public class AddHeaderResponseInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        /**
         * 2.1.2检测到目标X-Content-Type-Options响应头缺失
         * 将您的服务器配置为在所有传出请求上发送值为“nosniff”的““”头
         */
        httpServletResponse.setHeader("X-Content-Type-Options","nosniff");
        /**
         * 2.1.3.检测到目标X-XSS-Protection响应头缺失
         * 将您的服务器配置为在所有传出请求上发送值为“1”（例如已启用）的“X-XSS-Protection”头。
         * 0：禁用XSS保护；1：启用XSS保护；1; mode=block：启用XSS保护，并在检查到XSS攻击时，停止渲染页面
         */
        httpServletResponse.setHeader("X-XSS-Protection","1");
        /**
         * 2.1.5检测到目标Content-Security-Policy响应头缺失
         * 将您的服务器配置为发送“Content-Security-Policy”头
         * 指令参考： https://imququ.com/post/content-security-policy-reference.html
         */
        httpServletResponse.setHeader("Content-Security-Policy","");
        /**
         * 2.1.6.点击劫持：X-Frame-Options未配置
         * 修改web服务器配置，添加X-Frame-Options响应头。赋值有如下三种：
         * 1、DENY：不能被嵌入到任何iframe或者frame中。
         * 2、SAMEORIGIN:页面只能被本站页面嵌入到iframe或者frame中。
         * 3、ALLOW-FROM uri：只能被嵌入到指定域名的框架中。
         */
        httpServletResponse.setHeader("X-Frame-Options","SAMEORIGIN");


        httpServletResponse.setHeader("Referrer-Policy","unsafe-url");
        httpServletResponse.setHeader("X-Permitted-Cross-Domain-Policies","none");
        httpServletResponse.setHeader("X-Download-Options","noopen");
        httpServletResponse.setHeader("Strict-Transport-Security","max-age=16070400; includeSubdomains");


        // 解决安全漏洞：检测到目标服务器启用了OPTIONS方法
        httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS");
        httpServletResponse.setHeader("Access-Control-Max-Age", "86400");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", "*");
        // 如果是OPTIONS则结束请求
        if (HttpMethod.OPTIONS.toString().equals(httpServletRequest.getMethod())) {
            httpServletResponse.setStatus(HttpStatus.NO_CONTENT.value());
            return false;
        }



        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
