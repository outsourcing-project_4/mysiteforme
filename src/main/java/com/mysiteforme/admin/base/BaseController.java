package com.mysiteforme.admin.base;

import com.mysiteforme.admin.entity.Dict_jck_ywld;
import com.mysiteforme.admin.entity.User;
import com.mysiteforme.admin.realm.AuthRealm.ShiroUser;
import com.mysiteforme.admin.service.*;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {
	
	public User getCurrentUser() {
		ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		if(shiroUser == null) {
			return null;
		}
		User loginUser = userService.selectById(shiroUser.getId());
		return loginUser;
	}

	@Autowired
	protected UserService userService;

	@Autowired
	protected MenuService menuService;

	@Autowired
	protected RoleService roleService;

	@Autowired
	protected DictService dictService;

	@Autowired
	protected Dict_ggbService dict_ggbService;

	@Autowired
	protected Dict_ztbService dict_ztbService;

	@Autowired
	protected Dict_dcsService dict_dcsService;

	@Autowired
	protected Dict_dcs2Service dict_dcs2Service;

	@Autowired
	protected Dict_xckService dict_xckService;

	@Autowired
	protected Dict_bgsService dict_bgsService;

	@Autowired
	protected Dict_xfkService dict_xfkService;

	@Autowired
	protected Dict_zgkService dict_zgkService;

	@Autowired
	protected Dict_fzkService dict_fzkService;

	@Autowired
	protected Dict_jck_srzxService dict_jck_srzxService;

	@Autowired
	protected Dict_jck_hjwsService dict_jck_hjwsService;

	@Autowired
	protected Dict_jck_szssService dict_jck_szssService;

	@Autowired
	protected Dict_jck_yllhService dict_jck_yllhService;

	@Autowired
	protected Dict_jck_wyzfService dict_jck_wyzfService;

	/**
	*新增 监察科（其他案件）
	* */
	@Autowired
	protected Dict_jck_qtajService dict_jck_qtajService;

	/**
	 *新增 监察科（业务亮点）
	 * */
	@Autowired
	protected Dict_jck_ywldService dict_jck_ywldService;
	/**
	 *新增 监察科（备注）
	 * */
	@Autowired
	protected Dict_jck_bzService dict_jck_bzService;

	@Autowired
	protected Dict_dsjkService dict_dsjkService;

	@Autowired
	protected TeamService teamService;

	@Autowired
	protected RescourceService rescourceService;

	@Autowired
	protected TableService tableService;

	@Autowired
	protected SiteService siteService;

	@Autowired
	protected LogService logService;

	@Autowired
	protected BlogArticleService blogArticleService;

	@Autowired
	protected BlogChannelService blogChannelService;

	@Autowired
	protected BlogCommentService blogCommentService;

	@Autowired
	protected BlogTagsService blogTagsService;

	@Autowired
	protected QuartzTaskService quartzTaskService;

	@Autowired
	protected QuartzTaskLogService quartzTaskLogService;

	@Autowired
	protected UploadInfoService uploadInfoService;

	@Autowired
	protected TeamDcsService teamDcsService;
}
