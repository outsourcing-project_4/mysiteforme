package com.mysiteforme.admin.base;

import com.mysiteforme.admin.realm.AuthRealm.ShiroUser;
import org.apache.shiro.SecurityUtils;

/**
 * Created by wangl on 2017/11/25.
 * todo:
 */
public class MySysUser {
    /**
     * 取出Shiro中的当前用户LoginName.
     */
    public static String icon() {
        return ShiroUser().getIcon();
    }

    public static Long id() {

        //以下三行代码是给房总数据平台接口使用的，访问接口时状态为未登录状态，此时ShiroUser()==null，这将会导致MyHandlerInterceptor类第45行报空指针异常
        //所以当未登陆时给id赋一个默认id：1（ps：1是admin账号的id）
        if(ShiroUser()==null){
            return Long.parseLong("1");
        }

        return ShiroUser().getId();
    }

    public static String loginName() {
        return ShiroUser().getloginName();
    }

    public static String nickName(){
        return ShiroUser().getNickName();
    }

    public static ShiroUser ShiroUser() {
        ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        return user;
    }
}
