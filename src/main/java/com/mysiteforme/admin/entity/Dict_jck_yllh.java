package com.mysiteforme.admin.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 监察科（园林绿化）考核条目表
 * </p>
 *
 * @author lsd
 * @since 2021-12-19
 */
@TableName("sys_dict_jck_yllh")
public class Dict_jck_yllh extends DataEntity<Dict_jck_yllh> {

    private static final long serialVersionUID = 1L;

    /**
     * 数据值
     */
	private String value;
    /**
     * 标签名
     */
	private String label;
    /**
     * 类型
     */
	private String type;
    /**
     * 描述
     */
	private String type2;
	/**
	 * 描述2
	 */
	private String type3;
	/**
	 * 描述3
	 */
	private String description;
	/**
	 * 备注
	 */
	private String remarks;
	/**
	 * 创建日期
	 */
	private Date create_date;
    /**
     * 排序（升序）
     */
	private Integer sort;
	/**
	 * 条数
	 */
	private Integer number;
	/**
	 * 可见标志
	 */
	private Integer visable_flag;
	/**
     * 父级编号
     */
	@TableField("parent_id")
	private String parentId;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType2() {
		return type2;
	}

	public void setType2(String type2) {
		this.type2 = type2;
	}

	public String getType3() {
		return type3;
	}

	public void setType3(String type3) {
		this.type3 = type3;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getVisable_flag() {
		return visable_flag;
	}

	public void setVisable_flag(Integer visable_flag) {
		this.visable_flag = visable_flag;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Dict_ggb{" +
				"value='" + value + '\'' +
				", label='" + label + '\'' +
				", type='" + type + '\'' +
				", type2='" + type2 + '\'' +
				", type3='" + type3 + '\'' +
				", description='" + description + '\'' +
				", remarks='" + remarks + '\'' +
				", create_date=" + create_date +
				", sort=" + sort +
				", number=" + number +
				", visable_flag=" + visable_flag +
				", parentId='" + parentId + '\'' +
				'}';
	}
}
