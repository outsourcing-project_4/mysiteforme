package com.mysiteforme.admin.entity;

/**
 * 此实体类主要用于中队督察室分数排名，再通过排名进行赋分
*/

public class Team_dcs {
    private Integer id;
    private String teamName;
    private float pointDcs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getTeamName() {
        return teamName;
    }

    public void setIdtTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Float getPointDcs() {
        return pointDcs;
    }
    public void setPointDcs(Float pointDcs) {
        this.pointDcs = pointDcs;
    }

}
