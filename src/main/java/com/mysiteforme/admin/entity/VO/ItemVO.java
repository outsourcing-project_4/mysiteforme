package com.mysiteforme.admin.entity.VO;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

public class ItemVO implements Serializable {

    //数据库law_item_
    /**
     * 加/扣分 point
     */
    private String point;
    /**
     * 违法事项
     */
    private String law_item;
    /**
     * 违法事项
     */
    private String law_item_leibie;
    /**
     * 违法事项
     */
    private String law_item_pingfen;
    /*
    *  违规人员
    * */
    private String lawPeople;

    /**
     * 描述
     * */
    private String description;


    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getDescription() {
            return description;
        }

    public void setDescription(String description) {
            this.description = description;
        }

    public String getLaw_item() {
        return law_item;
    }

    public void setLaw_item(String law_item) {
        this.law_item = law_item;
    }

    public String getLaw_item_leibie() {
        return law_item_leibie;
    }

    public void setLaw_item_leibie(String law_item_leibie) {
        this.law_item_leibie = law_item_leibie;
    }

    public String getLaw_item_pingfen() {
        return law_item_pingfen;
    }

    public void setLaw_item_pingfen(String law_item_pingfen) {
        this.law_item_pingfen = law_item_pingfen;
    }

    public String getLawPeople() {
        return lawPeople;
    }

    public void setLawPeople(String lawPeople) {
        this.lawPeople = lawPeople;
    }

}




