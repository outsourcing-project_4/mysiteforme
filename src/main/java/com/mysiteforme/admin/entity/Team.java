package com.mysiteforme.admin.entity;

import com.baomidou.mybatisplus.annotations.TableName;

public class Team {

    /**
     * 中队名称
     */
    private String team_name;
    /**
     * 中队分数_治违办
     */
    private float point_zwb;
    /**
     * 中队分数_广告办
     */
    private float point_ggb;
    /**
     * 中队分数_渣土办
     */
    private float point_ztb;
    /**
     * 中队分数_督察室（改为指挥中心）
     */
    private float point_dcs;
    /**
     * 中队分数_督察室（指挥中心案件处置）
     */
    private float point_dcs_ajcz;
    /**
     * 中队分数_督察室（指挥中心慧商助手使用）
     */
    private float point_dcs_hs;
    /**
     * 中队分数_宣传科
     */
    private float point_xck;
    /**
     * 中队分数_办公室
     */
    private float point_bgs;
    /**
     * 中队分数_信访科
     */
    private float point_xfk;
    /**
     * 中队分数_政工科
     */
    private float point_zgk;
    /**
     * 中队分数_法制科
     */
    private float point_fzk;
    /**
     * 中队分数_监察科_市容秩序
     */
    private float point_jck_srzx;
    /**
     * 中队分数_监察科_环境卫生
     */
    private float point_jck_hjws;
    /**
     * 中队分数_监察科_环境卫生_占路经营
     */
    private float point_jck_hjws_zljy;
    /**
     * 中队分数_监察科_环境卫生_小广告
     */
    private float point_jck_hjws_xgg;
    /**
     * 中队分数_监察科_环境卫生_卫生责任区
     */
    private float point_jck_hjws_ws;
    /**
     * 中队分数_监察科_环境卫生_垃圾分类
     */
    private float point_jck_hjws_ljfl;
    /**
     * 中队分数_监察科_市政设施
     */
    private float point_jck_szss;
    /**
     * 中队分数_监察科_市政设施_燃气
     */
    private float point_jck_szss_rq;
    /**
     * 中队分数_监察科_市政设施_排污
     */
    private float point_jck_szss_pw;
    /**
     * 中队分数_监察科_市政设施_掘路
     */
    private float point_jck_szss_jl;
    /**
     * 中队分数_监察科_园林绿化
     */
    private float point_jck_yllh;
    /**
     * 中队分数_监察科_园林绿化_绿化
     */
    private float point_jck_yllh_lh;
    /**
     * 中队分数_监察科_园林绿化_油烟噪声扬尘
     */
    private float point_jck_yllh_yy;
    /**
     * 中队分数_监察科_园林绿化_渣土车+
     */
    private float point_jck_yllh_ztc1;
    /**
     * 中队分数_监察科_园林绿化_渣土车-
     */
    private float point_jck_yllh_ztc2;
    /**
     * 中队分数_监察科_园林绿化_渣土车
     */
    private float point_jck_yllh_ztc;
    /**
     * 中队分数_监察科_其他案件
     */
    private float point_jck_qtaj;
    /**
     * 中队分数_监察科_业务亮点
     */
    private float point_jck_ywld;
    /**
     * 中队分数_监察科_备注
     */
    private float point_jck_bz;
    /**
     * 中队分数_监察科_物业执法
     */
    private float point_jck_wyzf;
    /**
     * 中队分数_监察科_物业执法_物管企业处罚
     */
    private float point_jck_wyzf_wgqy;
    /**
     * 中队分数_监察科_物业执法_圈绿毁绿
     */
    private float point_jck_wyzf_qlhl;
    /**
     * 中队分数_监察科_物业执法_地桩地锁乱堆乱放
     */
    private float point_jck_wyzf_ldlf;
    /**
     * 中队分数_监察科_物业执法_物业小区环境整治减分项
     */
    private float point_jck_wyzf_wyxq;
    /**
     * 中队分数_监察科_物业执法_地桩地锁乱堆乱放计算过后显示
     */
    private float point_jck_wyzf_ldlf2;
    /**
     * 中队分数_监察科_总
     */
    private float point_jck_total;
    /**
     * 中队分数_大数据科
     */
    private float point_dsjk;
    /**
     * 中队分数_领导评议
     */
    private float point_ldpy;
    /**
     * 中队分数_总分
     */
    private float point_total;


    public float getPoint_zwb() {
        return point_zwb;
    }

    public void setPoint_zwb(float point_zwb) {
        this.point_zwb = point_zwb;
    }

    public float getPoint_ggb() {
        return point_ggb;
    }

    public void setPoint_ggb(float point_ggb) {
        this.point_ggb = point_ggb;
    }

    public float getPoint_ztb() {
        return point_ztb;
    }

    public void setPoint_ztb(float point_ztb) {
        this.point_ztb = point_ztb;
    }

    public float getPoint_dcs() {
        return point_dcs;
    }

    public void setPoint_dcs(float point_dcs) {
        this.point_dcs = point_dcs;
    }

    public float getPoint_dcs_ajcz() {
        return point_dcs_ajcz;
    }

    public void setPoint_dcs_ajcz(float point_dcs_ajcz) { this.point_dcs_ajcz = point_dcs_ajcz;}

    public float getPoint_dcs_hs() {
        return point_dcs_hs;
    }

    public void setPoint_dcs_hs(float point_dcs_hs) { this.point_dcs_hs = point_dcs_hs;}

    public float getPoint_xck() {
        return point_xck;
    }

    public void setPoint_xck(float point_xck) {
        this.point_xck = point_xck;
    }

    public float getPoint_bgs() {
        return point_bgs;
    }

    public void setPoint_bgs(float point_bgs) {
        this.point_bgs = point_bgs;
    }

    public float getPoint_xfk() {
        return point_xfk;
    }

    public void setPoint_xfk(float point_xfk) {
        this.point_xfk = point_xfk;
    }

    public float getPoint_zgk() {
        return point_zgk;
    }

    public void setPoint_zgk(float point_zgk) {
        this.point_zgk = point_zgk;
    }

    public float getPoint_fzk() {
        return point_fzk;
    }

    public void setPoint_fzk(float point_fzk) {
        this.point_fzk = point_fzk;
    }

    public float getPoint_jck_srzx() {
        return point_jck_srzx;
    }

    public void setPoint_jck_srzx(float point_jck_srzx) {
        this.point_jck_srzx = point_jck_srzx;
    }

    public float getPoint_jck_hjws() {
        return point_jck_hjws;
    }

    public void setPoint_jck_hjws(float point_jck_hjws) {
        this.point_jck_hjws = point_jck_hjws;
    }

    public  float getPoint_jck_hjws_zljy(){return point_jck_hjws_zljy;}

    public  void setPoint_jck_hjws_zljy(float point_jck_hjws_zljy){this.point_jck_hjws_zljy=point_jck_hjws_zljy;}

    public  float getPoint_jck_hjws_xgg(){return point_jck_hjws_xgg;}

    public  void setPoint_jck_hjws_xgg(float point_jck_hjws_xgg){this.point_jck_hjws_xgg=point_jck_hjws_xgg;}

    public  float getPoint_jck_hjws_ws(){return point_jck_hjws_ws;}

    public  void setPoint_jck_hjws_ws(float point_jck_hjws_ws){this.point_jck_hjws_ws=point_jck_hjws_ws;}

    public  float getPoint_jck_hjws_ljfl(){return point_jck_hjws_ljfl;}

    public  void setPoint_jck_hjws_ljfl(float point_jck_hjws_ljfl){this.point_jck_hjws_ljfl=point_jck_hjws_ljfl;}

    public float getPoint_jck_szss() {
        return point_jck_szss;
    }

    public void setPoint_jck_szss(float point_jck_szss) {
        this.point_jck_szss = point_jck_szss;
    }

    public float getPoint_jck_szss_rq(){return point_jck_szss_rq;}

    public void setPoint_jck_szss_rq(float point_jck_szss_rq) {this.point_jck_szss_rq = point_jck_szss_rq;}

    public float getPoint_jck_szss_pw(){return point_jck_szss_pw;}

    public void setPoint_jck_szss_pw(float point_jck_szss_pw) {
        this.point_jck_szss_pw = point_jck_szss_pw;
    }

    public float getPoint_jck_szss_jl(){return point_jck_szss_jl;}

    public void setPoint_jck_szss_jl(float point_jck_szss_jl) {
        this.point_jck_szss_jl = point_jck_szss_jl;
    }

    public float getPoint_jck_yllh() {
        return point_jck_yllh;
    }

    public void setPoint_jck_yllh(float point_jck_yllh) {
        this.point_jck_yllh = point_jck_yllh;
    }

    public float getPoint_jck_yllh_lh() {
        return point_jck_yllh_lh;
    }

    public void setPoint_jck_yllh_lh(float point_jck_yllh_lh) {
        this.point_jck_yllh_lh = point_jck_yllh_lh;
    }

    public float getPoint_jck_yllh_yy() {
        return point_jck_yllh_yy;
    }

    public void setPoint_jck_yllh_yy(float point_jck_yllh_yy) {
        this.point_jck_yllh_yy = point_jck_yllh_yy;
    }

    public float getPoint_jck_yllh_ztc1() {
        return point_jck_yllh_ztc1;
    }

    public void setPoint_jck_yllh_ztc1(float point_jck_yllh_ztc1) {
        this.point_jck_yllh_ztc1 = point_jck_yllh_ztc1;
    }

    public float getPoint_jck_yllh_ztc2() {
        return point_jck_yllh_ztc2;
    }

    public void setPoint_jck_yllh_ztc2(float point_jck_yllh_ztc2) {
        this.point_jck_yllh_ztc2 = point_jck_yllh_ztc2;
    }

    public float getPoint_jck_yllh_ztc() {
        return point_jck_yllh_ztc;
    }

    public void setPoint_jck_yllh_ztc(float point_jck_yllh_ztc) {
        this.point_jck_yllh_ztc = point_jck_yllh_ztc;
    }

    public float getPoint_jck_qtaj() {return point_jck_qtaj; }

    public void setPoint_jck_qtaj(float point_jck_qtaj) { this.point_jck_qtaj = point_jck_qtaj; }

    public float getPoint_jck_ywld() {
        return point_jck_ywld;
    }

    public void setPoint_jck_ywld(float point_jck_ywld) {
        this.point_jck_ywld = point_jck_ywld;
    }

    public float getPoint_jck_bz() {
        return point_jck_bz;
    }

    public void setPoint_jck_bz(float point_jck_bz) {
        this.point_jck_bz = point_jck_bz;
    }

    public float getPoint_jck_wyzf() {
        return point_jck_wyzf;
    }

    public void setPoint_jck_wyzf(float point_jck_wyzf) {
        this.point_jck_wyzf = point_jck_wyzf;
    }

    public float getPoint_jck_wyzf_wgqy() {
        return point_jck_wyzf_wgqy;
    }

    public void setPoint_jck_wyzf_wgqy(float point_jck_wyzf_wgqy) {this.point_jck_wyzf_wgqy = point_jck_wyzf_wgqy;}

    public float getPoint_jck_wyzf_qlhl() {return point_jck_wyzf_qlhl;}

    public void setPoint_jck_wyzf_qlhl(float point_jck_wyzf_qlhl) {this.point_jck_wyzf_qlhl = point_jck_wyzf_qlhl;}

    public float getPoint_jck_wyzf_ldlf() {return point_jck_wyzf_ldlf;}

    public void setPoint_jck_wyzf_ldlf(float point_jck_wyzf_ldlf) {this.point_jck_wyzf_ldlf = point_jck_wyzf_ldlf;}

    public float getPoint_jck_wyzf_wyxq() {return point_jck_wyzf_wyxq;}

    public void setPoint_jck_wyzf_wyxq(float point_jck_wyzf_wyxq) {this.point_jck_wyzf_wyxq = point_jck_wyzf_wyxq;}

    public float getPoint_jck_wyzf_ldlf2() {return point_jck_wyzf_ldlf2;}

    public void setPoint_jck_wyzf_ldlf2(float point_jck_wyzf_ldlf2) {this.point_jck_wyzf_ldlf2 = point_jck_wyzf_ldlf2;}


    public float getPoint_jck_total() {
        return point_jck_total;
    }

    public void setPoint_jck_total(float point_jck_total) {
        this.point_jck_total = point_jck_total;
    }

    public float getPoint_dsjk() {
        return point_dsjk;
    }

    public void setPoint_dsjk(float point_dsjk) {
        this.point_dsjk = point_dsjk;
    }

    public float getPoint_ldpy() {
        return point_ldpy;
    }

    public void setPoint_ldpy(float point_ldpy) {
        this.point_ldpy = point_ldpy;
    }

    public float getPoint_total() {
        return point_total;
    }

    public void setPoint_total(float point_total) {
        this.point_total = point_total;
    }

    public String getTeam() {
        return team_name;
    }

    public void setTeam(String team_name) {
        this.team_name = team_name;
    }

    @Override
    public String toString() {
        return "Team{" +
                "team_name='" + team_name + '\'' +
                ", point_zwb=" + point_zwb +
                ", point_ggb=" + point_ggb +
                ", point_ztb=" + point_ztb +
                ", point_dcs=" + point_dcs +
                ", point_dcs_ajcz=" + point_dcs_ajcz +
                ", point_dcs_hs=" + point_dcs_hs +
                ", point_xck=" + point_xck +
                ", point_bgs=" + point_bgs +
                ", point_xfk=" + point_xfk +
                ", point_zgk=" + point_zgk +
                ", point_fzk=" + point_fzk +
                ", point_jck_srzx=" + point_jck_srzx +
                ", point_jck_hjws=" + point_jck_hjws +
                ", point_jck_hjws_zljy=" + point_jck_hjws_zljy +
                ", point_jck_hjws_xgg=" + point_jck_hjws_xgg +
                ", point_jck_hjws_ws=" + point_jck_hjws_ws +
                ", point_jck_hjws_ljfl=" + point_jck_hjws_ljfl +
                ", point_jck_szss=" + point_jck_szss +
                ", point_jck_szss_rq=" + point_jck_szss_rq +
                ", point_jck_szss_pw=" + point_jck_szss_pw +
                ", point_jck_szss_jl=" + point_jck_szss_jl +
                ", point_jck_yllh=" + point_jck_yllh +
                ", point_jck_yllh_lh=" + point_jck_yllh_lh +
                ", point_jck_yllh_yy=" + point_jck_yllh_yy +
                ", point_jck_yllh_ztc1=" + point_jck_yllh_ztc1 +
                ", point_jck_yllh_ztc2=" + point_jck_yllh_ztc2 +
                ", point_jck_qtaj=" + point_jck_qtaj +
                ", point_jck_ywld=" + point_jck_ywld +
                ", point_jck_bz=" + point_jck_bz +
                ", point_jck_wyzf=" + point_jck_wyzf +
                ", point_jck_wyzf_wgqy=" + point_jck_wyzf_wgqy +
                ", point_jck_wyzf_qlhl=" + point_jck_wyzf_qlhl +
                ", point_jck_wyzf_ldlf=" + point_jck_wyzf_ldlf +
                ", point_jck_wyzf_wyxq=" + point_jck_wyzf_wyxq +
                ", point_jck_wyzf_ldlf2=" + point_jck_wyzf_ldlf2 +
                ", point_jck_total=" + point_jck_total +
                ", point_dsjk=" + point_dsjk +
                ", point_ldpy=" + point_ldpy +
                ", point_total=" + point_total +
                '}';
    }


}




