package com.mysiteforme.admin.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.Dict;
import com.mysiteforme.admin.entity.Dict_ggb;

/**
 * <p>
  * 字典表 Mapper 接口
 * </p>
 *
 * @author lsd
 * @since 2021-12-12
 */
public interface Dict_ggbDao extends BaseMapper<Dict_ggb> {

}