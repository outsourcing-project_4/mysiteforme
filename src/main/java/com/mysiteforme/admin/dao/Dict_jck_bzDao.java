package com.mysiteforme.admin.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.Dict_jck_bz;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author wcl
 * @since 2023-8-21
 */
public interface Dict_jck_bzDao extends BaseMapper<Dict_jck_bz> {

}