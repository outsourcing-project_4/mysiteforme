package com.mysiteforme.admin.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.Team;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public interface TeamDao {
     public List<String> queryTeam();

     public List<Team> queryPoint_zwb();
     public List<Team> queryPoint_historyZwb1();
     public Integer changeTeamPoint_zwb(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_zwb(@Param("team_name")String team_name);
     //消除zwb浮点型分数归零的误差
     public Integer fixdict_zwbPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_ggb();
     public List<Team> queryPoint_historyGgb1();
     public List<Team> queryPoint_historyGgb2();
     public Integer changeTeamPoint_ggb(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_ggb(@Param("team_name")String team_name);
     //消除ggb浮点型分数归零的误差
     public Integer fixdict_ggbPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_ztb();
     public Integer changeTeamPoint_ztb(@Param("team_name")String team_name, @Param("point")float point);

     public List<Team> queryPoint_dcs();
     public List<Team> queryPoint_historyDcs1();
     public Integer changeTeamPoint_dcs(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_dcs_ajcz(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_dcs_hs(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_dcs(@Param("team_name")String team_name);
     //消除dcs浮点型分数归零的误差
     public Integer fixdict_dcsPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_dcs_ajczPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_dcs_hcPointError(@Param("team_name")String team_name,@Param("point")double point);


     public List<Team> queryPoint_xck();
     public List<Team> queryPoint_historyXck1();
     public Integer changeTeamPoint_xck(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_xck(@Param("team_name")String team_name);
     //消除xck浮点型分数归零的误差
     public Integer fixdict_xckPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_bgs();
     public Integer changeTeamPoint_bgs(@Param("team_name")String team_name, @Param("point")float point);

     public List<Team> queryPoint_xfk();
     public List<Team> queryPoint_historyXfk1();
     public List<Team> queryPoint_historyXfk2();
     public Integer changeTeamPoint_xfk(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_xfk(@Param("team_name")String team_name);
     //消除zgk浮点型分数归零的误差
     public Integer fixdict_xfkPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_zgk();
     public List<Team> queryPoint_historyZgk1();
     public Integer changeTeamPoint_zgk(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_zgk(@Param("team_name")String team_name);
     //消除zgk浮点型分数归零的误差
     public Integer fixdict_zgkPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_fzk();
     public List<Team> queryPoint_historyFzk1();
     public Integer changeTeamPoint_fzk(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_fzk(@Param("team_name")String team_name);
     //消除fzk浮点型分数归零的误差
     public Integer fixdict_fzkPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_jck_srzx();
     public List<Team> queryPoint_jck_historySrzx1();
     public List<Team> queryPoint_jck_historySrzx2();
     public Integer changeTeamPoint_jck_srzx(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_jck_srzx(@Param("team_name")String team_name);
     public Integer xiaxianTeamPoint_jck_srzx(@Param("team_name")String team_name,@Param("Point_jck_srzx")float Point_jck_srzx);
     public Integer rechangeTeamPoint_jck_srzx(@Param("team_name")String team_name, @Param("point")float point);


     public List<Team> queryPoint_jck_hjws();
     public List<Team> queryPoint_jck_historyHjws1();
     public List<Team> queryPoint_jck_historyHjws2();
     public Integer changeTeamPoint_jck_hjws(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_hjws_zljy(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_hjws_xgg(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_hjws_ws(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_hjws_ljfl(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_jck_hjws(@Param("team_name")String team_name);
     public Integer zeroTeamPoint_jck_hjws(@Param("team_name")String team_name);
     //消除hjws浮点型分数归零的误差
     public Integer fixdict_jck_hjwsPointError(@Param("team_name")String team_name,@Param("point")double point);
     //消除hjws_zljy浮点型分数归零的误差
     public Integer fixdict_jck_hjws_zljyPointError(@Param("team_name")String team_name,@Param("point")double point);
     //消除hjws_xgg浮点型分数归零的误差
     public Integer fixdict_jck_hjws_xggPointError(@Param("team_name")String team_name,@Param("point")double point);
     //消除hjws_ws浮点型分数归零的误差
     public Integer fixdict_jck_hjws_wsPointError(@Param("team_name")String team_name,@Param("point")double point);
     //消除hjws_ljfl浮点型分数归零的误差
     public Integer fixdict_jck_hjws_ljflPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_jck_szss();
     public List<Team> queryPoint_jck_historySzss1();
     public List<Team> queryPoint_jck_historySzss2();
     public Integer changeTeamPoint_jck_szss(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_szss_rq(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_szss_pw(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_szss_jl(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_jck_szss(@Param("team_name")String team_name);
     //消除szss浮点型分数归零的误差
     public Integer fixdict_jck_szssPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_szss_rqPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_szss_pwPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_szss_jlPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_jck_yllh();
     public List<Team> queryPoint_jck_historyYllh1();
     public List<Team> queryPoint_jck_historyYllh2();
     public Integer changeTeamPoint_jck_yllh(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_yllh_lh(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_yllh_yy(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_yllh_ztc1(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_yllh_ztc2(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_jck_yllh(@Param("team_name")String team_name);
     public Integer xiaxianTeamPoint_jck_yllh(@Param("team_name")String team_name,@Param("Point_jck_yllh")float Point_jck_yllh);
     //消除yllh浮点型分数归零的误差
     public Integer fixdict_jck_yllhPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_yllh_lhPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_yllh_yyPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_yllh_ztc1PointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_yllh_ztc2PointError(@Param("team_name")String team_name,@Param("point")double point);

     //新增监察科（其他案件）
     public List<Team> queryPoint_jck_qtaj();
     public List<Team> queryPoint_jck_historyQtaj1();
     public List<Team> queryPoint_jck_historyQtaj2();
     public Integer changeTeamPoint_jck_qtaj(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_jck_qtaj(@Param("team_name")String team_name);
     //消除qtaj浮点型分数归零的误差
     public Integer fixdict_jck_qtajPointError(@Param("team_name")String team_name,@Param("point")double point);

     //新增监察科（业务亮点）
     public List<Team> queryPoint_jck_ywld();
     public List<Team> queryPoint_jck_historyYwld1();
     public Integer changeTeamPoint_jck_ywld(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_jck_ywld(@Param("team_name")String team_name);
     //消除wyzf浮点型分数归零的误差
     public Integer fixdict_jck_ywldPointError(@Param("team_name")String team_name,@Param("point")double point);

     //新增监察科（备注）
     public List<Team> queryPoint_jck_bz();
     public Integer changeTeamPoint_jck_bz(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_jck_bz(@Param("team_name")String team_name);
     //消除bz浮点型分数归零的误差
     public Integer fixdict_jck_bzPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_jck_wyzf();
     public List<Team> queryPoint_jck_historyWyzf1();
     public Integer changeTeamPoint_jck_wyzf(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_wyzf_wgqy(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_wyzf_qlhl(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_wyzf_ldlf(@Param("team_name")String team_name, @Param("point")float point);
     public Integer changeTeamPoint_jck_wyzf_wyxq(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_jck_wyzf(@Param("team_name")String team_name);
     //消除wyzf浮点型分数归零的误差
     public Integer fixdict_jck_wyzfPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_wyzf_wgqyPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_wyzf_qlhlPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_wyzf_ldlfPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer fixdict_jck_wyzf_wyxqPointError(@Param("team_name")String team_name,@Param("point")double point);

     public List<Team> queryPoint_jck_total();

     public List<Team> queryPoint_dsjk();
     public List<Team> queryPoint_historyDsjk1();
     public Integer changeTeamPoint_dsjk(@Param("team_name")String team_name, @Param("point")float point);
     public Team queryTeamPoint_dsjk(@Param("team_name")String team_name);
     //消除dsjk浮点型分数归零的误差
     public Integer fixdict_dsjkPointError(@Param("team_name")String team_name,@Param("point")double point);
     public Integer assignTeamPoint_dsjk(@Param("team_name")String team_name, @Param("point")float point);

     public Integer changeTeamPoint_ldpy(@Param("team_name")String team_name, @Param("point")float point);

     public Integer resetPoint(@Param("resetPoint_zwb")float resetPoint_zwb, @Param("resetPoint_ggb")float resetPoint_ggb, @Param("resetPoint_ztb")float resetPoint_ztb,
                               @Param("resetPoint_dcs")float resetPoint_dcs,@Param("resetPoint_dcs_ajcz")float resetPoint_dcs_ajcz,@Param("resetPoint_dcs_hs")float resetPoint_dcs_hs, @Param("resetPoint_xck")float resetPoint_xck, @Param("resetPoint_bgs")float resetPoint_bgs,
                               @Param("resetPoint_xfk")float resetPoint_xfk, @Param("resetPoint_fzk")float resetPoint_fzk, @Param("resetPoint_zgk")float resetPoint_zgk,
                               @Param("resetPoint_jck_srzx")float resetPoint_jck_srzx, @Param("resetPoint_jck_hjws")float resetPoint_jck_hjws,@Param("resetPoint_jck_hjws_zljy")float resetPoint_jck_hjws_zljy,
                               @Param("resetPoint_jck_hjws_xgg")float resetPoint_jck_hjws_xgg,@Param("resetPoint_jck_hjws_ws")float resetPoint_jck_hjws_ws,@Param("resetPoint_jck_hjws_ljfl")float resetPoint_jck_hjws_ljfl,
                               @Param("resetPoint_jck_szss")float resetPoint_jck_szss, @Param("resetPoint_jck_szss_rq")float resetPoint_jck_szss_rq,@Param("resetPoint_jck_szss_pw")float resetPoint_jck_szss_pw,@Param("resetPoint_jck_szss_jl")float resetPoint_jck_szss_jl,
                               @Param("resetPoint_jck_yllh")float resetPoint_jck_yllh, @Param("resetPoint_jck_yllh_lh")float resetPoint_jck_yllh_lh,@Param("resetPoint_jck_yllh_yy")float resetPoint_jck_yllh_yy,@Param("resetPoint_jck_yllh_ztc1")float resetPoint_jck_yllh_ztc1,
                               @Param("resetPoint_jck_yllh_ztc2")float resetPoint_jck_yllh_ztc2, @Param("resetPoint_jck_qtaj")float resetPoint_jck_qtaj,@Param("resetPoint_jck_ywld")float resetPoint_jck_ywld,@Param("resetPoint_jck_bz")float resetPoint_jck_bz,
                               @Param("resetPoint_jck_wyzf")float resetPoint_jck_wyzf,@Param("resetPoint_jck_wyzf_wgqy")float resetPoint_jck_wyzf_wgqy,@Param("resetPoint_jck_wyzf_qlhl")float resetPoint_jck_wyzf_qlhl,@Param("resetPoint_jck_wyzf_ldlf")float resetPoint_jck_wyzf_ldlf,
                               @Param("resetPoint_jck_wyzf_wyxq")float resetPoint_jck_wyzf_wyxq,@Param("resetPoint_dsjk")float resetPoint_dsjk, @Param("resetPoint_ldpy")float resetPoint_ldpy);

}
