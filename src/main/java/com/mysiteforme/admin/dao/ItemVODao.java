package com.mysiteforme.admin.dao;

import com.mysiteforme.admin.entity.VO.ItemVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ItemVODao {

    //治违办
    public List<ItemVO> queryList_zwb();
    public List<ItemVO> queryLeibie_zwb();
    public List<ItemVO> queryPingfen_zwb(String law_item_leibie);
    public List<ItemVO> queryList_description_zwb(String law_item_pingfen);
    public List<ItemVO> queryPoint_zwb(String description);
    public Integer changePoint_zwb(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //广告办
    public List<ItemVO> queryList_ggb();
    public List<ItemVO> queryLeibie_ggb();
    public List<ItemVO> queryPingfen_ggb(String law_item_leibie);
    public List<ItemVO> queryList_description_ggb(String law_item_pingfen);
    public List<ItemVO> queryPoint_ggb(String description);
    public Integer changePoint_ggb(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //渣土办
    public List<ItemVO> queryList_ztb();
    public List<ItemVO> queryLeibie_ztb();
    public List<ItemVO> queryPingfen_ztb(String law_item_leibie);
    public List<ItemVO> queryList_description_ztb(String law_item_pingfen);
    public List<ItemVO> queryPoint_ztb(String description);
    public Integer changePoint_ztb(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //指挥中心
    public List<ItemVO> queryList_fws();
    public List<ItemVO> queryList_fws2();
    public List<ItemVO> queryLeibie_fws();
    public List<ItemVO> queryLeibie_fws2();
    public List<ItemVO> queryPingfen_fws(String law_item_leibie);
    public List<ItemVO> queryPingfen_fws2(String law_item_leibie);
    public List<ItemVO> queryList_description_fws(String law_item_pingfen);
    public List<ItemVO> queryList_description_fws2(String law_item_pingfen);
    public List<ItemVO> queryPoint_fws(String description);
    public List<ItemVO> queryPoint_fws2(String description);
    public Integer changePoint_fws(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //宣传科
    public List<ItemVO> queryList_xck();
    public List<ItemVO> queryLeibie_xck();
    public List<ItemVO> queryPingfen_xck(String law_item_leibie);
    public List<ItemVO> queryList_description_xck(String law_item_pingfen);
    public List<ItemVO> queryPoint_xck(String description);
    public Integer changePoint_xck(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //办公室
    public List<ItemVO> queryList_bgs();
    public List<ItemVO> queryLeibie_bgs();
    public List<ItemVO> queryPingfen_bgs(String law_item_leibie);
    public List<ItemVO> queryList_description_bgs(String law_item_pingfen);
    public List<ItemVO> queryPoint_bgs(String description);
    public Integer changePoint_bgs(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //信访科
    public List<ItemVO> queryList_xfk();
    public List<ItemVO> queryLeibie_xfk();
    public List<ItemVO> queryPingfen_xfk(String law_item_leibie);
    public List<ItemVO> queryList_description_xfk(String law_item_pingfen);
    public List<ItemVO> queryPoint_xfk(String description);
    public Integer changePoint_xfk(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //政工科
    public List<ItemVO> queryList_zgk();
    public List<ItemVO> queryLeibie_zgk();
    public List<ItemVO> queryPingfen_zgk(String law_item_leibie);
    public List<ItemVO> queryList_description_zgk(String law_item_pingfen);
    public List<ItemVO> queryPoint_zgk(String description);
    public Integer changePoint_zgk(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //法制科
    public List<ItemVO> queryList_fzk();
    public List<ItemVO> queryLeibie_fzk();
    public List<ItemVO> queryPingfen_fzk(String law_item_leibie);
    public List<ItemVO> queryList_description_fzk(String law_item_pingfen);
    public List<ItemVO> queryPoint_fzk(String description);
    public Integer changePoint_fzk(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //监察科_市容秩序
    public List<ItemVO> queryList_jck_srzx();
    public List<ItemVO> queryLeibie_jck_srzx();
    public List<ItemVO> queryPingfen_jck_srzx(String law_item_leibie);
    public List<ItemVO> queryList_description_jck_srzx(String law_item_pingfen);
    public List<ItemVO> queryPoint_jck_srzx(String description);
    public Integer changePoint_jck_srzx(@Param("type")String type,@Param("description")String description,@Param("point") String point);
    //监察科_环境卫生
    public List<ItemVO> queryList_jck_hjws();
    public List<ItemVO> queryLeibie_jck_hjws();
    public List<ItemVO> queryPingfen_jck_hjws(String law_item_leibie);
    public List<ItemVO> queryList_description_jck_hjws(String law_item_pingfen);
    public List<ItemVO> queryPoint_jck_hjws(String description);
    public Integer changePoint_jck_hjws(@Param("type")String type,@Param("description")String description,@Param("point") String point);
    //监察科_市政设施
    public List<ItemVO> queryList_jck_szss();
    public List<ItemVO> queryLeibie_jck_szss();
    public List<ItemVO> queryPingfen_jck_szss(String law_item_leibie);
    public List<ItemVO> queryList_description_jck_szss(String law_item_pingfen);
    public List<ItemVO> queryPoint_jck_szss(String description);
    public Integer changePoint_jck_szss(@Param("type")String type,@Param("description")String description,@Param("point") String point);
    //监察科_园林绿化
    public List<ItemVO> queryList_jck_yllh();
    public List<ItemVO> queryLeibie_jck_yllh();
    public List<ItemVO> queryPingfen_jck_yllh(String law_item_leibie);
    public List<ItemVO> queryList_description_jck_yllh(String law_item_pingfen);
    public List<ItemVO> queryPoint_jck_yllh(String description);
    public Integer changePoint_jck_yllh(@Param("type")String type,@Param("description")String description,@Param("point") String point);
    //监察科_其他案件
    public List<ItemVO> queryList_jck_qtaj();
    public List<ItemVO> queryLeibie_jck_qtaj();
    public List<ItemVO> queryPingfen_jck_qtaj(String law_item_leibie);
    public List<ItemVO> queryList_description_jck_qtaj(String law_item_pingfen);
    public List<ItemVO> queryPoint_jck_qtaj(String description);
    public Integer changePoint_jck_qtaj(@Param("type")String type,@Param("description")String description,@Param("point") String point);
    //监察科_业务亮点
    public List<ItemVO> queryList_jck_ywld();
    public List<ItemVO> queryLeibie_jck_ywld();
    public List<ItemVO> queryPingfen_jck_ywld(String law_item_leibie);
    public List<ItemVO> queryList_description_jck_ywld(String law_item_pingfen);
    public List<ItemVO> queryPoint_jck_ywld(String description);
    public Integer changePoint_jck_ywld(@Param("type")String type,@Param("description")String description,@Param("point") String point);
    //监察科_备注
    public List<ItemVO> queryList_jck_bz();
    public List<ItemVO> queryLeibie_jck_bz();
    public List<ItemVO> queryPingfen_jck_bz(String law_item_leibie);
    public List<ItemVO> queryList_description_jck_bz(String law_item_pingfen);
    public List<ItemVO> queryPoint_jck_bz(String description);
    public Integer changePoint_jck_bz(@Param("type")String type,@Param("description")String description,@Param("point") String point);
    //监察科_物业执法
    public List<ItemVO> queryList_jck_wyzf();
    public List<ItemVO> queryLeibie_jck_wyzf();
    public List<ItemVO> queryPingfen_jck_wyzf(String law_item_leibie);
    public List<ItemVO> queryList_description_jck_wyzf(String law_item_pingfen);
    public List<ItemVO> queryPoint_jck_wyzf(String description);
    public Integer changePoint_jck_wyzf(@Param("type")String type,@Param("description")String description,@Param("point") String point);

    //大数据科
    public List<ItemVO> queryList_dsjk();
    public List<ItemVO> queryLeibie_dsjk();
    public List<ItemVO> queryPingfen_dsjk(String law_item_leibie);
    public List<ItemVO> queryList_description_dsjk(String law_item_pingfen);
    public List<ItemVO> queryPoint_dsjk(String description);
    public Integer changePoint_dsjk(@Param("type")String type,@Param("description")String description,@Param("point") String point);

}
