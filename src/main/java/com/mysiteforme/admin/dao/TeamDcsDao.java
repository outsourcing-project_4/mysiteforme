package com.mysiteforme.admin.dao;

import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.entity.Team_dcs;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TeamDcsDao {
    public List<Team_dcs> queryPoint_dcs();
    public Integer changeTeamPoint_dsjk(@Param("team_name")String team_name, @Param("point")float point);
    public Integer resetPoint(@Param("resetPoint_dcs")float resetPoint_dcs);
}
