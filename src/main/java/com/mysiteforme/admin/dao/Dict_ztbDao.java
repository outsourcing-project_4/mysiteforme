package com.mysiteforme.admin.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.Dict_ggb;
import com.mysiteforme.admin.entity.Dict_ztb;

/**
 * <p>
  * 字典表 Mapper 接口
 * </p>
 *
 * @author lsd
 * @since 2021-12-16
 */
public interface Dict_ztbDao extends BaseMapper<Dict_ztb> {

}