package com.mysiteforme.admin.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.Dict_xck;

/**
 * <p>
  * 字典表 Mapper 接口
 * </p>
 *
 * @author lsd
 * @since 2021-12-19
 */
public interface Dict_xckDao extends BaseMapper<Dict_xck> {

}