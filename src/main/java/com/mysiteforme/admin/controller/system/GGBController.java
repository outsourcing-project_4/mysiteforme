package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.Dict;
import com.mysiteforme.admin.entity.Dict_ggb;
import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/12.
 * todo:
 * 广告办controller
 */
@Controller
@RequestMapping("admin/system/ggb")
public class GGBController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_ggb> wrapper1 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_ggb dict_ggb = dict_ggbService.selectById(id);
        wrapper1.eq("type","广告招牌").eq("label",dict_ggb.getLabel()).eq("visable_flag",1);
        Integer number = dict_ggb.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_ggb.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_ggb.setValue(String.valueOf(newValue));
        dict_ggb.setNumber(number+1);
//        teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_ggbService.saveOrUpdateDict(dict_ggb);
        if(dict_ggb.getType().equals("广告招牌")){
            List<Dict_ggb> Dict_ggb_List= dict_ggbService.selectList(wrapper1);
            double sum=Dict_ggb_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-8){
                if(floatSum-newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),-8);
                } else if((floatSum-newValue.subtract(oldValue).floatValue()<-8)){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-newValue.subtract(oldValue).floatValue()>=-8)){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())-8);
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }
            }else if(floatSum>-8&&floatSum<2) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -8)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 8+floatSum );
                    Team team = teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_ggb();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(), value);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=2){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), floatSum-2);

                }
                else{
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                }
            }
            else if(floatSum>=2) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() > 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - newValue.subtract(oldValue).floatValue() <= 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 2-(floatSum - newValue.subtract(oldValue).floatValue()));
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                }
                else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }
            }

        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_ggb> wrapper1 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_ggb dict_ggb = dict_ggbService.selectById(id);
        wrapper1.eq("type","广告招牌").eq("label",dict_ggb.getLabel()).eq("visable_flag",1);
        Integer number = dict_ggb.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_ggb.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_ggb.setValue(String.valueOf(newValue));
        dict_ggb.setNumber(number-1);
//        teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_ggbService.saveOrUpdateDict(dict_ggb);
        if(dict_ggb.getType().equals("广告招牌")){
            List<Dict_ggb> Dict_ggb_List= dict_ggbService.selectList(wrapper1);
            double sum=Dict_ggb_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            //指定的误差范围
            double error=1E-6;
            if(floatSum<=-8){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),8);
                }else if(floatSum+newValue.subtract(oldValue).floatValue()<=-8){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-8){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 8+(floatSum+newValue.subtract(oldValue).floatValue()));
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }
            }else if(floatSum>-8&&floatSum<2){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-8){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), -(8+floatSum));
                    Team team = teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_ggb();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(), value);
                }else if(floatSum+newValue.subtract(oldValue).floatValue()>=2){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),2-floatSum);
                }else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                }
            }
            else if(floatSum>=2) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-8){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),-10);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), floatSum + newValue.subtract(oldValue).floatValue()-2);
                } else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }

            }

        }
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_ggb> wrapper1 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_ggb dict_ggb = dict_ggbService.selectById(id);
        wrapper1.eq("type","广告招牌").eq("label",dict_ggb.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), -Float.parseFloat(dict_ggb.getValue()));
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_ggbService.deleteDict(id);
        if(dict_ggb.getType().equals("广告招牌")){
            List<Dict_ggb> Dict_ggb_List= dict_ggbService.selectList(wrapper1);
            double sum=Dict_ggb_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_ggb.getValue());
            //指定的误差范围
            double error=1E-6;
            if(floatSum<=-8){
                if(floatSum-Float.parseFloat(dict_ggb.getValue())>=2){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),10);
                }else if(floatSum-Float.parseFloat(dict_ggb.getValue())>=0){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),8+(floatSum-Float.parseFloat(dict_ggb.getValue())));
                }else if(floatSum-Float.parseFloat(dict_ggb.getValue())<=-8){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_ggb.getValue())>=-8){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 8+(floatSum-Float.parseFloat(dict_ggb.getValue())));
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }
            }else if(floatSum>-8&&floatSum<2){
                if(floatSum-Float.parseFloat(dict_ggb.getValue())<=-8){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), -(8+floatSum));
                    Team team = teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_ggb();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(), value);
                }else if(floatSum-Float.parseFloat(dict_ggb.getValue())>=2){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 2-floatSum);
                }else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), -Float.parseFloat(dict_ggb.getValue()));
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                }
            }
            else if(floatSum>=2) {
                if(floatSum-Float.parseFloat(dict_ggb.getValue())<=-8){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),-10);
                }else if ((floatSum-Float.parseFloat(dict_ggb.getValue()) > 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum-Float.parseFloat(dict_ggb.getValue()) <= 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), floatSum - Float.parseFloat(dict_ggb.getValue())-2);
                } else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }

            }

        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/ggb/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_ggb> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_ggb> layerData = new LayerData<>();
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_ggb> dataPage = dict_ggbService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_ggb> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_ggb> layerData = new LayerData<>();
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_ggb> dataPage = dict_ggbService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/ggb/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/ggb/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/ggb/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_ggb dict_ggb){
//        List<String> values = Arrays.asList("中队日常巡查发现违规设置广告牌匾案源（横幅、立牌除外）输入智信管理系统，在规定期限内拆除结案每处加 0.1 分", "市、区督办转办案源立案、处罚、拆除且在规定期限内结案的每件加 0.2 分", "中队日常巡查发现新增违规广告招牌（横幅、立牌除外）并立案、处罚、拆除结案的，每件加 0.2 分");
        EntityWrapper<Dict_ggb> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type","广告招牌").eq("label",dict_ggb.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict_ggb.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_ggb.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_ggbService.getCountByType(dict_ggb.getType())==0){
            dict_ggb.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_ggb.setSort(dict_ggbService.getMaxSortByType(dict_ggb.getType()));
        }
        dict_ggbService.saveOrUpdateDict(dict_ggb);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), Float.parseFloat(dict_ggb.getValue()));
        if(dict_ggb.getType().equals("广告招牌")){
            List<Dict_ggb> Dict_ggb_List= dict_ggbService.selectList(wrapper1);
            double sum=Dict_ggb_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-8){
                if(floatSum-Float.parseFloat(dict_ggb.getValue())>=2){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),-10);
                }else if(floatSum-Float.parseFloat(dict_ggb.getValue())>=0){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),-8-(floatSum-Float.parseFloat(dict_ggb.getValue())));
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                } else if((floatSum-Float.parseFloat(dict_ggb.getValue())<-8)){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_ggb.getValue())>=-8)){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), -(floatSum-Float.parseFloat(dict_ggb.getValue()))-8);
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }
            }else if(floatSum>-8&&floatSum<=2) {
                if ((floatSum - Float.parseFloat(dict_ggb.getValue()) <= -8)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 8+floatSum );
                    Team team = teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_ggb();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(), value);
                }else if(floatSum - Float.parseFloat(dict_ggb.getValue())>=2){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), floatSum-2);

                }
                else{
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), Float.parseFloat(dict_ggb.getValue()));
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                }
            }
            else if(floatSum>=2) {
                if ((floatSum - Float.parseFloat(dict_ggb.getValue()) > 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - Float.parseFloat(dict_ggb.getValue()) <= 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 2-(floatSum - Float.parseFloat(dict_ggb.getValue())));
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                }
                else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }
            }

        }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_ggb dict_ggb = dict_ggbService.selectById(id);
        model.addAttribute("dict_ggb",dict_ggb);
        return "admin/system/ggb/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_ggb dict_ggb){
        EntityWrapper<Dict_ggb> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type","广告招牌").eq("label",dict_ggb.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(dict_ggb.getId()==null || dict_ggb.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_ggb.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_ggb.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_ggb.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_ggb.getSort() == null || dict_ggb.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_ggb oldDict_ggb = dict_ggbService.selectById(dict_ggb.getId());
        if(!oldDict_ggb.getType().equals(dict_ggb.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_ggb.getLabel().equals(dict_ggb.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_ggb.getValue().equals(dict_ggb.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_ggbService.saveOrUpdateDict(dict_ggb);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), Float.parseFloat(dict_ggb.getValue()) - Float.parseFloat(oldDict_ggb.getValue()));
        if(dict_ggb.getType().equals("广告招牌")){
            List<Dict_ggb> Dict_ggb_List= dict_ggbService.selectList(wrapper1);
            double sum=Dict_ggb_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_ggb.getValue()) - Float.parseFloat(oldDict_ggb.getValue());
//            float point=(float)1.4;
            if(floatSum<=-8){
                if(floatSum-differ>=2){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),-10);
                } else if((floatSum-differ<-8)){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-8)){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), -(floatSum-differ)-8);
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }
            }else if(floatSum>-8&&floatSum<2) {
                if ((floatSum - differ <= -8)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 8+floatSum );
                    Team team = teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_ggb();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(), value);
                }else if(floatSum - differ>=2){
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), floatSum-2);

                }
                else{
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), differ);
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_ggbPointError(dict_ggb.getLabel(),value);
                }
            }
            else if(floatSum>=2) {
                if ((floatSum - differ > 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 2)) {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 2-(floatSum - differ));
                    Team team=teamService.queryTeamPoint_ggb(dict_ggb.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_ggb();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                }
                else {
                    teamService.changeTeamPoint_ggb(dict_ggb.getLabel(), 0);
                }
            }

        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }

    //广告办分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_ggb> platformGgbList(
            @RequestParam ("label")String label,@RequestParam(value = "page",defaultValue = "1")Integer page,
                                                 @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                                 ServletRequest request){
        if(limit==null){
            return null;
        };
        LayerData<Dict_ggb> layerData = new LayerData<>();
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_ggb> dataPage = dict_ggbService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }


}
