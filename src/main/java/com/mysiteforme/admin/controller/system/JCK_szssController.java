package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.Dict_jck_hjws;
import com.mysiteforme.admin.entity.Dict_jck_szss;
import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/20.
 * todo:
 * 监察科-市政设施controller
 */
@Controller
@RequestMapping("admin/system/jck_szss")
public class JCK_szssController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_szss> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_szss> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_szss> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_szss> wrapper4 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_szss dict_jck_szss = dict_jck_szssService.selectById(id);
        wrapper1.eq("type3","燃气立案处罚情况.").eq("description","每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","燃气立案处罚情况.").eq("description","（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","排污立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","占掘路立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_szss.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_jck_szss.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_szss.setValue(String.valueOf(newValue));
        dict_jck_szss.setNumber(number+1);
//        teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_jck_szssService.saveOrUpdateDict(dict_jck_szss);
        //当增加条数的条例评分内容为“燃气立案处罚情况.”时，查询所用评分内容为“燃气立案处罚情况.”且"每月燃气处罚案件普通程序案件每完成1件得0.5分"的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper1);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&(floatSum-newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.5||(floatSum-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“燃气立案处罚情况.”时，查询所用评分内容为“燃气立案处罚情况.”且"（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）"的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper2);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<1&&(floatSum-newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<1||(floatSum-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());

            }
        }
        //当增加条数的条例评分内容为“排污立案处罚情况.”时，查询所用评分内容为“排污立案处罚情况.”的条例总分是否高于1分。当超过1分时对分数进行限制，超过1分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("排污立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper3);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<1&&(floatSum-newValue.subtract(oldValue).floatValue())>1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从0.*跨度到1.*等情况
                }else if((floatSum-1)<1||(floatSum-newValue.subtract(oldValue).floatValue())<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 1-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), 1-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“占掘路立案处罚情况.”时，查询所用评分内容为“占掘路立案处罚情况.”的条例总分是否高于1分。当超过1分时对分数进行限制，超过1分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("占掘路立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper4);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<1&&(floatSum-newValue.subtract(oldValue).floatValue())>1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从0.*跨度到1.*等情况
                }else if((floatSum-1)<1||(floatSum-newValue.subtract(oldValue).floatValue())<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 1-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), 1-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_szss> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_szss> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_szss> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_szss> wrapper4 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_szss dict_jck_szss = dict_jck_szssService.selectById(id);
        wrapper1.eq("type3","燃气立案处罚情况.").eq("description","每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","燃气立案处罚情况.").eq("description","（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","排污立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","占掘路立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_szss.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_jck_szss.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_szss.setValue(String.valueOf(newValue));
        dict_jck_szss.setNumber(number-1);
//        teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
        //当减少条数的条例评分内容为“燃气立案处罚情况.”时，查询所用评分内容为“燃气立案处罚情况.”且"每月燃气处罚案件普通程序案件每完成1件得0.5分"的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper1);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&(floatSum+newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum-2)<0.5||(floatSum+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“燃气立案处罚情况.”时，查询所用评分内容为“燃气立案处罚情况.”且"（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）"的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper2);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<1&&(floatSum+newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum-2)<1||(floatSum+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“排污立案处罚情况.”时，查询所用评分内容为“排污立案处罚情况.”的条例总分是否高于1分。当超过1分时对分数进行限制，超过1分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("排污立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper3);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<1&&(floatSum+newValue.subtract(oldValue).floatValue())>1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从1.*跨度到0.*等情况
                }else if((floatSum-1)<1||(floatSum+newValue.subtract(oldValue).floatValue())<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -(1-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), -(1-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“占掘路立案处罚情况.”时，查询所用评分内容为“占掘路立案处罚情况.”的条例总分是否高于1分。当超过1分时对分数进行限制，超过1分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("占掘路立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper4);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<1&&(floatSum+newValue.subtract(oldValue).floatValue())>1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从1.*跨度到0.*等情况
                }else if((floatSum-1)<1||(floatSum+newValue.subtract(oldValue).floatValue())<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -(1-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), -(1-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        dict_jck_szssService.saveOrUpdateDict(dict_jck_szss);
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_szss> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_szss> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_szss> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_szss> wrapper4 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_jck_szss dict_jck_szss = dict_jck_szssService.selectById(id);
        wrapper1.eq("type3","燃气立案处罚情况.").eq("description","每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","燃气立案处罚情况.").eq("description","（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","排污立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","占掘路立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -Float.parseFloat(dict_jck_szss.getValue()));
        dict_jck_szssService.deleteDict(id);
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper1);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_szss.getValue());
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&floatSum-Float.parseFloat(dict_jck_szss.getValue())>2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum-2)<0.5||Float.parseFloat(dict_jck_szss.getValue())>0.5)&&(floatSum-Float.parseFloat(dict_jck_szss.getValue()))<2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), floatSum-Float.parseFloat(dict_jck_szss.getValue())-2);
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), floatSum-Float.parseFloat(dict_jck_szss.getValue())-2);
                    updateTeamPoint(dict_jck_szss.getLabel(),error);
                }else{
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -Float.parseFloat(dict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), -Float.parseFloat(dict_jck_szss.getValue()));
                //若扣分项为多次+1后的项，删除后有可能导致分数项小于零，此处进行归零
                updateTeamPoint(dict_jck_szss.getLabel(),error);
            }
        }
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper2);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_szss.getValue());
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<1&&floatSum-Float.parseFloat(dict_jck_szss.getValue())>2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum-2)<1||Float.parseFloat(dict_jck_szss.getValue())>1)&&(floatSum-Float.parseFloat(dict_jck_szss.getValue()))<2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), floatSum-Float.parseFloat(dict_jck_szss.getValue())-2);
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), floatSum-Float.parseFloat(dict_jck_szss.getValue())-2);
                    updateTeamPoint(dict_jck_szss.getLabel(),error);
                }else{
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -Float.parseFloat(dict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), -Float.parseFloat(dict_jck_szss.getValue()));
                updateTeamPoint(dict_jck_szss.getLabel(),error);
            }
        }
        if(dict_jck_szss.getType3().equals("排污立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper3);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_szss.getValue());
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<1&&floatSum-Float.parseFloat(dict_jck_szss.getValue())>1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从1.*跨度到0.*的情况
                }else if(((floatSum-1)<1||Float.parseFloat(dict_jck_szss.getValue())>1)&&(floatSum-Float.parseFloat(dict_jck_szss.getValue()))<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), floatSum-Float.parseFloat(dict_jck_szss.getValue())-1);
                    teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), floatSum-Float.parseFloat(dict_jck_szss.getValue())-1);
                    updateTeamPoint(dict_jck_szss.getLabel(),error);
                }else{
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -Float.parseFloat(dict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), -Float.parseFloat(dict_jck_szss.getValue()));
                updateTeamPoint(dict_jck_szss.getLabel(),error);
            }
        }
        if(dict_jck_szss.getType3().equals("占掘路立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper4);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_szss.getValue());
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<1&&floatSum-Float.parseFloat(dict_jck_szss.getValue())>1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从1.*跨度到0.*的情况
                }else if(((floatSum-1)<1||Float.parseFloat(dict_jck_szss.getValue())>1)&&(floatSum-Float.parseFloat(dict_jck_szss.getValue()))<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), floatSum-Float.parseFloat(dict_jck_szss.getValue())-1);
                    teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), floatSum-Float.parseFloat(dict_jck_szss.getValue())-1);
                    updateTeamPoint(dict_jck_szss.getLabel(),error);
                }else{
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -Float.parseFloat(dict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), -Float.parseFloat(dict_jck_szss.getValue()));
                updateTeamPoint(dict_jck_szss.getLabel(),error);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/jck_szss/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_jck_szss> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_szss> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_szss> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_szss> dataPage = dict_jck_szssService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_jck_szss> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                         ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_szss> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_szss> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_szss> dataPage = dict_jck_szssService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_szss/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_szss/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_szss/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_jck_szss dict_jck_szss){
        EntityWrapper<Dict_jck_szss> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","燃气立案处罚情况.").eq("description","每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_szss> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type3","燃气立案处罚情况.").eq("description","（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_szss> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type3","排污立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_szss> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("type3","占掘路立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        if(StringUtils.isBlank(dict_jck_szss.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_jck_szss.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_szssService.getCountByType(dict_jck_szss.getType())==0){
            dict_jck_szss.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_jck_szss.setSort(dict_jck_szssService.getMaxSortByType(dict_jck_szss.getType()));
        }
        dict_jck_szssService.saveOrUpdateDict(dict_jck_szss);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()));
        //当评分内容为“燃气立案处罚情况”时，查询所用评分内容为“燃气立案处罚情况”且“每月燃气处罚案件普通程序案件每完成1件得0.5分”案件总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper1);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&floatSum-Float.parseFloat(dict_jck_szss.getValue())>2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.5){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_szss.getValue())));
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_szss.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()));
            }
        }
        //当评分内容为“燃气立案处罚情况”时，查询所用评分内容为“燃气立案处罚情况”且“（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）”案件总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper2);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<1&&floatSum-Float.parseFloat(dict_jck_szss.getValue())>2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_szss.getValue())));
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_szss.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()));
            }
        }
        //当评分内容为“排污立案处罚情况.”时，查询所用评分内容为“排污立案处罚情况.”案件总分是否高于1分。当超过1分时对分数进行限制，超过1分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("排污立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper3);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<1&&floatSum-Float.parseFloat(dict_jck_szss.getValue())>1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从0.*跨度到1.*等情况
                }else if((floatSum-1)<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 1-(floatSum-Float.parseFloat(dict_jck_szss.getValue())));
                    teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), 1-(floatSum-Float.parseFloat(dict_jck_szss.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()));
            }
        }
        //当评分内容为“占掘路立案处罚情况.”时，查询所用评分内容为“占掘路立案处罚情况.”案件总分是否高于1分。当超过1分时对分数进行限制，超过1分的条例增加都不会导致分数增加
        if(dict_jck_szss.getType3().equals("占掘路立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper4);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<1&&floatSum-Float.parseFloat(dict_jck_szss.getValue())>1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(),0);
                    //处理分数从0.*跨度到1.*等情况
                }else if((floatSum-1)<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 1-(floatSum-Float.parseFloat(dict_jck_szss.getValue())));
                    teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), 1-(floatSum-Float.parseFloat(dict_jck_szss.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()));
            }
        }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_jck_szss dict_jck_szss = dict_jck_szssService.selectById(id);
        model.addAttribute("dict_jck_szss",dict_jck_szss);
        return "admin/system/jck_szss/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_jck_szss dict_jck_szss){
        EntityWrapper<Dict_jck_szss> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","燃气立案处罚情况.").eq("description","每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_szss> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type3","燃气立案处罚情况.").eq("description","（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_szss> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type3","排污立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_szss> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("type3","占掘路立案处罚情况.").eq("label",dict_jck_szss.getLabel()).eq("visable_flag",1);
        if(dict_jck_szss.getId()==null || dict_jck_szss.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_jck_szss.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_jck_szss.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_jck_szss.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_szss.getSort() == null || dict_jck_szss.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_jck_szss oldDict_jck_szss = dict_jck_szssService.selectById(dict_jck_szss.getId());
        if(!oldDict_jck_szss.getType().equals(dict_jck_szss.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_jck_szss.getLabel().equals(dict_jck_szss.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_jck_szss.getValue().equals(dict_jck_szss.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_jck_szssService.saveOrUpdateDict(dict_jck_szss);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("每月燃气处罚案件普通程序案件每完成1件得0.5分，最多2分。")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper1);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
            if(floatSum>2){
                //处理分数从1.*跨度到2.*等情况
                if(Float.parseFloat(dict_jck_szss.getValue())>Float.parseFloat(oldDict_jck_szss.getValue())&&oldfloatSum<2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()))));
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()))));
                }else  {teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }
            ////处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_szss.getValue())<Float.parseFloat(oldDict_jck_szss.getValue())&&oldfloatSum>2) {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_szss.getValue())-Float.parseFloat(oldDict_jck_szss.getValue())))));
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_szss.getValue())-Float.parseFloat(oldDict_jck_szss.getValue())))));
            }else{
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
            }
        }
        if(dict_jck_szss.getType3().equals("燃气立案处罚情况.")&&dict_jck_szss.getDescription().equals("（注：较大案件是指对个人处罚500元以上，对企业处罚2万元以上），每完成1件较大案件得1分（最多2分）")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper2);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
            if(floatSum>2){
                //处理分数从1.*跨度到2.*等情况
                if(Float.parseFloat(dict_jck_szss.getValue())>Float.parseFloat(oldDict_jck_szss.getValue())&&oldfloatSum<2){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()))));
                    teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()))));
                }else  {teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }
            ////处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_szss.getValue())<Float.parseFloat(oldDict_jck_szss.getValue())&&oldfloatSum>2) {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_szss.getValue())-Float.parseFloat(oldDict_jck_szss.getValue())))));
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_szss.getValue())-Float.parseFloat(oldDict_jck_szss.getValue())))));
            }else{
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_rq(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
            }
        }
        if(dict_jck_szss.getType3().equals("排污立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper3);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
            if(floatSum>1){
                //处理分数从0.*跨度到1.*等情况
                if(Float.parseFloat(dict_jck_szss.getValue())>Float.parseFloat(oldDict_jck_szss.getValue())&&oldfloatSum<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 1-(floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()))));
                    teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), 1-(floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()))));
                }else  {teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }
            ////处理分数从1.*跨度到0.*等情况
            else if(Float.parseFloat(dict_jck_szss.getValue())<Float.parseFloat(oldDict_jck_szss.getValue())&&oldfloatSum>1) {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -(1-(oldfloatSum+(Float.parseFloat(dict_jck_szss.getValue())-Float.parseFloat(oldDict_jck_szss.getValue())))));
                teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), -(1-(oldfloatSum+(Float.parseFloat(dict_jck_szss.getValue())-Float.parseFloat(oldDict_jck_szss.getValue())))));
            }else{
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_pw(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
            }
        }
        if(dict_jck_szss.getType3().equals("占掘路立案处罚情况.")){
            List<Dict_jck_szss> Dict_jck_szss_List= dict_jck_szssService.selectList(wrapper4);
            double sum=Dict_jck_szss_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
            if(floatSum>1){
                //处理分数从0.*跨度到1.*等情况
                if(Float.parseFloat(dict_jck_szss.getValue())>Float.parseFloat(oldDict_jck_szss.getValue())&&oldfloatSum<1){
                    teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 1-(floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()))));
                    teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), 1-(floatSum-(Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()))));
                }else  {teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), 0);
                }
            }
            ////处理分数从1.*跨度到0.*等情况
            else if(Float.parseFloat(dict_jck_szss.getValue())<Float.parseFloat(oldDict_jck_szss.getValue())&&oldfloatSum>1) {
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), -(1-(oldfloatSum+(Float.parseFloat(dict_jck_szss.getValue())-Float.parseFloat(oldDict_jck_szss.getValue())))));
                teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), -(1-(oldfloatSum+(Float.parseFloat(dict_jck_szss.getValue())-Float.parseFloat(oldDict_jck_szss.getValue())))));
            }else{
                teamService.changeTeamPoint_jck_szss(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
                teamService.changeTeamPoint_jck_szss_jl(dict_jck_szss.getLabel(), Float.parseFloat(dict_jck_szss.getValue()) - Float.parseFloat(oldDict_jck_szss.getValue()));
            }
        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }

    //监察科市政公用大分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_jck_szss> platformJck_SzssList(
                                                         @RequestParam ("label") String label,
                                                         @RequestParam(value = "page",defaultValue = "1")Integer page,
                                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                                         ServletRequest request){
        if(limit==null){
            return null;
        };
        LayerData<Dict_jck_szss> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_szss> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0

        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_jck_szss> dataPage = dict_jck_szssService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
    private void updateTeamPoint(String label, double error) {
        Team team=teamService.queryTeamPoint_jck_szss(label);
        //消除删除条例分数归零浮点型产生的误差
        double value=team.getPoint_jck_szss();
        double value1=team.getPoint_jck_szss_rq();
        double value2=team.getPoint_jck_szss_pw();
        double value3=team.getPoint_jck_szss_jl();
        if(Math.abs(value)<error){
            value=0.0;
        }
        if(Math.abs(value1)<error){
            value1=0.0;
        }
        if(Math.abs(value2)<error){
            value2=0.0;
        }
        if(Math.abs(value3)<error){
            value3=0.0;
        }
        teamService.fixdict_jck_szssPointError(label,value);
        teamService.fixdict_jck_szss_rqPointError(label,value1);
        teamService.fixdict_jck_szss_pwPointError(label,value2);
        teamService.fixdict_jck_szss_jlPointError(label,value3);
    }

}
