package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.*;
import com.mysiteforme.admin.entity.Dict_xfk;
import com.mysiteforme.admin.entity.Dict_xfk;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/19.
 * todo:
 * 信访科controller
 */
@Controller
@RequestMapping("admin/system/xfk")
public class XFKController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_xfk> wrapper1 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_xfk dict_xfk = dict_xfkService.selectById(id);
        wrapper1.eq("type","信访科").eq("label",dict_xfk.getLabel()).eq("visable_flag",1);
        Integer number = dict_xfk.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_xfk.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_xfk.setValue(String.valueOf(newValue));
        dict_xfk.setNumber(number+1);
//        teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_xfkService.saveOrUpdateDict(dict_xfk);
        if(dict_xfk.getType().equals("信访科")){
            List<Dict_xfk> Dict_xfk_List= dict_xfkService.selectList(wrapper1);
            double sum=Dict_xfk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-2){
                if(floatSum-newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),-2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                } else if((floatSum-newValue.subtract(oldValue).floatValue()<-2)){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-newValue.subtract(oldValue).floatValue()>=-2)){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())-2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }
            }else if(floatSum>-2&&floatSum<0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -2)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 2+floatSum );
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), floatSum);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
                else{
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), newValue.subtract(oldValue).floatValue());
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -(floatSum - newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
                else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }
            }

        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_xfk> wrapper1 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_xfk dict_xfk = dict_xfkService.selectById(id);
        wrapper1.eq("type","信访科").eq("label",dict_xfk.getLabel()).eq("visable_flag",1);
        Integer number = dict_xfk.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_xfk.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_xfk.setValue(String.valueOf(newValue));
        dict_xfk.setNumber(number-1);
//        teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_xfkService.saveOrUpdateDict(dict_xfk);
        if(dict_xfk.getType().equals("信访科")){
            List<Dict_xfk> Dict_xfk_List= dict_xfkService.selectList(wrapper1);
            double sum=Dict_xfk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            if(floatSum<=-2){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else if(floatSum+newValue.subtract(oldValue).floatValue()<=-2){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-2){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 2+(floatSum+newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_xfk.getLabel(), error);
                } else{
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }
            }else if(floatSum>-2&&floatSum<0){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-2){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -(2+floatSum));
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -floatSum);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), newValue.subtract(oldValue).floatValue());
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
            }
            else if(floatSum>=0) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-2){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),-2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                    updateTeamPoint(dict_xfk.getLabel(), error);
                } else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }

            }

        }
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_xfk> wrapper1 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_xfk dict_xfk = dict_xfkService.selectById(id);
        wrapper1.eq("type","信访科").eq("label",dict_xfk.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -Float.parseFloat(dict_xfk.getValue()));
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_xfkService.deleteDict(id);
        if(dict_xfk.getType().equals("信访科")){
            List<Dict_xfk> Dict_xfk_List= dict_xfkService.selectList(wrapper1);
            double sum=Dict_xfk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_xfk.getValue());
            if(floatSum<=-2){
                if(floatSum-Float.parseFloat(dict_xfk.getValue())>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else if(floatSum-Float.parseFloat(dict_xfk.getValue())<=-2){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_xfk.getValue())>=-2){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 2+(floatSum-Float.parseFloat(dict_xfk.getValue())));
                    updateTeamPoint(dict_xfk.getLabel(), error);
                } else{
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }
            }else if(floatSum>-2&&floatSum<0){
                if(floatSum-Float.parseFloat(dict_xfk.getValue())<=-2){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -(2+floatSum));
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else if(floatSum-Float.parseFloat(dict_xfk.getValue())>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -floatSum);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -Float.parseFloat(dict_xfk.getValue()));
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
            }
            else if(floatSum>=0) {
                if(floatSum-Float.parseFloat(dict_xfk.getValue())<=-2){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),-2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else if ((floatSum-Float.parseFloat(dict_xfk.getValue()) > 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum-Float.parseFloat(dict_xfk.getValue()) <= 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), floatSum - Float.parseFloat(dict_xfk.getValue()));
                } else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }

            }

        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/xfk/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_xfk> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_xfk> layerData = new LayerData<>();
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_xfk> dataPage = dict_xfkService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_xfk> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_xfk> layerData = new LayerData<>();
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_xfk> dataPage = dict_xfkService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/xfk/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/xfk/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/xfk/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_xfk dict_xfk){
        EntityWrapper<Dict_xfk> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type","信访科").eq("label",dict_xfk.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict_xfk.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_xfk.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_xfkService.getCountByType(dict_xfk.getType())==0){
            dict_xfk.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_xfk.setSort(dict_xfkService.getMaxSortByType(dict_xfk.getType()));
        }
        dict_xfkService.saveOrUpdateDict(dict_xfk);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), Float.parseFloat(dict_xfk.getValue()));
        if(dict_xfk.getType().equals("信访科")){
            List<Dict_xfk> Dict_xfk_List= dict_xfkService.selectList(wrapper1);
            double sum=Dict_xfk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-2){
                if(floatSum-Float.parseFloat(dict_xfk.getValue())>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),-2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                } else if((floatSum-Float.parseFloat(dict_xfk.getValue())<-2)){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_xfk.getValue())>=-2)){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -(floatSum-Float.parseFloat(dict_xfk.getValue()))-2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }
            }else if(floatSum>-2&&floatSum<0) {
                if ((floatSum - Float.parseFloat(dict_xfk.getValue()) <= -2)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 2+floatSum );
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else if(floatSum - Float.parseFloat(dict_xfk.getValue())>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), floatSum);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
                else{
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), Float.parseFloat(dict_xfk.getValue()));
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - Float.parseFloat(dict_xfk.getValue()) > 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - Float.parseFloat(dict_xfk.getValue()) <= 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -(floatSum - Float.parseFloat(dict_xfk.getValue())));
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
                else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }
            }

        }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_xfk dict_xfk = dict_xfkService.selectById(id);
        model.addAttribute("dict_xfk",dict_xfk);
        return "admin/system/xfk/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_xfk dict_xfk){
        EntityWrapper<Dict_xfk> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type","信访科").eq("label",dict_xfk.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(dict_xfk.getId()==null || dict_xfk.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_xfk.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_xfk.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_xfk.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_xfk.getSort() == null || dict_xfk.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_xfk oldDict_xfk = dict_xfkService.selectById(dict_xfk.getId());
        if(!oldDict_xfk.getType().equals(dict_xfk.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_xfk.getLabel().equals(dict_xfk.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_xfk.getValue().equals(dict_xfk.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_xfkService.saveOrUpdateDict(dict_xfk);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), Float.parseFloat(dict_xfk.getValue()) - Float.parseFloat(oldDict_xfk.getValue()));
        if(dict_xfk.getType().equals("信访科")){
            List<Dict_xfk> Dict_xfk_List= dict_xfkService.selectList(wrapper1);
            double sum=Dict_xfk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_xfk.getValue()) - Float.parseFloat(oldDict_xfk.getValue());
//            float point=(float)1.4;
            if(floatSum<=-2){
                if(floatSum-differ>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),-2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                } else if((floatSum-differ<-2)){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-2)){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -(floatSum-differ)-2);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }
            }else if(floatSum>-2&&floatSum<0) {
                if ((floatSum - differ <= -2)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 2+floatSum );
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }else if(floatSum - differ>=0){
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), floatSum);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
                else{
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), differ);
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - differ > 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 0)) {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), -(floatSum - differ));
                    updateTeamPoint(dict_xfk.getLabel(), error);
                }
                else {
                    teamService.changeTeamPoint_xfk(dict_xfk.getLabel(), 0);
                }
            }

        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }

    //信访科大分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_xfk> platformJck_XfkList(@RequestParam ("label") String label,
                                                   @RequestParam(value = "page",defaultValue = "1")Integer page,
                                                   @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                                   ServletRequest request){
        if(limit==null){
            return null;
        };
        LayerData<Dict_xfk> layerData = new LayerData<>();
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");wrapper.eq("label", label);
        Page<Dict_xfk> dataPage = dict_xfkService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
    //消除浮点型数做加减造成的误差
    private void updateTeamPoint(String label, double error) {
        Team team = teamService.queryTeamPoint_xfk(label);
        double value = team.getPoint_xfk();
        if (Math.abs(value) < error) {
            value = 0.0;
        }
        teamService.fixdict_xfkPointError(label, value);
    }


}
