package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.*;
import com.mysiteforme.admin.entity.Dict_jck_qtaj;
import com.mysiteforme.admin.entity.Dict_jck_qtaj;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2023/8/19.
 * todo:
 * 监察科-其他案件controller
 */
@Controller
@RequestMapping("admin/system/jck_qtaj")
public class JCK_qtajController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_qtaj> wrapper1 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_qtaj dict_jck_qtaj = dict_jck_qtajService.selectById(id);
        wrapper1.eq("type3","其它行政处罚类案件").eq("label",dict_jck_qtaj.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_qtaj.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_jck_qtaj.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_qtaj.setValue(String.valueOf(newValue));
        dict_jck_qtaj.setNumber(number+1);
//        teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_jck_qtajService.saveOrUpdateDict(dict_jck_qtaj);
        //当增加条数的条例评分内容为“其它行政处罚类案件”时，查询所用评分内容为“其它行政处罚类案件”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_qtaj.getType3().equals("其它行政处罚类案件")){
            List<Dict_jck_qtaj> Dict_jck_qtaj_List= dict_jck_qtajService.selectList(wrapper1);
            double sum=Dict_jck_qtaj_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&(floatSum-newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.5||(floatSum-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_qtaj> wrapper1 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_qtaj dict_jck_qtaj = dict_jck_qtajService.selectById(id);
        wrapper1.eq("type3","其它行政处罚类案件").eq("label",dict_jck_qtaj.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_qtaj.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_jck_qtaj.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_qtaj.setValue(String.valueOf(newValue));
        dict_jck_qtaj.setNumber(number-1);
//        teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), newValue.subtract(oldValue).floatValue());
        //当减少条数的条例评分内容为“其它行政处罚类案件”时，查询所用评分内容为“其它行政处罚类案件”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_qtaj.getType3().equals("其它行政处罚类案件")){
            List<Dict_jck_qtaj> Dict_jck_qtaj_List= dict_jck_qtajService.selectList(wrapper1);
            double sum=Dict_jck_qtaj_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&(floatSum+newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum-2)<0.5||(floatSum+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        dict_jck_qtajService.saveOrUpdateDict(dict_jck_qtaj);
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_qtaj> wrapper1 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_jck_qtaj dict_jck_qtaj = dict_jck_qtajService.selectById(id);
        wrapper1.eq("type3","其它行政处罚类案件").eq("label",dict_jck_qtaj.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), -Float.parseFloat(dict_jck_qtaj.getValue()));
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_jck_qtajService.deleteDict(id);
        if(dict_jck_qtaj.getType3().equals("其它行政处罚类案件")){
            List<Dict_jck_qtaj> Dict_jck_qtaj_List= dict_jck_qtajService.selectList(wrapper1);
            double sum=Dict_jck_qtaj_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_qtaj.getValue());
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&floatSum-Float.parseFloat(dict_jck_qtaj.getValue())>2){
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum-2)<0.5||Float.parseFloat(dict_jck_qtaj.getValue())>0.5)&&(floatSum-Float.parseFloat(dict_jck_qtaj.getValue()))<2){
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), floatSum-Float.parseFloat(dict_jck_qtaj.getValue())-2);
                } else{
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), -Float.parseFloat(dict_jck_qtaj.getValue()));
                //若扣分项为多次+1后的项，删除后有可能导致分数项小于零，此处进行归零
                Team team=teamService.queryTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel());
//                if(team.getPoint_jck_qtaj()<0) {
//                    teamService.zeroTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel());
//                }
                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_jck_qtaj();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_jck_qtajPointError(dict_jck_qtaj.getLabel(),value);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/jck_qtaj/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_jck_qtaj> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                         ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_qtaj> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_qtaj> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_qtaj> dataPage = dict_jck_qtajService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_jck_qtaj> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                         ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_qtaj> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_qtaj> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_qtaj> dataPage = dict_jck_qtajService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_qtaj/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_qtaj/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_qtaj/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_jck_qtaj dict_jck_qtaj){
        EntityWrapper<Dict_jck_qtaj> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","其它行政处罚类案件").eq("label",dict_jck_qtaj.getLabel()).eq("visable_flag",1);
        if(StringUtils.isBlank(dict_jck_qtaj.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_jck_qtaj.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_qtajService.getCountByType(dict_jck_qtaj.getType())==0){
            dict_jck_qtaj.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_jck_qtaj.setSort(dict_jck_qtajService.getMaxSortByType(dict_jck_qtaj.getType()));
        }
        dict_jck_qtajService.saveOrUpdateDict(dict_jck_qtaj);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), Float.parseFloat(dict_jck_qtaj.getValue()));
        //当评分内容为“其它行政处罚类案件”时，查询所用评分内容为“其它行政处罚类案件”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_qtaj.getType3().equals("其它行政处罚类案件")){
            List<Dict_jck_qtaj> Dict_jck_qtaj_List= dict_jck_qtajService.selectList(wrapper1);
            double sum=Dict_jck_qtaj_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&floatSum-Float.parseFloat(dict_jck_qtaj.getValue())>2){
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.5){
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_qtaj.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), Float.parseFloat(dict_jck_qtaj.getValue()));
            }
        }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_jck_qtaj dict_jck_qtaj = dict_jck_qtajService.selectById(id);
        model.addAttribute("dict_jck_qtaj",dict_jck_qtaj);
        return "admin/system/jck_qtaj/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_jck_qtaj dict_jck_qtaj){
        EntityWrapper<Dict_jck_qtaj> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","其它行政处罚类案件").eq("label",dict_jck_qtaj.getLabel()).eq("visable_flag",1);
        if(dict_jck_qtaj.getId()==null || dict_jck_qtaj.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_jck_qtaj.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_jck_qtaj.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_jck_qtaj.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_qtaj.getSort() == null || dict_jck_qtaj.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_jck_qtaj oldDict_jck_qtaj = dict_jck_qtajService.selectById(dict_jck_qtaj.getId());
        if(!oldDict_jck_qtaj.getType().equals(dict_jck_qtaj.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_jck_qtaj.getLabel().equals(dict_jck_qtaj.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_jck_qtaj.getValue().equals(dict_jck_qtaj.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_jck_qtajService.saveOrUpdateDict(dict_jck_qtaj);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), Float.parseFloat(dict_jck_qtaj.getValue()) - Float.parseFloat(oldDict_jck_qtaj.getValue()));
        if(dict_jck_qtaj.getType3().equals("其它行政处罚类案件")){
            List<Dict_jck_qtaj> Dict_jck_qtaj_List= dict_jck_qtajService.selectList(wrapper1);
            double sum=Dict_jck_qtaj_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_qtaj.getValue()) - Float.parseFloat(oldDict_jck_qtaj.getValue()));
            if(floatSum>2){
                //处理分数从1.*跨度到2.*等情况
                if(Float.parseFloat(dict_jck_qtaj.getValue())>Float.parseFloat(oldDict_jck_qtaj.getValue())&&oldfloatSum<2){
                    teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_qtaj.getValue()) - Float.parseFloat(oldDict_jck_qtaj.getValue()))));
                }else  {teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), 0);
                }
            }
            ////处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_qtaj.getValue())<Float.parseFloat(oldDict_jck_qtaj.getValue())&&oldfloatSum>2) {
                teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_qtaj.getValue())-Float.parseFloat(oldDict_jck_qtaj.getValue())))));
            }else{
                teamService.changeTeamPoint_jck_qtaj(dict_jck_qtaj.getLabel(), Float.parseFloat(dict_jck_qtaj.getValue()) - Float.parseFloat(oldDict_jck_qtaj.getValue()));
            }
        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }

    //监察科园林绿化大分数接口分数接口，供大屏幕显示数据用


    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_jck_qtaj> platformJck_qtajList(@RequestParam ("label") String label,
                                                         @RequestParam(value = "page",defaultValue = "1")Integer page,
                                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,

                                                         ServletRequest request){
        if(limit==null){
            return null;
        };

        LayerData<Dict_jck_qtaj> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_qtaj> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0

        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_jck_qtaj> dataPage = dict_jck_qtajService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }



}
