package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.*;
import com.mysiteforme.admin.entity.Dict_dcs;
import com.mysiteforme.admin.entity.Dict_dcs2;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/19.
 * todo:
 * 督察室controller
 * update by wcl on 2023/9/8.
 * 改为指挥中心controller
 */
@Controller
@RequestMapping("admin/system/dcs")
public class DCSController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_dcs> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_dcs> wrapper2 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_dcs dict_dcs = dict_dcsService.selectById(id);
        wrapper1.eq("type3","诉转案").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
//        wrapper2.eq("type3","慧商程序应用").or().eq("type3","智慧执法系统实际应用").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
        Integer number = dict_dcs.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_dcs.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_dcs.setValue(String.valueOf(newValue));
        dict_dcs.setNumber(number+1);
//        teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_dcsService.saveOrUpdateDict(dict_dcs);
        if(dict_dcs.getType3().equals("诉转案")){
            List<Dict_dcs> Dict_dcs_List= dict_dcsService.selectList(wrapper1);
            double sum=Dict_dcs_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if (floatSum>=1.7){
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), 0);
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(),0);
            }else {
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), newValue.subtract(oldValue).floatValue());
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(), newValue.subtract(oldValue).floatValue());
            }

        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_dcs> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_dcs> wrapper2 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_dcs dict_dcs = dict_dcsService.selectById(id);
        wrapper1.eq("type3","诉转案").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
//        wrapper2.eq("type3","慧商程序应用").or().eq("type3","智慧执法系统实际应用").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
        Integer number = dict_dcs.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_dcs.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_dcs.setValue(String.valueOf(newValue));
        dict_dcs.setNumber(number-1);
//        teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_dcsService.saveOrUpdateDict(dict_dcs);
        if(dict_dcs.getType3().equals("诉转案")){
            List<Dict_dcs> Dict_dcs_List= dict_dcsService.selectList(wrapper1);
            double sum=Dict_dcs_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if (floatSum<=1.5){
                teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(), newValue.subtract(oldValue).floatValue());
            }else {
                teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), 0);
                teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(),0);
            }

        }
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_dcs> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_dcs> wrapper2 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_dcs dict_dcs = dict_dcsService.selectById(id);
        wrapper1.eq("type3","诉转案").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
//        wrapper2.eq("type3","慧商程序应用").or().eq("type3","智慧执法系统实际应用").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), -Float.parseFloat(dict_dcs.getValue()));

        //指定的误差范围
        double error=1E-6;
        if(dict_dcs.getType3().equals("诉转案")){
            List<Dict_dcs> Dict_dcs_List= dict_dcsService.selectList(wrapper1);
            double sum=Dict_dcs_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float init_point=(float)0.4;
            float point=Float.parseFloat(dict_dcs.getValue());
            int y=Dict_dcs_List.toArray().length;
            if(y==1){
                teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), -(Float.parseFloat(dict_dcs.getValue())+init_point));
                teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(),-(Float.parseFloat(dict_dcs.getValue())+init_point));
                updateTeamPoint(dict_dcs.getLabel(), error);
            }else if (y>1){
                if (floatSum>=1.7&&floatSum-point>=1.6){
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), 0);
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(),0);
                }else if (floatSum-point<1.6&&floatSum>=1.7){
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), -Float.parseFloat(dict_dcs.getValue())+(floatSum+init_point-2));
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(), -Float.parseFloat(dict_dcs.getValue())+(floatSum+init_point-2));
                }else {
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), -Float.parseFloat(dict_dcs.getValue()));
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(), -Float.parseFloat(dict_dcs.getValue()));
                }
            }

        }
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_dcsService.deleteDict(id);
        return RestResponse.success();
    }

    @PostMapping("deleteById2")
    @ResponseBody
    public RestResponse deleteById2(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_dcs2> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_dcs2> wrapper2 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_dcs2 dict_dcs2 = dict_dcs2Service.selectById(id);
        wrapper1.eq("type3","派件处置").eq("label",dict_dcs2.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","慧商程序应用").or().eq("type3","智慧执法系统实际应用").eq("label",dict_dcs2.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_dcs(dict_dcs2.getLabel(), -Float.parseFloat(dict_dcs2.getValue()));
        if(dict_dcs2.getType3().equals("派件处置")){
            List<Dict_dcs2> Dict_dcs_List= dict_dcs2Service.selectList(wrapper1);
            double sum=Dict_dcs_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            int y=Dict_dcs_List.toArray().length;
            //删除之前的和
            float floatSum1 = (float) sum;
            float x= Float.parseFloat(dict_dcs2.getValue());
            float floatSum2 = (float) (sum * 0.02);
            float floatSum3 = (float) ((floatSum1-x) * 0.02);
            //删除前的加分数值
            float result1 = (float)(Math.round((floatSum2/y) * 100.0) / 100.0);
            //删除后的加分数值
            float result2 = (float)(Math.round((floatSum3/(y-1)) * 100.0) / 100.0);
            //指定的误差范围
            double error=1E-6;
            if(y>1){
                teamService.changeTeamPoint_dcs(dict_dcs2.getLabel(), -result1+result2);
                teamService.changeTeamPoint_dcs_ajcz(dict_dcs2.getLabel(), -result1+result2);
            }else if (y==1){
                teamService.changeTeamPoint_dcs(dict_dcs2.getLabel(), -result1);
                teamService.changeTeamPoint_dcs_ajcz(dict_dcs2.getLabel(), -result1);
                updateTeamPoint(dict_dcs2.getLabel(), error);
            }
        }
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_dcs2Service.deleteDict(id);

        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/dcs/list";
    }

    @GetMapping("list2")
    @SysLog("跳转系统字典页面")
    public String list2(){
        return "admin/system/dcs/list2";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_dcs> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_dcs> layerData = new LayerData<>();
        EntityWrapper<Dict_dcs> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_dcs> dataPage = dict_dcsService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list2")
    @ResponseBody
    public LayerData<Dict_dcs2> list2(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_dcs2> layerData = new LayerData<>();
        EntityWrapper<Dict_dcs2> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_dcs2> dataPage = dict_dcs2Service.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_dcs> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_dcs> layerData = new LayerData<>();
        EntityWrapper<Dict_dcs> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_dcs> dataPage = dict_dcsService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist2")
    @ResponseBody
    public LayerData<Dict_dcs2> historylist2(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                           @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                           ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_dcs2> layerData = new LayerData<>();
        EntityWrapper<Dict_dcs2> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_dcs2> dataPage = dict_dcs2Service.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dcs/add";
    }

    @GetMapping("add2")
    public String add2(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dcs/add2";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dcs/history";
    }

    @GetMapping("history2")
    public String history2(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dcs/history2";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dcs/zdlist";
    }

    @GetMapping("zdList2")
    public String zdList2(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dcs/zdlist2";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_dcs dict_dcs){
        EntityWrapper<Dict_dcs> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_dcs> wrapper2 = new EntityWrapper<>();
        wrapper1.eq("type3","诉转案").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
//        wrapper2.eq("type3","慧商程序应用").or().eq("type3","智慧执法系统实际应用").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict_dcs.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_dcs.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_dcsService.getCountByType(dict_dcs.getType())==0){
            dict_dcs.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_dcs.setSort(dict_dcsService.getMaxSortByType(dict_dcs.getType()));
        }
        dict_dcsService.saveOrUpdateDict(dict_dcs);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), Float.parseFloat(dict_dcs.getValue()));
        if(dict_dcs.getType3().equals("诉转案")){
            List<Dict_dcs> Dict_dcs_List= dict_dcsService.selectList(wrapper1);
            double sum=Dict_dcs_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float init_point=(float)0.4;
            int y=Dict_dcs_List.toArray().length;
            if(y==1){
                teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), Float.parseFloat(dict_dcs.getValue())+init_point);
                teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(),Float.parseFloat(dict_dcs.getValue())+init_point);
            }else if (y>1){
                if (floatSum>=1.7){
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), 0);
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(),0);
                }else {
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), Float.parseFloat(dict_dcs.getValue()));
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(), Float.parseFloat(dict_dcs.getValue()));
                }
            }

        }

        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add2")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add2(Dict_dcs2 dict_dcs2){
        EntityWrapper<Dict_dcs2> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_dcs2> wrapper2 = new EntityWrapper<>();
        wrapper1.eq("type3","派件处置").eq("label",dict_dcs2.getLabel()).eq("visable_flag",1);
//        wrapper2.eq("type3","慧商程序应用").or().eq("type3","智慧执法系统实际应用").eq("label",dict_dcs2.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict_dcs2.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_dcs2.getComplete())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        else if (StringUtils.isBlank(dict_dcs2.getTotal())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_dcs2Service.getCountByType(dict_dcs2.getType())==0){
            dict_dcs2.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_dcs2.setSort(dict_dcsService.getMaxSortByType(dict_dcs2.getType()));
        }
        if(Float.parseFloat(dict_dcs2.getComplete())>Float.parseFloat(dict_dcs2.getTotal())){
            return RestResponse.failure("派件完成数不能大于派件总数");
        }
        float complete = Float.parseFloat(dict_dcs2.getComplete());
        float total = Float.parseFloat(dict_dcs2.getTotal());
        if (complete==0 && total==0){
            int value=100;
            String v = String.valueOf(value);
            dict_dcs2.setValue(v);
        }else {
            int intValue = Math.round((complete / total) * 100);
            //百分制分值
            String result = String.valueOf(intValue);
            dict_dcs2.setValue(result);
        }
        dict_dcs2Service.saveOrUpdateDict(dict_dcs2);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), Float.parseFloat(dict_dcs.getValue()));
        if(dict_dcs2.getType3().equals("派件处置")){
            //乘以权值后的得分加到中队分值
            List<Dict_dcs2> Dict_dcs_List= dict_dcs2Service.selectList(wrapper1);
            int y=Dict_dcs_List.toArray().length;
            double sum = Dict_dcs_List.stream()
                    .mapToDouble(dict -> Float.parseFloat(dict.getValue()))
                    .sum();
            if(y==1&&complete==0 && total==0) {
                float result2 = (float) (Math.round(1 * 100.0) / 100.0)*2;;
                teamService.changeTeamPoint_dcs(dict_dcs2.getLabel(), result2);
                teamService.changeTeamPoint_dcs_ajcz(dict_dcs2.getLabel(), result2);
            }else if (y==1){
                float result2 = (float) (Math.round(complete / total * 100.0) / 100.0) * 2;;
                teamService.changeTeamPoint_dcs(dict_dcs2.getLabel(), result2);
                teamService.changeTeamPoint_dcs_ajcz(dict_dcs2.getLabel(), result2);
            }else if (y>1&&complete==0 && total==0){
                float result2 = (float) (Math.round(1 * 100.0) / 100.0)*2;;
                float floatSum = (float) (sum * 0.02);
                float result3 = (float)(Math.round((floatSum/y) * 100.0) / 100.0);
                teamService.changeTeamPoint_dcs(dict_dcs2.getLabel(), -(float)(Math.round((floatSum-result2)/(y-1) * 100.0) / 100.0)+result3);
                teamService.changeTeamPoint_dcs_ajcz(dict_dcs2.getLabel(), -(float)(Math.round((floatSum-result2)/(y-1) * 100.0) / 100.0)+result3);
            }else if(y>1){
                float result2 = (float) (Math.round(complete / total * 100.0) / 100.0) * 2;;
                float floatSum = (float) (sum * 0.02);
                float result3 = (float)(Math.round((floatSum/y) * 100.0) / 100.0);
                teamService.changeTeamPoint_dcs(dict_dcs2.getLabel(), -(float)(Math.round((floatSum-result2)/(y-1) * 100.0) / 100.0)+result3);
                teamService.changeTeamPoint_dcs_ajcz(dict_dcs2.getLabel(), -(float)(Math.round((floatSum-result2)/(y-1) * 100.0) / 100.0)+result3);
            }
        }

         return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_dcs dict_dcs = dict_dcsService.selectById(id);
        model.addAttribute("dict_dcs",dict_dcs);
        return "admin/system/dcs/edit";
    }

    @GetMapping("edit2")
    public String edit2(Long id,Model model){
        Dict_dcs2 dict_dcs2 = dict_dcs2Service.selectById(id);
        model.addAttribute("dict_dcs2",dict_dcs2);
        return "admin/system/dcs/edit2";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_dcs dict_dcs){
        EntityWrapper<Dict_dcs> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_dcs> wrapper2 = new EntityWrapper<>();
        wrapper1.eq("type3","诉转案").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
//        wrapper2.eq("type3","慧商程序应用").or().eq("type3","智慧执法系统实际应用").eq("label",dict_dcs.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(dict_dcs.getId()==null || dict_dcs.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_dcs.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_dcs.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_dcs.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_dcs.getSort() == null || dict_dcs.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_dcs oldDict_dcs = dict_dcsService.selectById(dict_dcs.getId());
        if(!oldDict_dcs.getType().equals(dict_dcs.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_dcs.getLabel().equals(dict_dcs.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_dcs.getValue().equals(dict_dcs.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_dcsService.saveOrUpdateDict(dict_dcs);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), Float.parseFloat(dict_dcs.getValue()) - Float.parseFloat(oldDict_dcs.getValue()));
        if(dict_dcs.getType3().equals("诉转案")) {
            List<Dict_dcs> Dict_dcs_List = dict_dcsService.selectList(wrapper1);
            double sum = Dict_dcs_List.stream().mapToDouble(dict -> Double.parseDouble(dict.getValue())).sum();
            float floatSum = (float) sum;
            float init_point=(float)0.4;
            float point=(float)1.6;
            float differ = Float.parseFloat(dict_dcs.getValue()) - Float.parseFloat(oldDict_dcs.getValue());
            if (floatSum - differ >= 1.6) {
                if (floatSum >= 1.6) {
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), 0);
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(), 0);
                    updateTeamPoint(dict_dcs.getLabel(), error);
                } else if ((floatSum < 1.6)) {
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), (floatSum+init_point-2));
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(), (floatSum+init_point-2));
                    //处理分数跨度的情况
                }
            }else if (floatSum - differ <= 1.6){
                if(floatSum >= 1.6){
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), (point-(floatSum - differ)));
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(), (point-(floatSum - differ)));
                }
                if(floatSum < 1.6&& floatSum>0) {
                    teamService.changeTeamPoint_dcs(dict_dcs.getLabel(), differ);
                    teamService.changeTeamPoint_dcs_hs(dict_dcs.getLabel(), differ);
                }
            }
        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }



    //指挥中心分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_dcs> platformDcsList(@RequestParam ("label") String label,
                                               @RequestParam(value = "page",defaultValue = "1")Integer page,
                                               @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                               ServletRequest request){
        if(limit==null){
            return null;
        };
        LayerData<Dict_dcs> layerData = new LayerData<>();
        EntityWrapper<Dict_dcs> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0

        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_dcs> dataPage = dict_dcsService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
    //消除浮点型数做加减造成的误差
    private void updateTeamPoint(String label, double error) {
        Team team = teamService.queryTeamPoint_dcs(label);
        double value = team.getPoint_dcs();
        double value1 = team.getPoint_dcs_ajcz();
        double value2 = team.getPoint_dcs_hs();
        if (Math.abs(value) < error) {
            value = 0.0;
        }
        if (Math.abs(value1) < error) {
            value1 = 0.0;
        }
        if (Math.abs(value2) < error) {
            value2 = 0.0;
        }
        teamService.fixdict_dcsPointError(label, value);
        teamService.fixdict_dcs_ajczPointError(label, value1);
        teamService.fixdict_dcs_hcPointError(label, value2);
    }
}

