package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.*;
import com.mysiteforme.admin.entity.Dict_xck;
import com.mysiteforme.admin.entity.Dict_xck;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/19.
 * todo:
 * 宣传科controller
 */
@Controller
@RequestMapping("admin/system/xck")
public class XCKController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_xck> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_xck> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_xck> wrapper3 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_xck dict_xck = dict_xckService.selectById(id);
        wrapper1.eq("type2","上报信息").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type2","新闻报道").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type2","网络舆情").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        Integer number = dict_xck.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_xck.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_xck.setValue(String.valueOf(newValue));
        dict_xck.setNumber(number+1);
//        teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_xckService.saveOrUpdateDict(dict_xck);
        if(dict_xck.getType2().equals("上报信息")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper1);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-1.1;
            if(floatSum<=-1.1){
                if((floatSum-newValue.subtract(oldValue).floatValue()<-1.1)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度的情况
                }
                if ((floatSum - newValue.subtract(oldValue).floatValue() > -1.1)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())+point);
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }
            }else if(floatSum>-1.1&&floatSum<0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -1.1)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point + floatSum);
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }
            else{
                teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
                Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_xck();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
            }
        }
        if(dict_xck.getType2().equals("新闻报道")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper2);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-0.4;
            if(floatSum<=-0.4){
                if((floatSum-newValue.subtract(oldValue).floatValue()<-0.4)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度的情况
                }
                if ((floatSum - newValue.subtract(oldValue).floatValue() > -0.4)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())+point);
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }
            }else if(floatSum>-0.4&&floatSum<0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -0.4)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point + floatSum);
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }
            else{
                teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
                Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_xck();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
            }
        }
        if(dict_xck.getType2().equals("网络舆情")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper3);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if((floatSum-newValue.subtract(oldValue).floatValue()<-0.5)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度的情况
                }
                if ((floatSum - newValue.subtract(oldValue).floatValue() > -0.5)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())+point);
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }
            }else if(floatSum>-0.5&&floatSum<0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -0.5)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point + floatSum);
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }
            else{
                teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
                Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_xck();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
            }
        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_xck> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_xck> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_xck> wrapper3 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_xck dict_xck = dict_xckService.selectById(id);
        wrapper1.eq("type2","上报信息").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type2","新闻报道").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type2","网络舆情").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        Integer number = dict_xck.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_xck.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_xck.setValue(String.valueOf(newValue));
        dict_xck.setNumber(number-1);
//        teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_xckService.saveOrUpdateDict(dict_xck);
        if(dict_xck.getType2().equals("上报信息")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper1);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            float point=(float)-1.1;
            if(floatSum<=-1.1){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-1.1){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-1.1){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point+(floatSum+newValue.subtract(oldValue).floatValue()));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else if(floatSum>-1.1&&floatSum<0){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-1.1){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),point);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                } else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }

            }

        }
        if(dict_xck.getType2().equals("新闻报道")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper2);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            float point=(float)-0.4;
            if(floatSum<=-0.4){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-0.4){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-0.4){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point+(floatSum+newValue.subtract(oldValue).floatValue()));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else if(floatSum>-0.4&&floatSum<0){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-0.4){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),point);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                } else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }

            }

        }
        if(dict_xck.getType2().equals("网络舆情")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper3);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-0.5){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-0.5){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point+(floatSum+newValue.subtract(oldValue).floatValue()));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else if(floatSum>-0.5&&floatSum<0){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-0.5){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),point);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                } else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }

            }

        }
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_xck> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_xck> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_xck> wrapper3 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_xck dict_xck = dict_xckService.selectById(id);
        wrapper1.eq("type2","上报信息").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type2","新闻报道").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type2","网络舆情").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_xck(dict_xck.getLabel(), -Float.parseFloat(dict_xck.getValue()));
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_xckService.deleteDict(id);
        if(dict_xck.getType2().equals("上报信息")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper1);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_xck.getValue());
            float point=(float)-1.1;
            if(floatSum<=-1.1){
                if(floatSum-Float.parseFloat(dict_xck.getValue())>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),-point);
                }else if(floatSum-Float.parseFloat(dict_xck.getValue())<=-1.1){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_xck.getValue())>=-1.1){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point+(floatSum-Float.parseFloat(dict_xck.getValue())));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else if(floatSum>-1.1&&floatSum<0){
                if(floatSum-Float.parseFloat(dict_xck.getValue())<=-1.1){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(-point+floatSum));
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }else if(floatSum-Float.parseFloat(dict_xck.getValue())>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -Float.parseFloat(dict_xck.getValue()));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }else {
                teamService.changeTeamPoint_xck(dict_xck.getLabel(), -Float.parseFloat(dict_xck.getValue()));
                Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_xck();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
            }
        }
        if(dict_xck.getType2().equals("新闻报道")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper2);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_xck.getValue());
            float point=(float)-0.4;
            if(floatSum<=-0.4){
                if(floatSum-Float.parseFloat(dict_xck.getValue())>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),-point);
                }else if(floatSum-Float.parseFloat(dict_xck.getValue())<=-0.4){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_xck.getValue())>=-0.4){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point+(floatSum-Float.parseFloat(dict_xck.getValue())));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else if(floatSum>-0.4&&floatSum<0){
                if(floatSum-Float.parseFloat(dict_xck.getValue())<=-0.4){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(-point+floatSum));
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }else if(floatSum-Float.parseFloat(dict_xck.getValue())>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -Float.parseFloat(dict_xck.getValue()));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }else {
                teamService.changeTeamPoint_xck(dict_xck.getLabel(), -Float.parseFloat(dict_xck.getValue()));
                Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_xck();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
            }
        }
        if(dict_xck.getType2().equals("网络舆情")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper3);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_xck.getValue());
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum-Float.parseFloat(dict_xck.getValue())>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),-point);
                }else if(floatSum-Float.parseFloat(dict_xck.getValue())<=-0.5){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_xck.getValue())>=-0.5){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point+(floatSum-Float.parseFloat(dict_xck.getValue())));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else if(floatSum>-0.5&&floatSum<0){
                if(floatSum-Float.parseFloat(dict_xck.getValue())<=-0.5){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(-point+floatSum));
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }else if(floatSum-Float.parseFloat(dict_xck.getValue())>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -Float.parseFloat(dict_xck.getValue()));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }else {
                teamService.changeTeamPoint_xck(dict_xck.getLabel(), -Float.parseFloat(dict_xck.getValue()));
                Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_xck();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/xck/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_xck> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_xck> layerData = new LayerData<>();
        EntityWrapper<Dict_xck> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_xck> dataPage = dict_xckService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }


    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_xck> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_xck> layerData = new LayerData<>();
        EntityWrapper<Dict_xck> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_xck> dataPage = dict_xckService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/xck/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/xck/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/xck/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_xck dict_xck){
        EntityWrapper<Dict_xck> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type2","上报信息").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_xck> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type2","新闻报道").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_xck> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type2","网络舆情").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict_xck.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_xck.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_xckService.getCountByType(dict_xck.getType())==0){
            dict_xck.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_xck.setSort(dict_xckService.getMaxSortByType(dict_xck.getType()));
        }
        dict_xckService.saveOrUpdateDict(dict_xck);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_xck(dict_xck.getLabel(), Float.parseFloat(dict_xck.getValue()));
        if(dict_xck.getType2().equals("上报信息")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper1);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-1.1;
            if(floatSum<=-1.1){
                if(floatSum-Float.parseFloat(dict_xck.getValue())>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),point);
                } else if((floatSum-Float.parseFloat(dict_xck.getValue())<-1.1)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_xck.getValue())>=-1.1)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum-Float.parseFloat(dict_xck.getValue()))+point);
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else{
                teamService.changeTeamPoint_xck(dict_xck.getLabel(), Float.parseFloat(dict_xck.getValue()));
                Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_xck();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
            }

        }
        if(dict_xck.getType2().equals("新闻报道")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper2);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-0.4;
            if(floatSum<=-0.4){
                if(floatSum-Float.parseFloat(dict_xck.getValue())>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),point);
                } else if((floatSum-Float.parseFloat(dict_xck.getValue())<-0.4)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_xck.getValue())>=-0.4)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum-Float.parseFloat(dict_xck.getValue()))+point);
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else{
                teamService.changeTeamPoint_xck(dict_xck.getLabel(), Float.parseFloat(dict_xck.getValue()));
                Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_xck();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
            }

        }
        if(dict_xck.getType2().equals("网络舆情")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper3);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum-Float.parseFloat(dict_xck.getValue())>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),point);
                } else if((floatSum-Float.parseFloat(dict_xck.getValue())<-0.5)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_xck.getValue())>=-0.5)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum-Float.parseFloat(dict_xck.getValue()))+point);
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else{
                teamService.changeTeamPoint_xck(dict_xck.getLabel(), Float.parseFloat(dict_xck.getValue()));
                Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_xck();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
            }

        }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_xck dict_xck = dict_xckService.selectById(id);
        model.addAttribute("dict_xck",dict_xck);
        return "admin/system/xck/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_xck dict_xck){
        EntityWrapper<Dict_xck> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type2","上报信息").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_xck> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type2","新闻报道").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_xck> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type2","网络舆情").eq("label",dict_xck.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(dict_xck.getId()==null || dict_xck.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_xck.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_xck.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_xck.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_xck.getSort() == null || dict_xck.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_xck oldDict_xck = dict_xckService.selectById(dict_xck.getId());
        if(!oldDict_xck.getType().equals(dict_xck.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_xck.getLabel().equals(dict_xck.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_xck.getValue().equals(dict_xck.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_xckService.saveOrUpdateDict(dict_xck);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_xck(dict_xck.getLabel(), Float.parseFloat(dict_xck.getValue()) - Float.parseFloat(oldDict_xck.getValue()));
        if(dict_xck.getType2().equals("上报信息")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper1);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_xck.getValue()) - Float.parseFloat(oldDict_xck.getValue());
            float point=(float)-1.1;
            if(floatSum<=-1.1){
                if(floatSum-differ>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),point);
                } else if((floatSum-differ<-1.1)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-1.1)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum-differ)+point);
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else if(floatSum>-1.1&&floatSum<0) {
                if ((floatSum - differ <= -1.1)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point+floatSum );
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }else if(floatSum - differ>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), differ);
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - differ > 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum - differ));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                }
                else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }

        }
        if(dict_xck.getType2().equals("新闻报道")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper2);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_xck.getValue()) - Float.parseFloat(oldDict_xck.getValue());
            float point=(float)-0.4;
            if(floatSum<=-0.4){
                if(floatSum-differ>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),point);
                } else if((floatSum-differ<-0.4)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-0.4)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum-differ)+point);
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else if(floatSum>-0.4&&floatSum<0) {
                if ((floatSum - differ <= -0.4)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point+floatSum );
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }else if(floatSum - differ>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), differ);
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - differ > 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum - differ));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                }
                else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }

        }
        if(dict_xck.getType2().equals("网络舆情")){
            List<Dict_xck> Dict_xck_List= dict_xckService.selectList(wrapper3);
            double sum=Dict_xck_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_xck.getValue()) - Float.parseFloat(oldDict_xck.getValue());
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum-differ>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),point);
                } else if((floatSum-differ<-0.5)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-0.5)){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum-differ)+point);
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }else if(floatSum>-0.5&&floatSum<0) {
                if ((floatSum - differ <= -0.5)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -point+floatSum );
                    Team team = teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_xck();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(), value);
                }else if(floatSum - differ>=0){
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), differ);
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_xckPointError(dict_xck.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - differ > 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 0)) {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), -(floatSum - differ));
                    Team team=teamService.queryTeamPoint_xck(dict_xck.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_xck();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                }
                else {
                    teamService.changeTeamPoint_xck(dict_xck.getLabel(), 0);
                }
            }

        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }

    //宣传科大分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_xck> platformJck_XckList(
                                                         @RequestParam ("label") String label,
                                                         @RequestParam(value = "page",defaultValue = "1")Integer page,
                                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                                         ServletRequest request){
        if(limit==null){
            return null;
        };
        LayerData<Dict_xck> layerData = new LayerData<>();
        EntityWrapper<Dict_xck> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0wrapper.eq("label", label);
        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_xck> dataPage = dict_xckService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

}
