package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.Dict_dsjk;
import com.mysiteforme.admin.entity.Dict_ggb;
import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.entity.Team_dcs;
import com.mysiteforme.admin.service.TeamDcsService;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/19.
 * todo:
 * 督察室controller
 */
@Controller
@RequestMapping("admin/system/dsjk")
public class DSJKController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        float a=2;
        float b=1;
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_dsjk dict_dsjk = dict_dsjkService.selectById(id);
        Integer number = dict_dsjk.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_dsjk.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_dsjk.setValue(String.valueOf(newValue));
        dict_dsjk.setNumber(number+1);
        teamDcsService.changeTeamPoint_dsjk(dict_dsjk.getLabel(), newValue.subtract(oldValue).floatValue());
        updateTeamPoint(dict_dsjk.getLabel(),error);
        dict_dsjkService.saveOrUpdateDict(dict_dsjk);
        List<Team_dcs> dcs_pointList=teamDcsService.queryPoint_dcs();
        for(int i=0;i<dcs_pointList.size();i++){
            Team_dcs teamDcs=dcs_pointList.get(i);
            if(i>=1) {
                Team_dcs teamDcs1 = dcs_pointList.get(i - 1);
                if (!teamDcs1.getPointDcs().equals(teamDcs.getPointDcs())) {
                    float point = (float) (a - (b * 0.1));
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), point);
                    b=b+1;
                }else if (teamDcs.getPointDcs().equals(teamDcs1.getPointDcs())) {
                    Team team1 = teamService.queryTeamPoint_dsjk(teamDcs1.getTeamName());
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), team1.getPoint_dsjk());
                }
            }
            if(i==0){
                teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), 2);
            }
        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        float a=2;
        float b=1;
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_dsjk dict_dsjk = dict_dsjkService.selectById(id);
        Integer number = dict_dsjk.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_dsjk.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_dsjk.setValue(String.valueOf(newValue));
        dict_dsjk.setNumber(number-1);
        teamDcsService.changeTeamPoint_dsjk(dict_dsjk.getLabel(), newValue.subtract(oldValue).floatValue());
        updateTeamPoint(dict_dsjk.getLabel(),error);
        dict_dsjkService.saveOrUpdateDict(dict_dsjk);
        List<Team_dcs> dcs_pointList=teamDcsService.queryPoint_dcs();
        for(int i=0;i<dcs_pointList.size();i++){
            Team_dcs teamDcs=dcs_pointList.get(i);
            if(i>=1) {
                Team_dcs teamDcs1 = dcs_pointList.get(i - 1);
                if (!teamDcs1.getPointDcs().equals(teamDcs.getPointDcs())) {
                    float point = (float) (a - (b * 0.1));
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), point);
                    b=b+1;
                }else if (teamDcs.getPointDcs().equals(teamDcs1.getPointDcs())) {
                    Team team1 = teamService.queryTeamPoint_dsjk(teamDcs1.getTeamName());
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), team1.getPoint_dsjk());
                }
            }
            if(i==0){
                teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), 2);
            }
        }
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        float a=2;
        float b=1;
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_dsjk dict_dsjk = dict_dsjkService.selectById(id);
        teamDcsService.changeTeamPoint_dsjk(dict_dsjk.getLabel(), -Float.parseFloat(dict_dsjk.getValue()));
        updateTeamPoint(dict_dsjk.getLabel(),error);
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_dsjkService.deleteDict(id);
        List<Team_dcs> dcs_pointList=teamDcsService.queryPoint_dcs();
        for(int i=0;i<dcs_pointList.size();i++){
            Team_dcs teamDcs=dcs_pointList.get(i);
            if(i>=1) {
                Team_dcs teamDcs1 = dcs_pointList.get(i - 1);
                if (!teamDcs1.getPointDcs().equals(teamDcs.getPointDcs())) {
                    float point = (float) (a - (b * 0.1));
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), point);
                    b=b+1;
                }else if (teamDcs.getPointDcs().equals(teamDcs1.getPointDcs())) {
                    Team team1 = teamService.queryTeamPoint_dsjk(teamDcs1.getTeamName());
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), team1.getPoint_dsjk());
                }
            }
            if(i==0){
                teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), 2);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/dsjk/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_dsjk> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_dsjk> layerData = new LayerData<>();
        EntityWrapper<Dict_dsjk> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_dsjk> dataPage = dict_dsjkService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_dsjk> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                     @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                     ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_dsjk> layerData = new LayerData<>();
        EntityWrapper<Dict_dsjk> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_dsjk> dataPage = dict_dsjkService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dsjk/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dsjk/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dsjk/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_dsjk dict_dsjk){
        float a=2;
        float b=1;
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict_dsjk.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_dsjk.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_dsjkService.getCountByType(dict_dsjk.getType())==0){
            dict_dsjk.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_dsjk.setSort(dict_dsjkService.getMaxSortByType(dict_dsjk.getType()));
        }
        dict_dsjkService.saveOrUpdateDict(dict_dsjk);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
        teamDcsService.changeTeamPoint_dsjk(dict_dsjk.getLabel(), Float.parseFloat(dict_dsjk.getValue()));
        updateTeamPoint(dict_dsjk.getLabel(),error);
        List<Team_dcs> dcs_pointList=teamDcsService.queryPoint_dcs();
        for(int i=0;i<dcs_pointList.size();i++){
            Team_dcs teamDcs=dcs_pointList.get(i);
            if(i>=1) {
                Team_dcs teamDcs1 = dcs_pointList.get(i - 1);
                if (!teamDcs1.getPointDcs().equals(teamDcs.getPointDcs())) {
                    float point = (float) (a - (b * 0.1));
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), point);
                    b=b+1;
                }else if (teamDcs.getPointDcs().equals(teamDcs1.getPointDcs())) {
                    Team team1 = teamService.queryTeamPoint_dsjk(teamDcs1.getTeamName());
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), team1.getPoint_dsjk());
                }
            }
            if(i==0){
                teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), 2);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_dsjk dict_dsjk = dict_dsjkService.selectById(id);
        model.addAttribute("dict_dsjk",dict_dsjk);
        return "admin/system/dsjk/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_dsjk dict_dsjk){
        float a=2;
        float b=1;
        //指定的误差范围
        double error=1E-6;
        if(dict_dsjk.getId()==null || dict_dsjk.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_dsjk.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_dsjk.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_dsjk.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_dsjk.getSort() == null || dict_dsjk.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_dsjk oldDict_dsjk = dict_dsjkService.selectById(dict_dsjk.getId());
        if(!oldDict_dsjk.getType().equals(dict_dsjk.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_dsjk.getLabel().equals(dict_dsjk.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_dsjk.getValue().equals(dict_dsjk.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_dsjkService.saveOrUpdateDict(dict_dsjk);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
        teamDcsService.changeTeamPoint_dsjk(dict_dsjk.getLabel(), Float.parseFloat(dict_dsjk.getValue()) - Float.parseFloat(oldDict_dsjk.getValue()));
        updateTeamPoint(dict_dsjk.getLabel(),error);
        List<Team_dcs> dcs_pointList=teamDcsService.queryPoint_dcs();
        for(int i=0;i<dcs_pointList.size();i++){
            Team_dcs teamDcs=dcs_pointList.get(i);
            if(i>=1) {
                Team_dcs teamDcs1 = dcs_pointList.get(i - 1);
                if (!teamDcs1.getPointDcs().equals(teamDcs.getPointDcs())) {
                    float point = (float) (a - (b * 0.1));
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), point);
                    b=b+1;
                }else if (teamDcs.getPointDcs().equals(teamDcs1.getPointDcs())) {
                    Team team1 = teamService.queryTeamPoint_dsjk(teamDcs1.getTeamName());
                    teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), team1.getPoint_dsjk());
                }
            }
            if(i==0){
                teamService.assignTeamPoint_dsjk(teamDcs.getTeamName(), 2);
            }
        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }

    //大数据科分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_dsjk> platformDsjkList(@RequestParam ("label") String label,
                                               @RequestParam(value = "page",defaultValue = "1")Integer page,
                                               @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                               ServletRequest request){
        if(limit==null){
            return null;
        };
        LayerData<Dict_dsjk> layerData = new LayerData<>();
        EntityWrapper<Dict_dsjk> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);

        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0

        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_dsjk> dataPage = dict_dsjkService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
    //消除浮点型数做加减造成的误差
    private void updateTeamPoint(String label, double error) {
        Team team = teamService.queryTeamPoint_dsjk(label);
        double value = team.getPoint_dcs();
        if (Math.abs(value) < error) {
            value = 0.0;
        }
        teamService.fixdict_dsjkPointError(label, value);
    }

}
