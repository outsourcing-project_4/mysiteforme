package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.*;
import com.mysiteforme.admin.entity.Dict_zgk;
import com.mysiteforme.admin.entity.Dict_zgk;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/19.
 * todo:
 * 政工科controller
 */
@Controller
@RequestMapping("admin/system/zgk")
public class ZGKController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_zgk> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_zgk> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_zgk> wrapper3 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_zgk dict_zgk = dict_zgkService.selectById(id);
        wrapper1.eq("type2","学习强国学习考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1).or().eq("type2","灯塔e 支部").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type2","执纪问责考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type2","协勤员管理考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        Integer number = dict_zgk.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_zgk.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_zgk.setValue(String.valueOf(newValue));
        dict_zgk.setNumber(number+1);
//        teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_zgkService.saveOrUpdateDict(dict_zgk);
        if(dict_zgk.getType2().equals("学习强国学习考核")||dict_zgk.getType2().equals("灯塔e 支部")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper1);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-1){
                if((floatSum-newValue.subtract(oldValue).floatValue()<-1)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度的情况
                }
                if ((floatSum - newValue.subtract(oldValue).floatValue() > -1)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())-1);
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }
            }else if(floatSum>-1&&floatSum<0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -1)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 1 + floatSum);
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }
            else{
                teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
                Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zgk();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
            }
        }
        if(dict_zgk.getType2().equals("执纪问责考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper2);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if((floatSum-newValue.subtract(oldValue).floatValue()<-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度的情况
                }
                if ((floatSum - newValue.subtract(oldValue).floatValue() > -0.5)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())+point);
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }
            }else if(floatSum>-0.5&&floatSum<0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -0.5)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -point + floatSum);
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }
            else{
                teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
                Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zgk();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
            }
        }
        if(dict_zgk.getType2().equals("协勤员管理考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper3);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if((floatSum-newValue.subtract(oldValue).floatValue()<-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度的情况
                }
                if ((floatSum - newValue.subtract(oldValue).floatValue() > -0.5)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())+point);
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }
            }else if(floatSum>-0.5&&floatSum<0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -0.5)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -point + floatSum);
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }
            else{
                teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
                Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zgk();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
            }
        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_zgk> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_zgk> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_zgk> wrapper3 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_zgk dict_zgk = dict_zgkService.selectById(id);
        wrapper1.eq("type2","学习强国学习考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1).or().eq("type2","灯塔e 支部").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type2","执纪问责考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type2","协勤员管理考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        Integer number = dict_zgk.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_zgk.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_zgk.setValue(String.valueOf(newValue));
        dict_zgk.setNumber(number-1);
//        teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_zgkService.saveOrUpdateDict(dict_zgk);
        if(dict_zgk.getType2().equals("学习强国学习考核")||dict_zgk.getType2().equals("灯塔e 支部")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper1);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            if(floatSum<=-1){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-1){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-1){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 1+(floatSum+newValue.subtract(oldValue).floatValue()));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else if(floatSum>-1&&floatSum<0){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-1){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),-1);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                } else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }

            }

        }
        if(dict_zgk.getType2().equals("执纪问责考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper2);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -point+(floatSum+newValue.subtract(oldValue).floatValue()));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else if(floatSum>-0.5&&floatSum<0){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),point);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                } else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }

            }

        }
        if(dict_zgk.getType2().equals("协勤员管理考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper3);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -point+(floatSum+newValue.subtract(oldValue).floatValue()));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else if(floatSum>-0.5&&floatSum<0){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), newValue.subtract(oldValue).floatValue());
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),point);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                } else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }

            }

        }
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_zgk> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_zgk> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_zgk> wrapper3 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_zgk dict_zgk = dict_zgkService.selectById(id);
        wrapper1.eq("type2","学习强国学习考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1).or().eq("type2","灯塔e 支部").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type2","执纪问责考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type2","协勤员管理考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -Float.parseFloat(dict_zgk.getValue()));
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_zgkService.deleteDict(id);
        if(dict_zgk.getType2().equals("学习强国学习考核")||dict_zgk.getType2().equals("灯塔e 支部")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper1);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_zgk.getValue());
            if(floatSum<=-1){
                if(floatSum-Float.parseFloat(dict_zgk.getValue())>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),1);
                }else if(floatSum-Float.parseFloat(dict_zgk.getValue())<=-1){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_zgk.getValue())>=-1){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 1+(floatSum-Float.parseFloat(dict_zgk.getValue())));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else if(floatSum>-1&&floatSum<0){
                if(floatSum-Float.parseFloat(dict_zgk.getValue())<=-1){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(1+floatSum));
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }else if(floatSum-Float.parseFloat(dict_zgk.getValue())>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -Float.parseFloat(dict_zgk.getValue()));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }else {
                teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -Float.parseFloat(dict_zgk.getValue()));
                Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zgk();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
            }
        }
        if(dict_zgk.getType2().equals("执纪问责考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper2);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_zgk.getValue());
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum-Float.parseFloat(dict_zgk.getValue())>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),-point);
                }else if(floatSum-Float.parseFloat(dict_zgk.getValue())<=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_zgk.getValue())>=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -point+(floatSum-Float.parseFloat(dict_zgk.getValue())));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else if(floatSum>-0.5&&floatSum<0){
                if(floatSum-Float.parseFloat(dict_zgk.getValue())<=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(-point+floatSum));
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }else if(floatSum-Float.parseFloat(dict_zgk.getValue())>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -Float.parseFloat(dict_zgk.getValue()));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }else {
                teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -Float.parseFloat(dict_zgk.getValue()));
                Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zgk();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
            }
        }
        if(dict_zgk.getType2().equals("协勤员管理考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper3);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_zgk.getValue());
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum-Float.parseFloat(dict_zgk.getValue())>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),point);
                }else if(floatSum-Float.parseFloat(dict_zgk.getValue())<=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_zgk.getValue())>=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -point+(floatSum-Float.parseFloat(dict_zgk.getValue())));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                } else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else if(floatSum>-0.5&&floatSum<0){
                if(floatSum-Float.parseFloat(dict_zgk.getValue())<=-0.5){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(-point+floatSum));
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }else if(floatSum-Float.parseFloat(dict_zgk.getValue())>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -Float.parseFloat(dict_zgk.getValue()));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }else {
                teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -Float.parseFloat(dict_zgk.getValue()));
                Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zgk();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/zgk/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_zgk> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_zgk> layerData = new LayerData<>();
        EntityWrapper<Dict_zgk> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_zgk> dataPage = dict_zgkService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_zgk> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_zgk> layerData = new LayerData<>();
        EntityWrapper<Dict_zgk> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_zgk> dataPage = dict_zgkService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/zgk/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/zgk/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/zgk/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_zgk dict_zgk){
        EntityWrapper<Dict_zgk> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type2","学习强国学习考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1).or().eq("type2","灯塔e 支部").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_zgk> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type2","执纪问责考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_zgk> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type2","协勤员管理考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict_zgk.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_zgk.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_zgkService.getCountByType(dict_zgk.getType())==0){
            dict_zgk.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_zgk.setSort(dict_zgkService.getMaxSortByType(dict_zgk.getType()));
        }
        dict_zgkService.saveOrUpdateDict(dict_zgk);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), Float.parseFloat(dict_zgk.getValue()));
        if(dict_zgk.getType2().equals("学习强国学习考核")||dict_zgk.getType2().equals("灯塔e 支部")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper1);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-1){
                if(floatSum-Float.parseFloat(dict_zgk.getValue())>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),-1);
                } else if((floatSum-Float.parseFloat(dict_zgk.getValue())<-1)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_zgk.getValue())>=-1)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum-Float.parseFloat(dict_zgk.getValue()))-1);
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else{
                teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), Float.parseFloat(dict_zgk.getValue()));
                Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zgk();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
            }

        }
        if(dict_zgk.getType2().equals("执纪问责考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper2);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum-Float.parseFloat(dict_zgk.getValue())>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),point);
                } else if((floatSum-Float.parseFloat(dict_zgk.getValue())<-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_zgk.getValue())>=-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum-Float.parseFloat(dict_zgk.getValue()))+point);
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else{
                teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), Float.parseFloat(dict_zgk.getValue()));
                Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zgk();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
            }

        }
        if(dict_zgk.getType2().equals("协勤员管理考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper3);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum-Float.parseFloat(dict_zgk.getValue())>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),point);
                } else if((floatSum-Float.parseFloat(dict_zgk.getValue())<-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_zgk.getValue())>=-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum-Float.parseFloat(dict_zgk.getValue()))+point);
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else{
                teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), Float.parseFloat(dict_zgk.getValue()));
                Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zgk();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
            }

        }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_zgk dict_zgk = dict_zgkService.selectById(id);
        model.addAttribute("dict_zgk",dict_zgk);
        return "admin/system/zgk/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_zgk dict_zgk){
        EntityWrapper<Dict_zgk> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type2","学习强国学习考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1).or().eq("type2","灯塔e 支部").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_zgk> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type2","执纪问责考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_zgk> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type2","协勤员管理考核").eq("label",dict_zgk.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(dict_zgk.getId()==null || dict_zgk.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_zgk.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_zgk.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_zgk.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_zgk.getSort() == null || dict_zgk.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_zgk oldDict_zgk = dict_zgkService.selectById(dict_zgk.getId());
        if(!oldDict_zgk.getType().equals(dict_zgk.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_zgk.getLabel().equals(dict_zgk.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_zgk.getValue().equals(dict_zgk.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_zgkService.saveOrUpdateDict(dict_zgk);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), Float.parseFloat(dict_zgk.getValue()) - Float.parseFloat(oldDict_zgk.getValue()));
        if(dict_zgk.getType2().equals("学习强国学习考核")||dict_zgk.getType2().equals("灯塔e 支部")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper1);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_zgk.getValue()) - Float.parseFloat(oldDict_zgk.getValue());
//            float point=(float)1.4;
            if(floatSum<=-1){
                if(floatSum-differ>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),-1);
                } else if((floatSum-differ<-1)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-1)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum-differ)-1);
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else if(floatSum>-1&&floatSum<0) {
                if ((floatSum - differ <= -1)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 1+floatSum );
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }else if(floatSum - differ>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), differ);
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - differ > 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum - differ));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                }
                else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }
        }
        if(dict_zgk.getType2().equals("执纪问责考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper2);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_zgk.getValue()) - Float.parseFloat(oldDict_zgk.getValue());
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum-differ>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),point);
                } else if((floatSum-differ<-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum-differ)+point);
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else if(floatSum>-0.5&&floatSum<0) {
                if ((floatSum - differ <= -0.5)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -point+floatSum );
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }else if(floatSum - differ>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), differ);
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - differ > 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum - differ));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                }
                else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }
        }
        if(dict_zgk.getType2().equals("协勤员管理考核")){
            List<Dict_zgk> Dict_zgk_List= dict_zgkService.selectList(wrapper3);
            double sum=Dict_zgk_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_zgk.getValue()) - Float.parseFloat(oldDict_zgk.getValue());
            float point=(float)-0.5;
            if(floatSum<=-0.5){
                if(floatSum-differ>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),point);
                } else if((floatSum-differ<-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-0.5)){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum-differ)+point);
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }else if(floatSum>-0.5&&floatSum<0) {
                if ((floatSum - differ <= -0.5)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -point+floatSum );
                    Team team = teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value = team.getPoint_zgk();
                    if (Math.abs(value) < error) {
                        value = 0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(), value);
                }else if(floatSum - differ>=0){
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), floatSum);

                }
                else{
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), differ);
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                    teamService.fixdict_zgkPointError(dict_zgk.getLabel(),value);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - differ > 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 0)) {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), -(floatSum - differ));
                    Team team=teamService.queryTeamPoint_zgk(dict_zgk.getLabel());

                    //消除删除条例分数归零浮点型产生的误差
                    double value=team.getPoint_zgk();
                    if(Math.abs(value)<error){
                        value=0.0;
                    }
                }
                else {
                    teamService.changeTeamPoint_zgk(dict_zgk.getLabel(), 0);
                }
            }
        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }


    //政工科大分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_zgk> platformJck_ZgkList(@RequestParam ("label") String label,
                                                   @RequestParam(value = "page",defaultValue = "1")Integer page,
                                                   @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                                   ServletRequest request){
        if(limit==null){
            return null;
        };
        LayerData<Dict_zgk> layerData = new LayerData<>();
        EntityWrapper<Dict_zgk> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0

        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_zgk> dataPage = dict_zgkService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
}
