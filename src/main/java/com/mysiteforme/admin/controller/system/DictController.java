package com.mysiteforme.admin.controller.system;

import
        com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.*;
import com.mysiteforme.admin.entity.Dict;
import com.mysiteforme.admin.entity.Dict;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by wangl on 2018/1/1.
 * todo:
 */
@Controller
@RequestMapping("admin/system/dict")
public class DictController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper4 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper5 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper6 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper7 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper8 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper9 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict dict = dictService.selectById(id);
        wrapper1.eq("type3","存量违建治理4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper5.eq("type3","达到“无违建”创建标准并验收通过的加分项最多1.4分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper6.eq("type3","专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper7.eq("type3","一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper8.eq("type3","对于提报已完成的项目检查发现情况总分1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper9.eq("type3","对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        Integer number = dict.getNumber();
        BigDecimal oldValue = new BigDecimal(dict.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict.setValue(String.valueOf(newValue));
        dict.setNumber(number+1);
//        teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
        dictService.saveOrUpdateDict(dict);
        //当增加条数的条例评分内容为“存量违建治理4.2分”时，查询所用评分内容为“存量违建治理4.2分”的条例总分是否低于-4.2分。当低于-4.2分时对分数进行限制，低于-4.2分的条例增加都不会导致分数减少
        if(dict.getType3().equals("存量违建治理4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper1);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)(-4.2);
            float floatSum=(float)sum;
            if(floatSum<-4.2){
                //帮下一个判定条件过滤分数在-4.2到-4.28分变化的情况
                if((floatSum+4.2)>-0.28&&(floatSum-newValue.subtract(oldValue).floatValue())<-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-4.2分跨度到大于-4.2分等情况
                }else if((floatSum+4.2)>-0.28||(floatSum-newValue.subtract(oldValue).floatValue())>-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分”时，
        // 查询所用评分内容为“未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分”的条例总分是否低于-2.8分。当低于-2.8分时对分数进行限制，低于-2.8分的条例增加都不会导致分数减少
        if(dict.getType3().equals("未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper2);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)(-2.8);
            float floatSum=(float)sum;
            if(floatSum<-2.8){
                //帮下一个判定条件过滤分数在-2.8到-4.2分变化的情况
                if((floatSum+2.8)>-1.4&&(floatSum-newValue.subtract(oldValue).floatValue())<-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-2.8分跨度到大于-2.8分等情况
                }else if((floatSum+2.8)>-1.4||(floatSum-newValue.subtract(oldValue).floatValue())>-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分”时，查询所用评分内容为“新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分”的条例总分是否低于-4.2分。当低于-4.2分时对分数进行限制，低于-4.2分的条例增加都不会导致分数减少
        if(dict.getType3().equals("新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper3);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)(-4.2);
            float floatSum=(float)sum;
            if(floatSum<-4.2){
                //帮下一个判定条件过滤分数在-4.2到-5.6分变化的情况
                if((floatSum+4.2)>-1.4&&(floatSum-newValue.subtract(oldValue).floatValue())<-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-4.2分跨度到大于-4.2分等情况
                }else if((floatSum+4.2)>-1.4||(floatSum-newValue.subtract(oldValue).floatValue())>-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分”时，
        // 查询所用评分内容为“城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分”的条例总分是否低于-2.8分。当低于-2.8分时对分数进行限制，低于-2.8分的条例增加都不会导致分数减少
        if(dict.getType3().equals("城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper4);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)(-2.8);
            float floatSum=(float)sum;
            if(floatSum<-2.8){
                //帮下一个判定条件过滤分数在-2.8到-3.08分变化的情况
                if((floatSum+2.8)>-0.28&&(floatSum-newValue.subtract(oldValue).floatValue())<-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-2.8分跨度到大于-2.8分等情况
                }else if((floatSum+2.8)>-0.28||(floatSum-newValue.subtract(oldValue).floatValue())>-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“达到“无违建”创建标准并验收通过的加分项最多1.4分”时，查询所用评分内容为“达到“无违建”创建标准并验收通过的加分项最多1.4分”的条例总分是否高于1.4分。当超过1.4分时对分数进行限制，超过1.4分的条例增加都不会导致分数增加
        if(dict.getType3().equals("达到“无违建”创建标准并验收通过的加分项最多1.4分")){
            List<Dict> Dict_List= dictService.selectList(wrapper5);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)1.4;
            if(floatSum>1.4){
                //帮下一个判定条件过滤分数在1.4到2.1分变化的情况
                if((floatSum-1.4)<0.7&&(floatSum-newValue.subtract(oldValue).floatValue())>1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从1.*跨度到1.4*等情况
                }else if((floatSum-1.4)<0.7||(floatSum-newValue.subtract(oldValue).floatValue())<1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分”时，
        // 查询所用评分内容为“专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分”的条例总分是否低于-1.2分。当低于-1.2分时对分数进行限制，低于-1.2分的条例增加都不会导致分数减少
        if(dict.getType3().equals("专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper6);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)(-1.2);
            float floatSum=(float)sum;
            if(floatSum<-1.2){
                //帮下一个判定条件过滤分数在-1.2到-1.56分变化的情况
                if((floatSum+1.2)>-0.36&&(floatSum-newValue.subtract(oldValue).floatValue())<-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-1.2分跨度到小于-1.2分等情况
                }else if((floatSum+1.2)>-0.36||(floatSum-newValue.subtract(oldValue).floatValue())>-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分”时，
        // 查询所用评分内容为“一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分”的条例总分是否低于-1.8分。当低于-1.8分时对分数进行限制，低于-1.8分的条例增加都不会导致分数减少
        if(dict.getType3().equals("一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper7);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)(-1.8);
            float floatSum=(float)sum;
            if(floatSum<-1.8){
                //帮下一个判定条件过滤分数在-1.8到-2.1分变化的情况
                if((floatSum+1.8)>-0.3&&(floatSum-newValue.subtract(oldValue).floatValue())<-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-1.8分跨度到小于-1.8分等情况
                }else if((floatSum+1.8)>-0.3||(floatSum-newValue.subtract(oldValue).floatValue())>-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“对于提报已完成的项目检查发现情况总分1.8分”时，
        // 查询所用评分内容为“对于提报已完成的项目检查发现情况总分1.8分”的条例总分是否低于-1.8分。当低于-1.8分时对分数进行限制，低于-1.8分的条例增加都不会导致分数减少
        if(dict.getType3().equals("对于提报已完成的项目检查发现情况总分1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper8);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)(-1.8);
            float floatSum=(float)sum;
            if(floatSum<-1.8){
                //帮下一个判定条件过滤分数在-1.8到-2.1分变化的情况
                if((floatSum+1.8)>-0.3&&(floatSum-newValue.subtract(oldValue).floatValue())<-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-1.8分跨度到小于-1.8分等情况
                }else if((floatSum+1.8)>-0.3||(floatSum-newValue.subtract(oldValue).floatValue())>-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分”时，
        // 查询所用评分内容为“对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分”的条例总分是否低于-1.2分。当低于-1.2分时对分数进行限制，低于-1.2分的条例增加都不会导致分数减少
        if(dict.getType3().equals("对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper9);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)(-1.2);
            float floatSum=(float)sum;
            if(floatSum<-1.2){
                //帮下一个判定条件过滤分数在-1.2到-1.8分变化的情况
                if((floatSum+1.2)>-0.6&&(floatSum-newValue.subtract(oldValue).floatValue())<-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-1.2分跨度到小于-1.2分等情况
                }else if((floatSum+1.2)>-0.6||(floatSum-newValue.subtract(oldValue).floatValue())>-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper4 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper5 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper6 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper7 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper8 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper9 = new EntityWrapper<>();
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict dict = dictService.selectById(id);
        wrapper1.eq("type3","存量违建治理4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper5.eq("type3","达到“无违建”创建标准并验收通过的加分项最多1.4分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper6.eq("type3","专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper7.eq("type3","一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper8.eq("type3","对于提报已完成的项目检查发现情况总分1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper9.eq("type3","对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        Integer number = dict.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict.setValue(String.valueOf(newValue));
        dict.setNumber(number-1);
//        teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
        //当减少条数的条例评分内容为“存量违建治理4.2分”时，查询所用评分内容为“存量违建治理4.2分”的条例总分是否低于-4.2分。当低于-4.2分时对分数进行限制，低于-4.2分的条例增加都不会导致分数减少
        if(dict.getType3().equals("存量违建治理4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper1);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-4.2);
            if(floatSum<-4.2){
                //帮下一个判定条件过滤分数在-4.2到-4.28分变化的情况
                if((floatSum+4.2)>-0.28&&(floatSum+newValue.subtract(oldValue).floatValue())<-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-4.2分跨度到小于-4.2分等情况
                }else if((floatSum+4.2)>-0.28||(floatSum+newValue.subtract(oldValue).floatValue())>-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分”时，
        // 查询所用评分内容为“未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分”的条例总分是否低于-2.8分。当低于-2.8分时对分数进行限制，低于-2.8分的条例增加都不会导致分数减少
        if(dict.getType3().equals("未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper2);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-2.8);
            if(floatSum<-2.8){
                //帮下一个判定条件过滤分数在-2.8到-4.2分变化的情况
                if((floatSum+2.8)>-1.4&&(floatSum+newValue.subtract(oldValue).floatValue())<-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-2.8分跨度到小于-2.8分等情况
                }else if((floatSum+2.8)>-1.4||(floatSum+newValue.subtract(oldValue).floatValue())>-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分”时，查询所用评分内容为“新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分”的条例总分是否低于-4.2分。当低于-4.2分时对分数进行限制，低于-4.2分的条例增加都不会导致分数减少
        if(dict.getType3().equals("新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper3);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-4.2);
            if(floatSum<-4.2){
                //帮下一个判定条件过滤分数在-4.2到-5.6分变化的情况
                if((floatSum+4.2)>-1.4&&(floatSum+newValue.subtract(oldValue).floatValue())<-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-4.2分跨度到小于-4.2分等情况
                }else if((floatSum+4.2)>-1.4||(floatSum+newValue.subtract(oldValue).floatValue())>-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分”时，
        // 查询所用评分内容为“城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分”的条例总分是否低于-2.8分。当低于-2.8分时对分数进行限制，低于-2.8分的条例增加都不会导致分数减少
        if(dict.getType3().equals("城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper4);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-2.8);
            if(floatSum<-2.8){
                //帮下一个判定条件过滤分数在-2.8到-3.08分变化的情况
                if((floatSum+2.8)>-0.28&&(floatSum+newValue.subtract(oldValue).floatValue())<-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-2.8分跨度到小于-2.8分等情况
                }else if((floatSum+2.8)>-0.28||(floatSum+newValue.subtract(oldValue).floatValue())>-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“达到“无违建”创建标准并验收通过的加分项最多1.4分”时，查询所用评分内容为“达到“无违建”创建标准并验收通过的加分项最多1.4分”的条例总分是否高于1.4分。当超过1.4分时对分数进行限制，超过1.4分的条例增加都不会导致分数增加
        if(dict.getType3().equals("达到“无违建”创建标准并验收通过的加分项最多1.4分")){
            List<Dict> Dict_List= dictService.selectList(wrapper5);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)1.4;
            if(floatSum>1.4){
                //帮下一个判定条件过滤分数在1.4到2.1分变化的情况
                if((floatSum-1.4)<0.7&&(floatSum+newValue.subtract(oldValue).floatValue())>1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从1.*跨度到1.4*等情况
                }else if((floatSum-1.4)<0.7||(floatSum+newValue.subtract(oldValue).floatValue())<1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分”时，
        // 查询所用评分内容为“专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分”的条例总分是否低于-1.2分。当低于-1.2分时对分数进行限制，低于-1.2分的条例增加都不会导致分数减少
        if(dict.getType3().equals("专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper6);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-1.2);
            if(floatSum<-1.2){
                //帮下一个判定条件过滤分数在-1.2到-1.56分变化的情况
                if((floatSum+1.2)>-0.36&&(floatSum+newValue.subtract(oldValue).floatValue())<-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-1.2分跨度到大于-1.2分等情况
                }else if((floatSum+1.2)>-0.36||(floatSum+newValue.subtract(oldValue).floatValue())>-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分”时，
        // 查询所用评分内容为“一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分”的条例总分是否低于-1.8分。当低于-1.8分时对分数进行限制，低于-1.8分的条例增加都不会导致分数减少
        if(dict.getType3().equals("一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper7);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-1.8);
            if(floatSum<-1.8){
                //帮下一个判定条件过滤分数在-1.8到-2.1分变化的情况
                if((floatSum+1.8)>-0.3&&(floatSum+newValue.subtract(oldValue).floatValue())<-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-1.8分跨度到大于-1.8分等情况
                }else if((floatSum+1.8)>-0.3||(floatSum+newValue.subtract(oldValue).floatValue())>-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“对于提报已完成的项目检查发现情况总分1.8分”时，
        // 查询所用评分内容为“对于提报已完成的项目检查发现情况总分1.8分”的条例总分是否低于-1.8分。当低于-1.8分时对分数进行限制，低于-1.8分的条例增加都不会导致分数减少
        if(dict.getType3().equals("对于提报已完成的项目检查发现情况总分1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper8);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-1.8);
            if(floatSum<-1.8){
                //帮下一个判定条件过滤分数在-1.8到-2.1分变化的情况
                if((floatSum+1.8)>-0.3&&(floatSum+newValue.subtract(oldValue).floatValue())<-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-1.8分跨度到大于-1.8分等情况
                }else if((floatSum+1.8)>-0.3||(floatSum+newValue.subtract(oldValue).floatValue())>-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分”时，
        // 查询所用评分内容为“对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分”的条例总分是否低于-1.2分。当低于-1.2分时对分数进行限制，低于-1.2分的条例增加都不会导致分数减少
        if(dict.getType3().equals("对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper9);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-1.2);
            if(floatSum<-1.2){
                //帮下一个判定条件过滤分数在-1.2到-1.8分变化的情况
                if((floatSum+1.2)>-0.6&&(floatSum+newValue.subtract(oldValue).floatValue())<-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-1.2分跨度到大于-1.2分等情况
                }else if((floatSum+1.2)>-0.6||(floatSum+newValue.subtract(oldValue).floatValue())>-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        dictService.saveOrUpdateDict(dict);
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper4 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper5 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper6 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper7 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper8 = new EntityWrapper<>();
        EntityWrapper<Dict> wrapper9 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict dict = dictService.selectById(id);
        wrapper1.eq("type3","存量违建治理4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper5.eq("type3","达到“无违建”创建标准并验收通过的加分项最多1.4分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper6.eq("type3","专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper7.eq("type3","一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper8.eq("type3","对于提报已完成的项目检查发现情况总分1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        wrapper9.eq("type3","对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dictService.deleteDict(id);
        if(dict.getType3().equals("存量违建治理4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper1);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)4.2;
            float floatSum=(float)sum+Float.parseFloat(dict.getValue());
            if(floatSum<-4.2){
                //帮下一个判定条件过滤分数在-4.2到-4.28分变化的情况
                if((floatSum+4.2)>-0.28&&floatSum-Float.parseFloat(dict.getValue())<-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-4.2分跨度到小于-4.2分等情况
                }else if(((floatSum+4.2)>-0.28||Float.parseFloat(dict.getValue())<-0.28)&&(floatSum-Float.parseFloat(dict.getValue()))>-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), floatSum-Float.parseFloat(dict.getValue())+point);
                } else{
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
            }
        }
        if(dict.getType3().equals("未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper2);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)2.8;
            float floatSum=(float)sum+Float.parseFloat(dict.getValue());
            if(floatSum<-2.8){
                //帮下一个判定条件过滤分数在-2.8到-4.2分变化的情况
                if((floatSum+2.8)>-1.4&&floatSum-Float.parseFloat(dict.getValue())<-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-2.8分跨度到小于-2.8分等情况
                }else if(((floatSum+2.8)>-1.4||Float.parseFloat(dict.getValue())<-1.4)&&(floatSum-Float.parseFloat(dict.getValue()))>-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), floatSum-Float.parseFloat(dict.getValue())+point);
                } else{
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
            }
        }
        if(dict.getType3().equals("新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper3);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)4.2;
            float floatSum=(float)sum+Float.parseFloat(dict.getValue());
            if(floatSum<-4.2){
                //帮下一个判定条件过滤分数在-4.2到-5.6分变化的情况
                if((floatSum+4.2)>-1.4&&floatSum-Float.parseFloat(dict.getValue())<-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-4.2分跨度到小于-4.2分等情况
                }else if(((floatSum+4.2)>-1.4||Float.parseFloat(dict.getValue())<-1.4)&&(floatSum-Float.parseFloat(dict.getValue()))>-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), floatSum-Float.parseFloat(dict.getValue())+point);
                } else{
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
            }
        }
        if(dict.getType3().equals("城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper4);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)2.8;
            float floatSum=(float)sum+Float.parseFloat(dict.getValue());
            if(floatSum<-2.8){
                //帮下一个判定条件过滤分数在-2.8到-3.08分变化的情况
                if((floatSum+2.8)>-0.28&&floatSum-Float.parseFloat(dict.getValue())<-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从大于-2.8分跨度到小于-2.8分等情况
                }else if(((floatSum+2.8)>-0.28||Float.parseFloat(dict.getValue())<-0.28)&&(floatSum-Float.parseFloat(dict.getValue()))>-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), floatSum-Float.parseFloat(dict.getValue())+point);
                } else{
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
            }
        }
        if(dict.getType3().equals("达到“无违建”创建标准并验收通过的加分项最多1.4分")){
            List<Dict> Dict_List= dictService.selectList(wrapper5);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict.getValue());
            float point=(float)1.4;
            if(floatSum>1.4){
                //帮下一个判定条件过滤分数在1.4到2.1分变化的情况
                if((floatSum-1.4)<0.7&&floatSum-Float.parseFloat(dict.getValue())>1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从1.*跨度到1.4*的情况
                }else if(((floatSum-1.4)<0.7||Float.parseFloat(dict.getValue())>0.7)&&(floatSum-Float.parseFloat(dict.getValue()))<1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), floatSum-Float.parseFloat(dict.getValue())-point);
                } else{
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
                //若扣分项为多次+1后的项，删除后有可能导致分数项小于零，此处进行归零
                Team team=teamService.queryTeamPoint_zwb(dict.getLabel());
//                if(team.getPoint()<0) {
//                    teamService.zeroTeamPoint(dict.getLabel());
//                }
                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zwb();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zwbPointError(dict.getLabel(),value);
            }
        }
        if(dict.getType3().equals("专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper6);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)1.2;
            float floatSum=(float)sum+Float.parseFloat(dict.getValue());
            if(floatSum<-1.2){
                //帮下一个判定条件过滤分数在-1.2到-1.56分变化的情况
                if((floatSum+1.2)>-0.36&&floatSum-Float.parseFloat(dict.getValue())<-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-1.2分跨度到大于-1.2分等情况
                }else if(((floatSum+1.2)>-0.36||Float.parseFloat(dict.getValue())<-0.36)&&(floatSum-Float.parseFloat(dict.getValue()))>-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), floatSum-Float.parseFloat(dict.getValue())+point);
                } else{
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
            }
        }
        if(dict.getType3().equals("一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper7);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)1.8;
            float floatSum=(float)sum+Float.parseFloat(dict.getValue());
            if(floatSum<-1.8){
                //帮下一个判定条件过滤分数在-1.8到-2.1分变化的情况
                if((floatSum+1.8)>-0.3&&floatSum-Float.parseFloat(dict.getValue())<-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-1.8分跨度到大于-1.8分等情况
                }else if(((floatSum+1.8)>-0.3||Float.parseFloat(dict.getValue())<-0.3)&&(floatSum-Float.parseFloat(dict.getValue()))>-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), floatSum-Float.parseFloat(dict.getValue())+point);
                } else{
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
            }
        }
        if(dict.getType3().equals("对于提报已完成的项目检查发现情况总分1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper8);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)1.8;
            float floatSum=(float)sum+Float.parseFloat(dict.getValue());
            if(floatSum<-1.8){
                //帮下一个判定条件过滤分数在-1.8到-2.1分变化的情况
                if((floatSum+1.8)>-0.3&&floatSum-Float.parseFloat(dict.getValue())<-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-1.8分跨度到大于-1.8分等情况
                }else if(((floatSum+1.8)>-0.3||Float.parseFloat(dict.getValue())<-0.3)&&(floatSum-Float.parseFloat(dict.getValue()))>-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), floatSum-Float.parseFloat(dict.getValue())+point);
                } else{
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
            }
        }
        if(dict.getType3().equals("对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper9);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)1.2;
            float floatSum=(float)sum+Float.parseFloat(dict.getValue());
            if(floatSum<-1.2){
                //帮下一个判定条件过滤分数在-1.2到-1.8分变化的情况
                if((floatSum+1.2)>-0.6&&floatSum-Float.parseFloat(dict.getValue())<-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从小于-1.2分跨度到大于-1.2分等情况
                }else if(((floatSum+1.2)>-0.6||Float.parseFloat(dict.getValue())<-0.6)&&(floatSum-Float.parseFloat(dict.getValue()))>-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), floatSum-Float.parseFloat(dict.getValue())+point);
                } else{
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -Float.parseFloat(dict.getValue()));
            }
        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/dict/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict> layerData = new LayerData<>();
        EntityWrapper<Dict> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict> dataPage = dictService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict> layerData = new LayerData<>();
        EntityWrapper<Dict> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict> dataPage = dictService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/zdlist";
    }

    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict dict){
        EntityWrapper<Dict> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","存量违建治理4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type3","未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type3","新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("type3","城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper5 = new EntityWrapper<>();
        wrapper5.eq("type3","达到“无违建”创建标准并验收通过的加分项最多1.4分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper6 = new EntityWrapper<>();
        wrapper6.eq("type3","专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper7 = new EntityWrapper<>();
        wrapper7.eq("type3","一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper8 = new EntityWrapper<>();
        wrapper8.eq("type3","对于提报已完成的项目检查发现情况总分1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper9 = new EntityWrapper<>();
        wrapper9.eq("type3","对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dictService.getCountByType(dict.getType())==0){
            dict.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict.setSort(dictService.getMaxSortByType(dict.getType()));
        }
        dictService.saveOrUpdateDict(dict);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
        //当评分内容为“存量违建治理4.2分”时，查询所用评分内容为“存量违建治理4.2分”的条例总分是否小于-4.2分。当低于-4.2分时对分数进行限制，低于-4.2分的条例增加都不会导致分数再降低
        if(dict.getType3().equals("存量违建治理4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper1);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)-4.2;
            float floatSum=(float)sum;
            if(floatSum<-4.2){
                //帮下一个判定条件过滤分数在-4.2到-4.48分变化的情况
                if((floatSum+4.2)>-0.28&&floatSum-Float.parseFloat(dict.getValue())<-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从-4.*跨度到大于-4.2分等情况
                }else if((floatSum+4.2)>-0.28){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-Float.parseFloat(dict.getValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
                Team team=teamService.queryTeamPoint_zwb(dict.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zwb();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zwbPointError(dict.getLabel(),value);
            }
        }
        //当评分内容为“未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分”时，
        // 查询所用评分内容为“未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分”的条例总分是否小于-2.8分。当低于-2.8分时对分数进行限制，低于-2.8分的条例增加都不会导致分数再降低
        if(dict.getType3().equals("未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper2);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)-2.8;
            float floatSum=(float)sum;
            if(floatSum<-2.8){
                //帮下一个判定条件过滤分数在-2.8到-4.2分变化的情况
                if((floatSum+2.8)>-1.4&&floatSum-Float.parseFloat(dict.getValue())<-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从-2.*跨度到大于-2.8分等情况
                }else if((floatSum+2.8)>-1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-Float.parseFloat(dict.getValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
                Team team=teamService.queryTeamPoint_zwb(dict.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zwb();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zwbPointError(dict.getLabel(),value);
            }
        }
        //当评分内容为“新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分”时，查询所用评分内容为“新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分”的条例总分是否小于-4.2分。当低于-4.2分时对分数进行限制，低于-4.2分的条例增加都不会导致分数再降低
        if(dict.getType3().equals("新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper3);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)-4.2;
            float floatSum=(float)sum;
            if(floatSum<-4.2){
                //帮下一个判定条件过滤分数在-4.2到-5.6分变化的情况
                if((floatSum+4.2)>-1.4&&floatSum-Float.parseFloat(dict.getValue())<-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从-4.*跨度到大于-4.2分等情况
                }else if((floatSum+4.2)>-1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-Float.parseFloat(dict.getValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
                Team team=teamService.queryTeamPoint_zwb(dict.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zwb();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zwbPointError(dict.getLabel(),value);
            }
        }
        //当评分内容为“城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分”时，
        // 查询所用评分内容为“城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分”的条例总分是否小于-2.8分。当低于-2.8分时对分数进行限制，低于-2.8分的条例增加都不会导致分数再降低
        if(dict.getType3().equals("城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper4);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)-2.8;
            float floatSum=(float)sum;
            if(floatSum<-2.8){
                //帮下一个判定条件过滤分数在-2.8到-3.08分变化的情况
                if((floatSum+2.8)>-0.28&&floatSum-Float.parseFloat(dict.getValue())<-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从-2.*跨度到大于-2.8分等情况
                }else if((floatSum+2.8)>-0.28){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-Float.parseFloat(dict.getValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
                Team team=teamService.queryTeamPoint_zwb(dict.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zwb();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zwbPointError(dict.getLabel(),value);
            }
        }
        //当评分内容为“达到“无违建”创建标准并验收通过的加分项最多1.4分”时，查询所用评分内容为“达到“无违建”创建标准并验收通过的加分项最多1.4分”的条例总分是否高于1.4分。当超过1.4分时对分数进行限制，超过1.4分的条例增加都不会导致分数增加
        if(dict.getType3().equals("达到“无违建”创建标准并验收通过的加分项最多1.4分")){
            List<Dict> Dict_List= dictService.selectList(wrapper5);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)1.4;
            if(floatSum>1.4){
                //帮下一个判定条件过滤分数在1.4到2.1分变化的情况
                if((floatSum-1.4)<0.7&&floatSum-Float.parseFloat(dict.getValue())>1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从1.*跨度到1.4*等情况
                }else if((floatSum-1.4)<0.7){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-Float.parseFloat(dict.getValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
            }
        }
        //当评分内容为“专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分”时，
        // 查询所用评分内容为“专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分”的条例总分是否小于-1.2分。当低于-1.2分时对分数进行限制，低于-1.2分的条例增加都不会导致分数再降低
        if(dict.getType3().equals("专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper6);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)-1.2;
            float floatSum=(float)sum;
            if(floatSum<-1.2){
                //帮下一个判定条件过滤分数在-1.2到-1.56分变化的情况
                if((floatSum+1.2)>-0.36&&floatSum-Float.parseFloat(dict.getValue())<-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从-1.*跨度到大于-1.2分等情况
                }else if((floatSum+1.2)>-0.36){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-Float.parseFloat(dict.getValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
                Team team=teamService.queryTeamPoint_zwb(dict.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zwb();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zwbPointError(dict.getLabel(),value);
            }
        }
        //当评分内容为“一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分”时，
        // 查询所用评分内容为“一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分”的条例总分是否小于-1.8分。当低于-1.8分时对分数进行限制，低于-1.8分的条例增加都不会导致分数再降低
        if(dict.getType3().equals("一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper7);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)-1.8;
            float floatSum=(float)sum;
            if(floatSum<-1.8){
                //帮下一个判定条件过滤分数在-1.8到-2.1分变化的情况
                if((floatSum+1.8)>-0.3&&floatSum-Float.parseFloat(dict.getValue())<-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从-1.*跨度到大于-1.8分等情况
                }else if((floatSum+1.8)>-0.3){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-Float.parseFloat(dict.getValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
                Team team=teamService.queryTeamPoint_zwb(dict.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zwb();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zwbPointError(dict.getLabel(),value);
            }
        }
        //当评分内容为“对于提报已完成的项目检查发现情况总分1.8分”时，
        // 查询所用评分内容为“对于提报已完成的项目检查发现情况总分1.8分”的条例总分是否小于-1.8分。当低于-1.8分时对分数进行限制，低于-1.8分的条例增加都不会导致分数再降低
        if(dict.getType3().equals("对于提报已完成的项目检查发现情况总分1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper8);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)-1.8;
            float floatSum=(float)sum;
            if(floatSum<-1.8){
                //帮下一个判定条件过滤分数在-1.8到-2.1分变化的情况
                if((floatSum+1.8)>-0.3&&floatSum-Float.parseFloat(dict.getValue())<-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从-1.*跨度到大于-1.8分等情况
                }else if((floatSum+1.8)>-0.3){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-Float.parseFloat(dict.getValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
                Team team=teamService.queryTeamPoint_zwb(dict.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zwb();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zwbPointError(dict.getLabel(),value);
            }
        }
        //当评分内容为“对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分”时，
        // 查询所用评分内容为“对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分”的条例总分是否小于-1.2分。当低于-1.2分时对分数进行限制，低于-1.2分的条例增加都不会导致分数再降低
        if(dict.getType3().equals("对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper9);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float point=(float)-1.2;
            float floatSum=(float)sum;
            if(floatSum<-1.2){
                //帮下一个判定条件过滤分数在-1.2到-1.8分变化的情况
                if((floatSum+1.2)>-0.6&&floatSum-Float.parseFloat(dict.getValue())<-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(),0);
                    //处理分数从-1.*跨度到大于-1.2分等情况
                }else if((floatSum+1.2)>-0.6){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-Float.parseFloat(dict.getValue())));
                }else {
                    teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()));
                Team team=teamService.queryTeamPoint_zwb(dict.getLabel());

                //消除删除条例分数归零浮点型产生的误差
                double value=team.getPoint_zwb();
                if(Math.abs(value)<error){
                    value=0.0;
                }
                teamService.fixdict_zwbPointError(dict.getLabel(),value);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict dict = dictService.selectById(id);
        model.addAttribute("dict",dict);
        return "admin/system/dict/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict dict){
        EntityWrapper<Dict> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","存量违建治理4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type3","未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type3","新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("type3","城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper5 = new EntityWrapper<>();
        wrapper5.eq("type3","达到“无违建”创建标准并验收通过的加分项最多1.4分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper6 = new EntityWrapper<>();
        wrapper6.eq("type3","专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper7 = new EntityWrapper<>();
        wrapper7.eq("type3","一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper8 = new EntityWrapper<>();
        wrapper8.eq("type3","对于提报已完成的项目检查发现情况总分1.8分").eq("label",dict.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict> wrapper9 = new EntityWrapper<>();
        wrapper9.eq("type3","对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分").eq("label",dict.getLabel()).eq("visable_flag",1);
        if(dict.getId()==null || dict.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict.getSort() == null || dict.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict oldDict = dictService.selectById(dict.getId());
        if(!oldDict.getType().equals(dict.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict.getLabel().equals(dict.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict.getValue().equals(dict.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dictService.saveOrUpdateDict(dict);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
        if(dict.getType3().equals("存量违建治理4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper1);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-4.2);
            float oldfloatSum=floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            if(floatSum<-4.2){
                //处理分数从大于-4.2跨度到小于-4.2分等情况
                if(Float.parseFloat(dict.getValue())<Float.parseFloat(oldDict.getValue())&&oldfloatSum>-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-oldfloatSum);
                }else  {teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }
            ////处理分数从小于-4.2分跨度到大于-4.2分等情况
            else if(Float.parseFloat(dict.getValue())>Float.parseFloat(oldDict.getValue())&&oldfloatSum<-4.2) {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-floatSum));
            }else{
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            }
        }
        if(dict.getType3().equals("未按期完成存量违建治理计划的，上报已拆违建经检查发现面积不符、拆除不彻底、垃圾未清运等问题的，拆后复建等治理共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper2);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-2.8);
            float oldfloatSum=floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            if(floatSum<-2.8){
                //处理分数从大于-2.8跨度到小于-2.8分等情况
                if(Float.parseFloat(dict.getValue())<Float.parseFloat(oldDict.getValue())&&oldfloatSum>-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-oldfloatSum);
                }else  {teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }
            ////处理分数从小于-2.8分跨度到大于-2.8分等情况
            else if(Float.parseFloat(dict.getValue())>Float.parseFloat(oldDict.getValue())&&oldfloatSum<-2.8) {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-floatSum));
            }else{
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            }
        }
        if(dict.getType3().equals("新生违建，涉及上述重点专项工作或重点区域的，拆后复建的等治理共4.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper3);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-4.2);
            float oldfloatSum=floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            if(floatSum<-4.2){
                //处理分数从大于-4.2跨度到小于-4.2分等情况
                if(Float.parseFloat(dict.getValue())<Float.parseFloat(oldDict.getValue())&&oldfloatSum>-4.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-oldfloatSum);
                }else  {teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }
            ////处理分数从小于-4.2分跨度到大于-4.2分等情况
            else if(Float.parseFloat(dict.getValue())>Float.parseFloat(oldDict.getValue())&&oldfloatSum<-4.2) {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-floatSum));
            }else{
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            }
        }
        if(dict.getType3().equals("城市更新建设、桥下空间、高架路两侧、李村河周边等专项工作，及其他交办督办事项共2.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper4);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-2.8);
            float oldfloatSum=floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            if(floatSum<-2.8){
                //处理分数从大于-2.8跨度到小于-2.8分等情况
                if(Float.parseFloat(dict.getValue())<Float.parseFloat(oldDict.getValue())&&oldfloatSum>-2.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-oldfloatSum);
                }else  {teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }
            ////处理分数从小于-2.8分跨度到大于-2.8分等情况
            else if(Float.parseFloat(dict.getValue())>Float.parseFloat(oldDict.getValue())&&oldfloatSum<-2.8) {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-floatSum));
            }else{
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            }
        }
        if(dict.getType3().equals("达到“无违建”创建标准并验收通过的加分项最多1.4分")){
            List<Dict> Dict_List= dictService.selectList(wrapper5);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)1.4;
            float oldfloatSum=floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            if(floatSum>1.4){
                //处理分数从1.*跨度到1.4*等情况
                if(Float.parseFloat(dict.getValue())>Float.parseFloat(oldDict.getValue())&&oldfloatSum<1.4){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-(floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()))));
                }else  {teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }
            ////处理分数从大于1.4跨度到小于1.4的情况
            else if(Float.parseFloat(dict.getValue())<Float.parseFloat(oldDict.getValue())&&oldfloatSum>1.4) {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-(oldfloatSum+(Float.parseFloat(dict.getValue())-Float.parseFloat(oldDict.getValue())))));
            }else{
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            }
        }
        if(dict.getType3().equals("专人负责，对台账内的项目进行动态管理，及时汇报进展情况及提报档案资料情况共1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper6);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-1.2);
            float oldfloatSum=floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            if(floatSum<-1.2){
                //处理分数从大于-1.2跨度到小于-1.2分等情况
                if(Float.parseFloat(dict.getValue())<Float.parseFloat(oldDict.getValue())&&oldfloatSum>-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-oldfloatSum);
                }else  {teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }
            ////处理分数从小于-1.2分跨度到大于-1.2分等情况
            else if(Float.parseFloat(dict.getValue())>Float.parseFloat(oldDict.getValue())&&oldfloatSum<-1.2) {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-floatSum));
            }else{
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            }
        }
        if(dict.getType3().equals("一季度完成不少于 50%的台账任务，二季度全部完成整治任务等任务要求分数项共1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper7);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-1.8);
            float oldfloatSum=floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            if(floatSum<-1.8){
                //处理分数从大于-1.8跨度到小于-1.8分等情况
                if(Float.parseFloat(dict.getValue())<Float.parseFloat(oldDict.getValue())&&oldfloatSum>-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-oldfloatSum);
                }else  {teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }
            ////处理分数从小于-1.8分跨度到大于-1.8分等情况
            else if(Float.parseFloat(dict.getValue())>Float.parseFloat(oldDict.getValue())&&oldfloatSum<-1.8) {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-floatSum));
            }else{
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            }
        }
        if(dict.getType3().equals("对于提报已完成的项目检查发现情况总分1.8分")){
            List<Dict> Dict_List= dictService.selectList(wrapper8);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-1.8);
            float oldfloatSum=floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            if(floatSum<-1.8){
                //处理分数从大于-1.8跨度到小于-1.8分等情况
                if(Float.parseFloat(dict.getValue())<Float.parseFloat(oldDict.getValue())&&oldfloatSum>-1.8){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-oldfloatSum);
                }else  {teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }
            ////处理分数从小于-1.8分跨度到大于-1.8分等情况
            else if(Float.parseFloat(dict.getValue())>Float.parseFloat(oldDict.getValue())&&oldfloatSum<-1.8) {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-floatSum));
            }else{
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            }
        }
        if(dict.getType3().equals("对于重大决策部署、重要会议确定事项，领导批示、 指示和交办事项督办落实情况总分1.2分")){
            List<Dict> Dict_List= dictService.selectList(wrapper9);
            double sum=Dict_List.stream().mapToDouble(dict2-> Double.parseDouble(dict2.getValue())).sum();
            float floatSum=(float)sum;
            float point=(float)(-1.2);
            float oldfloatSum=floatSum-(Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            if(floatSum<-1.2){
                //处理分数从大于-1.2跨度到小于-1.2分等情况
                if(Float.parseFloat(dict.getValue())<Float.parseFloat(oldDict.getValue())&&oldfloatSum>-1.2){
                    teamService.changeTeamPoint_zwb(dict.getLabel(), point-oldfloatSum);
                }else  {teamService.changeTeamPoint_zwb(dict.getLabel(), 0);
                }
            }
            ////处理分数从小于-1.2分跨度到大于-1.2分等情况
            else if(Float.parseFloat(dict.getValue())>Float.parseFloat(oldDict.getValue())&&oldfloatSum<-1.2) {
                teamService.changeTeamPoint_zwb(dict.getLabel(), -(point-floatSum));
            }else{
                teamService.changeTeamPoint_zwb(dict.getLabel(), Float.parseFloat(dict.getValue()) - Float.parseFloat(oldDict.getValue()));
            }
        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }



    //治违办大分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict> platformJck_ZgkList(@RequestParam ("label") String label,
                                                   @RequestParam(value = "page",defaultValue = "1")Integer page,
                                                   @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                                   ServletRequest request){
        if(limit==null){
            return null;
        };
        LayerData<Dict> layerData = new LayerData<>();
        EntityWrapper<Dict> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict> dataPage = dictService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
}
