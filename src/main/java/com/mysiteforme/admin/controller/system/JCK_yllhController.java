package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.*;
import com.mysiteforme.admin.entity.Dict_jck_yllh;
import com.mysiteforme.admin.entity.Dict_jck_yllh;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/20.
 * todo:
 * 监察科-园林绿化controller
 */
@Controller
@RequestMapping("admin/system/jck_yllh")
public class JCK_yllhController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_yllh> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_yllh> wrapper2 = new EntityWrapper<>();
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理
        EntityWrapper<Dict_jck_yllh> wrapper3 = new EntityWrapper<>();
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_yllh> wrapper4 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_yllh> wrapper5 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_yllh dict_jck_yllh = dict_jck_yllhService.selectById(id);
        wrapper1.eq("type3","绿化立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","油烟、噪声、扬尘立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1).or().eq("description","每月完成1件普通程序案件加1分，最多得4分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        List<String> values = Arrays.asList("新增渣土堆体未及时发现、处置的，每处扣1分", "发现后处置不力，视情况扣2-3分", "参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分");
        wrapper5.eq("type3","渣土车立案处罚情况.").in("description",values).eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_yllh.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_jck_yllh.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_yllh.setValue(String.valueOf(newValue));
        dict_jck_yllh.setNumber(number+1);
//        teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_jck_yllhService.saveOrUpdateDict(dict_jck_yllh);
        //当增加条数的条例评分内容为“绿化立案处罚情况.”时，查询所用评分内容为“小广告立案绿化立案处罚情况.处罚情况”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("绿化立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper1);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<1&&(floatSum-newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<1||(floatSum-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当增加条数的条例评分内容为“油烟、噪声、扬尘立案处罚情况.”时，查询所用评分内容为“油烟、噪声、扬尘立案处罚情况.”的条例总分是否高于4分。当超过4分时对分数进行限制，超过4分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("油烟、噪声、扬尘立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper2);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum-4)<1&&(floatSum-newValue.subtract(oldValue).floatValue())>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从3.*跨度到4.*等情况
                }else if((floatSum-4)<1||(floatSum-newValue.subtract(oldValue).floatValue())<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), 4-(floatSum-newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当评分内容为“渣土车立案处罚情况.”时，查询所用评分内容为“渣土车立案处罚情况.”且为“简易程序案件得分”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件简易程序案件加0.5分，最多得2分")){
            List<Dict_jck_yllh> Dict_jck_yllh_List2= dict_jck_yllhService.selectList(wrapper4);
            List<Dict_jck_yllh> Dict_jck_yllh_List1= dict_jck_yllhService.selectList(wrapper3);
            double sum1=Dict_jck_yllh_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_yllh_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            //先查看“简易程序案件得分”是否达到上限，若达到则执行以下语句不加分。
            if(floatSum2>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum2-2)<0.5&&floatSum2-newValue.subtract(oldValue).floatValue()>2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum2-2)<0.5||(floatSum2-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 2-(floatSum2-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 2-(floatSum2-newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否达到上限，若达到则执行以下语句不加分，若未达到才会正常加分
            else if (floatSum1>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum1-4)<1&&floatSum1-newValue.subtract(oldValue).floatValue()>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从4.*跨度到5.*等情况
                }else if((floatSum1-4)<1||(floatSum1-newValue.subtract(oldValue).floatValue())<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(floatSum1-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 4-(floatSum1-newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                //正常加分
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当评分内容为“渣土车立案处罚情况.”时，查询所用评分内容为“渣土车立案处罚情况.”的条例总分是否高于4分。当超过4分时对分数进行限制，超过4分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件普通程序案件加1分，最多得4分")){
            List<Dict_jck_yllh> Dict_jck_yllh_List1= dict_jck_yllhService.selectList(wrapper3);
            List<Dict_jck_yllh> Dict_jck_yllh_List2= dict_jck_yllhService.selectList(wrapper4);
            double sum1=Dict_jck_yllh_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_yllh_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            if(floatSum2>2&&floatSum1-floatSum2<=2){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }else if(floatSum1>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum1-4)<1&&floatSum1-newValue.subtract(oldValue).floatValue()>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从3.*跨度到4.*等情况
                }else if((floatSum1-4)<1||(floatSum1-newValue.subtract(oldValue).floatValue())<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(floatSum1-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 4-(floatSum1-newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&(dict_jck_yllh.getDescription().equals("发现后处置不力，视情况扣2-3分")||dict_jck_yllh.getDescription().equals("新增渣土堆体未及时发现、处置的，每处扣1分")||dict_jck_yllh.getDescription().equals("参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分"))){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper5);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-4){
                if((floatSum-newValue.subtract(oldValue).floatValue()<-4)){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数跨度的情况
                }
                if ((floatSum - newValue.subtract(oldValue).floatValue() > -4)) {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())-4);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())-4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }
            }else if(floatSum>-4&&floatSum<0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -4)) {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4 + floatSum);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), 4 + floatSum);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), floatSum);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }
                else{
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }
            }
            else{
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_yllh> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_yllh> wrapper2 = new EntityWrapper<>();
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理
        EntityWrapper<Dict_jck_yllh> wrapper3 = new EntityWrapper<>();
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_yllh> wrapper4 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_yllh> wrapper5 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_yllh dict_jck_yllh = dict_jck_yllhService.selectById(id);
        wrapper1.eq("type3","绿化立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","油烟、噪声、扬尘立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1).or().eq("description","每月完成1件普通程序案件加1分，最多得4分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        List<String> values = Arrays.asList("新增渣土堆体未及时发现、处置的，每处扣1分", "发现后处置不力，视情况扣2-3分", "参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分");
        wrapper5.eq("type3","渣土车立案处罚情况.").in("description",values).eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_yllh.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_jck_yllh.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_yllh.setValue(String.valueOf(newValue));
        dict_jck_yllh.setNumber(number-1);
//        teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
        //当减少条数的条例评分内容为“绿化立案处罚情况.”时，查询所用评分内容为“绿化立案处罚情况.”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("绿化立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper1);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<1&&(floatSum+newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum-2)<1||(floatSum+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
//当减少条数的条例评分内容为“油烟、噪声、扬尘立案处罚情况.”时，查询所用评分内容为“油烟、噪声、扬尘立案处罚情况.”的条例总分是否高于4分。当超过4分时对分数进行限制，超过4分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("油烟、噪声、扬尘立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper2);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum-4)<1&&(floatSum+newValue.subtract(oldValue).floatValue())>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从4.*跨度到3.*等情况
                }else if((floatSum-4)<1||(floatSum+newValue.subtract(oldValue).floatValue())<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(4-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), -(4-(floatSum+newValue.subtract(oldValue).floatValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当减少条数内容为“渣土车立案处罚情况.”时，查询所用评分内容为“渣土车立案处罚情况.”且为“简易程序案件得分”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例减少时进行相应操作
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件简易程序案件加0.5分，最多得2分")){
            List<Dict_jck_yllh> Dict_jck_yllh_List2= dict_jck_yllhService.selectList(wrapper4);
            List<Dict_jck_yllh> Dict_jck_yllh_List1= dict_jck_yllhService.selectList(wrapper3);
            double sum1=Dict_jck_yllh_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_yllh_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            //先查看“简易程序案件得分”是否低于上限，若低于则执行以下语句减分。
            if(floatSum2>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum2-2)<0.5&&floatSum2+newValue.subtract(oldValue).floatValue()>2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum2-2)<0.5||(floatSum2+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(2-(floatSum2+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -(2-(floatSum2+newValue.subtract(oldValue).floatValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否高于上限变为低于上限，若低于则执行以下语句减分，若一直低于才会正常扣分
            else if (floatSum1>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum1-4)<1&&floatSum1+newValue.subtract(oldValue).floatValue()>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从3.*跨度到2.*等情况
                }else if((floatSum1-4)<1||(floatSum1+newValue.subtract(oldValue).floatValue())<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(4-(floatSum1+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -(4-(floatSum1+newValue.subtract(oldValue).floatValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                //正常加分
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当减少条数评分内容为“渣土车立案处罚情况.”时，查询所用评分内容为“渣土车立案处罚情况.”的条例总分是否高于4分。当超过4分时对分数进行限制，超过4分的条例减少时进行相应操作
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件普通程序案件加1分，最多得4分")){
            List<Dict_jck_yllh> Dict_jck_yllh_List1= dict_jck_yllhService.selectList(wrapper3);
            List<Dict_jck_yllh> Dict_jck_yllh_List2= dict_jck_yllhService.selectList(wrapper4);
            double sum1=Dict_jck_yllh_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_yllh_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            if(floatSum2>2&&floatSum1-floatSum2<=2){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }else if(floatSum1>4){
                //帮下一个判定条件过滤分数在5到4分变化的情况
                if((floatSum1-4)<1&&floatSum1+newValue.subtract(oldValue).floatValue()>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从4.*跨度到3.*等情况
                }else if((floatSum1-4)<1||(floatSum1+newValue.subtract(oldValue).floatValue())<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(4-(floatSum1+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -(4-(floatSum1+newValue.subtract(oldValue).floatValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        dict_jck_yllhService.saveOrUpdateDict(dict_jck_yllh);
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&(dict_jck_yllh.getDescription().equals("发现后处置不力，视情况扣2-3分")||dict_jck_yllh.getDescription().equals("新增渣土堆体未及时发现、处置的，每处扣1分")||dict_jck_yllh.getDescription().equals("参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分"))){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper5);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            if(floatSum<=-4){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4+(floatSum+newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), 4+(floatSum+newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                } else{
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else if(floatSum>-4&&floatSum<0){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -floatSum);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), -floatSum);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), newValue.subtract(oldValue).floatValue());
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }
            }
            else if(floatSum>=0) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),-4);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(),-4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                } else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }

            }

        }
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_yllh> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_yllh> wrapper2 = new EntityWrapper<>();
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理
        EntityWrapper<Dict_jck_yllh> wrapper3 = new EntityWrapper<>();
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_yllh> wrapper4 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_yllh> wrapper5 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_jck_yllh dict_jck_yllh = dict_jck_yllhService.selectById(id);
        wrapper1.eq("type3","绿化立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","油烟、噪声、扬尘立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1).or().eq("description","每月完成1件普通程序案件加1分，最多得4分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        List<String> values = Arrays.asList("新增渣土堆体未及时发现、处置的，每处扣1分", "发现后处置不力，视情况扣2-3分", "参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分");
        wrapper5.eq("type3","渣土车立案处罚情况.").in("description",values).eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_jck_yllhService.deleteDict(id);
        if(dict_jck_yllh.getType3().equals("绿化立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper1);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_yllh.getValue());
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<1&&floatSum-Float.parseFloat(dict_jck_yllh.getValue())>2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum-2)<1||Float.parseFloat(dict_jck_yllh.getValue())>1)&&(floatSum-Float.parseFloat(dict_jck_yllh.getValue()))<2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum-Float.parseFloat(dict_jck_yllh.getValue())-2);
                    teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), floatSum-Float.parseFloat(dict_jck_yllh.getValue())-2);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                } else{
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        if(dict_jck_yllh.getType3().equals("油烟、噪声、扬尘立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper2);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_yllh.getValue());
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到4.5分变化的情况
                if((floatSum-4)<1&&floatSum-Float.parseFloat(dict_jck_yllh.getValue())>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从4.*跨度到3.*的情况
                }else if(((floatSum-4)<1||Float.parseFloat(dict_jck_yllh.getValue())>1)&&(floatSum-Float.parseFloat(dict_jck_yllh.getValue()))<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum-Float.parseFloat(dict_jck_yllh.getValue())-4);
                    teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), floatSum-Float.parseFloat(dict_jck_yllh.getValue())-4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else{
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件简易程序案件加0.5分，最多得2分")){
            List<Dict_jck_yllh> Dict_jck_yllh_List2= dict_jck_yllhService.selectList(wrapper4);
            List<Dict_jck_yllh> Dict_jck_yllh_List1= dict_jck_yllhService.selectList(wrapper3);
            double sum1=Dict_jck_yllh_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_yllh_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1+Float.parseFloat(dict_jck_yllh.getValue());
            float floatSum2=(float)sum2+Float.parseFloat(dict_jck_yllh.getValue());
            //先查看“简易程序案件得分”是否达到上限，若达到则执行以下语句不扣分。
            if(floatSum2>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum2-2)<0.5&&floatSum2-Float.parseFloat(dict_jck_yllh.getValue())>2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum2-2)<0.5||Float.parseFloat(dict_jck_yllh.getValue())>0.5)&&(floatSum2-Float.parseFloat(dict_jck_yllh.getValue()))<2&&(floatSum1-Float.parseFloat(dict_jck_yllh.getValue()))<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum2-Float.parseFloat(dict_jck_yllh.getValue())-2);
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), floatSum2-Float.parseFloat(dict_jck_yllh.getValue())-2);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                } else{
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否达到上限，若达到则执行以下语句不扣分，若未达到才会正常扣分
            else if (floatSum1>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum1-4)<1&&floatSum1-Float.parseFloat(dict_jck_yllh.getValue())>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从4.*跨度到3.*等情况
                }else if(((floatSum1-4)<1||Float.parseFloat(dict_jck_yllh.getValue())>1)&&(floatSum1-Float.parseFloat(dict_jck_yllh.getValue()))<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum1-Float.parseFloat(dict_jck_yllh.getValue())-4);
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), floatSum1-Float.parseFloat(dict_jck_yllh.getValue())-4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                //正常扣分
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件普通程序案件加1分，最多得4分")) {
            List<Dict_jck_yllh> Dict_jck_yllh_List2 = dict_jck_yllhService.selectList(wrapper4);
            List<Dict_jck_yllh> Dict_jck_yllh_List1 = dict_jck_yllhService.selectList(wrapper3);
            double sum1 = Dict_jck_yllh_List1.stream().mapToDouble(dict -> Double.parseDouble(dict.getValue())).sum();
            double sum2 = Dict_jck_yllh_List2.stream().mapToDouble(dict -> Double.parseDouble(dict.getValue())).sum();
            float floatSum1 = (float) sum1 + Float.parseFloat(dict_jck_yllh.getValue());
            float newfloatSum1 = (float) sum1;
            float floatSum2 = (float) sum2;
            if (floatSum2 > 2 && (floatSum1 - floatSum2) <= 2) {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            } else if(floatSum2 > 2&&newfloatSum1- floatSum2<=2&&floatSum1-floatSum2>2){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(4-(2+newfloatSum1-floatSum2)));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -(4-(2+newfloatSum1-floatSum2)));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
            else if (floatSum1 > 4) {
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if ((floatSum1 - 4) < 1 && floatSum1 - Float.parseFloat(dict_jck_yllh.getValue()) > 4) {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                    //处理分数从4.*跨度到3.*等情况
                } else if (((floatSum1 - 4) < 1 || Float.parseFloat(dict_jck_yllh.getValue()) > 1) && (floatSum1 - Float.parseFloat(dict_jck_yllh.getValue())) < 4) {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum1-Float.parseFloat(dict_jck_yllh.getValue()) - 4);
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), floatSum1-Float.parseFloat(dict_jck_yllh.getValue()) - 4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                } else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            } else {
                //正常扣分
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&(dict_jck_yllh.getDescription().equals("发现后处置不力，视情况扣2-3分")||dict_jck_yllh.getDescription().equals("新增渣土堆体未及时发现、处置的，每处扣1分")||dict_jck_yllh.getDescription().equals("参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分"))){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper5);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_jck_yllh.getValue());
            if(floatSum<=-4){
                if(floatSum-Float.parseFloat(dict_jck_yllh.getValue())>=0){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),4);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(),4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else if(floatSum-Float.parseFloat(dict_jck_yllh.getValue())<=-4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_jck_yllh.getValue())>=-4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4+(floatSum-Float.parseFloat(dict_jck_yllh.getValue())));
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), 4+(floatSum-Float.parseFloat(dict_jck_yllh.getValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                } else{
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else if(floatSum>-4&&floatSum<0){
                if(floatSum-Float.parseFloat(dict_jck_yllh.getValue())<=-4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(4+floatSum));
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), -(4+floatSum));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else if(floatSum-Float.parseFloat(dict_jck_yllh.getValue())>=0){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -floatSum);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), -floatSum);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), -Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/jck_yllh/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_jck_yllh> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_yllh> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_yllh> dataPage = dict_jck_yllhService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_jck_yllh> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                         ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_yllh> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0,0为历史条目
        Page<Dict_jck_yllh> dataPage = dict_jck_yllhService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_yllh/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_yllh/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_yllh/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_jck_yllh dict_jck_yllh){
        EntityWrapper<Dict_jck_yllh> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","绿化立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_yllh> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type3","油烟、噪声、扬尘立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理
        EntityWrapper<Dict_jck_yllh> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1).or().eq("description","每月完成1件普通程序案件加1分，最多得4分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_yllh> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_yllh> wrapper5 = new EntityWrapper<>();
        List<String> values = Arrays.asList("新增渣土堆体未及时发现、处置的，每处扣1分", "发现后处置不力，视情况扣2-3分", "参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分");
        wrapper5.eq("type3","渣土车立案处罚情况.").in("description",values).eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict_jck_yllh.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_jck_yllh.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_yllhService.getCountByType(dict_jck_yllh.getType())==0){
            dict_jck_yllh.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_jck_yllh.setSort(dict_jck_yllhService.getMaxSortByType(dict_jck_yllh.getType()));
        }
        dict_jck_yllhService.saveOrUpdateDict(dict_jck_yllh);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
        //当评分内容为“绿化立案处罚情况.”时，查询所用评分内容为“绿化立案处罚情况.”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("绿化立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper1);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<1&&floatSum-Float.parseFloat(dict_jck_yllh.getValue())>2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<1){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_yllh.getValue())));
                    teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_yllh.getValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当评分内容为“油烟、噪声、扬尘立案处罚情况.”时，查询所用评分内容为“油烟、噪声、扬尘立案处罚情况.”的条例总分是否高于4分。当超过4分时对分数进行限制，超过4分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("油烟、噪声、扬尘立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper2);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum-4)<1&&floatSum-Float.parseFloat(dict_jck_yllh.getValue())>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从3.*跨度到4.*等情况
                }else if((floatSum-4)<1){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(floatSum-Float.parseFloat(dict_jck_yllh.getValue())));
                    teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), 4-(floatSum-Float.parseFloat(dict_jck_yllh.getValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当评分内容为“渣土车立案处罚情况.”时，查询所用评分内容为“渣土车立案处罚情况.”且为“简易程序案件得分”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件简易程序案件加0.5分，最多得2分")){
            List<Dict_jck_yllh> Dict_jck_yllh_List2= dict_jck_yllhService.selectList(wrapper4);
            List<Dict_jck_yllh> Dict_jck_yllh_List1= dict_jck_yllhService.selectList(wrapper3);
            List<Dict_jck_yllh> Dict_jck_yllh_List3= dict_jck_yllhService.selectList(wrapper5);
            double sum1=Dict_jck_yllh_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_yllh_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum3=Dict_jck_yllh_List3.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float oldfloatSum1=(float)sum1-Float.parseFloat(dict_jck_yllh.getValue());
            float floatSum2=(float)sum2;
            float floatSum3=(float)sum3;
            //先查看“简易程序案件得分”是否达到上限，若达到则执行以下语句不加分。
            if(floatSum2>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum2-2)<0.5&&floatSum2-Float.parseFloat(dict_jck_yllh.getValue())>2&&floatSum3==0){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if(floatSum1<2&&floatSum1>=0){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }
                else if((floatSum2-2)<0.5){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 2-(floatSum2-Float.parseFloat(dict_jck_yllh.getValue())));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 2-(floatSum2-Float.parseFloat(dict_jck_yllh.getValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否达到上限，若达到则执行以下语句不加分，若未达到才会正常加分
            else if (floatSum1>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum1-4)<1&&floatSum1-Float.parseFloat(dict_jck_yllh.getValue())>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从3.*跨度到4.*等情况
                }else if((floatSum1-4)<1){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(floatSum1-Float.parseFloat(dict_jck_yllh.getValue())));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 4-(floatSum1-Float.parseFloat(dict_jck_yllh.getValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }
            else if(oldfloatSum1<0&&floatSum1<=0){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
            }else if(oldfloatSum1<0&&floatSum1>0){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(floatSum1-Float.parseFloat(dict_jck_yllh.getValue())));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 4-(floatSum1-Float.parseFloat(dict_jck_yllh.getValue())));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }else {
                //正常加分
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当评分内容为“渣土车立案处罚情况.”时，查询所用评分内容为“渣土车立案处罚情况.”的条例总分是否高于4分。当超过4分时对分数进行限制，超过4分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件普通程序案件加1分，最多得4分")){
            List<Dict_jck_yllh> Dict_jck_yllh_List1= dict_jck_yllhService.selectList(wrapper3);
            List<Dict_jck_yllh> Dict_jck_yllh_List2= dict_jck_yllhService.selectList(wrapper4);
            List<Dict_jck_yllh> Dict_jck_yllh_List3= dict_jck_yllhService.selectList(wrapper5);
            double sum1=Dict_jck_yllh_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_yllh_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum3=Dict_jck_yllh_List3.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float oldfloatSum1=(float)sum1-Float.parseFloat(dict_jck_yllh.getValue());
            float floatSum2=(float)sum2;
            if(floatSum2>2&&floatSum1-floatSum2<=2&&floatSum1-floatSum2>0){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }else if(floatSum1>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum1-4)<1&&floatSum1-Float.parseFloat(dict_jck_yllh.getValue())>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从3.*跨度到4.*等情况
                }else if((floatSum1-4)<1){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(floatSum1-Float.parseFloat(dict_jck_yllh.getValue())));
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 4-(floatSum1-Float.parseFloat(dict_jck_yllh.getValue())));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else if(oldfloatSum1<0&&floatSum1<=0){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
            }else if(oldfloatSum1<0&&floatSum1>0){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(floatSum1-Float.parseFloat(dict_jck_yllh.getValue())));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 4-(floatSum1-Float.parseFloat(dict_jck_yllh.getValue())));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }else{
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&(dict_jck_yllh.getDescription().equals("发现后处置不力，视情况扣2-3分")||dict_jck_yllh.getDescription().equals("新增渣土堆体未及时发现、处置的，每处扣1分")||dict_jck_yllh.getDescription().equals("参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分"))){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper5);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-4){
                if(floatSum-Float.parseFloat(dict_jck_yllh.getValue())>=0){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),-4);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(),-4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                } else if((floatSum-Float.parseFloat(dict_jck_yllh.getValue())<-4)){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_jck_yllh.getValue())>=-4)){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(floatSum-Float.parseFloat(dict_jck_yllh.getValue()))-4);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), -(floatSum-Float.parseFloat(dict_jck_yllh.getValue()))-4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else{
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }

            }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_jck_yllh dict_jck_yllh = dict_jck_yllhService.selectById(id);
        model.addAttribute("dict_jck_yllh",dict_jck_yllh);
        return "admin/system/jck_yllh/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_jck_yllh dict_jck_yllh){
        EntityWrapper<Dict_jck_yllh> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","绿化立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_yllh> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type3","油烟、噪声、扬尘立案处罚情况.").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理
        EntityWrapper<Dict_jck_yllh> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1).or().eq("description","每月完成1件普通程序案件加1分，最多得4分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        //对“渣土车立案处罚情况.”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_yllh> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("type3","渣土车立案处罚情况.").eq("description","每月完成1件简易程序案件加0.5分，最多得2分").eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_yllh> wrapper5 = new EntityWrapper<>();
        List<String> values = Arrays.asList("新增渣土堆体未及时发现、处置的，每处扣1分", "发现后处置不力，视情况扣2-3分", "参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分");
        wrapper5.eq("type3","渣土车立案处罚情况.").in("description",values).eq("label",dict_jck_yllh.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(dict_jck_yllh.getId()==null || dict_jck_yllh.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_jck_yllh.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_jck_yllh.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_jck_yllh.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_yllh.getSort() == null || dict_jck_yllh.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_jck_yllh oldDict_jck_yllh = dict_jck_yllhService.selectById(dict_jck_yllh.getId());
        if(!oldDict_jck_yllh.getType().equals(dict_jck_yllh.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_jck_yllh.getLabel().equals(dict_jck_yllh.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_jck_yllh.getValue().equals(dict_jck_yllh.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_jck_yllhService.saveOrUpdateDict(dict_jck_yllh);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
        if(dict_jck_yllh.getType3().equals("绿化立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper1);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
            if(floatSum>2){
                //处理分数从1.*跨度到2.*等情况
                if(Float.parseFloat(dict_jck_yllh.getValue())>Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum<2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()))));
                    teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()))));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else  {teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }
            ////处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_yllh.getValue())<Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum>2) {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }else{
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_lh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        if(dict_jck_yllh.getType3().equals("油烟、噪声、扬尘立案处罚情况.")){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper2);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到4.5分变化的情况
                if(Float.parseFloat(dict_jck_yllh.getValue())>Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(floatSum-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()))));
                    teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), 4-(floatSum-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()))));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else  {teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }
            //处理分数从4.*跨度到3.*等情况
            else if(Float.parseFloat(dict_jck_yllh.getValue())<Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum>4) {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(4-(oldfloatSum+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), -(4-(oldfloatSum+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }else{
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_yy(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当评分内容为“渣土车立案处罚情况.”时，查询所用评分内容为“渣土车立案处罚情况.”且为“简易程序案件得分”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件简易程序案件加0.5分，最多得2分")){
            List<Dict_jck_yllh> Dict_jck_yllh_List2= dict_jck_yllhService.selectList(wrapper4);
            List<Dict_jck_yllh> Dict_jck_yllh_List1= dict_jck_yllhService.selectList(wrapper3);
            double sum1=Dict_jck_yllh_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_yllh_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            float oldfloatSum1=floatSum1-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
            float oldfloatSum2=floatSum2-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
            //先查看“简易程序案件得分”是否达到上限，若达到则执行以下语句不加分。
            if(floatSum2>2){
                //总上限达到四分，“简单程序案件得分”再超过2分上限不做改变
                if(floatSum1>4&&floatSum1-floatSum2!=1&&floatSum1-floatSum2!=2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if(Float.parseFloat(dict_jck_yllh.getValue())>Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum2<2){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 2-oldfloatSum2);
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 2-oldfloatSum2);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }//处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_yllh.getValue())<Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum2>2&&floatSum1<4){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(2-(oldfloatSum2+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -(2-(oldfloatSum2+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否达到上限，若达到则执行以下语句不加分，若未达到才会正常加分
            else if (floatSum1>4){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum1-4)<1&&oldfloatSum1>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从2.*跨度到3.*等情况
                }else if(Float.parseFloat(dict_jck_yllh.getValue())>Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum1<3){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-oldfloatSum1);
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 4-oldfloatSum1);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }//处理分数从3.*跨度到2.*等情况
            else if(Float.parseFloat(dict_jck_yllh.getValue())<Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum1>4){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(4-(oldfloatSum1+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -(4-(oldfloatSum1+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }else {
                //正常加减分
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        //当评分内容为“渣土车立案处罚情况.”时，查询所用评分内容为“渣土车立案处罚情况.”的条例总分是否高于3分。当超过3分时对分数进行限制，超过3分的条例增加都不会导致分数增加
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&dict_jck_yllh.getDescription().equals("每月完成1件普通程序案件加1分，最多得4分")){
            List<Dict_jck_yllh> Dict_jck_yllh_List1= dict_jck_yllhService.selectList(wrapper3);
            List<Dict_jck_yllh> Dict_jck_yllh_List2= dict_jck_yllhService.selectList(wrapper4);
            double sum1=Dict_jck_yllh_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_yllh_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            float oldfloatSum1=floatSum1-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
            float oldfloatSum2=floatSum2-(Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
            if(floatSum2>2&&floatSum1-floatSum2<=2&&floatSum1-oldfloatSum1<=2&&floatSum1-oldfloatSum1>0&&Float.parseFloat(dict_jck_yllh.getValue())<Float.parseFloat(oldDict_jck_yllh.getValue())){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum1-oldfloatSum1);
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), floatSum1-oldfloatSum1);
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
            else if(floatSum2>2&&floatSum1-floatSum2<=2&&floatSum1-oldfloatSum1<=2&&Float.parseFloat(dict_jck_yllh.getValue())>Float.parseFloat(oldDict_jck_yllh.getValue())){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum1-oldfloatSum1);
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), floatSum1-oldfloatSum1);
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
            else if(floatSum2>2&&floatSum1-floatSum2<=2&&Float.parseFloat(dict_jck_yllh.getValue())<Float.parseFloat(oldDict_jck_yllh.getValue())){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(4-(2+floatSum1-floatSum2)));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -(4-(2+floatSum1-floatSum2)));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }else if(floatSum2>2&&oldfloatSum1-floatSum2<=2&&Float.parseFloat(dict_jck_yllh.getValue())>Float.parseFloat(oldDict_jck_yllh.getValue())){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-(2+oldfloatSum1-floatSum2));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 4-(2+oldfloatSum1-floatSum2));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
            else if(floatSum1>4){
                //帮下一个判定条件过滤分数在4到5分变化的情况
                if((floatSum1-4)<1&&oldfloatSum1>4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数从3.*跨度到4.*等情况
                }else if(Float.parseFloat(dict_jck_yllh.getValue())>Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum1<4){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4-oldfloatSum1);
                    teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), 4-oldfloatSum1);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }//应对从4分以上降低到4分的情况，这种情况分数应该不变
            else if(floatSum1==4&&oldfloatSum1>4){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
            }
            //处理分数从4.*跨度到3.*等情况
            else if(Float.parseFloat(dict_jck_yllh.getValue())<Float.parseFloat(oldDict_jck_yllh.getValue())&&oldfloatSum1>4){
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(4-(oldfloatSum1+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), -(4-(oldfloatSum1+(Float.parseFloat(dict_jck_yllh.getValue())-Float.parseFloat(oldDict_jck_yllh.getValue())))));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
            else {
                teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
                teamService.changeTeamPoint_jck_yllh_ztc1(dict_jck_yllh.getLabel(), Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue()));
                updateTeamPoint(dict_jck_yllh.getLabel(),error);
            }
        }
        if(dict_jck_yllh.getType3().equals("渣土车立案处罚情况.")&&(dict_jck_yllh.getDescription().equals("发现后处置不力，视情况扣2-3分")||dict_jck_yllh.getDescription().equals("新增渣土堆体未及时发现、处置的，每处扣1分")||dict_jck_yllh.getDescription().equals("参加联合执法路查行动应遵守有关要求，到位不及时每人次扣 0.2 分"))){
            List<Dict_jck_yllh> Dict_jck_yllh_List= dict_jck_yllhService.selectList(wrapper5);
            double sum=Dict_jck_yllh_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_jck_yllh.getValue()) - Float.parseFloat(oldDict_jck_yllh.getValue());
//            float point=(float)1.4;
            if(floatSum<=-4){
                if(floatSum-differ>=0){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),-4);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(),-4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                } else if((floatSum-differ<-4)){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-4)){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(floatSum-differ)-4);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), -(floatSum-differ)-4);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }else if(floatSum>-4&&floatSum<0) {
                if ((floatSum - differ <= -4)) {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 4+floatSum );
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), 4+floatSum );
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }else if(floatSum - differ>=0){
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), floatSum);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), floatSum);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }
                else{
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), differ);
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), differ);
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - differ > 0)) {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 0)) {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), -(floatSum - differ));
                    teamService.changeTeamPoint_jck_yllh_ztc2(dict_jck_yllh.getLabel(), -(floatSum - differ));
                    updateTeamPoint(dict_jck_yllh.getLabel(),error);
                }
                else {
                    teamService.changeTeamPoint_jck_yllh(dict_jck_yllh.getLabel(), 0);
                }
            }
        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }

    //监察科园林绿化大分数接口分数接口，供大屏幕显示数据用


    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_jck_yllh> platformJck_YllhList(@RequestParam ("label") String label,
                                                         @RequestParam(value = "page",defaultValue = "1")Integer page,
                                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,

                                                         ServletRequest request){
        if(limit==null){
            return null;
        };

        LayerData<Dict_jck_yllh> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0

        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_jck_yllh> dataPage = dict_jck_yllhService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
    private void updateTeamPoint(String label, double error) {
        Team team=teamService.queryTeamPoint_jck_yllh(label);
        //消除删除条例分数归零浮点型产生的误差
        double value=team.getPoint_jck_yllh();
        double value1=team.getPoint_jck_yllh_lh();
        double value2=team.getPoint_jck_yllh_yy();
        double value3=team.getPoint_jck_yllh_ztc1();
        double value4=team.getPoint_jck_yllh_ztc2();
        if(Math.abs(value)<error){
            value=0.0;
        }
        if(Math.abs(value1)<error){
            value1=0.0;
        }
        if(Math.abs(value2)<error){
            value2=0.0;
        }
        if(Math.abs(value3)<error){
            value3=0.0;
        }
        if(Math.abs(value4)<error){
            value4=0.0;
        }
        teamService.fixdict_jck_yllhPointError(label,value);
        teamService.fixdict_jck_yllh_lhPointError(label,value1);
        teamService.fixdict_jck_yllh_yyPointError(label,value2);
        teamService.fixdict_jck_yllh_ztc1PointError(label,value3);
        teamService.fixdict_jck_yllh_ztc2PointError(label,value4);
    }


}
