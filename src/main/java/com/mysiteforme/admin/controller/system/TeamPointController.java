package com.mysiteforme.admin.controller.system;

import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;

/**
 * Created by lsd on 2021/11/26.
 * todo:
 */
@Controller
@RequestMapping("teamPoint")
public class TeamPointController extends BaseController {
    private static final Log log = LogFactory.get();

    //结算
    @PostMapping("settlement")
    @ResponseBody
    public RestResponse settlement(){
        boolean dict_bgs_resultFlag = dict_bgsService.updateVisableFlag();
        boolean dict_dcs_resultFlag = dict_dcsService.updateVisableFlag();
        boolean dict_dsjk_resultFlag = dict_dsjkService.updateVisableFlag();
        boolean dict_fzk_resultFlag = dict_fzkService.updateVisableFlag();
        boolean dict_ggb_resultFlag = dict_ggbService.updateVisableFlag();
        boolean dict_jck_hjws_resultFlag = dict_jck_hjwsService.updateVisableFlag();
        boolean dict_jck_srzx_resultFlag = dict_jck_srzxService.updateVisableFlag();
        boolean dict_jck_szss_resultFlag = dict_jck_szssService.updateVisableFlag();
        boolean dict_jck_wyzf_resultFlag = dict_jck_wyzfService.updateVisableFlag();
        boolean dict_jck_yllh_resultFlag = dict_jck_yllhService.updateVisableFlag();
        boolean dict_jck_qtaj_resultFlag = dict_jck_qtajService.updateVisableFlag();
        boolean dict_jck_ywld_resultFlag = dict_jck_ywldService.updateVisableFlag();
        boolean dict_jck_bz_resultFlag = dict_jck_bzService.updateVisableFlag();
        boolean dict_xck_resultFlag = dict_xckService.updateVisableFlag();
        boolean dict_xfk_resultFlag = dict_xfkService.updateVisableFlag();
        boolean dict_zgk_resultFlag = dict_zgkService.updateVisableFlag();
        boolean dict_ztb_resultFlag = dict_ztbService.updateVisableFlag();
        boolean dict_resultFlag = dictService.updateVisableFlag();
        if(dict_bgs_resultFlag == false){
            return RestResponse.failure("渣土办(建筑垃圾执法)条目清除失败！");
        }
        if(dict_dcs_resultFlag == false){
            return RestResponse.failure("指挥中心条目清除失败！");
        }
        if(dict_dsjk_resultFlag == false){
            return RestResponse.failure("大数据科条目清除失败！");
        }
        if(dict_fzk_resultFlag == false){
            return RestResponse.failure("法制科条目清除失败！");
        }
        if(dict_ggb_resultFlag == false){
            return RestResponse.failure("广告办条目清除失败！");
        }
        if(dict_jck_hjws_resultFlag == false){
            return RestResponse.failure("监察科(市容环卫)条目清除失败！");
        }
        if(dict_jck_srzx_resultFlag == false){
            return RestResponse.failure("监察科(市容秩序)条目清除失败！");
        }
        if(dict_jck_szss_resultFlag == false){
            return RestResponse.failure("监察科(市政公用)条目清除失败！");
        }
        if(dict_jck_wyzf_resultFlag == false){
            return RestResponse.failure("监察科(物业执法)条目清除失败！");
        }
        if(dict_jck_yllh_resultFlag == false){
            return RestResponse.failure("监察科(园林绿化)条目清除失败！");
        }
        if(dict_jck_qtaj_resultFlag == false){
            return RestResponse.failure("监察科(其他案件)条目清除失败！");
        }
        if(dict_jck_ywld_resultFlag == false){
            return RestResponse.failure("监察科(业务亮点)条目清除失败！");
        }
        if(dict_jck_bz_resultFlag == false){
            return RestResponse.failure("监察科(备注)条目清除失败！");
        }
        if(dict_xck_resultFlag == false){
            return RestResponse.failure("宣传科条目清除失败！");
        }
        if(dict_xfk_resultFlag == false){
            return RestResponse.failure("信访科条目清除失败！");
        }
        if(dict_zgk_resultFlag == false){
            return RestResponse.failure("政工科条目清除失败！");
        }
        if(dict_ztb_resultFlag == false){
            return RestResponse.failure("渣土办(环保类执法)条目清除失败！");
        }
        if(dict_resultFlag == false){
            return RestResponse.failure("治违办条目清除失败！");
        }
        return RestResponse.success();
    }

    //重置分数
    @PostMapping("resetPoint")
    @ResponseBody
    public RestResponse resetPoint(){
        teamService.resetPoint(20,8,0,0,0,0,2,0,2,6,
                2,10,0,0,0,0,0,0,0,0,0,
                0,0,0,0,0,0,0,0,0,0,0,0,0,2,0);
        teamDcsService.resetPoint(100);
        return RestResponse.success();
    }

    @PostMapping("editPoint_ldpy")
    @SysLog("修改领导评议分数")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldPoint_ldpy",required = false)String oldPoint_ldpy,
                                 @RequestParam(value = "newPoint_ldpy",required = false)String newPoint_ldpy,
                                 @RequestParam(value = "team",required = false)String team){
        //先判断输入的是否为数字
        try {
            Float.parseFloat(newPoint_ldpy);
        } catch (Exception e) {
            return RestResponse.failure("非法字符！ "+e.getMessage());
        }
        Float transPoint = Float.parseFloat(newPoint_ldpy);
        if(transPoint>20){
            return RestResponse.failure("修改失败！领导评议分数不可大于20分");
        }
        teamService.changeTeamPoint_ldpy(team,transPoint);
        return RestResponse.success();
    }

    @GetMapping("zwbPoint")
    @SysLog("跳转治违办中队分数排名页面")
    public String list(){
        return "teamPointList_zwb";
    }

    @PostMapping("zwbPoint")
    @ResponseBody
    public LayerData<Team> list(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_zwb());
        return layerData;
    }

    @GetMapping("historyZwbPoint1")
    @SysLog("跳转治违办中队分数排名页面(23.3季度)")
    public String list_historyZwb1(){
        return "teamPointList_zwb1";
    }

    @PostMapping("historyZwbPoint1")
    @ResponseBody
    public LayerData<Team> list_historyZwb1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyZwb1());
        return layerData;
    }

    @GetMapping("ggbPoint")
    @SysLog("跳转广告办中队分数排名页面")
    public String list_ggb(){
        return "teamPointList_ggb";
    }

    @PostMapping("ggbPoint")
    @ResponseBody
    public LayerData<Team> list_ggb(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_ggb());
        return layerData;
    }

    @GetMapping("historyGgbPoint1")
    @SysLog("跳转广告办中队分数排名(23.3季度)页面")
    public String list_historyGgb1(){
        return "teamPointList_ggb1";
    }

    @PostMapping("historyGgbPoint1")
    @ResponseBody
    public LayerData<Team> list_historyGgb1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyGgb1());
        return layerData;
    }

    @GetMapping("historyGgbPoint2")
    @SysLog("跳转广告办中队分数排名(23.4季度)页面")
    public String list_historyGgb2(){
        return "teamPointList_ggb2";
    }

    @PostMapping("historyGgbPoint2")
    @ResponseBody
    public LayerData<Team> list_historyGgb2(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyGgb2());
        return layerData;
    }



    @GetMapping("ztbPoint")
    @SysLog("跳转渣土办中队分数排名页面")
    public String list_ztb(){
        return "teamPointList_ztb";
    }

    @PostMapping("ztbPoint")
    @ResponseBody
    public LayerData<Team> list_ztb(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_ztb());
        return layerData;
    }

    @GetMapping("dcsPoint")
    @SysLog("跳转指挥中心中队分数排名页面")
    public String list_dcs(){
        return "teamPointList_dcs";
    }

    @PostMapping("dcsPoint")
    @ResponseBody
    public LayerData<Team> list_dcs(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_dcs());
        return layerData;
    }

    @GetMapping("historyDcsPoint1")
    @SysLog("跳转指挥中心中队分数排名页面(23.3季度)")
    public String list_historyDcs1(){
        return "teamPointList_dcs1";
    }

    @PostMapping("historyDcsPoint1")
    @ResponseBody
    public LayerData<Team> list_historyDcs1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyDcs1());
        return layerData;
    }

    @GetMapping("xckPoint")
    @SysLog("跳转宣传科中队分数排名页面")
    public String list_xck(){
        return "teamPointList_xck";
    }

    @PostMapping("xckPoint")
    @ResponseBody
    public LayerData<Team> list_xck(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_xck());
        return layerData;
    }

    @GetMapping("historyXckPoint1")
    @SysLog("跳转宣传科中队分数排名页面")
    public String list_historyXck1(){
        return "teamPointList_xck1";
    }

    @PostMapping("historyXckPoint1")
    @ResponseBody
    public LayerData<Team> list_historyXck1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyXck1());
        return layerData;
    }

    @GetMapping("bgsPoint")
    @SysLog("跳转办公室中队分数排名页面")
    public String list_bgs(){
        return "teamPointList_bgs";
    }

    @PostMapping("bgsPoint")
    @ResponseBody
    public LayerData<Team> list_bgs(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_bgs());
        return layerData;
    }

    @GetMapping("xfkPoint")
    @SysLog("跳转信访科中队分数排名页面")
    public String list_xfk(){
        return "teamPointList_xfk";
    }

    @PostMapping("xfkPoint")
    @ResponseBody
    public LayerData<Team> list_xfk(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_xfk());
        return layerData;
    }

    @GetMapping("historyXfkPoint1")
    @SysLog("跳转信访科中队分数排名(23.3季度)页面")
    public String list_historyXfk1(){
        return "teamPointList_xfk1";
    }

    @PostMapping("historyXfkPoint1")
    @ResponseBody
    public LayerData<Team> list_historyXfk1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyXfk1());
        return layerData;
    }

    @GetMapping("historyXfkPoint2")
    @SysLog("跳转信访科中队分数排名(23.4季度)页面")
    public String list_historyXfk2(){
        return "teamPointList_xfk2";
    }

    @PostMapping("historyXfkPoint2")
    @ResponseBody
    public LayerData<Team> list_historyXfk2(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyXfk2());
        return layerData;
    }

    @GetMapping("zgkPoint")
    @SysLog("跳转政工科中队分数排名页面")
    public String list_zgk(){
        return "teamPointList_zgk";
    }

    @PostMapping("zgkPoint")
    @ResponseBody
    public LayerData<Team> list_zgk(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_zgk());
        return layerData;
    }

    @GetMapping("historyZgkPoint1")
    @SysLog("跳转政工科中队分数排名页面(23.3季度)")
    public String list_historyZgk1(){
        return "teamPointList_zgk1";
    }

    @PostMapping("historyZgkPoint1")
    @ResponseBody
    public LayerData<Team> list_historyZgk1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyZgk1());
        return layerData;
    }

    @GetMapping("fzkPoint")
    @SysLog("跳转法制科中队分数排名页面")
    public String list_fzk(){
        return "teamPointList_fzk";
    }

    @PostMapping("fzkPoint")
    @ResponseBody
    public LayerData<Team> list_fzk(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_fzk());
        return layerData;
    }

    @GetMapping("historyFzkPoint1")
    @SysLog("跳转法制科中队分数排名(23.3季度)页面")
    public String list_historyFzk1(){
        return "teamPointList_fzk1";
    }

    @PostMapping("historyFzkPoint1")
    @ResponseBody
    public LayerData<Team> list_historyFzk1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyFzk1());
        return layerData;
    }

    @GetMapping("jck_srzxPoint")
    @SysLog("跳转监察科(市容秩序)中队分数排名页面")
    public String list_jck_srzx(){
        return "teamPointList_jck_srzx";
    }

    @PostMapping("jck_srzxPoint")
    @ResponseBody
    public LayerData<Team> list_jck_srzx(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_srzx());
        return layerData;
    }

    @GetMapping("jck_historySrzxPoint1")
    @SysLog("跳转监察科(市容秩序)中队分数排名(23.3季度)页面")
    public String list_jck_historySrzx1(){ return "teamPointList_jck_srzx1"; }

    @PostMapping("jck_historySrzxPoint1")
    @ResponseBody
    public LayerData<Team> list_jck_historySrzx1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historySrzx1());
        return layerData;
    }

    @GetMapping("jck_historySrzxPoint2")
    @SysLog("跳转监察科(市容秩序)中队分数排名(23.4季度)页面")
    public String list_jck_historySrzx2(){ return "teamPointList_jck_srzx2"; }

    @PostMapping("jck_historySrzxPoint2")
    @ResponseBody
    public LayerData<Team> list_jck_historySrzx2(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historySrzx2());
        return layerData;
    }

    @GetMapping("jck_hjwsPoint")
    @SysLog("跳转监察科(环境卫生)中队分数排名页面")
    public String list_jck_hjws(){
        return "teamPointList_jck_hjws";
    }

    @PostMapping("jck_hjwsPoint")
    @ResponseBody
    public LayerData<Team> list_jck_hjws(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_hjws());
        return layerData;
    }

    @GetMapping("jck_historyHjwsPoint1")
    @SysLog("跳转监察科(环境卫生)中队分数排名(23.3季度)页面")
    public String list_jck_historyHjws1(){
        return "teamPointList_jck_hjws1";
    }

    @PostMapping("jck_historyHjwsPoint1")
    @ResponseBody
    public LayerData<Team> list_jck_historyHjws1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historyHjws1());
        return layerData;
    }

    @GetMapping("jck_historyHjwsPoint2")
    @SysLog("跳转监察科(环境卫生)中队分数排名(23.4季度)页面")
    public String list_jck_historyHjws2(){
        return "teamPointList_jck_hjws2";
    }

    @PostMapping("jck_historyHjwsPoint2")
    @ResponseBody
    public LayerData<Team> list_jck_historyHjws2(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historyHjws2());
        return layerData;
    }

    @GetMapping("jck_szssPoint")
    @SysLog("跳转监察科(市政设施)中队分数排名页面")
    public String list_jck_szss(){
        return "teamPointList_jck_szss";
    }

    @PostMapping("jck_szssPoint")
    @ResponseBody
    public LayerData<Team> list_jck_szss(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_szss());
        return layerData;
    }

    @GetMapping("jck_historySzssPoint1")
    @SysLog("跳转监察科(市政设施)中队分数排名(23.3季度)页面")
    public String list_jck_historySzss1(){
        return "teamPointList_jck_szss1";
    }

    @PostMapping("jck_historySzssPoint1")
    @ResponseBody
    public LayerData<Team> list_jck_historySzss1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historySzss1());
        return layerData;
    }

    @GetMapping("jck_historySzssPoint2")
    @SysLog("跳转监察科(市政设施)中队分数排名(23.4季度)页面")
    public String list_jck_historySzss2(){
        return "teamPointList_jck_szss2";
    }

    @PostMapping("jck_historySzssPoint2")
    @ResponseBody
    public LayerData<Team> list_jck_historySzss2(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historySzss2());
        return layerData;
    }

    @GetMapping("jck_yllhPoint")
    @SysLog("跳转监察科(园林绿化)中队分数排名页面")
    public String list_jck_yllh(){
        return "teamPointList_jck_yllh";
    }

    @PostMapping("jck_yllhPoint")
    @ResponseBody
    public LayerData<Team> list_jck_yllh(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_yllh());
        return layerData;
    }

    @GetMapping("jck_historyYllhPoint1")
    @SysLog("跳转监察科(园林绿化)中队分数排名(23.3季度)页面")
    public String list_jck_historyYllh1(){
        return "teamPointList_jck_yllh1";
    }

    @PostMapping("jck_historyYllhPoint1")
    @ResponseBody
    public LayerData<Team> list_jck_historyYllh1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historyYllh1());
        return layerData;
    }

    @GetMapping("jck_historyYllhPoint2")
    @SysLog("跳转监察科(园林绿化)中队分数排名(23.4季度)页面")
    public String list_jck_historyYllh2(){
        return "teamPointList_jck_yllh2";
    }

    @PostMapping("jck_historyYllhPoint2")
    @ResponseBody
    public LayerData<Team> list_jck_historyYllh2(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historyYllh2());
        return layerData;
    }
//新增监察科(其他案件)模块

    @GetMapping("jck_qtajPoint")
    @SysLog("跳转监察科(其他案件)中队分数排名页面")
    public String list_jck_qtaj(){
        return "teamPointList_jck_qtaj";
    }

    @PostMapping("jck_qtajPoint")
    @ResponseBody
    public LayerData<Team> list_jck_qtaj(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_qtaj());
        return layerData;
    }

    @GetMapping("jck_historyQtajPoint1")
    @SysLog("跳转监察科(其他案件)中队分数排名页面(23.3季度)")
    public String list_jck_historyQtaj1(){
        return "teamPointList_jck_qtaj1";
    }

    @PostMapping("jck_historyQtajPoint1")
    @ResponseBody
    public LayerData<Team> list_jck_historyQtaj1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historyQtaj1());
        return layerData;
    }

    @GetMapping("jck_historyQtajPoint2")
    @SysLog("跳转监察科(其他案件)中队分数排名页面(23.4季度)")
    public String list_jck_historyQtaj2(){
        return "teamPointList_jck_qtaj2";
    }

    @PostMapping("jck_historyQtajPoint2")
    @ResponseBody
    public LayerData<Team> list_jck_historyQtaj2(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historyQtaj2());
        return layerData;
    }
//新增监察科（业务亮点）模块
    @GetMapping("jck_ywldPoint")
    @SysLog("跳转监察科(业务亮点)中队分数排名页面")
    public String list_jck_ywld(){
        return "teamPointList_jck_ywld";
    }

    @PostMapping("jck_ywldPoint")
    @ResponseBody
    public LayerData<Team> list_jck_ywld(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_ywld());
        return layerData;
    }

    @GetMapping("jck_historyYwldPoint1")
    @SysLog("跳转监察科(业务亮点)中队分数排名(23.3季度)页面")
    public String list_jck_historyYwld1(){
        return "teamPointList_jck_ywld1";
    }

    @PostMapping("jck_historyYwldPoint1")
    @ResponseBody
    public LayerData<Team> list_jck_historyYwld1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historyYwld1());
        return layerData;
    }

    //新增监察科（备注）模块
    @GetMapping("jck_bzPoint")
    @SysLog("跳转监察科(备注)中队分数排名页面")
    public String list_jck_bz(){
        return "teamPointList_jck_bz";
    }

    @PostMapping("jck_bzPoint")
    @ResponseBody
    public LayerData<Team> list_jck_bz(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_bz());
        return layerData;
    }

    @GetMapping("jck_wyzfPoint")
    @SysLog("跳转监察科(物业执法)中队分数排名页面")
    public String list_jck_wyzf(){
        return "teamPointList_jck_wyzf";
    }

    @PostMapping("jck_wyzfPoint")
    @ResponseBody
    public LayerData<Team> list_jck_wyzf(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_wyzf());
        return layerData;
    }

    @GetMapping("jck_historyWyzfPoint1")
    @SysLog("跳转监察科(物业执法)中队分数排名(23.3季度)页面")
    public String list_jck_historyWyzf1(){
        return "teamPointList_jck_wyzf1";
    }

    @PostMapping("jck_historyWyzfPoint1")
    @ResponseBody
    public LayerData<Team> list_jck_historyWyzf1(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_historyWyzf1());
        return layerData;
    }

    @GetMapping("jck_totalPoint")
    @SysLog("跳转监察科(总)中队分数排名页面")
    public String list_jck_total(){
        return "teamPointList_jck_total";
    }

    @PostMapping("jck_totalPoint")
    @ResponseBody
    public LayerData<Team> list_jck_total(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_total());
        return layerData;
    }

    @GetMapping("zhongduiPoint")
    @SysLog("中队账号查看分数,跳转中队分数排名页面")
    public String list_zhongdui(){
        return "admin/system/bgs/zhongduilist";
    }

    @PostMapping("zhongduiPoint")
    @ResponseBody
    public LayerData<Team> list_zhongdui(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_total());
        return layerData;
    }

    @GetMapping("ldpy")
    @SysLog("跳转领导评议中队分数排名页面")
    public String ldpy(){
        return "teamPointList_ldpy";
    }

    @PostMapping("ldpy")
    @ResponseBody
    public LayerData<Team> ldpy(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_total());
        return layerData;
    }

    //大队分数接口，供大屏幕显示数据用
    @GetMapping("platformTeamPoint")
    @ResponseBody
    public LayerData<Team> platformTeamPoint(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_jck_total());
        return layerData;
    }

    @GetMapping("dsjkPoint")
    @SysLog("跳转督察室中队分数排名页面")
    public String list_dsjk(){
        return "teamPointList_dsjk";
    }

    @PostMapping("dsjkPoint")
    @ResponseBody
    public LayerData<Team> list_dsjk(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_dsjk());
        return layerData;
    }

    @GetMapping("historyDsjkPoint1")
    @SysLog("跳转大数据科中队分数排名(23.3季度)页面")
    public String list_historydsjk(){
        return "teamPointList_dsjk1";
    }

    @PostMapping("historyDsjkPoint1")
    @ResponseBody
    public LayerData<Team> list_historydsjk(ServletRequest request){
        LayerData<Team> layerData = new LayerData<>();
        layerData.setData(teamService.queryPoint_historyDsjk1());
        return layerData;
    }

}
