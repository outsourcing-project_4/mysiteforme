package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.*;
import com.mysiteforme.admin.entity.Dict_jck_wyzf;
import com.mysiteforme.admin.entity.Dict_jck_wyzf;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/20.
 * todo:
 * 监察科-物业执法controller
 */
@Controller
@RequestMapping("admin/system/jck_wyzf")
public class JCK_wyzfController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_wyzf> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_wyzf> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_wyzf> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_wyzf> wrapper4 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_wyzf dict_jck_wyzf = dict_jck_wyzfService.selectById(id);
        wrapper1.eq("type3","物管企业处罚立案处罚情况.").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        wrapper2.eq("description","小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        wrapper3.eq("description","清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        wrapper4.eq("description","每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_wyzf.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_jck_wyzf.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_wyzf.setValue(String.valueOf(newValue));
        dict_jck_wyzf.setNumber(number+1);
//        teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_jck_wyzfService.saveOrUpdateDict(dict_jck_wyzf);
        //当增加条数的条例评分内容为“物管企业处罚立案处罚情况.”时，查询所用评分内容为“物管企业处罚立案处罚情况.”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_wyzf.getType3().equals("物管企业处罚立案处罚情况.")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper1);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<1&&(floatSum-newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<1||(floatSum-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper2);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>1){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-1)<0.5&&(floatSum-newValue.subtract(oldValue).floatValue())>1){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-1)<0.5||(floatSum-newValue.subtract(oldValue).floatValue())<1){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 1-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), 1-(floatSum-newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper3);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<0.2&&(floatSum-newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.2||(floatSum-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper4);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-3){
                if((floatSum-newValue.subtract(oldValue).floatValue()<-3)){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数跨度的情况
                }
                if ((floatSum - newValue.subtract(oldValue).floatValue() > -3)) {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())-3);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), -(floatSum-newValue.subtract(oldValue).floatValue())-3);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }
            }else if(floatSum>-3&&floatSum<0) {
                if ((floatSum - newValue.subtract(oldValue).floatValue() <= -3)) {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 3 + floatSum);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), 3 + floatSum);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else if(floatSum - newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), floatSum);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), floatSum);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }
                else{
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }
            }
            else{
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_wyzf> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_wyzf> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_wyzf> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_wyzf> wrapper4 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_wyzf dict_jck_wyzf = dict_jck_wyzfService.selectById(id);
        wrapper1.eq("type3","物管企业处罚立案处罚情况.").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        wrapper2.eq("description","小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        wrapper3.eq("description","清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        wrapper4.eq("description","每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_wyzf.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_jck_wyzf.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_wyzf.setValue(String.valueOf(newValue));
        dict_jck_wyzf.setNumber(number-1);
//        teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
        //当减少条数的条例评分内容为“物管企业处罚立案处罚情况.”时，查询所用评分内容为“物管企业处罚立案处罚情况.”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_wyzf.getType3().equals("物管企业处罚立案处罚情况.")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper1);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<1&&(floatSum+newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum-2)<1||(floatSum+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper2);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>1){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-1)<0.5&&(floatSum+newValue.subtract(oldValue).floatValue())>1){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum-1)<0.5||(floatSum+newValue.subtract(oldValue).floatValue())<1){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(1-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), -(1-(floatSum+newValue.subtract(oldValue).floatValue())));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper3);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<0.2&&(floatSum+newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum-2)<0.2||(floatSum+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        dict_jck_wyzfService.saveOrUpdateDict(dict_jck_wyzf);
        if(dict_jck_wyzf.getDescription().equals("每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper4);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum-newValue.subtract(oldValue).floatValue();
            if(floatSum<=-3){
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-3){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)+newValue.subtract(oldValue).floatValue()>=-3){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 3+(floatSum+newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), 3+(floatSum+newValue.subtract(oldValue).floatValue()));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                } else{
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else if(floatSum>-3&&floatSum<0){
                if(floatSum+newValue.subtract(oldValue).floatValue()>=0){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -floatSum);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), -floatSum);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), newValue.subtract(oldValue).floatValue());
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }
            }
            else if(floatSum>=0) {
                if(floatSum+newValue.subtract(oldValue).floatValue()<=-3){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),-3);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(),-3);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else if ((floatSum+newValue.subtract(oldValue).floatValue() > 0)) {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum+newValue.subtract(oldValue).floatValue() <= 0)) {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), floatSum +  newValue.subtract(oldValue).floatValue());
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                } else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }

            }

        }
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        EntityWrapper<Dict_jck_wyzf> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_wyzf> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_wyzf> wrapper3 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_jck_wyzf dict_jck_wyzf = dict_jck_wyzfService.selectById(id);
        wrapper1.eq("type3","物管企业处罚立案处罚情况.").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        wrapper2.eq("description","小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        wrapper3.eq("description","清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_wyzf> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("description","每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_jck_wyzfService.deleteDict(id);
        if(dict_jck_wyzf.getType3().equals("物管企业处罚立案处罚情况.")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper1);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_wyzf.getValue());
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<1&&floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum-2)<1||Float.parseFloat(dict_jck_wyzf.getValue())>1)&&(floatSum-Float.parseFloat(dict_jck_wyzf.getValue()))<2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), floatSum-Float.parseFloat(dict_jck_wyzf.getValue())-2);
                    teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), floatSum-Float.parseFloat(dict_jck_wyzf.getValue())-2);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                } else{
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper2);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_wyzf.getValue());
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<0.5&&floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>1){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum-1)<0.5||Float.parseFloat(dict_jck_wyzf.getValue())>0.5)&&(floatSum-Float.parseFloat(dict_jck_wyzf.getValue()))<1){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), floatSum-Float.parseFloat(dict_jck_wyzf.getValue())-1);
                    teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), floatSum-Float.parseFloat(dict_jck_wyzf.getValue())-1);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                } else{
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper3);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_wyzf.getValue());
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.2分变化的情况
                if((floatSum-2)<0.2&&floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum-2)<0.2||Float.parseFloat(dict_jck_wyzf.getValue())>0.2)&&(floatSum-Float.parseFloat(dict_jck_wyzf.getValue()))<2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), floatSum-Float.parseFloat(dict_jck_wyzf.getValue())-2);
                    teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), floatSum-Float.parseFloat(dict_jck_wyzf.getValue())-2);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                } else{
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper4);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            //删除之前的和
            float floatSum=(float)sum+Float.parseFloat(dict_jck_wyzf.getValue());
            if(floatSum<=-3){
                if(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>=0){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),3);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(),3);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else if(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())<=-3){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数跨度情况
                }else if((floatSum)-Float.parseFloat(dict_jck_wyzf.getValue())>=-3){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 3+(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())));
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), 3+(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                } else{
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else if(floatSum>-3&&floatSum<0){
                if(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())<=-3){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(3+floatSum));
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), -(3+floatSum));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else if(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>=0){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -floatSum);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), -floatSum);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), -Float.parseFloat(dict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/jck_wyzf/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_jck_wyzf> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                    @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                    ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_wyzf> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_wyzf> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_wyzf> dataPage = dict_jck_wyzfService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_jck_wyzf> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                         ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_wyzf> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_wyzf> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_wyzf> dataPage = dict_jck_wyzfService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_wyzf/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_wyzf/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_wyzf/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_jck_wyzf dict_jck_wyzf){
        EntityWrapper<Dict_jck_wyzf> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","物管企业处罚立案处罚情况.").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_wyzf> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("description","小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_wyzf> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("description","清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_wyzf> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("description","每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(StringUtils.isBlank(dict_jck_wyzf.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_jck_wyzf.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_wyzfService.getCountByType(dict_jck_wyzf.getType())==0){
            dict_jck_wyzf.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_jck_wyzf.setSort(dict_jck_wyzfService.getMaxSortByType(dict_jck_wyzf.getType()));
        }
        dict_jck_wyzfService.saveOrUpdateDict(dict_jck_wyzf);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()));
        //当评分内容为“物管企业处罚立案处罚情况.”时，查询所用评分内容为“小广物管企业处罚立案处罚情况.告”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_wyzf.getType3().equals("物管企业处罚立案处罚情况.")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper1);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<1&&floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<1){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())));
                    teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper2);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>1){
                //帮下一个判定条件过滤分数在1到1.5分变化的情况
                if((floatSum-1)<0.5&&floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>1){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从0.*跨度到1.*等情况
                }else if((floatSum-1)<0.5){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 1-(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())));
                    teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), 1-(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper3);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum-2)<0.2&&floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())));
                    teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                    
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper4);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
//            float point=(float)1.4;
            if(floatSum<=-3){
                if(floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>=0){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),-3);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(),-3);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                } else if((floatSum-Float.parseFloat(dict_jck_wyzf.getValue())<-3)){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-Float.parseFloat(dict_jck_wyzf.getValue())>=-3)){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(floatSum-Float.parseFloat(dict_jck_wyzf.getValue()))-3);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), -(floatSum-Float.parseFloat(dict_jck_wyzf.getValue()))-3);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else{
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }

        }
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_jck_wyzf dict_jck_wyzf = dict_jck_wyzfService.selectById(id);
        model.addAttribute("dict_jck_wyzf",dict_jck_wyzf);
        return "admin/system/jck_wyzf/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_jck_wyzf dict_jck_wyzf){
        EntityWrapper<Dict_jck_wyzf> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","物管企业处罚立案处罚情况.").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_wyzf> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("description","小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_wyzf> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("description","清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_wyzf> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("description","每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分").eq("label",dict_jck_wyzf.getLabel()).eq("visable_flag",1);
        //指定的误差范围
        double error=1E-6;
        if(dict_jck_wyzf.getId()==null || dict_jck_wyzf.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_jck_wyzf.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_jck_wyzf.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_jck_wyzf.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_wyzf.getSort() == null || dict_jck_wyzf.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_jck_wyzf oldDict_jck_wyzf = dict_jck_wyzfService.selectById(dict_jck_wyzf.getId());
        if(!oldDict_jck_wyzf.getType().equals(dict_jck_wyzf.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_jck_wyzf.getLabel().equals(dict_jck_wyzf.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_jck_wyzf.getValue().equals(dict_jck_wyzf.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_jck_wyzfService.saveOrUpdateDict(dict_jck_wyzf);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
        if(dict_jck_wyzf.getType3().equals("物管企业处罚立案处罚情况.")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper1);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
            if(floatSum>2){
                //处理分数从1.*跨度到2.*等情况
                if(Float.parseFloat(dict_jck_wyzf.getValue())>Float.parseFloat(oldDict_jck_wyzf.getValue())&&oldfloatSum<2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()))));
                    teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()))));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else  {teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }
            ////处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_wyzf.getValue())<Float.parseFloat(oldDict_jck_wyzf.getValue())&&oldfloatSum>2) {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_wyzf.getValue())-Float.parseFloat(oldDict_jck_wyzf.getValue())))));
                teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_wyzf.getValue())-Float.parseFloat(oldDict_jck_wyzf.getValue())))));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }else{
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_wgqy(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("小区内清理圈绿、毁绿的每户加 0.5 分（加到1分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper2);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
            if(floatSum>1){
                //处理分数从0.*跨度到1.*等情况
                if(Float.parseFloat(dict_jck_wyzf.getValue())>Float.parseFloat(oldDict_jck_wyzf.getValue())&&oldfloatSum<1){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 1-(floatSum-(Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()))));
                    teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), 1-(floatSum-(Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()))));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else  {teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }
            ////处理分数从1.*跨度到0.*等情况
            else if(Float.parseFloat(dict_jck_wyzf.getValue())<Float.parseFloat(oldDict_jck_wyzf.getValue())&&oldfloatSum>1) {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(1-(oldfloatSum+(Float.parseFloat(dict_jck_wyzf.getValue())-Float.parseFloat(oldDict_jck_wyzf.getValue())))));
                teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), -(1-(oldfloatSum+(Float.parseFloat(dict_jck_wyzf.getValue())-Float.parseFloat(oldDict_jck_wyzf.getValue())))));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }else{
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_qlhl(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("清理地桩地锁、乱堆乱放、乱搭乱建的每户加 0.2 分（加到 2 分为止）")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper3);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
            if(floatSum>2){
                //处理分数从1.*跨度到2.*等情况
                if(Float.parseFloat(dict_jck_wyzf.getValue())>Float.parseFloat(oldDict_jck_wyzf.getValue())&&oldfloatSum<2){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()))));
                    teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()))));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else  {teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }
            ////处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_wyzf.getValue())<Float.parseFloat(oldDict_jck_wyzf.getValue())&&oldfloatSum>2) {
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_wyzf.getValue())-Float.parseFloat(oldDict_jck_wyzf.getValue())))));
                teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_wyzf.getValue())-Float.parseFloat(oldDict_jck_wyzf.getValue())))));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }else{
                teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
                teamService.changeTeamPoint_jck_wyzf_ldlf(dict_jck_wyzf.getLabel(), Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue()));
                updateTeamPoint(dict_jck_wyzf.getLabel(), error);
            }
        }
        if(dict_jck_wyzf.getDescription().equals("每发现 1 起圈绿、毁绿、地桩地锁、乱堆乱放的扣 0.1 分")){
            List<Dict_jck_wyzf> Dict_jck_wyzf_List= dict_jck_wyzfService.selectList(wrapper4);
            double sum=Dict_jck_wyzf_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float differ=Float.parseFloat(dict_jck_wyzf.getValue()) - Float.parseFloat(oldDict_jck_wyzf.getValue());
            if(floatSum<=-3){
                if(floatSum-differ>=0){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),-3);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(),-3);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                } else if((floatSum-differ<-3)){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(),0);
                    //处理分数跨度的情况
                }else if((floatSum-differ>=-3)){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(floatSum-differ)-3);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), -(floatSum-differ)-3);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }else if(floatSum>-3&&floatSum<0) {
                if ((floatSum - differ <= -3)) {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 3+floatSum );
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), 3+floatSum );
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }else if(floatSum - differ>=0){
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), floatSum);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), floatSum);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }
                else{
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), differ);
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), differ);
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }
            }
            else if(floatSum>=0) {
                if ((floatSum - differ > 0)) {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                    //处理分数跨度情况
                } else if ((floatSum - differ <= 0)) {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), -(floatSum - differ));
                    teamService.changeTeamPoint_jck_wyzf_wyxq(dict_jck_wyzf.getLabel(), -(floatSum - differ));
                    updateTeamPoint(dict_jck_wyzf.getLabel(), error);
                }
                else {
                    teamService.changeTeamPoint_jck_wyzf(dict_jck_wyzf.getLabel(), 0);
                }
            }
        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }

    //监察科物业执法大分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_jck_wyzf> platformJck_WyzfList(@RequestParam ("label") String label,
                                                         @RequestParam(value = "page",defaultValue = "1")Integer page,
                                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                                         ServletRequest request){
        if(limit==null){
            return null;
        };
        LayerData<Dict_jck_wyzf> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_wyzf> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0wrapper.eq("label", label);
        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_jck_wyzf> dataPage = dict_jck_wyzfService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
    //消除浮点型数做加减造成的误差
    private void updateTeamPoint(String label, double error) {
        Team team = teamService.queryTeamPoint_jck_wyzf(label);
        double value = team.getPoint_jck_wyzf();
        double value1 = team.getPoint_jck_wyzf_wgqy();
        double value2 = team.getPoint_jck_wyzf_qlhl();
        double value3 = team.getPoint_jck_wyzf_ldlf();
        double value4 = team.getPoint_jck_wyzf_wyxq();
        if (Math.abs(value) < error) {
            value = 0.0;
        }
        if (Math.abs(value1) < error) {
            value1 = 0.0;
        }
        if (Math.abs(value2) < error) {
            value2 = 0.0;
        }
        if (Math.abs(value3) < error) {
            value3 = 0.0;
        }
        if (Math.abs(value4) < error) {
            value4 = 0.0;
        }
        teamService.fixdict_jck_wyzfPointError(label, value);
        teamService.fixdict_jck_wyzf_wgqyPointError(label, value1);
        teamService.fixdict_jck_wyzf_qlhlPointError(label, value2);
        teamService.fixdict_jck_wyzf_ldlfPointError(label, value3);
        teamService.fixdict_jck_wyzf_wyxqPointError(label, value4);
    }


}
