package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.Dict_jck_hjws;
import com.mysiteforme.admin.entity.Dict_jck_srzx;
import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;


import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by lsd on 2021/12/20.
 * todo:
 * 监察科-环境卫生(已经更名为监察科-市容环卫)controller
 */
@Controller
@RequestMapping("admin/system/jck_hjws")
public class JCK_hjwsController extends BaseController {
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id", required = false) Long id) {
        EntityWrapper<Dict_jck_hjws> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_hjws> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_hjws> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_hjws> wrapper4 = new EntityWrapper<>();
        //对“垃圾分类立案处罚情况”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_hjws> wrapper5 = new EntityWrapper<>();
        if (id == null || id == 0) {
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_hjws dict_jck_hjws = dict_jck_hjwsService.selectById(id);
        wrapper1.eq("type3","占路经营立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","小广告立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","卫生责任区立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","垃圾分类立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper5.eq("type3","垃圾分类立案处罚情况").eq("description","每月完成1起简易程序案件得0.2分，（加到2分为止）").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_hjws.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_jck_hjws.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_hjws.setValue(String.valueOf(newValue));
        dict_jck_hjws.setNumber(number + 1);
//        teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_jck_hjwsService.saveOrUpdateDict(dict_jck_hjws);
        //当增加条数的条例评分内容为“占路经营立案处罚情况”时，查询所用评分内容为“占路经营立案处罚情况”的条例总分是否高于4分。当超过4分时对分数进行限制，超过4分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("占路经营立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper1);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到4.5分变化的情况
                if((floatSum-4)<0.5&&(floatSum-newValue.subtract(oldValue).floatValue())>4){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从3.*跨度到4.*等情况
                }else if((floatSum-4)<0.5||(floatSum-newValue.subtract(oldValue).floatValue())<4){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 4-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), 4-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“小广告立案处罚情况”时，查询所用评分内容为“小广告立案处罚情况”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("小广告立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper2);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&(floatSum-newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.5||(floatSum-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当增加条数的条例评分内容为“卫生责任区立案处罚情况”时，查询所用评分内容为“卫生责任区立案处罚情况”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("卫生责任区立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper3);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&floatSum-newValue.subtract(oldValue).floatValue()>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.5||(floatSum-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), 2-(floatSum-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当评分内容为“垃圾分类立案处罚情况”时，查询所用评分内容为“垃圾分类立案处罚情况”且为“简易程序案件得分”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起简易程序案件得0.2分，（加到2分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            //先查看“简易程序案件得分”是否达到上限，若达到则执行以下语句不加分。
            if(floatSum2>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum2-2)<0.2&&floatSum2-newValue.subtract(oldValue).floatValue()>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum2-2)<0.2||(floatSum2-newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 2-(floatSum2-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 2-(floatSum2-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否达到上限，若达到则执行以下语句不加分，若未达到才会正常加分
            else if (floatSum1>3){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum1-3)<1&&floatSum1-newValue.subtract(oldValue).floatValue()>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到3.*等情况
                }else if((floatSum1-3)<1||(floatSum1-newValue.subtract(oldValue).floatValue())<3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 3-(floatSum1-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 3-(floatSum1-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                //正常加分
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当评分内容为“垃圾分类立案处罚情况”时，查询所用评分内容为“垃圾分类立案处罚情况”的条例总分是否高于3分。当超过3分时对分数进行限制，超过3分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起普通程序案件，得1分，（加到3分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            if(floatSum2>2&&floatSum1-floatSum2<=1){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }else if(floatSum1>3){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum1-3)<1&&floatSum1-newValue.subtract(oldValue).floatValue()>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到3.*等情况
                }else if((floatSum1-3)<1||(floatSum1-newValue.subtract(oldValue).floatValue())<3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 3-(floatSum1-newValue.subtract(oldValue).floatValue()));
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 3-(floatSum1-newValue.subtract(oldValue).floatValue()));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id", required = false) Long id) {
        EntityWrapper<Dict_jck_hjws> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_hjws> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_hjws> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_hjws> wrapper4 = new EntityWrapper<>();
        //对“垃圾分类立案处罚情况”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_hjws> wrapper5 = new EntityWrapper<>();
        if (id == null || id == 0) {
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_hjws dict_jck_hjws = dict_jck_hjwsService.selectById(id);
        wrapper1.eq("type3","占路经营立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","小广告立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","卫生责任区立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","垃圾分类立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper5.eq("type3","垃圾分类立案处罚情况").eq("description","每月完成1起简易程序案件得0.2分，（加到2分为止）").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        Integer number = dict_jck_hjws.getNumber();
        if (number == 1) {
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_jck_hjws.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_hjws.setValue(String.valueOf(newValue));
        dict_jck_hjws.setNumber(number - 1);
//        teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
        //当减少条数的条例评分内容为“占路经营立案处罚情况”时，查询所用评分内容为“占路经营立案处罚情况”的条例总分是否高于4分。当超过4分时对分数进行限制，超过4分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("占路经营立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper1);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到4.5分变化的情况
                if((floatSum-4)<0.5&&(floatSum+newValue.subtract(oldValue).floatValue())>4){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从4.*跨度到3.*等情况
                }else if((floatSum-4)<0.5||(floatSum+newValue.subtract(oldValue).floatValue())<4){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(4-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), -(4-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“小广告立案处罚情况”时，查询所用评分内容为“小广告立案处罚情况”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("小广告立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper2);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&(floatSum+newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum-2)<0.5||(floatSum+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数的条例评分内容为“卫生责任区立案处罚情况”时，查询所用评分内容为“卫生责任区立案处罚情况”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("卫生责任区立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper3);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&(floatSum+newValue.subtract(oldValue).floatValue())>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum-2)<0.5||(floatSum+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), -(2-(floatSum+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数内容为“垃圾分类立案处罚情况”时，查询所用评分内容为“垃圾分类立案处罚情况”且为“简易程序案件得分”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例减少时进行相应操作
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起简易程序案件得0.2分，（加到2分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            //先查看“简易程序案件得分”是否低于上限，若低于则执行以下语句减分。
            if(floatSum2>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum2-2)<0.2&&floatSum2+newValue.subtract(oldValue).floatValue()>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到1.*等情况
                }else if((floatSum2-2)<0.2||(floatSum2+newValue.subtract(oldValue).floatValue())<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(2-(floatSum2+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -(2-(floatSum2+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否高于上限变为低于上限，若低于则执行以下语句减分，若一直低于才会正常扣分
            else if (floatSum1>3){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum1-3)<1&&floatSum1+newValue.subtract(oldValue).floatValue()>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从3.*跨度到2.*等情况
                }else if((floatSum1-3)<1||(floatSum1+newValue.subtract(oldValue).floatValue())<3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(3-(floatSum1+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -(3-(floatSum1+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                //正常加分
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        //当减少条数评分内容为“垃圾分类立案处罚情况”时，查询所用评分内容为“垃圾分类立案处罚情况”的条例总分是否高于3分。当超过3分时对分数进行限制，超过3分的条例减少时进行相应操作
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起普通程序案件，得1分，（加到3分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            if(floatSum2>2&&floatSum1-floatSum2<=1){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }else if(floatSum1>3){
                //帮下一个判定条件过滤分数在3到2分变化的情况
                if((floatSum1-3)<1&&floatSum1+newValue.subtract(oldValue).floatValue()>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从3.*跨度到2.*等情况
                }else if((floatSum1-3)<1||(floatSum1+newValue.subtract(oldValue).floatValue())<3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(3-(floatSum1+newValue.subtract(oldValue).floatValue())));
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -(3-(floatSum1+newValue.subtract(oldValue).floatValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), newValue.subtract(oldValue).floatValue());
            }
        }
        dict_jck_hjwsService.saveOrUpdateDict(dict_jck_hjws);
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id", required = false) Long id) {
        EntityWrapper<Dict_jck_hjws> wrapper1 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_hjws> wrapper2 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_hjws> wrapper3 = new EntityWrapper<>();
        EntityWrapper<Dict_jck_hjws> wrapper4 = new EntityWrapper<>();
        //对“垃圾分类立案处罚情况”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_hjws> wrapper5 = new EntityWrapper<>();
        //对“垃圾分类立案处罚情况”条例进行查询用于限制分数超过上限时的处理(用于限制普通程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_hjws> wrapper6 = new EntityWrapper<>();
        //指定的误差范围
        double error=1E-6;
        if (id == null || id == 0) {
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_jck_hjws dict_jck_hjws = dict_jck_hjwsService.selectById(id);
        dict_jck_hjwsService.deleteDict(id);
        wrapper1.eq("type3","占路经营立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper2.eq("type3","小广告立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper3.eq("type3","卫生责任区立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper4.eq("type3","垃圾分类立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper5.eq("type3","垃圾分类立案处罚情况").eq("description","每月完成1起简易程序案件得0.2分，（加到2分为止）").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        wrapper6.eq("type3","垃圾分类立案处罚情况").eq("description","每月完成1起普通程序案件，得1分，（加到3分为止）").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
//        teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
        if(dict_jck_hjws.getType3().equals("占路经营立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper1);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_hjws.getValue());
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到4.5分变化的情况
                if((floatSum-4)<0.5&&floatSum-Float.parseFloat(dict_jck_hjws.getValue())>4){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从4.*跨度到3.*的情况
                }else if(((floatSum-4)<0.5||Float.parseFloat(dict_jck_hjws.getValue())>0.5)&&(floatSum-Float.parseFloat(dict_jck_hjws.getValue()))<4){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), floatSum-Float.parseFloat(dict_jck_hjws.getValue())-4);
                    teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), floatSum-Float.parseFloat(dict_jck_hjws.getValue())-4);
                    updateTeamPoint(dict_jck_hjws.getLabel(),error);
                }else{
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                updateTeamPoint(dict_jck_hjws.getLabel(),error);
            }
        }
        if(dict_jck_hjws.getType3().equals("小广告立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper2);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_hjws.getValue());
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&floatSum-Float.parseFloat(dict_jck_hjws.getValue())>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum-2)<0.5||Float.parseFloat(dict_jck_hjws.getValue())>0.5)&&(floatSum-Float.parseFloat(dict_jck_hjws.getValue()))<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), floatSum-Float.parseFloat(dict_jck_hjws.getValue())-2);
                    teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), floatSum-Float.parseFloat(dict_jck_hjws.getValue())-2);
                    updateTeamPoint(dict_jck_hjws.getLabel(),error);
                } else{
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                updateTeamPoint(dict_jck_hjws.getLabel(),error);
            }
        }
        if(dict_jck_hjws.getType3().equals("卫生责任区立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper3);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum+Float.parseFloat(dict_jck_hjws.getValue());
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&floatSum-Float.parseFloat(dict_jck_hjws.getValue())>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum-2)<0.5||Float.parseFloat(dict_jck_hjws.getValue())>0.5)&&(floatSum-Float.parseFloat(dict_jck_hjws.getValue()))<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), floatSum-Float.parseFloat(dict_jck_hjws.getValue())-2);
                    teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), floatSum-Float.parseFloat(dict_jck_hjws.getValue())-2);
                    updateTeamPoint(dict_jck_hjws.getLabel(),error);
                } else{
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                updateTeamPoint(dict_jck_hjws.getLabel(),error);
            }
        }
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起简易程序案件得0.2分，（加到2分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1+Float.parseFloat(dict_jck_hjws.getValue());
            float floatSum2=(float)sum2+Float.parseFloat(dict_jck_hjws.getValue());
            //先查看“简易程序案件得分”是否达到上限，若达到则执行以下语句不扣分。
            if(floatSum2>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum2-2)<0.2&&floatSum2-Float.parseFloat(dict_jck_hjws.getValue())>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到1.*的情况
                }else if(((floatSum2-2)<0.2||Float.parseFloat(dict_jck_hjws.getValue())>0.2)&&(floatSum2-Float.parseFloat(dict_jck_hjws.getValue()))<2&&(floatSum1-Float.parseFloat(dict_jck_hjws.getValue()))<3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), floatSum2-Float.parseFloat(dict_jck_hjws.getValue())-2);
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), floatSum2-Float.parseFloat(dict_jck_hjws.getValue())-2);
                    updateTeamPoint(dict_jck_hjws.getLabel(),error);
                }
                else{
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否达到上限，若达到则执行以下语句不扣分，若未达到才会正常扣分
            else if (floatSum1>3){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum1-3)<1&&floatSum1-Float.parseFloat(dict_jck_hjws.getValue())>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从3.*跨度到2.*等情况
                }else if(((floatSum1-3)<1||Float.parseFloat(dict_jck_hjws.getValue())>1)&&(floatSum1-Float.parseFloat(dict_jck_hjws.getValue()))<3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), floatSum1-Float.parseFloat(dict_jck_hjws.getValue())-3);
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), floatSum1-Float.parseFloat(dict_jck_hjws.getValue())-3);
                    updateTeamPoint(dict_jck_hjws.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                //正常扣分
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                updateTeamPoint(dict_jck_hjws.getLabel(),error);
            }
        }
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起普通程序案件，得1分，（加到3分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            List<Dict_jck_hjws> Dict_jck_hjws_List3= dict_jck_hjwsService.selectList(wrapper6);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum3=Dict_jck_hjws_List3.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1+Float.parseFloat(dict_jck_hjws.getValue());
            float newfloatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            float floatSum3=(float)sum3+Float.parseFloat(dict_jck_hjws.getValue());
            float newfloatSum3=(float)sum3;
            if(floatSum2>2&&(floatSum1-floatSum2)==1){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                updateTeamPoint(dict_jck_hjws.getLabel(),error);
            }else if(floatSum2>2&&newfloatSum3<1){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(3-(2+newfloatSum3)));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -(3-(2+newfloatSum3)));
                updateTeamPoint(dict_jck_hjws.getLabel(),error);
            }
            else if(floatSum1>3){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum1-3)<1&&floatSum1-Float.parseFloat(dict_jck_hjws.getValue())>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从3.*跨度到2.*等情况
                }else if(((floatSum1-3)<1||Float.parseFloat(dict_jck_hjws.getValue())>1)&&(floatSum1-Float.parseFloat(dict_jck_hjws.getValue()))<3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), floatSum1-Float.parseFloat(dict_jck_hjws.getValue())-3);
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), floatSum1-Float.parseFloat(dict_jck_hjws.getValue())-3);
                    updateTeamPoint(dict_jck_hjws.getLabel(),error);
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                //正常扣分
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -Float.parseFloat(dict_jck_hjws.getValue()));
                updateTeamPoint(dict_jck_hjws.getLabel(),error);
            }
        }
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list() {
        return "admin/system/jck_hjws/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_jck_hjws> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                         @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                         ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_hjws> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_hjws> wrapper = new EntityWrapper<>();
        if (!map.isEmpty()) {
            String type = (String) map.get("type");
            if (StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String) map.get("label");
            if (StringUtils.isNotBlank(label)) {
                //依据部分或全部中队名称 模糊查询可见
                wrapper.like("label", label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if (StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)) {
                wrapper.between("create_date", date_start, date_end);
            }
        }
        wrapper.orderBy("type", false).orderBy("sort", false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_hjws> dataPage = dict_jck_hjwsService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_jck_hjws> historylist(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                         @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                         ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_hjws> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_hjws> wrapper = new EntityWrapper<>();
        if (!map.isEmpty()) {
            String type = (String) map.get("type");
            if (StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String) map.get("label");
            if (StringUtils.isNotBlank(label)) {
                //依据部分或全部中队名称 模糊查询可见
                wrapper.like("label", label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if (StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)) {
                wrapper.between("create_date", date_start, date_end);
            }
        }
        wrapper.orderBy("type", false).orderBy("sort", false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_hjws> dataPage = dict_jck_hjwsService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type", required = false) String type, Model model) {
        if (StringUtils.isNotBlank(type)) {
            model.addAttribute("type", type);
        }
        return "admin/system/jck_hjws/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type", required = false) String type, Model model) {
        if (StringUtils.isNotBlank(type)) {
            model.addAttribute("type", type);
        }
        return "admin/system/jck_hjws/history";
    }

    @GetMapping("zdList")
    public String zdList(@RequestParam(value = "type", required = false) String type, Model model) {
        if (StringUtils.isNotBlank(type)) {
            model.addAttribute("type", type);
        }
        return "admin/system/jck_hjws/zdlist";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type", required = false) String type, Model model) {
        if (StringUtils.isNotBlank(type)) {
            model.addAttribute("type", type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_jck_hjws dict_jck_hjws) {
        EntityWrapper<Dict_jck_hjws> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","占路经营立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_hjws> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type3","小广告立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_hjws> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type3","卫生责任区立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        //对“垃圾分类立案处罚情况”条例进行查询用于限制分数超过上限时的处理
        EntityWrapper<Dict_jck_hjws> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("type3","垃圾分类立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        //对“垃圾分类立案处罚情况”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_hjws> wrapper5 = new EntityWrapper<>();
        wrapper5.eq("type3","垃圾分类立案处罚情况").eq("description","每月完成1起简易程序案件得0.2分，（加到2分为止）").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        if (StringUtils.isBlank(dict_jck_hjws.getType())) {
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if (StringUtils.isBlank(dict_jck_hjws.getValue())) {
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if (dict_jck_hjwsService.getCountByType(dict_jck_hjws.getType()) == 0) {
            dict_jck_hjws.setSort(0);
        } else {
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_jck_hjws.setSort(dict_jck_hjwsService.getMaxSortByType(dict_jck_hjws.getType()));
        }
        dict_jck_hjwsService.saveOrUpdateDict(dict_jck_hjws);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
//        teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
        //当评分内容为“占路经营立案处罚情况”时，查询所用评分内容为“占路经营立案处罚情况”的条例总分是否高于4分。当超过4分时对分数进行限制，超过4分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("占路经营立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper1);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到4.5分变化的情况
                if((floatSum-4)<0.5&&floatSum-Float.parseFloat(dict_jck_hjws.getValue())>4){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从3.*跨度到4.*等情况
                }else if((floatSum-4)<0.5){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 4-(floatSum-Float.parseFloat(dict_jck_hjws.getValue())));
                    teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), 4-(floatSum-Float.parseFloat(dict_jck_hjws.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
            }
        }
        //当评分内容为“小广告”时，查询所用评分内容为“小广告”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("小广告立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper2);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&floatSum-Float.parseFloat(dict_jck_hjws.getValue())>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.5){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_hjws.getValue())));
                    teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_hjws.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
            }
        }
        //当评分内容为“卫生责任区立案处罚情况”时，查询所用评分内容为“卫生责任区立案处罚情况”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("卫生责任区立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper3);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            if(floatSum>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum-2)<0.5&&floatSum-Float.parseFloat(dict_jck_hjws.getValue())>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum-2)<0.5){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_hjws.getValue())));
                    teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), 2-(floatSum-Float.parseFloat(dict_jck_hjws.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
            }
        }

        //当评分内容为“垃圾分类立案处罚情况”时，查询所用评分内容为“垃圾分类立案处罚情况”且为“简易程序案件得分”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起简易程序案件得0.2分，（加到2分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            //先查看“简易程序案件得分”是否达到上限，若达到则执行以下语句不加分。
            if(floatSum2>2){
                //帮下一个判定条件过滤分数在2到2.5分变化的情况
                if((floatSum2-2)<0.2&&floatSum2-Float.parseFloat(dict_jck_hjws.getValue())>2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if((floatSum2-2)<0.2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 2-(floatSum2-Float.parseFloat(dict_jck_hjws.getValue())));
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 2-(floatSum2-Float.parseFloat(dict_jck_hjws.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否达到上限，若达到则执行以下语句不加分，若未达到才会正常加分
            else if (floatSum1>3){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum1-3)<1&&floatSum1-Float.parseFloat(dict_jck_hjws.getValue())>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到3.*等情况
                }else if((floatSum1-3)<1){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 3-(floatSum1-Float.parseFloat(dict_jck_hjws.getValue())));
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 3-(floatSum1-Float.parseFloat(dict_jck_hjws.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                //正常加分
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
            }
        }
        //当评分内容为“垃圾分类立案处罚情况”时，查询所用评分内容为“垃圾分类立案处罚情况”的条例总分是否高于3分。当超过3分时对分数进行限制，超过3分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起普通程序案件，得1分，（加到3分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            if(floatSum2>2&&floatSum1-floatSum2<=1){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
            }else if(floatSum1>3){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum1-3)<1&&floatSum1-Float.parseFloat(dict_jck_hjws.getValue())>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到3.*等情况
                }else if((floatSum1-3)<1){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 3-(floatSum1-Float.parseFloat(dict_jck_hjws.getValue())));
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 3-(floatSum1-Float.parseFloat(dict_jck_hjws.getValue())));
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()));
            }
        }

        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id, Model model) {
        Dict_jck_hjws dict_jck_hjws = dict_jck_hjwsService.selectById(id);
        model.addAttribute("dict_jck_hjws", dict_jck_hjws);
        return "admin/system/jck_hjws/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_jck_hjws dict_jck_hjws) {
        EntityWrapper<Dict_jck_hjws> wrapper1 = new EntityWrapper<>();
        wrapper1.eq("type3","占路经营立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_hjws> wrapper2 = new EntityWrapper<>();
        wrapper2.eq("type3","小广告立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        EntityWrapper<Dict_jck_hjws> wrapper3 = new EntityWrapper<>();
        wrapper3.eq("type3","卫生责任区立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        //对“垃圾分类立案处罚情况”条例进行查询用于限制分数超过上限时的处理
        EntityWrapper<Dict_jck_hjws> wrapper4 = new EntityWrapper<>();
        wrapper4.eq("type3","垃圾分类立案处罚情况").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        //对“垃圾分类立案处罚情况”条例进行查询用于限制分数超过上限时的处理(用于限制简易程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_hjws> wrapper5 = new EntityWrapper<>();
        wrapper5.eq("type3","垃圾分类立案处罚情况").eq("description","每月完成1起简易程序案件得0.2分，（加到2分为止）").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        //对“垃圾分类立案处罚情况”条例进行查询用于限制分数超过上限时的处理(用于限制普通程序案件分数超过上限时的处理)
        EntityWrapper<Dict_jck_hjws> wrapper6 = new EntityWrapper<>();
        wrapper6.eq("type3","垃圾分类立案处罚情况").eq("description","每月完成1起普通程序案件，得1分，（加到3分为止）").eq("label",dict_jck_hjws.getLabel()).eq("visable_flag",1);
        if (dict_jck_hjws.getId() == null || dict_jck_hjws.getId() == 0) {
            return RestResponse.failure("字典ID不能为空");
        }
        if (StringUtils.isBlank(dict_jck_hjws.getType())) {
            return RestResponse.failure("字典类型不能为空");
        }
        if (StringUtils.isBlank(dict_jck_hjws.getLabel())) {
            return RestResponse.failure("字典标签不能为空");
        }
        if (StringUtils.isBlank(dict_jck_hjws.getValue())) {
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if (dict_jck_hjws.getSort() == null || dict_jck_hjws.getSort() < 0) {
            return RestResponse.failure("排序值不正确");
        }
        Dict_jck_hjws oldDict_jck_hjws = dict_jck_hjwsService.selectById(dict_jck_hjws.getId());
        if (!oldDict_jck_hjws.getType().equals(dict_jck_hjws.getType())) {
            return RestResponse.failure("字典类型不能修改");
        }
        if (!oldDict_jck_hjws.getLabel().equals(dict_jck_hjws.getLabel())) {
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if (!oldDict_jck_hjws.getValue().equals(dict_jck_hjws.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_jck_hjwsService.saveOrUpdateDict(dict_jck_hjws);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
//        teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
        if(dict_jck_hjws.getType3().equals("占路经营立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper1);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            if(floatSum>4){
                //帮下一个判定条件过滤分数在4到4.5分变化的情况
                 if(Float.parseFloat(dict_jck_hjws.getValue())>Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum<4){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 4-(floatSum-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()))));
                     teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), 4-(floatSum-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()))));
                }else  {teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }
            //处理分数从4.*跨度到3.*等情况
            else if(Float.parseFloat(dict_jck_hjws.getValue())<Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum>4) {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(4-(oldfloatSum+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
                teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), -(4-(oldfloatSum+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
            }else{
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_zljy(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            }
        }

        if(dict_jck_hjws.getType3().equals("小广告立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper2);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            if(floatSum>2){
                //处理分数从1.*跨度到2.*等情况
                 if(Float.parseFloat(dict_jck_hjws.getValue())>Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()))));
                     teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()))));
                }else  {teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }
            ////处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_hjws.getValue())<Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum>2) {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
                teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
            }else{
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_xgg(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            }
        }

        if(dict_jck_hjws.getType3().equals("卫生责任区立案处罚情况")){
            List<Dict_jck_hjws> Dict_jck_hjws_List= dict_jck_hjwsService.selectList(wrapper3);
            double sum=Dict_jck_hjws_List.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum=(float)sum;
            float oldfloatSum=floatSum-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            if(floatSum>2){
                //处理分数从1.*跨度到2.*等情况
                 if(Float.parseFloat(dict_jck_hjws.getValue())>Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()))));
                    teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), 2-(floatSum-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()))));
                }else  {teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }
            //处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_hjws.getValue())<Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum>2) {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
                teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), -(2-(oldfloatSum+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
            }else{
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            }
        }
        //当评分内容为“垃圾分类立案处罚情况”时，查询所用评分内容为“垃圾分类立案处罚情况”且为“简易程序案件得分”的条例总分是否高于2分。当超过2分时对分数进行限制，超过2分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起简易程序案件得0.2分，（加到2分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            float oldfloatSum1=floatSum1-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            float oldfloatSum2=floatSum2-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            //先查看“简易程序案件得分”是否达到上限，若达到则执行以下语句不加分。
            if(floatSum2>2){
                //总上限达到三分，“简单程序案件得分”再超过2分上限不做改变
                if(floatSum1>3&&floatSum1-floatSum2!=1&&oldfloatSum2>=2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从1.*跨度到2.*等情况
                }else if(Float.parseFloat(dict_jck_hjws.getValue())>Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum2<2){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 2-oldfloatSum2);
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 2-oldfloatSum2);
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }//处理分数从2.*跨度到1.*等情况
            else if(Float.parseFloat(dict_jck_hjws.getValue())<Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum2>2&&floatSum1<3){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(2-(oldfloatSum2+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -(2-(oldfloatSum2+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
            }
            //执行以下语句则代表“简易程序案件得分”未达到上限，此条件查看垃圾分类条例的总得分是否达到上限，若达到则执行以下语句不加分，若未达到才会正常加分
            else if (floatSum1>3){
                //帮下一个判定条件过滤分数在2到3分变化的情况
                if((floatSum1-3)<1&&oldfloatSum1>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到3.*等情况
                }else if(Float.parseFloat(dict_jck_hjws.getValue())>Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum1<3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 3-oldfloatSum1);
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 3-oldfloatSum1);
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }//处理分数从3.*跨度到2.*等情况
            else if(Float.parseFloat(dict_jck_hjws.getValue())<Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum1>3){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(3-(oldfloatSum1+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -(3-(oldfloatSum1+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
            }else {
                //正常加减分
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            }
        }
        //当评分内容为“垃圾分类立案处罚情况”时，查询所用评分内容为“垃圾分类立案处罚情况”的条例总分是否高于3分。当超过3分时对分数进行限制，超过3分的条例增加都不会导致分数增加
        if(dict_jck_hjws.getType3().equals("垃圾分类立案处罚情况")&&dict_jck_hjws.getDescription().equals("每月完成1起普通程序案件，得1分，（加到3分为止）")){
            List<Dict_jck_hjws> Dict_jck_hjws_List1= dict_jck_hjwsService.selectList(wrapper4);
            List<Dict_jck_hjws> Dict_jck_hjws_List2= dict_jck_hjwsService.selectList(wrapper5);
            List<Dict_jck_hjws> Dict_jck_hjws_List3= dict_jck_hjwsService.selectList(wrapper6);
            double sum1=Dict_jck_hjws_List1.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum2=Dict_jck_hjws_List2.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            double sum3=Dict_jck_hjws_List3.stream().mapToDouble(dict-> Double.parseDouble(dict.getValue())).sum();
            float floatSum1=(float)sum1;
            float floatSum2=(float)sum2;
            float floatSum3=(float)sum3;
            float oldfloatSum1=floatSum1-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            float oldfloatSum2=floatSum2-(Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            if(floatSum2>2&&floatSum1-floatSum2<=1&&oldfloatSum1-floatSum1<=1&&oldfloatSum1-floatSum1>0&&Float.parseFloat(dict_jck_hjws.getValue())<Float.parseFloat(oldDict_jck_hjws.getValue())){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), floatSum1-oldfloatSum1);
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), floatSum1-oldfloatSum1);
            }
            else if(floatSum2>2&&floatSum1-floatSum2<=1&&floatSum1-oldfloatSum1<=1&&floatSum1-oldfloatSum1>0&&Float.parseFloat(dict_jck_hjws.getValue())>Float.parseFloat(oldDict_jck_hjws.getValue())){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), floatSum1-oldfloatSum1);
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), floatSum1-oldfloatSum1);
            }
            else if(floatSum2>2&&floatSum1-floatSum2<=1&&Float.parseFloat(dict_jck_hjws.getValue())<Float.parseFloat(oldDict_jck_hjws.getValue())){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(3-(2+floatSum1-floatSum2)));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -(3-(2+floatSum1-floatSum2)));
            }else if(floatSum2>2&&oldfloatSum1-floatSum2<=1&&Float.parseFloat(dict_jck_hjws.getValue())>Float.parseFloat(oldDict_jck_hjws.getValue())){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 3-(2+oldfloatSum1-floatSum2));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 3-(2+oldfloatSum1-floatSum2));
            }
            else if(floatSum1>3){
                //帮下一个判定条件过滤分数在3到4分变化的情况
                if((floatSum1-3)<1&&oldfloatSum1>3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
                    //处理分数从2.*跨度到3.*等情况
                }else if(Float.parseFloat(dict_jck_hjws.getValue())>Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum1<3){
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 3-oldfloatSum1);
                    teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), 3-oldfloatSum1);
                }else {
                    teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), 0);
                }
            }//应对从3分以上降低到3分的情况，这种情况分数应该不变
            else if(floatSum1==3&&oldfloatSum1>3){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(),0);
            }
            //处理分数从3.*跨度到2.*等情况
            else if(Float.parseFloat(dict_jck_hjws.getValue())<Float.parseFloat(oldDict_jck_hjws.getValue())&&oldfloatSum1>3){
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), -(3-(oldfloatSum1+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), -(3-(oldfloatSum1+(Float.parseFloat(dict_jck_hjws.getValue())-Float.parseFloat(oldDict_jck_hjws.getValue())))));
            }
            else {
                teamService.changeTeamPoint_jck_hjws(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
                teamService.changeTeamPoint_jck_hjws_ljfl(dict_jck_hjws.getLabel(), Float.parseFloat(dict_jck_hjws.getValue()) - Float.parseFloat(oldDict_jck_hjws.getValue()));
            }
        }
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value = "oldType", required = false) String oldType,
                                 @RequestParam(value = "newType", required = false) String newType) {
        if (StringUtils.isBlank(oldType)) {
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)) {
            return RestResponse.failure("新类型不能为空");
        }
        if (oldType.equals(newType)) {
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if (dictService.getCountByType(newType) > 0) {
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType, newType);
        return RestResponse.success();
    }

    //监察科市容环卫大分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_jck_hjws> platformJck_HjwsList(
            @RequestParam ("label") String label, @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit,
            ServletRequest request) {
        if(limit==null){
            return null;
        };
        LayerData<Dict_jck_hjws> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_hjws> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type", false).orderBy("sort", false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0

        wrapper.setSqlSelect("id", "value", "label", "type", "type2", "type3", "description", "number");
        Page<Dict_jck_hjws> dataPage = dict_jck_hjwsService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }
    //消除浮点型数做加减造成的误差
    private void updateTeamPoint(String label, double error) {
        Team team=teamService.queryTeamPoint_jck_hjws(label);
        //消除删除条例分数归零浮点型产生的误差
        double value=team.getPoint_jck_hjws();
        double value1=team.getPoint_jck_hjws_zljy();
        double value2=team.getPoint_jck_hjws_xgg();
        double value3=team.getPoint_jck_hjws_ws();
        double value4=team.getPoint_jck_hjws_ljfl();
        if(Math.abs(value)<error){
            value=0.0;
        }
        if(Math.abs(value1)<error){
            value1=0.0;
        }
        if(Math.abs(value2)<error){
            value2=0.0;
        }
        if(Math.abs(value3)<error){
            value3=0.0;
        }
        if(Math.abs(value4)<error){
            value4=0.0;
        }
        teamService.fixdict_jck_hjwsPointError(label,value);
        teamService.fixdict_jck_hjws_zljyPointError(label,value1);
        teamService.fixdict_jck_hjws_xggPointError(label,value2);
        teamService.fixdict_jck_hjws_wsPointError(label,value3);
        teamService.fixdict_jck_hjws_ljflPointError(label,value4);
    }

}
