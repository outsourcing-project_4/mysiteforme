package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.Dict_ztb;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by lsd on 2021/12/12.
 * todo:
 * 广告办controller
 */
@Controller
@RequestMapping("admin/system/ztb")
public class ZTBController extends BaseController {
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id", required = false) Long id) {
        if (id == null || id == 0) {
            return RestResponse.failure("字典ID错误");
        }
        Dict_ztb dict_ztb = dict_ztbService.selectById(id);
        Integer number = dict_ztb.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_ztb.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_ztb.setValue(String.valueOf(newValue));
        dict_ztb.setNumber(number + 1);
        teamService.changeTeamPoint_ztb(dict_ztb.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_ztbService.saveOrUpdateDict(dict_ztb);
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id", required = false) Long id) {
        if (id == null || id == 0) {
            return RestResponse.failure("字典ID错误");
        }
        Dict_ztb dict_ztb = dict_ztbService.selectById(id);
        Integer number = dict_ztb.getNumber();
        if (number == 1) {
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_ztb.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_ztb.setValue(String.valueOf(newValue));
        dict_ztb.setNumber(number - 1);
        teamService.changeTeamPoint_ztb(dict_ztb.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_ztbService.saveOrUpdateDict(dict_ztb);
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id", required = false) Long id) {
        if (id == null || id == 0) {
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_ztb dict_ztb = dict_ztbService.selectById(id);
        teamService.changeTeamPoint_ztb(dict_ztb.getLabel(), -Float.parseFloat(dict_ztb.getValue()));
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_ztbService.deleteDict(id);
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list() {
        return "admin/system/ztb/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_ztb> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                    ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_ztb> layerData = new LayerData<>();
        EntityWrapper<Dict_ztb> wrapper = new EntityWrapper<>();
        if (!map.isEmpty()) {
            String type = (String) map.get("type");
            if (StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String) map.get("label");
            if (StringUtils.isNotBlank(label)) {
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label", label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if (StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)) {
                wrapper.between("create_date", date_start, date_end);
            }
        }
        wrapper.orderBy("type", false).orderBy("sort", false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_ztb> dataPage = dict_ztbService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type", required = false) String type, Model model) {
        if (StringUtils.isNotBlank(type)) {
            model.addAttribute("type", type);
        }
        return "admin/system/ztb/add";
    }

    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type", required = false) String type, Model model) {
        if (StringUtils.isNotBlank(type)) {
            model.addAttribute("type", type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_ztb dict_ztb) {
        if (StringUtils.isBlank(dict_ztb.getType())) {
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if (StringUtils.isBlank(dict_ztb.getValue())) {
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if (dict_ztbService.getCountByType(dict_ztb.getType()) == 0) {
            dict_ztb.setSort(0);
        } else {
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_ztb.setSort(dict_ztbService.getMaxSortByType(dict_ztb.getType()));
        }
        dict_ztbService.saveOrUpdateDict(dict_ztb);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
        teamService.changeTeamPoint_ztb(dict_ztb.getLabel(), Float.parseFloat(dict_ztb.getValue()));
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id, Model model) {
        Dict_ztb dict_ztb = dict_ztbService.selectById(id);
        model.addAttribute("dict_ztb", dict_ztb);
        return "admin/system/ztb/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_ztb dict_ztb) {
        if (dict_ztb.getId() == null || dict_ztb.getId() == 0) {
            return RestResponse.failure("字典ID不能为空");
        }
        if (StringUtils.isBlank(dict_ztb.getType())) {
            return RestResponse.failure("字典类型不能为空");
        }
        if (StringUtils.isBlank(dict_ztb.getLabel())) {
            return RestResponse.failure("字典标签不能为空");
        }
        if (StringUtils.isBlank(dict_ztb.getValue())) {
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if (dict_ztb.getSort() == null || dict_ztb.getSort() < 0) {
            return RestResponse.failure("排序值不正确");
        }
        Dict_ztb oldDict_ztb = dict_ztbService.selectById(dict_ztb.getId());
        if (!oldDict_ztb.getType().equals(dict_ztb.getType())) {
            return RestResponse.failure("字典类型不能修改");
        }
        if (!oldDict_ztb.getLabel().equals(dict_ztb.getLabel())) {
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if (!oldDict_ztb.getValue().equals(dict_ztb.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_ztbService.saveOrUpdateDict(dict_ztb);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
        teamService.changeTeamPoint_ztb(dict_ztb.getLabel(), Float.parseFloat(dict_ztb.getValue()) - Float.parseFloat(oldDict_ztb.getValue()));
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value = "oldType", required = false) String oldType,
                                 @RequestParam(value = "newType", required = false) String newType) {
        if (StringUtils.isBlank(oldType)) {
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)) {
            return RestResponse.failure("新类型不能为空");
        }
        if (oldType.equals(newType)) {
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if (dictService.getCountByType(newType) > 0) {
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType, newType);
        return RestResponse.success();
    }

    //渣土办环保类执法大分数接口分数接口，供大屏幕显示数据用
    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_ztb> platformJck_ZtbList(@RequestParam("label") String label,
                                                   @RequestParam(value = "page", defaultValue = "1") Integer page,
                                                   @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                   ServletRequest request) {
        if(limit==null){
            return null;
        };
        LayerData<Dict_ztb> layerData = new LayerData<>();
        EntityWrapper<Dict_ztb> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type", false).orderBy("sort", false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0

        wrapper.setSqlSelect("id", "value", "label", "type", "type2", "type3", "description", "number");
        Page<Dict_ztb> dataPage = dict_ztbService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

}
