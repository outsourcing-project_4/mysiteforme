package com.mysiteforme.admin.controller.system;

import com.mysiteforme.admin.entity.Dict;
import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.entity.VO.ItemVO;
import com.mysiteforme.admin.service.ItemVoService;
import com.mysiteforme.admin.service.TeamService;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("item")
public class ItemVOController {
    private static final Log log = LogFactory.get();

    @Autowired
    private ItemVoService itemVoService;
    @Autowired
    private TeamService teamService;

/*
    @RequiresPermissions("sys:rescource:list")
*/
    //查询评价项目
    @GetMapping("queryType")
    public Map<String,Object> queryItemVO(String user){
        //建立map.put一一映射关系
        Map<String,Object> map = new HashMap<>();
        try {
            List<ItemVO> list;
            // 查询产品（添加时），将需要添加的产品做一个下拉框
            switch(user){
                case "zwb" :
                    list = itemVoService.queryList_zwb();//查询
                    break;
                case "ggb" :
                    list = itemVoService.queryList_ggb();//查询
                    break;
                case "ztb" :
                    list = itemVoService.queryList_ztb();//查询
                    break;
                case "dcs" :
                    list = itemVoService.queryList_fws();//查询
                    break;
                case "dcs2" :
                    list = itemVoService.queryList_fws2();//查询
                    break;
                case "xck" :
                    list = itemVoService.queryList_xck();//查询
                    break;
                case "bgs" :
                    list = itemVoService.queryList_bgs();//查询
                    break;
                case "xfk" :
                    list = itemVoService.queryList_xfk();//查询
                    break;
                case "zgk" :
                    list = itemVoService.queryList_zgk();//查询
                    break;
                case "fzk" :
                    list = itemVoService.queryList_fzk();//查询
                    break;
                case "jck_srzx" :
                    list = itemVoService.queryList_jck_srzx();//查询
                    break;
                case "jck_hjws" :
                    list = itemVoService.queryList_jck_hjws();//查询
                    break;
                case "jck_szss" :
                    list = itemVoService.queryList_jck_szss();//查询
                    break;
                case "jck_yllh" :
                    list = itemVoService.queryList_jck_yllh();//查询
                    break;
                case "jck_qtaj" :
                    list = itemVoService.queryList_jck_qtaj();//查询
                    break;
                case "jck_ywld" :
                    list = itemVoService.queryList_jck_ywld();//查询
                    break;
                case "jck_bz" :
                    list = itemVoService.queryList_jck_bz();//查询
                    break;
                case "jck_wyzf" :
                    list = itemVoService.queryList_jck_wyzf();//查询
                    break;
                case "dsjk" :
                    list = itemVoService.queryList_dsjk();//查询
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + user);
            }
            map.put("success",true);
            map.put("msg","查询数据库中的字典成功！");
            map.put("type",list);
        }catch (Exception e){
            log.error("查询数据库字典程序报错",e);
            map.put("success",false);
            map.put("msg","查询数据库中的字典失败！");
            map.put("type",null);
        }
        return map;
    }

    //查询类别
    @GetMapping("queryType2")
    public Map<String,Object> queryType2(String user){
        //建立map.put一一映射关系
        Map<String,Object> mapType2 = new HashMap<>();
        try {
            List<ItemVO> list;
            // 查询产品（添加时），将需要添加的产品做一个下拉框
            switch(user){
                case "zwb" :
                    list = itemVoService.queryLeibie_zwb();//查询
                    break;
                case "ggb" :
                    list = itemVoService.queryLeibie_ggb();//查询
                    break;
                case "ztb" :
                    list = itemVoService.queryLeibie_ztb();//查询
                    break;
                case "dcs" :
                    list = itemVoService.queryLeibie_fws();//查询
                    break;
                case "dcs2" :
                    list = itemVoService.queryLeibie_fws2();//查询
                    break;
                case "xck" :
                    list = itemVoService.queryLeibie_xck();//查询
                    break;
                case "bgs" :
                    list = itemVoService.queryLeibie_bgs();//查询
                    break;
                case "xfk" :
                    list = itemVoService.queryLeibie_xfk();//查询
                    break;
                case "zgk" :
                    list = itemVoService.queryLeibie_zgk();//查询
                    break;
                case "fzk" :
                    list = itemVoService.queryLeibie_fzk();//查询
                    break;
                case "jck_srzx" :
                    list = itemVoService.queryLeibie_jck_srzx();//查询
                    break;
                case "jck_hjws" :
                    list = itemVoService.queryLeibie_jck_hjws();//查询
                    break;
                case "jck_szss" :
                    list = itemVoService.queryLeibie_jck_szss();//查询
                    break;
                case "jck_yllh" :
                    list = itemVoService.queryLeibie_jck_yllh();//查询
                    break;
                case "jck_qtaj" :
                    list = itemVoService.queryLeibie_jck_qtaj();//查询
                    break;
                case "jck_ywld" :
                    list = itemVoService.queryLeibie_jck_ywld();//查询
                    break;
                case "jck_bz" :
                    list = itemVoService.queryLeibie_jck_bz();//查询
                    break;
                case "jck_wyzf" :
                    list = itemVoService.queryLeibie_jck_wyzf();//查询
                    break;
                case "dsjk" :
                    list = itemVoService.queryLeibie_dsjk();//查询
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + user);
            }
            mapType2.put("success",true);
            mapType2.put("msg","查询数据库中的字典成功！");
            mapType2.put("type2",list);
        }catch (Exception e){
            log.error("查询数据库字典程序报错",e);
            mapType2.put("success",false);
            mapType2.put("msg","查询数据库中的字典失败！");
            mapType2.put("type2",null);
        }
        return mapType2;
    }

    //查询评分内容
    @GetMapping("queryType3")
    public Map<String,Object> queryType3(String law_item_leibie , String user){
        //建立map.put一一映射关系
        Map<String,Object> mapType3 = new HashMap<>();
        try {
            List<ItemVO> list;
            // 查询产品（添加时），将需要添加的产品做一个下拉框
            switch(user){
                case "zwb" :
                    list = itemVoService.queryPingfen_zwb(law_item_leibie);//查询
                    break;
                case "ggb" :
                    list = itemVoService.queryPingfen_ggb(law_item_leibie);//查询
                    break;
                case "ztb" :
                    list = itemVoService.queryPingfen_ztb(law_item_leibie);//查询
                    break;
                case "dcs" :
                    list = itemVoService.queryPingfen_fws(law_item_leibie);//查询
                    break;
                case "dcs2" :
                    list = itemVoService.queryPingfen_fws2(law_item_leibie);//查询
                    break;
                case "xck" :
                    list = itemVoService.queryPingfen_xck(law_item_leibie);//查询
                    break;
                case "bgs" :
                    list = itemVoService.queryPingfen_bgs(law_item_leibie);//查询
                    break;
                case "xfk" :
                    list = itemVoService.queryPingfen_xfk(law_item_leibie);//查询
                    break;
                case "zgk" :
                    list = itemVoService.queryPingfen_zgk(law_item_leibie);//查询
                    break;
                case "fzk" :
                    list = itemVoService.queryPingfen_fzk(law_item_leibie);//查询
                    break;
                case "jck_srzx" :
                    list = itemVoService.queryPingfen_jck_srzx(law_item_leibie);//查询
                    break;
                case "jck_hjws" :
                    list = itemVoService.queryPingfen_jck_hjws(law_item_leibie);//查询
                    break;
                case "jck_szss" :
                    list = itemVoService.queryPingfen_jck_szss(law_item_leibie);//查询
                    break;
                case "jck_yllh" :
                    list = itemVoService.queryPingfen_jck_yllh(law_item_leibie);//查询
                    break;
                case "jck_qtaj" :
                    list = itemVoService.queryPingfen_jck_qtaj(law_item_leibie);//查询
                    break;
                case "jck_ywld" :
                    list = itemVoService.queryPingfen_jck_ywld(law_item_leibie);//查询
                    break;
                case "jck_bz" :
                    list = itemVoService.queryPingfen_jck_bz(law_item_leibie);//查询
                    break;
                case "jck_wyzf" :
                    list = itemVoService.queryPingfen_jck_wyzf(law_item_leibie);//查询
                    break;
                case "dsjk" :
                    list = itemVoService.queryPingfen_dsjk(law_item_leibie);//查询
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + user);
            }
            mapType3.put("success",true);
            mapType3.put("msg","查询数据库中的字典成功！");
            mapType3.put("type3",list);
        }catch (Exception e){
            log.error("查询数据库字典程序报错",e);
            mapType3.put("success",false);
            mapType3.put("msg","查询数据库中的字典失败！");
            mapType3.put("type3",null);
        }
        return mapType3;
    }

    //查询中队
    @GetMapping("queryTeam")
    public Map<String,Object> queryTeam(){
        //建立map.put一一映射关系
        Map<String,Object> mapTeam = new HashMap<>();
        try {
            // 查询产品（添加时），将需要添加的产品做一个下拉框
            List<String> list = teamService.queryTeam();//查询
            mapTeam.put("success",true);
            mapTeam.put("msg","查询数据库中的字典成功！");
            mapTeam.put("team",list);
        }catch (Exception e){
            log.error("查询数据库字典程序报错",e);
            mapTeam.put("success",false);
            mapTeam.put("msg","查询数据库中的字典失败！");
            mapTeam.put("team",null);
        }
        return mapTeam;
    }

    //查询评分标准
    @GetMapping("queryDescription")
    public Map<String,Object> queryItemVODescription(String law_item_pingfen , String user){
        Map<String,Object> map = new HashMap<>();
        try {
            List<ItemVO> list;
            // 查询产品（添加时），将需要添加的产品做一个下拉框
            switch(user){
                case "zwb" :
                    list = itemVoService.queryList_description_zwb(law_item_pingfen);//查询
                    break;
                case "ggb" :
                    list = itemVoService.queryList_description_ggb(law_item_pingfen);//查询
                    break;
                case "ztb" :
                    list = itemVoService.queryList_description_ztb(law_item_pingfen);//查询
                    break;
                case "dcs" :
                    list = itemVoService.queryList_description_fws(law_item_pingfen);//查询
                    break;
                case "dcs2" :
                    list = itemVoService.queryList_description_fws2(law_item_pingfen);//查询
                    break;
                case "xck" :
                    list = itemVoService.queryList_description_xck(law_item_pingfen);//查询
                    break;
                case "bgs" :
                    list = itemVoService.queryList_description_bgs(law_item_pingfen);//查询
                    break;
                case "xfk" :
                    list = itemVoService.queryList_description_xfk(law_item_pingfen);//查询
                    break;
                case "zgk" :
                    list = itemVoService.queryList_description_zgk(law_item_pingfen);//查询
                    break;
                case "fzk" :
                    list = itemVoService.queryList_description_fzk(law_item_pingfen);//查询
                    break;
                case "jck_srzx" :
                    list = itemVoService.queryList_description_jck_srzx(law_item_pingfen);//查询
                    break;
                case "jck_hjws" :
                    list = itemVoService.queryList_description_jck_hjws(law_item_pingfen);//查询
                    break;
                case "jck_szss" :
                    list = itemVoService.queryList_description_jck_szss(law_item_pingfen);//查询
                    break;
                case "jck_yllh" :
                    list = itemVoService.queryList_description_jck_yllh(law_item_pingfen);//查询
                    break;
                case "jck_qtaj" :
                    list = itemVoService.queryList_description_jck_qtaj(law_item_pingfen);//查询
                    break;
                case "jck_ywld" :
                    list = itemVoService.queryList_description_jck_ywld(law_item_pingfen);//查询
                    break;
                case "jck_bz" :
                    list = itemVoService.queryList_description_jck_bz(law_item_pingfen);//查询
                    break;
                case "jck_wyzf" :
                    list = itemVoService.queryList_description_jck_wyzf(law_item_pingfen);//查询
                    break;
                case "dsjk" :
                    list = itemVoService.queryList_description_dsjk(law_item_pingfen);//查询
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + user);
            }
            map.put("success",true);
            map.put("msg","查询数据库中的字典成功！");
            map.put("description",list);
        }catch (Exception e){
            log.error("查询数据库字典程序报错",e);
            map.put("success",false);
            map.put("msg","查询数据库中的字典失败！");
            map.put("description",null);
        }
        return map;
    }

    //查询分数
    @GetMapping("queryPoint")
    public Map<String,Object> queryPoint(String description , String user){
        Map<String,Object> map = new HashMap<>();
        try {
            List<ItemVO> list;
            // 查询考核加扣分
            switch(user){
                case "zwb" :
                    list = itemVoService.queryPoint_zwb(description);
                    break;
                case "ggb" :
                    list = itemVoService.queryPoint_ggb(description);
                    break;
                case "ztb" :
                    list = itemVoService.queryPoint_ztb(description);
                    break;
                case "dcs" :
                    list = itemVoService.queryPoint_fws(description);
                    break;
                case "dcs2" :
                    list = itemVoService.queryPoint_fws2(description);
                    break;
                case "xck" :
                    list = itemVoService.queryPoint_xck(description);
                    break;
                case "bgs" :
                    list = itemVoService.queryPoint_bgs(description);
                    break;
                case "xfk" :
                    list = itemVoService.queryPoint_xfk(description);
                    break;
                case "zgk" :
                    list = itemVoService.queryPoint_zgk(description);
                    break;
                case "fzk" :
                    list = itemVoService.queryPoint_fzk(description);
                    break;
                case "jck_srzx" :
                    list = itemVoService.queryPoint_jck_srzx(description);//查询
                    break;
                case "jck_hjws" :
                    list = itemVoService.queryPoint_jck_hjws(description);//查询
                    break;
                case "jck_szss" :
                    list = itemVoService.queryPoint_jck_szss(description);//查询
                    break;
                case "jck_yllh" :
                    list = itemVoService.queryPoint_jck_yllh(description);//查询
                    break;
                case "jck_qtaj" :
                    list = itemVoService.queryPoint_jck_qtaj(description);//查询
                    break;
                case "jck_ywld" :
                    list = itemVoService.queryPoint_jck_ywld(description);//查询
                    break;
                case "jck_bz" :
                    list = itemVoService.queryPoint_jck_bz(description);//查询
                    break;
                case "jck_wyzf" :
                    list = itemVoService.queryPoint_jck_wyzf(description);//查询
                    break;
                case "dsjk" :
                    list = itemVoService.queryPoint_dsjk(description);//查询
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + user);
            }
            map.put("success",true);
            map.put("msg","查询数据库中的加/扣分成功！");
            map.put("point",list);
        }catch (Exception e){
            log.error("查询数据库加/扣分报错",e);
            map.put("success",false);
            map.put("msg","查询数据库中的加/扣分失败！");
            map.put("point",null);
        }
        return map;
    }

    //    更改考核分数
    @GetMapping("changePoint")
    public Map<String,Object> changePoint(String type,String description,String point){
        Map<String,Object> map = new HashMap<>();
        try {
            // 查询考核加扣分
            Integer num = itemVoService.changePoint_zwb(type,description,point);
            if(num>0) {
                map.put("success", true);
                map.put("msg", "查询数据库中的加/扣分成功！");
            }else {
                map.put("msg", "分数无需更改！");
            }
        }catch (Exception e){
            log.error("查询数据库加/扣分报错",e);
            map.put("success",false);
            map.put("msg","查询数据库中的加/扣分失败！");
            /*map.put("ItemVO",null);*/
        }
        return map;
    }
}
