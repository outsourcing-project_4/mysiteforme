package com.mysiteforme.admin.controller.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.config.annotation.SysLog;
import com.mysiteforme.admin.base.BaseController;
import com.mysiteforme.admin.entity.Dict_jck_bz;
import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.xiaoleilu.hutool.log.Log;
import com.xiaoleilu.hutool.log.LogFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by wcl on 2023/8/21.
 * todo:
 * 监察科-备注controller
 */
@Controller
@RequestMapping("admin/system/jck_bz")
public class JCK_bzController extends BaseController{
    private static final Log log = LogFactory.get();

    //考核条目数量加一按键
    @PostMapping("addOneById")
    @ResponseBody
    public RestResponse addOneById(@RequestParam(value = "id",required = false)Long id){
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_bz dict_jck_bz = dict_jck_bzService.selectById(id);
        Integer number = dict_jck_bz.getNumber();
        BigDecimal oldValue = new BigDecimal(dict_jck_bz.getValue());
        BigDecimal newValue = oldValue.add(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_bz.setValue(String.valueOf(newValue));
        dict_jck_bz.setNumber(number+1);
        teamService.changeTeamPoint_jck_bz(dict_jck_bz.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_jck_bzService.saveOrUpdateDict(dict_jck_bz);
        return RestResponse.success();
    }

    //考核条目数量减一按键
    @PostMapping("minusOneById")
    @ResponseBody
    public RestResponse minusOneById(@RequestParam(value = "id",required = false)Long id){
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        Dict_jck_bz dict_jck_bz = dict_jck_bzService.selectById(id);
        Integer number = dict_jck_bz.getNumber();
        if(number==1){
            return RestResponse.failure("条目数不可少于1!");
        }
        BigDecimal oldValue = new BigDecimal(dict_jck_bz.getValue());
        BigDecimal newValue = oldValue.subtract(oldValue.divide(BigDecimal.valueOf(number)));
        dict_jck_bz.setValue(String.valueOf(newValue));
        dict_jck_bz.setNumber(number-1);
        teamService.changeTeamPoint_jck_bz(dict_jck_bz.getLabel(), newValue.subtract(oldValue).floatValue());
        dict_jck_bzService.saveOrUpdateDict(dict_jck_bz);
        return RestResponse.success();
    }

    @PostMapping("deleteById")
    @ResponseBody
    public RestResponse deleteById(@RequestParam(value = "id",required = false)Long id){
        //指定的误差范围
        double error=1E-6;
        if(id == null || id == 0){
            return RestResponse.failure("字典ID错误");
        }
        //根据条目的id先拿到条目的dict对象，再读取dict对象的分数属性，最后将所扣除或加上的分数再加上或扣除给相应中队
        Dict_jck_bz dict_jck_bz = dict_jck_bzService.selectById(id);
        teamService.changeTeamPoint_jck_bz(dict_jck_bz.getLabel(), -Float.parseFloat(dict_jck_bz.getValue()));
        Team team=teamService.queryTeamPoint_jck_bz(dict_jck_bz.getLabel());
//                if(team.getPoint_jck_bz()<0) {
//                    teamService.zeroTeamPoint_jck_bz(dict_jck_bz.getLabel());
//                }
        //消除删除条例分数归零浮点型产生的误差
        double value=team.getPoint_jck_bz();
        if(Math.abs(value)<error){
            value=0.0;
        }
        teamService.fixdict_jck_bzPointError(dict_jck_bz.getLabel(),value);
        //执行完分数反还操作后才可以执行删除条目列表中相关条目
        dict_jck_bzService.deleteDict(id);
        return RestResponse.success();
    }

    @GetMapping("list")
    @SysLog("跳转系统字典页面")
    public String list(){
        return "admin/system/jck_bz/list";
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("list")
    @ResponseBody
    public LayerData<Dict_jck_bz> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                         ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_bz> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_bz> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_bz> dataPage = dict_jck_bzService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @RequiresPermissions("sys:dict:list")
    @PostMapping("historylist")
    @ResponseBody
    public LayerData<Dict_jck_bz> historylist(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                       @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                       ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<Dict_jck_bz> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_bz> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
            String type = (String) map.get("type");
            if(StringUtils.isNotBlank(type)) {
                wrapper.eq("type", type);
            }
            String label = (String)map.get("label");
            if(StringUtils.isNotBlank(label)){
                //依据部分或全部中队名称 模糊查询
                wrapper.like("label",label);
            }
            String date_start = (String) map.get("date_start");
            String date_end = (String) map.get("date_end");
            if(StringUtils.isNotBlank(date_start) && StringUtils.isNotBlank(date_end)){
                wrapper.between("create_date",date_start,date_end);
            }
        }
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 0);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0
        Page<Dict_jck_bz> dataPage = dict_jck_bzService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }

    @GetMapping("add")
    public String add(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_bz/add";
    }

    @GetMapping("history")
    public String history(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/jck_bz/history";
    }


    //此为更改条目页面、此按钮前端暂未启用、以下代码也未修改
    @GetMapping("change")
    public String changeScore(@RequestParam(value = "type",required = false)String type,Model model){
        if(StringUtils.isNotBlank(type)){
            model.addAttribute("type",type);
        }
        return "admin/system/dict/change";
    }

    @RequiresPermissions("sys:dict:add")
    @PostMapping("add")
    @SysLog("新增系统字典")
    @ResponseBody
    public RestResponse add(Dict_jck_bz dict_jck_bz){
        if(StringUtils.isBlank(dict_jck_bz.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
//        if(StringUtils.isBlank(dict.getLabel())){
//            return RestResponse.failure("字典标签不能为空");
//        }
        if(StringUtils.isBlank(dict_jck_bz.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_bzService.getCountByType(dict_jck_bz.getType())==0){
            dict_jck_bz.setSort(0);
        }else{
            /*为了防止同一个人添加不进去 先注释*/
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label值【"+dict.getLabel()+"】");
//            }
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),dict.getValue())>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的label标签【"+dict.getLabel()+"】的value值【"+dict.getValue()+"】");
//            }
            dict_jck_bz.setSort(dict_jck_bzService.getMaxSortByType(dict_jck_bz.getType()));
        }
        dict_jck_bzService.saveOrUpdateDict(dict_jck_bz);
        //根据dict的信息 扣除相应中队在治违办部门的的分数
        teamService.changeTeamPoint_jck_bz(dict_jck_bz.getLabel(), Float.parseFloat(dict_jck_bz.getValue()));
        return RestResponse.success();
    }

    @GetMapping("edit")
    public String edit(Long id,Model model){
        Dict_jck_bz dict_jck_bz = dict_jck_bzService.selectById(id);
        model.addAttribute("dict_jck_bz",dict_jck_bz);
        return "admin/system/jck_bz/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @PostMapping("edit")
    @SysLog("编辑系统字典")
    @ResponseBody
    public RestResponse edit(Dict_jck_bz dict_jck_bz){
        if(dict_jck_bz.getId()==null || dict_jck_bz.getId() == 0){
            return RestResponse.failure("字典ID不能为空");
        }
        if(StringUtils.isBlank(dict_jck_bz.getType())){
            return RestResponse.failure("字典类型不能为空");
        }
        if(StringUtils.isBlank(dict_jck_bz.getLabel())){
            return RestResponse.failure("字典标签不能为空");
        }
        if(StringUtils.isBlank(dict_jck_bz.getValue())){
            return RestResponse.failure("字典标签对应的值不能为空");
        }
        if(dict_jck_bz.getSort() == null || dict_jck_bz.getSort() < 0){
            return RestResponse.failure("排序值不正确");
        }
        Dict_jck_bz oldDict_jck_bz = dict_jck_bzService.selectById(dict_jck_bz.getId());
        if(!oldDict_jck_bz.getType().equals(dict_jck_bz.getType())){
            return RestResponse.failure("字典类型不能修改");
        }
        if(!oldDict_jck_bz.getLabel().equals(dict_jck_bz.getLabel())){
//            if(dictService.getCountByAll(dict.getType(),dict.getLabel(),null)>0){
//                return RestResponse.failure("已经存在【"+dict.getType()+"】的考核中队值【"+dict.getLabel()+"】");
//            }
        }
        if(!oldDict_jck_bz.getValue().equals(dict_jck_bz.getValue())) {
//            if (dictService.getCountByAll(dict.getType(), dict.getLabel(), dict.getValue()) > 0) {
//                return RestResponse.failure("已经存在【" + dict.getType() + "】的考核中队标签【" + dict.getLabel() + "】的加/扣分值【" + dict.getValue() + "】");
//            }
        }
        dict_jck_bzService.saveOrUpdateDict(dict_jck_bz);
        //根据dict的信息 先加回原来扣的分数 在加上新输入的分数
        teamService.changeTeamPoint_jck_bz(dict_jck_bz.getLabel(), Float.parseFloat(dict_jck_bz.getValue()) - Float.parseFloat(oldDict_jck_bz.getValue()));
        return RestResponse.success();
    }

    @RequiresPermissions("sys:dict:editType")
    @PostMapping("editType")
    @SysLog("编辑系统字典类型")
    @ResponseBody
    public RestResponse editType(@RequestParam(value="oldType",required = false)String oldType,
                                 @RequestParam(value = "newType",required = false)String newType){
        if(StringUtils.isBlank(oldType)){
            return RestResponse.failure("原类型不能为空");
        }
        if (StringUtils.isBlank(newType)){
            return RestResponse.failure("新类型不能为空");
        }
        if(oldType.equals(newType)){
            return RestResponse.failure("TYPE值相等就没必要替换了吧");
        }
        if(dictService.getCountByType(newType)>0){
            return RestResponse.failure("TYPE值已经被使用了");
        }
        dictService.updateByType(oldType,newType);
        return RestResponse.success();
    }

    //监察科备注大分数接口分数接口，供大屏幕显示数据用


    @GetMapping("platformList")
    @ResponseBody
    public LayerData<Dict_jck_bz> platformJck_bzList(@RequestParam ("label") String label,
                                                         @RequestParam(value = "page",defaultValue = "1")Integer page,
                                                         @RequestParam(value = "limit",defaultValue = "10")Integer limit,

                                                         ServletRequest request){
        if(limit==null){
            return null;
        };

        LayerData<Dict_jck_bz> layerData = new LayerData<>();
        EntityWrapper<Dict_jck_bz> wrapper = new EntityWrapper<>();
        wrapper.eq("label", label);
        wrapper.orderBy("type",false).orderBy("sort",false);
        wrapper.eq("visable_flag", 1);//visable_flag是是否显示的标志，0不显示 1显示，结算报表时会将所有考核条目的visable_flag置为0

        wrapper.setSqlSelect("id","value","label","type","type2","type3","description","number");
        Page<Dict_jck_bz> dataPage = dict_jck_bzService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setCount(dataPage.getTotal());
        layerData.setData(dataPage.getRecords());
        return layerData;
    }



}

