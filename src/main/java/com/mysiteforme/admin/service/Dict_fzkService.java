package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_fzk;


import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2021-12-19
 */
public interface Dict_fzkService extends IService<Dict_fzk> {

    List<Dict_fzk>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_fzk dict_fzk);

    String deleteDict(Long id);

    List<Dict_fzk> saveDictList(String type, List<Dict_fzk> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
