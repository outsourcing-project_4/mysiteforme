package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict;
import com.mysiteforme.admin.entity.Dict_ggb;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2021-12-12
 */
public interface Dict_ggbService extends IService<Dict_ggb> {

    List<Dict_ggb>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_ggb dict_ggb);

    String deleteDict(Long id);

    List<Dict_ggb> saveDictList(String type, List<Dict_ggb> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
