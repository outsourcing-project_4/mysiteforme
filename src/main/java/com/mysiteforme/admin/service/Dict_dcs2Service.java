package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_dcs;
import com.mysiteforme.admin.entity.Dict_dcs2;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2024-04-15
 */
public interface Dict_dcs2Service extends IService<Dict_dcs2> {

    List<Dict_dcs2>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_dcs2 dict_dcs2);

    String deleteDict(Long id);

    List<Dict_dcs2> saveDictList(String type, List<Dict_dcs2> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}

