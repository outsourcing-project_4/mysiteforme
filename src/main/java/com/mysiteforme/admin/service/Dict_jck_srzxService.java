package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_jck_srzx;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2021-12-19
 */
public interface Dict_jck_srzxService extends IService<Dict_jck_srzx> {

    List<Dict_jck_srzx>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_jck_srzx dict_jck_srzx);

    String deleteDict(Long id);

    List<Dict_jck_srzx> saveDictList(String type, List<Dict_jck_srzx> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
