package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_dcs;
import com.mysiteforme.admin.entity.Dict_xck;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2021-12-12
 */
public interface Dict_xckService extends IService<Dict_xck> {

    List<Dict_xck>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_xck dict_xck);

    String deleteDict(Long id);

    List<Dict_xck> saveDictList(String type, List<Dict_xck> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
