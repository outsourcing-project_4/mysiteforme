package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_xfk;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2021-12-12
 */
public interface Dict_xfkService extends IService<Dict_xfk> {

    List<Dict_xfk>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_xfk dict_xfk);

    String deleteDict(Long id);

    List<Dict_xfk> saveDictList(String type, List<Dict_xfk> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
