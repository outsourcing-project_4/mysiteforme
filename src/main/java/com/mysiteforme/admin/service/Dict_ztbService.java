package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_ggb;
import com.mysiteforme.admin.entity.Dict_ztb;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2021-12-16
 */
public interface Dict_ztbService extends IService<Dict_ztb> {

    List<Dict_ztb>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_ztb dict_ztb);

    String deleteDict(Long id);

    List<Dict_ztb> saveDictList(String type, List<Dict_ztb> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
