package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_jck_bz;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author wcl
 * @since 2023-8-21
 */
public interface Dict_jck_bzService extends IService<Dict_jck_bz> {

    List<Dict_jck_bz>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_jck_bz dict_jck_bz);

    String deleteDict(Long id);

    List<Dict_jck_bz> saveDictList(String type, List<Dict_jck_bz> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
