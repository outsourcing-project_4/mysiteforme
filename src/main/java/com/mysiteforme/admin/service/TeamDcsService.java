package com.mysiteforme.admin.service;

import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.entity.Team_dcs;

import java.util.List;

public interface TeamDcsService {
    List<Team_dcs> queryPoint_dcs();

    Integer changeTeamPoint_dsjk(String team_name,float point);;

    Integer resetPoint(float resetPoint_dcs);
}
