package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_bgs;


import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2021-12-19
 */
public interface Dict_bgsService extends IService<Dict_bgs> {

    List<Dict_bgs>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_bgs dict_bgs);

    String deleteDict(Long id);

    List<Dict_bgs> saveDictList(String type, List<Dict_bgs> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
