package com.mysiteforme.admin.service.impl;

import com.mysiteforme.admin.dao.TeamDcsDao;
import com.mysiteforme.admin.entity.Team_dcs;
import com.mysiteforme.admin.service.TeamDcsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamDcsServiceImpl implements TeamDcsService {
    @Autowired
    private TeamDcsDao team_dcsDao;

    @Override
    public List<Team_dcs> queryPoint_dcs() {
        return team_dcsDao.queryPoint_dcs();
    }

    @Override
    public Integer changeTeamPoint_dsjk(String team_name,float point) {
        return team_dcsDao.changeTeamPoint_dsjk(team_name,point);
    }
    @Override
    public Integer resetPoint(float resetPoint_dcs) {
        return team_dcsDao.resetPoint(resetPoint_dcs);
    }

}
