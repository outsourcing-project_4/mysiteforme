package com.mysiteforme.admin.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.Dict_jck_yllhDao;
import com.mysiteforme.admin.entity.Dict_jck_yllh;
import com.mysiteforme.admin.service.Dict_jck_yllhService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author lsd
 * @since 2021-12-16
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class Dict_jck_yllhServiceImpl extends ServiceImpl<Dict_jck_yllhDao, Dict_jck_yllh> implements Dict_jck_yllhService {

//    @Cacheable(value = "dictCache",key = "#type",unless = "#result == null or #result.size() == 0")
    @Override
    public List<Dict_jck_yllh> getDictByType(String type) {
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        wrapper.eq("del_flag",false);
        wrapper.orderBy("sort");
        return selectList(wrapper);
    }

    @Override
    public Integer getCountByType(String type) {
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        wrapper.eq("del_flag",false);
        return selectCount(wrapper);
    }

    @Override
    public Integer getMaxSortByType(String type) {
        Object o = selectObj(Condition.create().setSqlSelect("max(sort)").eq("type",type));
        int sort = 0;
        if(o != null){
            sort =  (Integer)o + 1;
        }
        return sort;
    }

    @Override
    public Integer getCountByAll(String type, String label, String value) {
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        if(StringUtils.isNotBlank(label)){
            wrapper.eq("label",label);
        }
        if(StringUtils.isNotBlank(value)){
            wrapper.eq("value",value);
        }
        wrapper.eq("del_flag",false);
        return selectCount(wrapper);
    }

//    @CacheEvict(value = "dictCache",key = "#dict.type",condition = "#dict.type ne null ")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void saveOrUpdateDict(Dict_jck_yllh dict_jck_yllh) {
        insertOrUpdate(dict_jck_yllh);
    }

//    @CacheEvict(value = "dictCache",key = "#result",beforeInvocation = false,condition = "#result ne  null ")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public String deleteDict(Long id) {
        Dict_jck_yllh dict_jck_yllh = baseMapper.selectById(id);
        baseMapper.deleteById(id);
        return dict_jck_yllh.getType();
    }

//    @CacheEvict(value = "dictCache",key = "#type",beforeInvocation = false)
    @Override
    public List<Dict_jck_yllh> saveDictList(String type, List<Dict_jck_yllh> list) {
        insertBatch(list);
        return list;
    }

//    @CacheEvict(value = "dictCache",key = "#type",beforeInvocation = false)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void deleteByType(String type) {
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        delete(wrapper);
    }

//    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void deleteByTableName(String tableName) {
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        wrapper.like("description","数据表【"+tableName+"】");
        delete(wrapper);
    }

//    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void updateByType(String oldType,String newType) {
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        wrapper.eq("type",oldType);
        List<Dict_jck_yllh> dicts_jck_yllh = baseMapper.selectList(wrapper);
        for (Dict_jck_yllh dict_jck_yllh : dicts_jck_yllh){
            dict_jck_yllh.setType(newType);
        }
        updateBatchById(dicts_jck_yllh);
    }

    //    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public boolean updateVisableFlag() {
        EntityWrapper<Dict_jck_yllh> wrapper = new EntityWrapper<>();
        wrapper.eq("visable_flag",1);
        List<Dict_jck_yllh> dicts_jck_yllh = baseMapper.selectList(wrapper);
        //如果list为空，说明没有扣分项目，所以无需设置VisableFlag，可以直接返回true表示操作成功
        if(dicts_jck_yllh.isEmpty()){
            return true;
        }
        for (Dict_jck_yllh dict_jck_yllh : dicts_jck_yllh){
            dict_jck_yllh.setVisable_flag(0);
        }
        boolean resultFlag = updateBatchById(dicts_jck_yllh);
        return resultFlag;
    }

}
