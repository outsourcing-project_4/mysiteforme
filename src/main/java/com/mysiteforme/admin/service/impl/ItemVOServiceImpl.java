package com.mysiteforme.admin.service.impl;

import com.mysiteforme.admin.dao.ItemVODao;
import com.mysiteforme.admin.entity.VO.ItemVO;
import com.mysiteforme.admin.service.ItemVoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemVOServiceImpl implements ItemVoService {

    @Autowired
    private ItemVODao itemVODao;

    //治违办
    @Override
    public List<ItemVO> queryList_zwb() {
        return itemVODao.queryList_zwb();
    }

    @Override
    public List<ItemVO> queryLeibie_zwb() {
        return itemVODao.queryLeibie_zwb();
    }

    @Override
    public List<ItemVO> queryPingfen_zwb(String law_item_leibie) {
        return itemVODao.queryPingfen_zwb(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_zwb(String law_item_pingfen) {
        return itemVODao.queryList_description_zwb(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_zwb(String description) {
        return itemVODao.queryPoint_zwb(description);
    }

    @Override
    public Integer changePoint_zwb(String type,String description,String point) {
//        return itemVODao.changePoint_zwb(type,description,point);
        return 0;
    }

    //广告办
    @Override
    public List<ItemVO> queryList_ggb() {
        return itemVODao.queryList_ggb();
    }

    @Override
    public List<ItemVO> queryLeibie_ggb() {
        return itemVODao.queryLeibie_ggb();
    }

    @Override
    public List<ItemVO> queryPingfen_ggb(String law_item_leibie) {
        return itemVODao.queryPingfen_ggb(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_ggb(String law_item_pingfen) {
        return itemVODao.queryList_description_ggb(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_ggb(String description) {
        return itemVODao.queryPoint_ggb(description);
    }

    @Override
    public Integer changePoint_ggb(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //渣土办
    @Override
    public List<ItemVO> queryList_ztb() {
        return itemVODao.queryList_ztb();
    }

    @Override
    public List<ItemVO> queryLeibie_ztb() {
        return itemVODao.queryLeibie_ztb();
    }

    @Override
    public List<ItemVO> queryPingfen_ztb(String law_item_leibie) {
        return itemVODao.queryPingfen_ztb(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_ztb(String law_item_pingfen) {
        return itemVODao.queryList_description_ztb(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_ztb(String description) {
        return itemVODao.queryPoint_ztb(description);
    }

    @Override
    public Integer changePoint_ztb(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //指挥中心
    @Override
    public List<ItemVO> queryList_fws() {
        return itemVODao.queryList_fws();
    }

    @Override
    public List<ItemVO> queryList_fws2() {
        return itemVODao.queryList_fws2();
    }

    @Override
    public List<ItemVO> queryLeibie_fws() {
        return itemVODao.queryLeibie_fws();
    }

    @Override
    public List<ItemVO> queryLeibie_fws2() {
        return itemVODao.queryLeibie_fws2();
    }

    @Override
    public List<ItemVO> queryPingfen_fws(String law_item_leibie) {
        return itemVODao.queryPingfen_fws(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryPingfen_fws2(String law_item_leibie) {
        return itemVODao.queryPingfen_fws2(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_fws(String law_item_pingfen) {
        return itemVODao.queryList_description_fws(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryList_description_fws2(String law_item_pingfen) {
        return itemVODao.queryList_description_fws2(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_fws(String description) {
        return itemVODao.queryPoint_fws(description);
    }

    @Override
    public List<ItemVO> queryPoint_fws2(String description) {
        return itemVODao.queryPoint_fws2(description);
    }

    @Override
    public Integer changePoint_fws(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //宣传科
    @Override
    public List<ItemVO> queryList_xck() {
        return itemVODao.queryList_xck();
    }

    @Override
    public List<ItemVO> queryLeibie_xck() {
        return itemVODao.queryLeibie_xck();
    }

    @Override
    public List<ItemVO> queryPingfen_xck(String law_item_leibie) {
        return itemVODao.queryPingfen_xck(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_xck(String law_item_pingfen) {
        return itemVODao.queryList_description_xck(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_xck(String description) {
        return itemVODao.queryPoint_xck(description);
    }

    @Override
    public Integer changePoint_xck(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    // 办公室
    @Override
    public List<ItemVO> queryList_bgs() {
        return itemVODao.queryList_bgs();
    }

    @Override
    public List<ItemVO> queryLeibie_bgs() {
        return itemVODao.queryLeibie_bgs();
    }

    @Override
    public List<ItemVO> queryPingfen_bgs(String law_item_leibie) {
        return itemVODao.queryPingfen_bgs(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_bgs(String law_item_pingfen) {
        return itemVODao.queryList_description_bgs(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_bgs(String description) {
        return itemVODao.queryPoint_bgs(description);
    }

    @Override
    public Integer changePoint_bgs(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    // 信访科
    @Override
    public List<ItemVO> queryList_xfk() {
        return itemVODao.queryList_xfk();
    }

    @Override
    public List<ItemVO> queryLeibie_xfk() {
        return itemVODao.queryLeibie_xfk();
    }

    @Override
    public List<ItemVO> queryPingfen_xfk(String law_item_leibie) {
        return itemVODao.queryPingfen_xfk(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_xfk(String law_item_pingfen) {
        return itemVODao.queryList_description_xfk(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_xfk(String description) {
        return itemVODao.queryPoint_xfk(description);
    }

    @Override
    public Integer changePoint_xfk(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    // 政工科
    @Override
    public List<ItemVO> queryList_zgk() {
        return itemVODao.queryList_zgk();
    }

    @Override
    public List<ItemVO> queryLeibie_zgk() {
        return itemVODao.queryLeibie_zgk();
    }

    @Override
    public List<ItemVO> queryPingfen_zgk(String law_item_leibie) {
        return itemVODao.queryPingfen_zgk(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_zgk(String law_item_pingfen) {
        return itemVODao.queryList_description_zgk(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_zgk(String description) {
        return itemVODao.queryPoint_zgk(description);
    }

    @Override
    public Integer changePoint_zgk(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    // 法制科
    @Override
    public List<ItemVO> queryList_fzk() {
        return itemVODao.queryList_fzk();
    }

    @Override
    public List<ItemVO> queryLeibie_fzk() {
        return itemVODao.queryLeibie_fzk();
    }

    @Override
    public List<ItemVO> queryPingfen_fzk(String law_item_leibie) {
        return itemVODao.queryPingfen_fzk(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_fzk(String law_item_pingfen) {
        return itemVODao.queryList_description_fzk(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_fzk(String description) {
        return itemVODao.queryPoint_fzk(description);
    }

    @Override
    public Integer changePoint_fzk(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //监察科_市容秩序
    @Override
    public List<ItemVO> queryList_jck_srzx() {
        return itemVODao.queryList_jck_srzx();
    }

    @Override
    public List<ItemVO> queryLeibie_jck_srzx() {
        return itemVODao.queryLeibie_jck_srzx();
    }

    @Override
    public List<ItemVO> queryPingfen_jck_srzx(String law_item_leibie) {
        return itemVODao.queryPingfen_jck_srzx(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_jck_srzx(String law_item_pingfen) {
        return itemVODao.queryList_description_jck_srzx(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_jck_srzx(String description) {
        return itemVODao.queryPoint_jck_srzx(description);
    }

    @Override
    public Integer changePoint_jck_srzx(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //监察科_环境卫生
    @Override
    public List<ItemVO> queryList_jck_hjws() {
        return itemVODao.queryList_jck_hjws();
    }

    @Override
    public List<ItemVO> queryLeibie_jck_hjws() {
        return itemVODao.queryLeibie_jck_hjws();
    }

    @Override
    public List<ItemVO> queryPingfen_jck_hjws(String law_item_leibie) {
        return itemVODao.queryPingfen_jck_hjws(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_jck_hjws(String law_item_pingfen) {
        return itemVODao.queryList_description_jck_hjws(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_jck_hjws(String description) {
        return itemVODao.queryPoint_jck_hjws(description);
    }

    @Override
    public Integer changePoint_jck_hjws(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //监察科_市政设施
    @Override
    public List<ItemVO> queryList_jck_szss() {
        return itemVODao.queryList_jck_szss();
    }

    @Override
    public List<ItemVO> queryLeibie_jck_szss() {
        return itemVODao.queryLeibie_jck_szss();
    }

    @Override
    public List<ItemVO> queryPingfen_jck_szss(String law_item_leibie) {
        return itemVODao.queryPingfen_jck_szss(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_jck_szss(String law_item_pingfen) {
        return itemVODao.queryList_description_jck_szss(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_jck_szss(String description) {
        return itemVODao.queryPoint_jck_szss(description);
    }

    @Override
    public Integer changePoint_jck_szss(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //监察科_园林绿化
    @Override
    public List<ItemVO> queryList_jck_yllh() {
        return itemVODao.queryList_jck_yllh();
    }

    @Override
    public List<ItemVO> queryLeibie_jck_yllh() {
        return itemVODao.queryLeibie_jck_yllh();
    }

    @Override
    public List<ItemVO> queryPingfen_jck_yllh(String law_item_leibie) {
        return itemVODao.queryPingfen_jck_yllh(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_jck_yllh(String law_item_pingfen) {
        return itemVODao.queryList_description_jck_yllh(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_jck_yllh(String description) {
        return itemVODao.queryPoint_jck_yllh(description);
    }

    @Override
    public Integer changePoint_jck_yllh(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //监察科_其他案件
    @Override
    public List<ItemVO> queryList_jck_qtaj() {
        return itemVODao.queryList_jck_qtaj();
    }

    @Override
    public List<ItemVO> queryLeibie_jck_qtaj() {
        return itemVODao.queryLeibie_jck_qtaj();
    }

    @Override
    public List<ItemVO> queryPingfen_jck_qtaj(String law_item_leibie) {
        return itemVODao.queryPingfen_jck_qtaj(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_jck_qtaj(String law_item_pingfen) {
        return itemVODao.queryList_description_jck_qtaj(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_jck_qtaj(String description) {
        return itemVODao.queryPoint_jck_qtaj(description);
    }

    @Override
    public Integer changePoint_jck_qtaj(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //监察科_业务亮点
    @Override
    public List<ItemVO> queryList_jck_ywld() {
        return itemVODao.queryList_jck_ywld();
    }

    @Override
    public List<ItemVO> queryLeibie_jck_ywld() {
        return itemVODao.queryLeibie_jck_ywld();
    }

    @Override
    public List<ItemVO> queryPingfen_jck_ywld(String law_item_leibie) {
        return itemVODao.queryPingfen_jck_ywld(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_jck_ywld(String law_item_pingfen) {
        return itemVODao.queryList_description_jck_ywld(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_jck_ywld(String description) {
        return itemVODao.queryPoint_jck_ywld(description);
    }

    @Override
    public Integer changePoint_jck_ywld(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }


    //监察科_备注
    @Override
    public List<ItemVO> queryList_jck_bz() {
        return itemVODao.queryList_jck_bz();
    }

    @Override
    public List<ItemVO> queryLeibie_jck_bz() {
        return itemVODao.queryLeibie_jck_bz();
    }

    @Override
    public List<ItemVO> queryPingfen_jck_bz(String law_item_leibie) {
        return itemVODao.queryPingfen_jck_bz(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_jck_bz(String law_item_pingfen) {
        return itemVODao.queryList_description_jck_bz(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_jck_bz(String description) {
        return itemVODao.queryPoint_jck_bz(description);
    }

    @Override
    public Integer changePoint_jck_bz(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }
    //监察科_物业执法
    @Override
    public List<ItemVO> queryList_jck_wyzf() {
        return itemVODao.queryList_jck_wyzf();
    }

    @Override
    public List<ItemVO> queryLeibie_jck_wyzf() {
        return itemVODao.queryLeibie_jck_wyzf();
    }

    @Override
    public List<ItemVO> queryPingfen_jck_wyzf(String law_item_leibie) {
        return itemVODao.queryPingfen_jck_wyzf(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_jck_wyzf(String law_item_pingfen) {
        return itemVODao.queryList_description_jck_wyzf(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_jck_wyzf(String description) {
        return itemVODao.queryPoint_jck_wyzf(description);
    }

    @Override
    public Integer changePoint_jck_wyzf(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

    //大数据科
    @Override
    public List<ItemVO> queryList_dsjk() {
        return itemVODao.queryList_dsjk();
    }

    @Override
    public List<ItemVO> queryLeibie_dsjk() {
        return itemVODao.queryLeibie_dsjk();
    }

    @Override
    public List<ItemVO> queryPingfen_dsjk(String law_item_leibie) {
        return itemVODao.queryPingfen_dsjk(law_item_leibie);
    }

    @Override
    public List<ItemVO> queryList_description_dsjk(String law_item_pingfen) {
        return itemVODao.queryList_description_dsjk(law_item_pingfen);
    }

    @Override
    public List<ItemVO> queryPoint_dsjk(String description) {
        return itemVODao.queryPoint_dsjk(description);
    }

    @Override
    public Integer changePoint_dsjk(String type,String description,String point) {
//        return itemVODao.changePoint(type,description,point);
        return 0;
    }

}
