package com.mysiteforme.admin.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.Dict_xfkDao;
import com.mysiteforme.admin.entity.Dict_xfk;
import com.mysiteforme.admin.service.Dict_xfkService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author lsd
 * @since 2021-12-16
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class Dict_xfkServiceImpl extends ServiceImpl<Dict_xfkDao, Dict_xfk> implements Dict_xfkService {

//    @Cacheable(value = "dictCache",key = "#type",unless = "#result == null or #result.size() == 0")
    @Override
    public List<Dict_xfk> getDictByType(String type) {
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        wrapper.eq("del_flag",false);
        wrapper.orderBy("sort");
        return selectList(wrapper);
    }

    @Override
    public Integer getCountByType(String type) {
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        wrapper.eq("del_flag",false);
        return selectCount(wrapper);
    }

    @Override
    public Integer getMaxSortByType(String type) {
        Object o = selectObj(Condition.create().setSqlSelect("max(sort)").eq("type",type));
        int sort = 0;
        if(o != null){
            sort =  (Integer)o + 1;
        }
        return sort;
    }

    @Override
    public Integer getCountByAll(String type, String label, String value) {
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        if(StringUtils.isNotBlank(label)){
            wrapper.eq("label",label);
        }
        if(StringUtils.isNotBlank(value)){
            wrapper.eq("value",value);
        }
        wrapper.eq("del_flag",false);
        return selectCount(wrapper);
    }

//    @CacheEvict(value = "dictCache",key = "#dict.type",condition = "#dict.type ne null ")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void saveOrUpdateDict(Dict_xfk dict_xfk) {
        insertOrUpdate(dict_xfk);
    }

//    @CacheEvict(value = "dictCache",key = "#result",beforeInvocation = false,condition = "#result ne  null ")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public String deleteDict(Long id) {
        Dict_xfk dict_xfk = baseMapper.selectById(id);
        baseMapper.deleteById(id);
        return dict_xfk.getType();
    }

//    @CacheEvict(value = "dictCache",key = "#type",beforeInvocation = false)
    @Override
    public List<Dict_xfk> saveDictList(String type, List<Dict_xfk> list) {
        insertBatch(list);
        return list;
    }

//    @CacheEvict(value = "dictCache",key = "#type",beforeInvocation = false)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void deleteByType(String type) {
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        delete(wrapper);
    }

//    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void deleteByTableName(String tableName) {
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        wrapper.like("description","数据表【"+tableName+"】");
        delete(wrapper);
    }

//    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void updateByType(String oldType,String newType) {
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        wrapper.eq("type",oldType);
        List<Dict_xfk> dicts_xfk = baseMapper.selectList(wrapper);
        for (Dict_xfk dict_xfk : dicts_xfk){
            dict_xfk.setType(newType);
        }
        updateBatchById(dicts_xfk);
    }

    //    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public boolean updateVisableFlag() {
        EntityWrapper<Dict_xfk> wrapper = new EntityWrapper<>();
        wrapper.eq("visable_flag",1);
        List<Dict_xfk> dicts_xfk = baseMapper.selectList(wrapper);
        //如果list为空，说明没有扣分项目，所以无需设置VisableFlag，可以直接返回true表示操作成功
        if(dicts_xfk.isEmpty()){
            return true;
        }
        for (Dict_xfk dict_xfk : dicts_xfk){
            dict_xfk.setVisable_flag(0);
        }
        boolean resultFlag = updateBatchById(dicts_xfk);
        return resultFlag;
    }

}
