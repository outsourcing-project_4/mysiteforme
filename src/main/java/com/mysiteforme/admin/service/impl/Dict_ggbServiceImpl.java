package com.mysiteforme.admin.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.Dict_ggbDao;
import com.mysiteforme.admin.entity.Dict_ggb;
import com.mysiteforme.admin.service.Dict_ggbService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author lsd
 * @since 2021-12-12
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class Dict_ggbServiceImpl extends ServiceImpl<Dict_ggbDao, Dict_ggb> implements Dict_ggbService {

//    @Cacheable(value = "dictCache",key = "#type",unless = "#result == null or #result.size() == 0")
    @Override
    public List<Dict_ggb> getDictByType(String type) {
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        wrapper.eq("del_flag",false);
        wrapper.orderBy("sort");
        return selectList(wrapper);
    }

    @Override
    public Integer getCountByType(String type) {
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        wrapper.eq("del_flag",false);
        return selectCount(wrapper);
    }

    @Override
    public Integer getMaxSortByType(String type) {
        Object o = selectObj(Condition.create().setSqlSelect("max(sort)").eq("type",type));
        int sort = 0;
        if(o != null){
            sort =  (Integer)o + 1;
        }
        return sort;
    }

    @Override
    public Integer getCountByAll(String type, String label, String value) {
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        if(StringUtils.isNotBlank(label)){
            wrapper.eq("label",label);
        }
        if(StringUtils.isNotBlank(value)){
            wrapper.eq("value",value);
        }
        wrapper.eq("del_flag",false);
        return selectCount(wrapper);
    }

//    @CacheEvict(value = "dictCache",key = "#dict.type",condition = "#dict.type ne null ")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void saveOrUpdateDict(Dict_ggb dict_ggb) {
        insertOrUpdate(dict_ggb);
    }

//    @CacheEvict(value = "dictCache",key = "#result",beforeInvocation = false,condition = "#result ne  null ")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public String deleteDict(Long id) {
        Dict_ggb dict_ggb = baseMapper.selectById(id);
        baseMapper.deleteById(id);
        return dict_ggb.getType();
    }

//    @CacheEvict(value = "dictCache",key = "#type",beforeInvocation = false)
    @Override
    public List<Dict_ggb> saveDictList(String type, List<Dict_ggb> list) {
        insertBatch(list);
        return list;
    }

//    @CacheEvict(value = "dictCache",key = "#type",beforeInvocation = false)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void deleteByType(String type) {
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        delete(wrapper);
    }

//    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void deleteByTableName(String tableName) {
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        wrapper.like("description","数据表【"+tableName+"】");
        delete(wrapper);
    }

//    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void updateByType(String oldType,String newType) {
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        wrapper.eq("type",oldType);
        List<Dict_ggb> dicts_ggb = baseMapper.selectList(wrapper);
        for (Dict_ggb dict_ggb : dicts_ggb){
            dict_ggb.setType(newType);
        }
        updateBatchById(dicts_ggb);
    }

    //    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public boolean updateVisableFlag() {
        EntityWrapper<Dict_ggb> wrapper = new EntityWrapper<>();
        wrapper.eq("visable_flag",1);
        List<Dict_ggb> dicts_ggb = baseMapper.selectList(wrapper);
        //如果list为空，说明没有扣分项目，所以无需设置VisableFlag，可以直接返回true表示操作成功
        if(dicts_ggb.isEmpty()){
            return true;
        }
        for (Dict_ggb dict_ggb : dicts_ggb){
            dict_ggb.setVisable_flag(0);
        }
        boolean resultFlag = updateBatchById(dicts_ggb);
        return resultFlag;
    }

}
