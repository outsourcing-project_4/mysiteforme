package com.mysiteforme.admin.service.impl;

import com.mysiteforme.admin.dao.TeamDao;
import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamDao teamDao;

    @Override
    public List<String> queryTeam() { return teamDao.queryTeam(); }

    @Override
    public List<Team> queryPoint_zwb() {
        return teamDao.queryPoint_zwb();
    }

    @Override
    public List<Team> queryPoint_historyZwb1() {
        return teamDao.queryPoint_historyZwb1();
    }

    @Override
    public Integer changeTeamPoint_zwb(String team_name,float point) {
            return teamDao.changeTeamPoint_zwb(team_name,point);
    }
    //获取当前中队的zwb分数
    @Override
    public Team queryTeamPoint_zwb(String team_name) {
        return teamDao.queryTeamPoint_zwb(team_name);
    }
    //消除zwb浮点型分数归零的误差
    @Override
    public Integer fixdict_zwbPointError(String team_name,double point) {
        return teamDao.fixdict_zwbPointError(team_name,point);
    }

    @Override
    public List<Team> queryPoint_ggb() {
        return teamDao.queryPoint_ggb();
    }

    @Override
    public List<Team> queryPoint_historyGgb1() {
        return teamDao.queryPoint_historyGgb1();
    }

    @Override
    public List<Team> queryPoint_historyGgb2() {
        return teamDao.queryPoint_historyGgb2();
    }

    @Override
    public Integer changeTeamPoint_ggb(String team_name,float point) {
        return teamDao.changeTeamPoint_ggb(team_name,point);
    }
    //获取当前中队的ggb分数
    @Override
    public Team queryTeamPoint_ggb(String team_name) {
        return teamDao.queryTeamPoint_ggb(team_name);
    }
    //消除ggb浮点型分数归零的误差
    @Override
    public Integer fixdict_ggbPointError(String team_name,double point) {
        return teamDao.fixdict_ggbPointError(team_name,point);
    }

    @Override
    public List<Team> queryPoint_ztb() {
        return teamDao.queryPoint_ztb();
    }

    @Override
    public Integer changeTeamPoint_ztb(String team_name,float point) {
        return teamDao.changeTeamPoint_ztb(team_name,point);
    }

    @Override
    public List<Team> queryPoint_dcs() {
        return teamDao.queryPoint_dcs();
    }

    @Override
    public List<Team> queryPoint_historyDcs1() {
        return teamDao.queryPoint_historyDcs1();
    }

    @Override
    public Integer changeTeamPoint_dcs(String team_name,float point) {
        return teamDao.changeTeamPoint_dcs(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_dcs_ajcz(String team_name,float point) {
        return teamDao.changeTeamPoint_dcs_ajcz(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_dcs_hs(String team_name,float point) {
        return teamDao.changeTeamPoint_dcs_hs(team_name,point);
    }
    //获取当前中队的dcs分数
    @Override
    public Team queryTeamPoint_dcs(String team_name) {
        return teamDao.queryTeamPoint_dcs(team_name);
    }
    //消除dcs浮点型分数归零的误差
    @Override
    public Integer fixdict_dcsPointError(String team_name,double point) {
        return teamDao.fixdict_dcsPointError(team_name,point);
    }
    @Override
    public Integer fixdict_dcs_ajczPointError(String team_name,double point) {
        return teamDao.fixdict_dcs_ajczPointError(team_name,point);
    }
    @Override
    public Integer fixdict_dcs_hcPointError(String team_name,double point) {
        return teamDao.fixdict_dcs_hcPointError(team_name,point);
    }


    @Override
    public List<Team> queryPoint_xck() {
        return teamDao.queryPoint_xck();
    }

    @Override
    public List<Team> queryPoint_historyXck1() {
        return teamDao.queryPoint_historyXck1();
    }

    @Override
    public Integer changeTeamPoint_xck(String team_name,float point) {
        return teamDao.changeTeamPoint_xck(team_name,point);
    }
    //获取当前中队的xck分数
    @Override
    public Team queryTeamPoint_xck(String team_name) {
        return teamDao.queryTeamPoint_xck(team_name);
    }
    //消除xck浮点型分数归零的误差
    @Override
    public Integer fixdict_xckPointError(String team_name,double point) {
        return teamDao.fixdict_xckPointError(team_name,point);
    }

    @Override
    public List<Team> queryPoint_bgs() {
        return teamDao.queryPoint_bgs();
    }

    @Override
    public Integer changeTeamPoint_bgs(String team_name,float point) {
        return teamDao.changeTeamPoint_bgs(team_name,point);
    }

    @Override
    public List<Team> queryPoint_xfk() {
        return teamDao.queryPoint_xfk();
    }

    @Override
    public List<Team> queryPoint_historyXfk1() {
        return teamDao.queryPoint_historyXfk1();
    }

    @Override
    public List<Team> queryPoint_historyXfk2() {
        return teamDao.queryPoint_historyXfk2();
    }

    @Override
    public Integer changeTeamPoint_xfk(String team_name,float point) {
        return teamDao.changeTeamPoint_xfk(team_name,point);
    }
    //获取当前中队的xfk分数
    @Override
    public Team queryTeamPoint_xfk(String team_name) {
        return teamDao.queryTeamPoint_xfk(team_name);
    }
    //消除xfk浮点型分数归零的误差
    @Override
    public Integer fixdict_xfkPointError(String team_name,double point) {
        return teamDao.fixdict_xfkPointError(team_name,point);
    }

    @Override
    public List<Team> queryPoint_zgk() {
        return teamDao.queryPoint_zgk();
    }

    @Override
    public List<Team> queryPoint_historyZgk1() {
        return teamDao.queryPoint_historyZgk1();
    }

    @Override
    public Integer changeTeamPoint_zgk(String team_name,float point) {
        return teamDao.changeTeamPoint_zgk(team_name,point);
    }
    //获取当前中队的zgk分数
    @Override
    public Team queryTeamPoint_zgk(String team_name) {
        return teamDao.queryTeamPoint_zgk(team_name);
    }
    //消除zgk浮点型分数归零的误差
    @Override
    public Integer fixdict_zgkPointError(String team_name,double point) {
        return teamDao.fixdict_zgkPointError(team_name,point);
    }


    @Override
    public List<Team> queryPoint_fzk() {
        return teamDao.queryPoint_fzk();
    }

    @Override
    public List<Team> queryPoint_historyFzk1() {
        return teamDao.queryPoint_historyFzk1();
    }

    @Override
    public Integer changeTeamPoint_fzk(String team_name,float point) {
        return teamDao.changeTeamPoint_fzk(team_name,point);
    }
    //获取当前中队的fzk分数
    @Override
    public Team queryTeamPoint_fzk(String team_name) {
        return teamDao.queryTeamPoint_fzk(team_name);
    }
    //消除fzk浮点型分数归零的误差
    @Override
    public Integer fixdict_fzkPointError(String team_name,double point) {
        return teamDao.fixdict_fzkPointError(team_name,point);
    }


    @Override
    public List<Team> queryPoint_jck_srzx() {
        return teamDao.queryPoint_jck_srzx();
    }

    @Override
    public List<Team> queryPoint_jck_historySrzx1() {
        return teamDao.queryPoint_jck_historySrzx1();
    }

    @Override
    public List<Team> queryPoint_jck_historySrzx2() {
        return teamDao.queryPoint_jck_historySrzx2();
    }

    @Override
    public Integer changeTeamPoint_jck_srzx(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_srzx(team_name,point);
    }
    //获取当前中队的srzx分数
    @Override
    public Team queryTeamPoint_jck_srzx(String team_name) {
        return teamDao.queryTeamPoint_jck_srzx(team_name);
    }
//归零低于下限的中队分数
    @Override
    public Integer xiaxianTeamPoint_jck_srzx(String team_name,float Point_jck_srzx) {
        if(Point_jck_srzx<0) {
            Point_jck_srzx=0;
        }else if(Point_jck_srzx>10){
            Point_jck_srzx=10;
        }
        return teamDao.xiaxianTeamPoint_jck_srzx(team_name,Point_jck_srzx);
    }
//重新计算分数
    @Override
    public Integer rechangeTeamPoint_jck_srzx(String team_name,float point) {
        return teamDao.rechangeTeamPoint_jck_srzx(team_name,point);
    }

    @Override
    public List<Team> queryPoint_jck_hjws() {
        return teamDao.queryPoint_jck_hjws();
    }

    @Override
    public List<Team> queryPoint_jck_historyHjws1() {
        return teamDao.queryPoint_jck_historyHjws1();
    }

    @Override
    public List<Team> queryPoint_jck_historyHjws2() {
        return teamDao.queryPoint_jck_historyHjws2();
    }

    @Override
    public Integer changeTeamPoint_jck_hjws(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_hjws(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_hjws_zljy(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_hjws_zljy(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_hjws_xgg(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_hjws_xgg(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_hjws_ws(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_hjws_ws(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_hjws_ljfl(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_hjws_ljfl(team_name,point);
    }

    //获取当前中队的hjws分数
    @Override
    public Team queryTeamPoint_jck_hjws(String team_name) {
        return teamDao.queryTeamPoint_jck_hjws(team_name);
    }

    @Override
    public Integer zeroTeamPoint_jck_hjws(String team_name) {
        return teamDao.zeroTeamPoint_jck_hjws(team_name);
    }
//消除hjws浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_hjwsPointError(String team_name,double point) {
        return teamDao.fixdict_jck_hjwsPointError(team_name,point);
    }
    //消除hjws_zljy浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_hjws_zljyPointError(String team_name,double point) {
        return teamDao.fixdict_jck_hjws_zljyPointError(team_name,point);
    }
    //消除hjws_xgg浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_hjws_xggPointError(String team_name,double point) {
        return teamDao.fixdict_jck_hjws_xggPointError(team_name,point);
    }
    //消除hjws_ws浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_hjws_wsPointError(String team_name,double point) {
        return teamDao.fixdict_jck_hjws_wsPointError(team_name,point);
    }
    //消除hjws_ljfl浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_hjws_ljflPointError(String team_name,double point) {
        return teamDao.fixdict_jck_hjws_ljflPointError(team_name,point);
    }


    @Override
    public List<Team> queryPoint_jck_szss() { return teamDao.queryPoint_jck_szss();}

    @Override
    public List<Team> queryPoint_jck_historySzss1() { return teamDao.queryPoint_jck_historySzss1();}

    @Override
    public List<Team> queryPoint_jck_historySzss2() { return teamDao.queryPoint_jck_historySzss2();}

    @Override
    public Integer changeTeamPoint_jck_szss(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_szss(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_szss_rq(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_szss_rq(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_szss_pw(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_szss_pw(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_szss_jl(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_szss_jl(team_name,point);
    }
    //获取当前中队的szss分数
    @Override
    public Team queryTeamPoint_jck_szss(String team_name) {
        return teamDao.queryTeamPoint_jck_szss(team_name);
    }
    //消除szss浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_szssPointError(String team_name,double point) {
        return teamDao.fixdict_jck_szssPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_szss_rqPointError(String team_name,double point) {
        return teamDao.fixdict_jck_szss_rqPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_szss_pwPointError(String team_name,double point) {
        return teamDao.fixdict_jck_szss_pwPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_szss_jlPointError(String team_name,double point) {
        return teamDao.fixdict_jck_szss_jlPointError(team_name,point);
    }

    @Override
    public List<Team> queryPoint_jck_yllh() {
        return teamDao.queryPoint_jck_yllh();
    }

    @Override
    public List<Team> queryPoint_jck_historyYllh1() {
        return teamDao.queryPoint_jck_historyYllh1();
    }

    @Override
    public List<Team> queryPoint_jck_historyYllh2() {
        return teamDao.queryPoint_jck_historyYllh2();
    }

    @Override
    public Integer changeTeamPoint_jck_yllh(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_yllh(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_yllh_lh(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_yllh_lh(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_yllh_yy(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_yllh_yy(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_yllh_ztc1(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_yllh_ztc1(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_yllh_ztc2(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_yllh_ztc2(team_name,point);
    }
    //归零低于下限的中队分数
    @Override
    public Integer xiaxianTeamPoint_jck_yllh(String team_name,float Point_jck_yllh) {
        if(Point_jck_yllh<0) {
            Point_jck_yllh=0;
        }
        return teamDao.xiaxianTeamPoint_jck_yllh(team_name,Point_jck_yllh);
    }
    //获取当前中队的yllh分数
    @Override
    public Team queryTeamPoint_jck_yllh(String team_name) {
        return teamDao.queryTeamPoint_jck_yllh(team_name);
    }
    //消除yllh浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_yllhPointError(String team_name,double point) {
        return teamDao.fixdict_jck_yllhPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_yllh_lhPointError(String team_name,double point) {
        return teamDao.fixdict_jck_yllh_lhPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_yllh_yyPointError(String team_name,double point) {
        return teamDao.fixdict_jck_yllh_yyPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_yllh_ztc1PointError(String team_name,double point) {
        return teamDao.fixdict_jck_yllh_ztc1PointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_yllh_ztc2PointError(String team_name,double point) {
        return teamDao.fixdict_jck_yllh_ztc2PointError(team_name,point);
    }



    //新增监察科（其他案件）
    @Override
    public List<Team> queryPoint_jck_qtaj() {
        return teamDao.queryPoint_jck_qtaj();
    }

    @Override
    public List<Team> queryPoint_jck_historyQtaj1() {
        return teamDao.queryPoint_jck_historyQtaj1();
    }

    @Override
    public List<Team> queryPoint_jck_historyQtaj2() {
        return teamDao.queryPoint_jck_historyQtaj2();
    }

    @Override
    public Integer changeTeamPoint_jck_qtaj(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_qtaj(team_name,point);
    }
    //获取当前中队的qtaj分数
    @Override
    public Team queryTeamPoint_jck_qtaj(String team_name) {
        return teamDao.queryTeamPoint_jck_qtaj(team_name);
    }
    //消除qtaj浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_qtajPointError(String team_name,double point) {
        return teamDao.fixdict_jck_qtajPointError(team_name,point);
    }

    //新增监察科（业务亮点）
    @Override
    public List<Team> queryPoint_jck_ywld() {
        return teamDao.queryPoint_jck_ywld();
    }

    @Override
    public List<Team> queryPoint_jck_historyYwld1() {
        return teamDao.queryPoint_jck_historyYwld1();
    }

    @Override
    public Integer changeTeamPoint_jck_ywld(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_ywld(team_name,point);
    }
    //获取当前中队的ywld分数
    @Override
    public Team queryTeamPoint_jck_ywld(String team_name) {
        return teamDao.queryTeamPoint_jck_ywld(team_name);
    }
    @Override
    public Integer fixdict_jck_ywldPointError(String team_name,double point) {
        return teamDao.fixdict_jck_ywldPointError(team_name,point);
    }
//新增监察科（备注）
    @Override
    public List<Team> queryPoint_jck_bz() {
        return teamDao.queryPoint_jck_bz();
    }

    @Override
    public Integer changeTeamPoint_jck_bz(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_bz(team_name,point);
    }
    @Override
    public Team queryTeamPoint_jck_bz(String team_name) {
        return teamDao.queryTeamPoint_jck_bz(team_name);
    }
    //消除bz浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_bzPointError(String team_name,double point) {
        return teamDao.fixdict_jck_bzPointError(team_name,point);
    }


    @Override
    public List<Team> queryPoint_jck_wyzf() {
        return teamDao.queryPoint_jck_wyzf();
    }

    @Override
    public List<Team> queryPoint_jck_historyWyzf1() {
        return teamDao.queryPoint_jck_historyWyzf1();
    }

    @Override
    public Integer changeTeamPoint_jck_wyzf(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_wyzf(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_wyzf_wgqy(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_wyzf_wgqy(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_wyzf_qlhl(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_wyzf_qlhl(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_wyzf_ldlf(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_wyzf_ldlf(team_name,point);
    }
    @Override
    public Integer changeTeamPoint_jck_wyzf_wyxq(String team_name,float point) {
        return teamDao.changeTeamPoint_jck_wyzf_wyxq(team_name,point);
    }
    //获取当前中队的wyzf分数
    @Override
    public Team queryTeamPoint_jck_wyzf(String team_name) {
        return teamDao.queryTeamPoint_jck_wyzf(team_name);
    }
    //消除wyzf浮点型分数归零的误差
    @Override
    public Integer fixdict_jck_wyzfPointError(String team_name,double point) {
        return teamDao.fixdict_jck_wyzfPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_wyzf_wgqyPointError(String team_name,double point) {
        return teamDao.fixdict_jck_wyzf_wgqyPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_wyzf_qlhlPointError(String team_name,double point) {
        return teamDao.fixdict_jck_wyzf_qlhlPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_wyzf_ldlfPointError(String team_name,double point) {
        return teamDao.fixdict_jck_wyzf_ldlfPointError(team_name,point);
    }
    @Override
    public Integer fixdict_jck_wyzf_wyxqPointError(String team_name,double point) {
        return teamDao.fixdict_jck_wyzf_wyxqPointError(team_name,point);
    }
//    @Override
//    public List<Team> queryPoint_jck_total(){
//        return teamDao.queryPoint_jck_total();
//    };
    @Override
    public List<Team> queryPoint_jck_total(){
        //对于求和所得的point_jck_total和point_total，底层sql语句执行的是float类型的计算，会出现精度丢失的情况
        //所以这里要用BigDecimal类型重新对point_jck_total和point_total进行精确求和计算
        List<Team> list = teamDao.queryPoint_jck_total();
        for(int i=0;i<list.size();i++){
            BigDecimal point_zwb = new BigDecimal(String.valueOf(list.get(i).getPoint_zwb()));
            BigDecimal point_ggb = new BigDecimal(String.valueOf(list.get(i).getPoint_ggb()));
            BigDecimal point_ztb = new BigDecimal(String.valueOf(list.get(i).getPoint_ztb()));
            BigDecimal point_dcs = new BigDecimal(String.valueOf(list.get(i).getPoint_dcs()));
            BigDecimal point_xck = new BigDecimal(String.valueOf(list.get(i).getPoint_xck()));
            BigDecimal point_bgs = new BigDecimal(String.valueOf(list.get(i).getPoint_bgs()));
            BigDecimal point_xfk = new BigDecimal(String.valueOf(list.get(i).getPoint_xfk()));
            BigDecimal point_fzk = new BigDecimal(String.valueOf(list.get(i).getPoint_fzk()));
            BigDecimal point_zgk = new BigDecimal(String.valueOf(list.get(i).getPoint_zgk()));
            BigDecimal point_dsjk = new BigDecimal(String.valueOf(list.get(i).getPoint_dsjk()));
            BigDecimal point_ldpy = new BigDecimal(String.valueOf(list.get(i).getPoint_ldpy()));

            BigDecimal point_jck_srzx = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_srzx()));
            BigDecimal point_jck_hjws = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_hjws()));
            BigDecimal point_jck_hjws_zljy = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_hjws_zljy()));
            BigDecimal point_jck_hjws_xgg = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_hjws_xgg()));
            BigDecimal point_jck_hjws_ws = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_hjws_ws()));
            BigDecimal point_jck_hjws_ljfl = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_hjws_ljfl()));
            BigDecimal point_jck_szss = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_szss()));
            BigDecimal point_jck_szss_rq = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_szss_rq()));
            BigDecimal point_jck_szss_pw = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_szss_pw()));
            BigDecimal point_jck_szss_jl = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_szss_jl()));
            BigDecimal point_jck_yllh = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_yllh()));
            BigDecimal point_jck_yllh_lh = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_yllh_lh()));
            BigDecimal point_jck_yllh_yy = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_yllh_yy()));
            BigDecimal point_jck_yllh_ztc1 = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_yllh_ztc1()));
            BigDecimal point_jck_yllh_ztc2 = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_yllh_ztc2()));
            BigDecimal point_jck_qtaj = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_qtaj()));
            BigDecimal point_jck_ywld = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_ywld()));
            BigDecimal point_jck_bz = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_bz()));
            BigDecimal point_jck_wyzf = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_wyzf()));
            BigDecimal point_jck_wyzf_ldlf = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_wyzf_ldlf()));
            BigDecimal point_jck_wyzf_wyxq = new BigDecimal(String.valueOf(list.get(i).getPoint_jck_wyzf_wyxq()));

            BigDecimal point_jck_total = point_jck_srzx.add(point_jck_hjws.add(point_jck_szss.add(point_jck_yllh.add(point_jck_wyzf.add(point_jck_qtaj.add(point_jck_ywld.add(point_jck_bz)))))));
            BigDecimal point_tmp = point_jck_total.add(point_zwb.add(point_ggb.add(point_ztb.add(point_dcs.add(point_xck.add(point_bgs.add(point_xfk.add(point_fzk.add(point_zgk.add(point_dsjk))))))))));
//            BigDecimal point_total = point_tmp.multiply(BigDecimal.valueOf(0.8)).add(point_ldpy);
            //修改为正常权重
            BigDecimal point_total = point_tmp.add(point_ldpy);
            BigDecimal point_jck_yllh_ztc=point_jck_yllh_ztc1.add(point_jck_yllh_ztc2);
            BigDecimal point_jck_wyzf_ldlf2=point_jck_wyzf_ldlf.add(point_jck_wyzf_wyxq);
            list.get(i).setPoint_jck_total(Float.parseFloat(String.valueOf(point_jck_total)));
            list.get(i).setPoint_total(Float.parseFloat(String.valueOf(point_total)));
            list.get(i).setPoint_jck_yllh_ztc(Float.parseFloat(String.valueOf(point_jck_yllh_ztc)));
            list.get(i).setPoint_jck_wyzf_ldlf2(Float.parseFloat(String.valueOf(point_jck_wyzf_ldlf2)));
        }
        return list;
    };

    @Override
    public List<Team> queryPoint_dsjk() {
        return teamDao.queryPoint_dsjk();
    }

    @Override
    public List<Team> queryPoint_historyDsjk1() {
        return teamDao.queryPoint_historyDsjk1();
    }

    @Override
    public Integer changeTeamPoint_dsjk(String team_name,float point) {
        return teamDao.changeTeamPoint_dsjk(team_name,point);
    }
    //获取当前中队的dcs分数
    @Override
    public Team queryTeamPoint_dsjk(String team_name) {
        return teamDao.queryTeamPoint_dsjk(team_name);
    }
    @Override
    public Integer fixdict_dsjkPointError(String team_name,double point) {
        return teamDao.fixdict_dsjkPointError(team_name,point);
    }
    @Override
    public Integer assignTeamPoint_dsjk(String team_name,float point) {
        return teamDao.assignTeamPoint_dsjk(team_name,point);
    }

    @Override
    public Integer changeTeamPoint_ldpy(String team_name,float point) {
        return teamDao.changeTeamPoint_ldpy(team_name,point);
    }

    @Override
    public Integer resetPoint(float resetPoint_zwb, float resetPoint_ggb, float resetPoint_ztb, float resetPoint_dcs,float resetPoint_dcs_ajcz,float resetPoint_dcs_hs, float resetPoint_xck, float resetPoint_bgs, float resetPoint_xfk, float resetPoint_fzk, float resetPoint_zgk, float resetPoint_jck_srzx, float resetPoint_jck_hjws,float resetPoint_jck_hjws_zljy,float resetPoint_jck_hjws_xgg,float resetPoint_jck_hjws_ws,float resetPoint_jck_hjws_ljfl, float resetPoint_jck_szss,
                              float resetPoint_jck_szss_rq,float resetPoint_jck_szss_pw,float resetPoint_jck_szss_jl,float resetPoint_jck_yllh,float resetPoint_jck_yllh_lh,float resetPoint_jck_yllh_yy,float resetPoint_jck_yllh_ztc1,float resetPoint_jck_yllh_ztc2,float resetPoint_jck_qtaj, float resetPoint_jck_ywld,float resetPoint_jck_bz,
                              float resetPoint_jck_wyzf,float resetPoint_jck_wyzf_wgqy,float resetPoint_jck_wyzf_qlhl,float resetPoint_jck_wyzf_ldlf,float resetPoint_jck_wyzf_wyxq, float resetPoint_dsjk, float resetPoint_ldpy) {
        return teamDao.resetPoint(resetPoint_zwb,resetPoint_ggb,resetPoint_ztb,resetPoint_dcs,resetPoint_dcs_ajcz,resetPoint_dcs_hs,resetPoint_xck,resetPoint_bgs,resetPoint_xfk,resetPoint_fzk,resetPoint_zgk,resetPoint_jck_srzx,resetPoint_jck_hjws,resetPoint_jck_hjws_zljy,resetPoint_jck_hjws_xgg,resetPoint_jck_hjws_ws,resetPoint_jck_hjws_ljfl,resetPoint_jck_szss,resetPoint_jck_szss_rq,resetPoint_jck_szss_pw,resetPoint_jck_szss_jl,
                resetPoint_jck_yllh,resetPoint_jck_yllh_lh,resetPoint_jck_yllh_yy,resetPoint_jck_yllh_ztc1,resetPoint_jck_yllh_ztc2,resetPoint_jck_qtaj,resetPoint_jck_ywld,resetPoint_jck_bz,resetPoint_jck_wyzf,resetPoint_jck_wyzf_wgqy,resetPoint_jck_wyzf_qlhl,resetPoint_jck_wyzf_ldlf,resetPoint_jck_wyzf_wyxq,resetPoint_dsjk,resetPoint_ldpy);
    }

}
