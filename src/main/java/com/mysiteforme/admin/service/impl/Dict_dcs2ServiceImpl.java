package com.mysiteforme.admin.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.entity.Dict_dcs2;
import com.mysiteforme.admin.dao.Dict_dcs2Dao;
import com.mysiteforme.admin.service.Dict_dcs2Service;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author lsd
 * @since 2021-12-16
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class Dict_dcs2ServiceImpl extends ServiceImpl<Dict_dcs2Dao, Dict_dcs2> implements Dict_dcs2Service {

    //    @Cacheable(value = "dictCache",key = "#type",unless = "#result == null or #result.size() == 0")
    @Override
    public List<Dict_dcs2> getDictByType(String type) {
        EntityWrapper<Dict_dcs2> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        wrapper.eq("del_flag",false);
        wrapper.orderBy("sort");
        return selectList(wrapper);
    }

    @Override
    public Integer getCountByType(String type) {
        EntityWrapper<Dict_dcs2> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        wrapper.eq("del_flag",false);
        return selectCount(wrapper);
    }

    @Override
    public Integer getMaxSortByType(String type) {
        Object o = selectObj(Condition.create().setSqlSelect("max(sort)").eq("type",type));
        int sort = 0;
        if(o != null){
            sort =  (Integer)o + 1;
        }
        return sort;
    }

    @Override
    public Integer getCountByAll(String type, String label, String value) {
        EntityWrapper<Dict_dcs2> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        if(StringUtils.isNotBlank(label)){
            wrapper.eq("label",label);
        }
        if(StringUtils.isNotBlank(value)){
            wrapper.eq("value",value);
        }
        wrapper.eq("del_flag",false);
        return selectCount(wrapper);
    }


    //    @CacheEvict(value = "dictCache",key = "#dict.type",condition = "#dict.type ne null ")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void saveOrUpdateDict(Dict_dcs2 dict_dcs2) {
        insertOrUpdate(dict_dcs2);
    }



    //    @CacheEvict(value = "dictCache",key = "#result",beforeInvocation = false,condition = "#result ne  null ")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public String deleteDict(Long id) {
        Dict_dcs2 dict_dcs2 = baseMapper.selectById(id);
        baseMapper.deleteById(id);
        return dict_dcs2.getType();
    }


    //    @CacheEvict(value = "dictCache",key = "#type",beforeInvocation = false)
    @Override
    public List<Dict_dcs2> saveDictList(String type, List<Dict_dcs2> list) {
        insertBatch(list);
        return list;
    }

    //    @CacheEvict(value = "dictCache",key = "#type",beforeInvocation = false)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void deleteByType(String type) {
        EntityWrapper<Dict_dcs2> wrapper = new EntityWrapper<>();
        wrapper.eq("type",type);
        delete(wrapper);
    }

    //    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void deleteByTableName(String tableName) {
        EntityWrapper<Dict_dcs2> wrapper = new EntityWrapper<>();
        wrapper.like("description","数据表【"+tableName+"】");
        delete(wrapper);
    }

    //    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public void updateByType(String oldType,String newType) {
        EntityWrapper<Dict_dcs2> wrapper = new EntityWrapper<>();
        wrapper.eq("type",oldType);
        List<Dict_dcs2> dicts_dcs = baseMapper.selectList(wrapper);
        for (Dict_dcs2 dict_dcs2 : dicts_dcs){
            dict_dcs2.setType(newType);
        }
        updateBatchById(dicts_dcs);
    }

    //    @CacheEvict(value = "dictCache",allEntries=true)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @Override
    public boolean updateVisableFlag() {
        EntityWrapper<Dict_dcs2> wrapper = new EntityWrapper<>();
        wrapper.eq("visable_flag",1);
        List<Dict_dcs2> dicts_dcs = baseMapper.selectList(wrapper);
        //如果list为空，说明没有扣分项目，所以无需设置VisableFlag，可以直接返回true表示操作成功
        if(dicts_dcs.isEmpty()){
            return true;
        }
        for (Dict_dcs2 dict_dcs2 : dicts_dcs){
            dict_dcs2.setVisable_flag(0);
        }
        boolean resultFlag = updateBatchById(dicts_dcs);
        return resultFlag;
    }

}

