package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_jck_qtaj;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author wcl
 * @since 2023-8-19
 */
public interface Dict_jck_qtajService extends IService<Dict_jck_qtaj> {

    List<Dict_jck_qtaj>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_jck_qtaj dict_jck_qtaj);

    String deleteDict(Long id);

    List<Dict_jck_qtaj> saveDictList(String type, List<Dict_jck_qtaj> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}