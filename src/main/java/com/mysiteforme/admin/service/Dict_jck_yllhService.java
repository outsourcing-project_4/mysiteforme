package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_jck_yllh;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2021-12-19
 */
public interface Dict_jck_yllhService extends IService<Dict_jck_yllh> {

    List<Dict_jck_yllh>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_jck_yllh dict_jck_yllh);

    String deleteDict(Long id);

    List<Dict_jck_yllh> saveDictList(String type, List<Dict_jck_yllh> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
