package com.mysiteforme.admin.service;

import com.mysiteforme.admin.entity.VO.ItemVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ItemVoService {

    //治违办
    List<ItemVO> queryList_zwb();
    List<ItemVO> queryLeibie_zwb();
    List<ItemVO> queryPingfen_zwb(String law_item_leibie);
    List<ItemVO> queryList_description_zwb(String law_item_pingfen);
    List<ItemVO> queryPoint_zwb(String description);
    Integer changePoint_zwb(String type,String description,String point);

    //广告办
    List<ItemVO> queryList_ggb();
    List<ItemVO> queryLeibie_ggb();
    List<ItemVO> queryPingfen_ggb(String law_item_leibie);
    List<ItemVO> queryList_description_ggb(String law_item_pingfen);
    List<ItemVO> queryPoint_ggb(String description);
    Integer changePoint_ggb(String type,String description,String point);

    //渣土办
    List<ItemVO> queryList_ztb();
    List<ItemVO> queryLeibie_ztb();
    List<ItemVO> queryPingfen_ztb(String law_item_leibie);
    List<ItemVO> queryList_description_ztb(String law_item_pingfen);
    List<ItemVO> queryPoint_ztb(String description);
    Integer changePoint_ztb(String type,String description,String point);

    //督察室
    List<ItemVO> queryList_fws();
    List<ItemVO> queryList_fws2();
    List<ItemVO> queryLeibie_fws();
    List<ItemVO> queryLeibie_fws2();
    List<ItemVO> queryPingfen_fws(String law_item_leibie);
    List<ItemVO> queryPingfen_fws2(String law_item_leibie);
    List<ItemVO> queryList_description_fws(String law_item_pingfen);
    List<ItemVO> queryList_description_fws2(String law_item_pingfen);
    List<ItemVO> queryPoint_fws(String description);
    List<ItemVO> queryPoint_fws2(String description);
    Integer changePoint_fws(String type,String description,String point);

    //宣传科
    List<ItemVO> queryList_xck();
    List<ItemVO> queryLeibie_xck();
    List<ItemVO> queryPingfen_xck(String law_item_leibie);
    List<ItemVO> queryList_description_xck(String law_item_pingfen);
    List<ItemVO> queryPoint_xck(String description);
    Integer changePoint_xck(String type,String description,String point);

    // 办公室
    List<ItemVO> queryList_bgs();
    List<ItemVO> queryLeibie_bgs();
    List<ItemVO> queryPingfen_bgs(String law_item_leibie);
    List<ItemVO> queryList_description_bgs(String law_item_pingfen);
    List<ItemVO> queryPoint_bgs(String description);
    Integer changePoint_bgs(String type,String description,String point);

    // 信访科
    List<ItemVO> queryList_xfk();
    List<ItemVO> queryLeibie_xfk();
    List<ItemVO> queryPingfen_xfk(String law_item_leibie);
    List<ItemVO> queryList_description_xfk(String law_item_pingfen);
    List<ItemVO> queryPoint_xfk(String description);
    Integer changePoint_xfk(String type,String description,String point);

    // 政工科
    List<ItemVO> queryList_zgk();
    List<ItemVO> queryLeibie_zgk();
    List<ItemVO> queryPingfen_zgk(String law_item_leibie);
    List<ItemVO> queryList_description_zgk(String law_item_pingfen);
    List<ItemVO> queryPoint_zgk(String description);
    Integer changePoint_zgk(String type,String description,String point);

    // 法制科
    List<ItemVO> queryList_fzk();
    List<ItemVO> queryLeibie_fzk();
    List<ItemVO> queryPingfen_fzk(String law_item_leibie);
    List<ItemVO> queryList_description_fzk(String law_item_pingfen);
    List<ItemVO> queryPoint_fzk(String description);
    Integer changePoint_fzk(String type,String description,String point);

    //监察科_市容秩序
    List<ItemVO> queryList_jck_srzx();
    List<ItemVO> queryLeibie_jck_srzx();
    List<ItemVO> queryPingfen_jck_srzx(String law_item_leibie);
    List<ItemVO> queryList_description_jck_srzx(String law_item_pingfen);
    List<ItemVO> queryPoint_jck_srzx(String description);
    Integer changePoint_jck_srzx(String type,String description,String point);

    //监察科_环境卫生
    List<ItemVO> queryList_jck_hjws();
    List<ItemVO> queryLeibie_jck_hjws();
    List<ItemVO> queryPingfen_jck_hjws(String law_item_leibie);
    List<ItemVO> queryList_description_jck_hjws(String law_item_pingfen);
    List<ItemVO> queryPoint_jck_hjws(String description);
    Integer changePoint_jck_hjws(String type,String description,String point);

    //监察科_市政设施
    List<ItemVO> queryList_jck_szss();
    List<ItemVO> queryLeibie_jck_szss();
    List<ItemVO> queryPingfen_jck_szss(String law_item_leibie);
    List<ItemVO> queryList_description_jck_szss(String law_item_pingfen);
    List<ItemVO> queryPoint_jck_szss(String description);
    Integer changePoint_jck_szss(String type,String description,String point);

    //监察科_园林绿化
    List<ItemVO> queryList_jck_yllh();
    List<ItemVO> queryLeibie_jck_yllh();
    List<ItemVO> queryPingfen_jck_yllh(String law_item_leibie);
    List<ItemVO> queryList_description_jck_yllh(String law_item_pingfen);
    List<ItemVO> queryPoint_jck_yllh(String description);
    Integer changePoint_jck_yllh(String type,String description,String point);

    //监察科_其他案件
    List<ItemVO> queryList_jck_qtaj();
    List<ItemVO> queryLeibie_jck_qtaj();
    List<ItemVO> queryPingfen_jck_qtaj(String law_item_leibie);
    List<ItemVO> queryList_description_jck_qtaj(String law_item_pingfen);
    List<ItemVO> queryPoint_jck_qtaj(String description);
    Integer changePoint_jck_qtaj(String type,String description,String point);

    //监察科_业务亮点
    List<ItemVO> queryList_jck_ywld();
    List<ItemVO> queryLeibie_jck_ywld();
    List<ItemVO> queryPingfen_jck_ywld(String law_item_leibie);
    List<ItemVO> queryList_description_jck_ywld(String law_item_pingfen);
    List<ItemVO> queryPoint_jck_ywld(String description);
    Integer changePoint_jck_ywld(String type,String description,String point);

    //监察科_备注
    List<ItemVO> queryList_jck_bz();
    List<ItemVO> queryLeibie_jck_bz();
    List<ItemVO> queryPingfen_jck_bz(String law_item_leibie);
    List<ItemVO> queryList_description_jck_bz(String law_item_pingfen);
    List<ItemVO> queryPoint_jck_bz(String description);
    Integer changePoint_jck_bz(String type,String description,String point);

    //监察科_物业执法
    List<ItemVO> queryList_jck_wyzf();
    List<ItemVO> queryLeibie_jck_wyzf();
    List<ItemVO> queryPingfen_jck_wyzf(String law_item_leibie);
    List<ItemVO> queryList_description_jck_wyzf(String law_item_pingfen);
    List<ItemVO> queryPoint_jck_wyzf(String description);
    Integer changePoint_jck_wyzf(String type,String description,String point);

    //大数据科
    List<ItemVO> queryList_dsjk();
    List<ItemVO> queryLeibie_dsjk();
    List<ItemVO> queryPingfen_dsjk(String law_item_leibie);
    List<ItemVO> queryList_description_dsjk(String law_item_pingfen);
    List<ItemVO> queryPoint_dsjk(String description);
    Integer changePoint_dsjk(String type,String description,String point);

}
