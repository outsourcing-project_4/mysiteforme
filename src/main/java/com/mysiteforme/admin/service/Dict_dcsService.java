package com.mysiteforme.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.Dict_dcs;
import com.mysiteforme.admin.entity.Dict_dcs2;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lsd
 * @since 2021-12-19
 */
public interface Dict_dcsService extends IService<Dict_dcs> {

    List<Dict_dcs>  getDictByType(String type);

    Integer getCountByType(String type);

    Integer getMaxSortByType(String type);

    Integer getCountByAll(String type,String label,String value);

    void saveOrUpdateDict(Dict_dcs dict_dcs);

    String deleteDict(Long id);

    List<Dict_dcs> saveDictList(String type, List<Dict_dcs> list);

    void deleteByType(String s);

    void deleteByTableName(String tableName);

    void updateByType(String oldType,String newType);

    boolean updateVisableFlag();
}
