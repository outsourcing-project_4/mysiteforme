package com.mysiteforme.admin.service;

import com.mysiteforme.admin.entity.Team;
import com.mysiteforme.admin.entity.Team_dcs;

import java.util.List;

public interface TeamService {

    List<String> queryTeam();

    List<Team> queryPoint_zwb();
    List<Team> queryPoint_historyZwb1();
    Integer changeTeamPoint_zwb(String team_name,float point);
    Team queryTeamPoint_zwb(String team_name);
    //消除zwb浮点型分数归零的误差
    Integer fixdict_zwbPointError(String team_name,double point);

    List<Team> queryPoint_ggb();
    List<Team> queryPoint_historyGgb1();
    List<Team> queryPoint_historyGgb2();
    Integer changeTeamPoint_ggb(String team_name,float point);
    Team queryTeamPoint_ggb(String team_name);
    //消除yllh浮点型分数归零的误差
    Integer fixdict_ggbPointError(String team_name,double point);

    List<Team> queryPoint_ztb();
    Integer changeTeamPoint_ztb(String team_name,float point);

    List<Team> queryPoint_dcs();
    List<Team> queryPoint_historyDcs1();
    Integer changeTeamPoint_dcs(String team_name,float point);
    Integer changeTeamPoint_dcs_ajcz(String team_name,float point);
    Integer changeTeamPoint_dcs_hs(String team_name,float point);
    Team queryTeamPoint_dcs(String team_name);
    //消除dcs浮点型分数归零的误差
    Integer fixdict_dcsPointError(String team_name,double point);
    Integer fixdict_dcs_ajczPointError(String team_name,double point);
    Integer fixdict_dcs_hcPointError(String team_name,double point);


    List<Team> queryPoint_xck();
    List<Team> queryPoint_historyXck1();
    Integer changeTeamPoint_xck(String team_name,float point);
    Team queryTeamPoint_xck(String team_name);
    //消除xck浮点型分数归零的误差
    Integer fixdict_xckPointError(String team_name,double point);

    List<Team> queryPoint_bgs();
    Integer changeTeamPoint_bgs(String team_name,float point);

    List<Team> queryPoint_xfk();
    List<Team> queryPoint_historyXfk1();
    List<Team> queryPoint_historyXfk2();
    Integer changeTeamPoint_xfk(String team_name,float point);
    Team queryTeamPoint_xfk(String team_name);
    //消除xfk浮点型分数归零的误差
    Integer fixdict_xfkPointError(String team_name,double point);

    List<Team> queryPoint_zgk();
    List<Team> queryPoint_historyZgk1();
    Integer changeTeamPoint_zgk(String team_name,float point);
    Team queryTeamPoint_zgk(String team_name);
    //消除zgk浮点型分数归零的误差
    Integer fixdict_zgkPointError(String team_name,double point);

    List<Team> queryPoint_fzk();
    List<Team> queryPoint_historyFzk1();
    Integer changeTeamPoint_fzk(String team_name,float point);
    Team queryTeamPoint_fzk(String team_name);
    //消除fzk浮点型分数归零的误差
    Integer fixdict_fzkPointError(String team_name,double point);

    List<Team> queryPoint_jck_srzx();
    List<Team> queryPoint_jck_historySrzx1();
    List<Team> queryPoint_jck_historySrzx2();
    Integer changeTeamPoint_jck_srzx(String team_name,float point);
    Team queryTeamPoint_jck_srzx(String team_name);
    Integer xiaxianTeamPoint_jck_srzx(String team_name,float Point_jck_srzx);
    Integer rechangeTeamPoint_jck_srzx(String team_name,float point);

    List<Team> queryPoint_jck_hjws();
    List<Team> queryPoint_jck_historyHjws1();
    List<Team> queryPoint_jck_historyHjws2();
    Integer changeTeamPoint_jck_hjws(String team_name,float point);
    Integer changeTeamPoint_jck_hjws_zljy(String team_name,float point);
    Integer changeTeamPoint_jck_hjws_xgg(String team_name,float point);
    Integer changeTeamPoint_jck_hjws_ws(String team_name,float point);
    Integer changeTeamPoint_jck_hjws_ljfl(String team_name,float point);
    Team queryTeamPoint_jck_hjws(String team_name);
    Integer zeroTeamPoint_jck_hjws(String team_name);
    //消除hjws浮点型分数归零的误差
    Integer fixdict_jck_hjwsPointError(String team_name,double point);
    Integer fixdict_jck_hjws_zljyPointError(String team_name,double point);
    Integer fixdict_jck_hjws_xggPointError(String team_name,double point);
    Integer fixdict_jck_hjws_wsPointError(String team_name,double point);
    Integer fixdict_jck_hjws_ljflPointError(String team_name,double point);

    List<Team> queryPoint_jck_szss();
    List<Team> queryPoint_jck_historySzss1();
    List<Team> queryPoint_jck_historySzss2();
    Integer changeTeamPoint_jck_szss(String team_name,float point);
    Integer changeTeamPoint_jck_szss_rq(String team_name,float point);
    Integer changeTeamPoint_jck_szss_pw(String team_name,float point);
    Integer changeTeamPoint_jck_szss_jl(String team_name,float point);
    Team queryTeamPoint_jck_szss(String team_name);
    //消除szss浮点型分数归零的误差
    Integer fixdict_jck_szssPointError(String team_name,double point);
    //消除szss_rq浮点型分数归零的误差
    Integer fixdict_jck_szss_rqPointError(String team_name,double point);
    //消除szss_pw浮点型分数归零的误差
    Integer fixdict_jck_szss_pwPointError(String team_name,double point);
    //消除szss_jl浮点型分数归零的误差
    Integer fixdict_jck_szss_jlPointError(String team_name,double point);

    List<Team> queryPoint_jck_yllh();
    List<Team> queryPoint_jck_historyYllh1();
    List<Team> queryPoint_jck_historyYllh2();
    Integer changeTeamPoint_jck_yllh(String team_name,float point);
    Integer changeTeamPoint_jck_yllh_lh(String team_name,float point);
    Integer changeTeamPoint_jck_yllh_yy(String team_name,float point);
    Integer changeTeamPoint_jck_yllh_ztc1(String team_name,float point);
    Integer changeTeamPoint_jck_yllh_ztc2(String team_name,float point);
    Integer xiaxianTeamPoint_jck_yllh(String team_name,float Point_jck_yllh);
    Team queryTeamPoint_jck_yllh(String team_name);
    //消除yllh浮点型分数归零的误差
    Integer fixdict_jck_yllhPointError(String team_name,double point);
    Integer fixdict_jck_yllh_lhPointError(String team_name,double point);
    Integer fixdict_jck_yllh_yyPointError(String team_name,double point);
    Integer fixdict_jck_yllh_ztc1PointError(String team_name,double point);
    Integer fixdict_jck_yllh_ztc2PointError(String team_name,double point);

    List<Team> queryPoint_jck_qtaj();
    List<Team> queryPoint_jck_historyQtaj1();
    List<Team> queryPoint_jck_historyQtaj2();
    Integer changeTeamPoint_jck_qtaj(String team_name,float point);
    Team queryTeamPoint_jck_qtaj(String team_name);
    //消除qtaj浮点型分数归零的误差
    Integer fixdict_jck_qtajPointError(String team_name,double point);

    List<Team> queryPoint_jck_ywld();
    List<Team> queryPoint_jck_historyYwld1();
    Integer changeTeamPoint_jck_ywld(String team_name,float point);
    Team queryTeamPoint_jck_ywld(String team_name);
    //消除ywld浮点型分数归零的误差
    Integer fixdict_jck_ywldPointError(String team_name,double point);

    List<Team> queryPoint_jck_bz();
    Integer changeTeamPoint_jck_bz(String team_name,float point);
    Team queryTeamPoint_jck_bz(String team_name);
    //消除bz浮点型分数归零的误差
    Integer fixdict_jck_bzPointError(String team_name,double point);

    List<Team> queryPoint_jck_wyzf();
    List<Team> queryPoint_jck_historyWyzf1();
    Integer changeTeamPoint_jck_wyzf(String team_name,float point);
    Integer changeTeamPoint_jck_wyzf_wgqy(String team_name,float point);
    Integer changeTeamPoint_jck_wyzf_qlhl(String team_name,float point);
    Integer changeTeamPoint_jck_wyzf_ldlf(String team_name,float point);
    Integer changeTeamPoint_jck_wyzf_wyxq(String team_name,float point);
    Team queryTeamPoint_jck_wyzf(String team_name);
    //消除wyzf浮点型分数归零的误差
    Integer fixdict_jck_wyzfPointError(String team_name,double point);
    Integer fixdict_jck_wyzf_wgqyPointError(String team_name,double point);
    Integer fixdict_jck_wyzf_qlhlPointError(String team_name,double point);
    Integer fixdict_jck_wyzf_ldlfPointError(String team_name,double point);
    Integer fixdict_jck_wyzf_wyxqPointError(String team_name,double point);

    List<Team> queryPoint_jck_total();

    List<Team> queryPoint_dsjk();
    List<Team> queryPoint_historyDsjk1();
    Integer changeTeamPoint_dsjk(String team_name,float point);
    Team queryTeamPoint_dsjk(String team_name);
    //消除dcs浮点型分数归零的误差
    Integer fixdict_dsjkPointError(String team_name,double point);
    Integer assignTeamPoint_dsjk(String team_name,float point);

    Integer changeTeamPoint_ldpy(String team_name,float point);

    Integer resetPoint(float resetPoint_zwb,float resetPoint_ggb,float resetPoint_ztb,float resetPoint_dcs,float resetPoint_dcs_ajcz,float resetPoint_dcs_hs,
                       float resetPoint_xck,float resetPoint_bgs,float resetPoint_xfk,float resetPoint_fzk,
                       float resetPoint_zgk,float resetPoint_jck_srzx,float resetPoint_jck_hjws,float resetPoint_jck_hjws_zljy,float resetPoint_jck_hjws_xgg,float resetPoint_jck_hjws_ws,float resetPoint_jck_hjws_ljfl,float resetPoint_jck_szss,
                       float resetPoint_jck_szss_rq,float resetPoint_jck_szss_pw,float resetPoint_jck_szss_jl, float resetPoint_jck_yllh,float resetPoint_jck_yllh_lh,float resetPoint_jck_yllh_yy,float resetPoint_jck_yllh_ztc1,float resetPoint_jck_yllh_ztc2,
                       float resetPoint_jck_qtaj,float resetPoint_jck_ywld,float resetPoint_jck_bz,float resetPoint_jck_wyzf,float resetPoint_jck_wyzf_wgqy,float resetPoint_jck_wyzf_qlhl,float resetPoint_jck_wyzf_ldlf,float resetPoint_jck_wyzf_wyxq,float resetPoint_dsjk,float resetPoint_ldpy);


}