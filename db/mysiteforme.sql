/*
 Navicat Premium Data Transfer

 Source Server         : 脚印科技MySQL【47.104.75.183】
 Source Server Type    : MySQL
 Source Server Version : 50738 (5.7.38)
 Source Host           : 47.104.75.183:30001
 Source Schema         : mysiteforme

 Target Server Type    : MySQL
 Target Server Version : 50738 (5.7.38)
 File Encoding         : 65001

 Date: 05/09/2024 14:35:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for blog_article
-- ----------------------------
DROP TABLE IF EXISTS `blog_article`;
CREATE TABLE `blog_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) NOT NULL COMMENT '标题,input,NO,false,true,true',
  `sub_title` varchar(255) DEFAULT NULL COMMENT '副标题,input,YES,false,false,false',
  `marks` varchar(255) DEFAULT NULL COMMENT '摘要,textarea,YES,false,false,false',
  `show_pic` varchar(255) DEFAULT NULL COMMENT '显示图片,uploadImg,YES,false,false,false',
  `category` varchar(255) DEFAULT NULL COMMENT '文章类型,radio,YES,false,true,true',
  `out_link_url` varchar(255) DEFAULT NULL COMMENT '外链地址,input,YES,false,false,false',
  `resources` varchar(255) DEFAULT NULL COMMENT '来源,input,YES,false,true,false',
  `publist_time` datetime DEFAULT NULL COMMENT '发布时间,timer,YES,false,true,true',
  `content` text NOT NULL COMMENT '内容,editor,NO,false,true,true',
  `text` text COMMENT '纯文字文章内容,textarea,YES,false,false,false',
  `click` int(11) DEFAULT NULL COMMENT '浏览量,0,YES,false,false,false',
  `channel_id` bigint(20) DEFAULT NULL COMMENT '栏目ID,0,YES,false,false,false',
  `sort` int(11) DEFAULT NULL COMMENT '排序值,0,YES,false,false,false',
  `is_top` bit(1) DEFAULT NULL COMMENT '是否置顶,switch,YES,true,true,false',
  `is_recommend` bit(1) DEFAULT NULL COMMENT '是否推荐,switch,YES,true,true,false',
  `status` int(11) DEFAULT NULL COMMENT '文章状态,0,YES,false,false,false',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='博客内容,1,uploadImg-showPic-YES,timer-publistTime-YES,editor-co';

-- ----------------------------
-- Records of blog_article
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for blog_article_tags
-- ----------------------------
DROP TABLE IF EXISTS `blog_article_tags`;
CREATE TABLE `blog_article_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `article_id` bigint(20) NOT NULL COMMENT '文章ID,0,NO,false,false,false',
  `tags_id` bigint(20) NOT NULL COMMENT '标签ID,0,NO,false,false,false',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签-文章关联表,3';

-- ----------------------------
-- Records of blog_article_tags
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for blog_channel
-- ----------------------------
DROP TABLE IF EXISTS `blog_channel`;
CREATE TABLE `blog_channel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '名称,input,NO,false,true,true',
  `site_id` bigint(20) DEFAULT NULL COMMENT '站点ID,0,YES,false,false,false',
  `href` varchar(500) DEFAULT NULL COMMENT '链接地址,input,YES,false,true,true',
  `logo` varchar(255) DEFAULT NULL COMMENT '栏目图标,input,YES,false,true,false',
  `is_base_channel` bit(1) DEFAULT NULL COMMENT '是否为主栏目,switch,YES,true,true,true',
  `can_comment` bit(1) DEFAULT NULL COMMENT '是否能够评论,switch,YES,true,true,true',
  `is_no_name` bit(1) DEFAULT NULL COMMENT '是否匿名,switch,YES,true,true,true',
  `is_can_aduit` bit(1) DEFAULT NULL COMMENT '是否开启审核,switch,YES,true,true,true',
  `seo_title` varchar(255) DEFAULT NULL COMMENT '网页title(seo),input,YES,false,false,false',
  `seo_keywords` varchar(255) DEFAULT NULL COMMENT '网页关键字(seo) ,input,YES,false,false,false',
  `seo_description` varchar(255) DEFAULT NULL COMMENT '网页描述(seo),textarea,YES,false,false,false',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父节点ID',
  `parent_ids` varchar(2000) DEFAULT NULL COMMENT '父节点联集',
  `level` bigint(20) DEFAULT NULL COMMENT '层级深度',
  `sort` smallint(6) DEFAULT NULL COMMENT '排序',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='博客栏目,2,switch-baseChannel-YES-true-is_base_channel,switch-ca';

-- ----------------------------
-- Records of blog_channel
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for blog_comment
-- ----------------------------
DROP TABLE IF EXISTS `blog_comment`;
CREATE TABLE `blog_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `content` text NOT NULL COMMENT '评论内容,textarea,NO,false,true,true',
  `type` int(11) DEFAULT NULL COMMENT 'ip,input,YES,false,true,true',
  `ip` varchar(255) DEFAULT NULL COMMENT 'ip,input,YES,false,true,true',
  `system` varchar(255) DEFAULT NULL COMMENT '操作系统,input,YES,false,true,false',
  `browser` varchar(255) DEFAULT NULL COMMENT '浏览器,input,YES,false,true,false',
  `floor` int(11) DEFAULT NULL COMMENT '楼层,0,YES,false,false,false',
  `channel_id` bigint(20) DEFAULT NULL COMMENT '栏目ID,0,YES,false,false,false',
  `article_id` int(11) DEFAULT NULL COMMENT '文章ID,0,YES,false,false,false',
  `reply_id` bigint(20) DEFAULT NULL COMMENT '回复评论ID,0,YES,false,false,false',
  `is_admin_reply` bit(1) DEFAULT NULL COMMENT '管理员是否回复,switch,YES,true,true,true',
  `reply_content` text COMMENT '管理员回复内容,textarea,YES,false,true,false',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='博客评论,1,switch-adminReply-YES-true-is_admin_reply';

-- ----------------------------
-- Records of blog_comment
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for blog_tags
-- ----------------------------
DROP TABLE IF EXISTS `blog_tags`;
CREATE TABLE `blog_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '标签名字,input,YES,false,true,true',
  `sort` int(11) DEFAULT NULL COMMENT '排序,0,YES,false,false,false',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='博客标签,1';

-- ----------------------------
-- Records of blog_tags
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for dcs_point
-- ----------------------------
DROP TABLE IF EXISTS `dcs_point`;
CREATE TABLE `dcs_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `team_name` varchar(50) DEFAULT NULL COMMENT '中队名称',
  `point_dcs` float DEFAULT NULL COMMENT '中队分数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dcs_point
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for hat_area
-- ----------------------------
DROP TABLE IF EXISTS `hat_area`;
CREATE TABLE `hat_area` (
  `id` int(11) NOT NULL,
  `areaID` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `father` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hat_area
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for history_point1
-- ----------------------------
DROP TABLE IF EXISTS `history_point1`;
CREATE TABLE `history_point1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `team_name` varchar(50) DEFAULT NULL COMMENT '中队名称',
  `point_zwb` float DEFAULT NULL,
  `point_ggb` float DEFAULT NULL,
  `point_ztb` float DEFAULT NULL,
  `point_dcs` float DEFAULT NULL,
  `point_dcs_ajcz` float DEFAULT NULL,
  `point_dcs_hs` float DEFAULT NULL,
  `point_xck` float DEFAULT NULL,
  `point_bgs` float DEFAULT NULL,
  `point_xfk` float DEFAULT NULL,
  `point_fzk` float DEFAULT NULL,
  `point_zgk` float DEFAULT NULL,
  `point_jck_srzx` float DEFAULT NULL,
  `point_jck_hjws` float DEFAULT NULL,
  `point_jck_hjws_zljy` float DEFAULT NULL,
  `point_jck_hjws_xgg` float DEFAULT NULL,
  `point_jck_hjws_ws` float DEFAULT NULL,
  `point_jck_hjws_ljfl` float DEFAULT NULL,
  `point_jck_szss` float DEFAULT NULL,
  `point_jck_szss_rq` float DEFAULT NULL,
  `point_jck_szss_pw` float DEFAULT NULL,
  `point_jck_szss_jl` float DEFAULT NULL,
  `point_jck_yllh` float DEFAULT NULL,
  `point_jck_yllh_lh` float DEFAULT NULL,
  `point_jck_yllh_yy` float DEFAULT NULL,
  `point_jck_yllh_ztc1` float DEFAULT NULL,
  `point_jck_yllh_ztc2` float DEFAULT NULL,
  `point_jck_qtaj` float DEFAULT NULL,
  `point_jck_ywld` float DEFAULT NULL,
  `point_jck_bz` float DEFAULT NULL,
  `point_jck_wyzf` float DEFAULT NULL,
  `point_jck_wyzf_wgqy` float DEFAULT NULL,
  `point_jck_wyzf_qlhl` float DEFAULT NULL,
  `point_jck_wyzf_ldlf` float DEFAULT NULL,
  `point_jck_wyzf_wyxq` float DEFAULT NULL,
  `point_dsjk` float unsigned zerofill DEFAULT NULL,
  `point_ldpy` float DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of history_point1
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for history_point2
-- ----------------------------
DROP TABLE IF EXISTS `history_point2`;
CREATE TABLE `history_point2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(50) DEFAULT NULL,
  `point_zwb` float DEFAULT NULL,
  `point_ggb` float DEFAULT NULL,
  `point_ztb` float DEFAULT NULL,
  `point_dcs` float DEFAULT NULL,
  `point_dcs_ajcz` float DEFAULT NULL,
  `point_dcs_hs` float DEFAULT NULL,
  `point_xck` float DEFAULT NULL,
  `point_bgs` float DEFAULT NULL,
  `point_xfk` float DEFAULT NULL,
  `point_fzk` float DEFAULT NULL,
  `point_zgk` float DEFAULT NULL,
  `point_jck_srzx` float DEFAULT NULL,
  `point_jck_hjws` float DEFAULT NULL,
  `point_jck_hjws_zljy` float DEFAULT NULL,
  `point_jck_hjws_xgg` float DEFAULT NULL,
  `point_jck_hjws_ws` float DEFAULT NULL,
  `point_jck_hjws_ljfl` float DEFAULT NULL,
  `point_jck_szss` float DEFAULT NULL,
  `point_jck_szss_rq` float DEFAULT NULL,
  `point_jck_szss_pw` float DEFAULT NULL,
  `point_jck_szss_jl` float DEFAULT NULL,
  `point_jck_yllh` float DEFAULT NULL,
  `point_jck_yllh_lh` float DEFAULT NULL,
  `point_jck_yllh_yy` float DEFAULT NULL,
  `point_jck_yllh_ztc1` float DEFAULT NULL,
  `point_jck_yllh_ztc2` float DEFAULT NULL,
  `point_jck_qtaj` float DEFAULT NULL,
  `point_jck_ywld` float DEFAULT NULL,
  `point_jck_bz` float DEFAULT NULL,
  `point_jck_wyzf` float DEFAULT NULL,
  `point_jck_wyzf_wgqy` float DEFAULT NULL,
  `point_jck_wyzf_qlhl` float DEFAULT NULL,
  `point_jck_wyzf_ldlf` float DEFAULT NULL,
  `point_jck_wyzf_wyxq` float DEFAULT NULL,
  `point_dsjk` float unsigned zerofill DEFAULT NULL,
  `point_ldpy` float DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of history_point2
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for history_point3
-- ----------------------------
DROP TABLE IF EXISTS `history_point3`;
CREATE TABLE `history_point3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(50) DEFAULT NULL,
  `point_zwb` float DEFAULT NULL,
  `point_ggb` float DEFAULT NULL,
  `point_ztb` float DEFAULT NULL,
  `point_dcs` float DEFAULT NULL,
  `point_dcs_ajcz` float DEFAULT NULL,
  `point_dcs_hs` float DEFAULT NULL,
  `point_xck` float DEFAULT NULL,
  `point_bgs` float DEFAULT NULL,
  `point_xfk` float DEFAULT NULL,
  `point_fzk` float DEFAULT NULL,
  `point_zgk` float DEFAULT NULL,
  `point_jck_srzx` float DEFAULT NULL,
  `point_jck_hjws` float DEFAULT NULL,
  `point_jck_hjws_zljy` float DEFAULT NULL,
  `point_jck_hjws_xgg` float DEFAULT NULL,
  `point_jck_hjws_ws` float DEFAULT NULL,
  `point_jck_hjws_ljfl` float DEFAULT NULL,
  `point_jck_szss` float DEFAULT NULL,
  `point_jck_szss_rq` float DEFAULT NULL,
  `point_jck_szss_pw` float DEFAULT NULL,
  `point_jck_szss_jl` float DEFAULT NULL,
  `point_jck_yllh` float DEFAULT NULL,
  `point_jck_yllh_lh` float DEFAULT NULL,
  `point_jck_yllh_yy` float DEFAULT NULL,
  `point_jck_yllh_ztc1` float DEFAULT NULL,
  `point_jck_yllh_ztc2` float DEFAULT NULL,
  `point_jck_qtaj` float DEFAULT NULL,
  `point_jck_ywld` float DEFAULT NULL,
  `point_jck_bz` float DEFAULT NULL,
  `point_jck_wyzf` float DEFAULT NULL,
  `point_jck_wyzf_wgqy` float DEFAULT NULL,
  `point_jck_wyzf_qlhl` float DEFAULT NULL,
  `point_jck_wyzf_ldlf` float DEFAULT NULL,
  `point_jck_wyzf_wyxq` float DEFAULT NULL,
  `point_dsjk` float unsigned zerofill DEFAULT NULL,
  `point_ldpy` float DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of history_point3
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_add
-- ----------------------------
DROP TABLE IF EXISTS `law_item_add`;
CREATE TABLE `law_item_add` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '治违办评价项目',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`,`law_item_pingfen`,`law_item_leibie`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_add
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_bgs
-- ----------------------------
DROP TABLE IF EXISTS `law_item_bgs`;
CREATE TABLE `law_item_bgs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) DEFAULT NULL,
  `law_item_leibie` varchar(255) DEFAULT NULL,
  `law_item_pingfen` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_bgs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_dcs
-- ----------------------------
DROP TABLE IF EXISTS `law_item_dcs`;
CREATE TABLE `law_item_dcs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) DEFAULT NULL,
  `law_item_leibie` varchar(255) DEFAULT NULL,
  `law_item_pingfen` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_dcs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_dsjk
-- ----------------------------
DROP TABLE IF EXISTS `law_item_dsjk`;
CREATE TABLE `law_item_dsjk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '督察室评价项目',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评价类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_dsjk
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_fws
-- ----------------------------
DROP TABLE IF EXISTS `law_item_fws`;
CREATE TABLE `law_item_fws` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '指挥中心评价项目',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_fws
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_fws2
-- ----------------------------
DROP TABLE IF EXISTS `law_item_fws2`;
CREATE TABLE `law_item_fws2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '指挥中心(派件处置)评价项目',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评价类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_fws2
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_fzk
-- ----------------------------
DROP TABLE IF EXISTS `law_item_fzk`;
CREATE TABLE `law_item_fzk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '法制科评价项目',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_fzk
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_ggb
-- ----------------------------
DROP TABLE IF EXISTS `law_item_ggb`;
CREATE TABLE `law_item_ggb` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '广告办评价项目',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_ggb
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_bz
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_bz`;
CREATE TABLE `law_item_jck_bz` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) DEFAULT NULL COMMENT '监察科(备注)评价项目',
  `law_item_leibie` varchar(255) DEFAULT NULL COMMENT '类别',
  `law_item_pingfen` varchar(255) DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of law_item_jck_bz
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_hjws
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_hjws`;
CREATE TABLE `law_item_jck_hjws` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) DEFAULT NULL,
  `law_item_leibie` varchar(255) DEFAULT NULL,
  `law_item_pingfen` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_jck_hjws
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_ljfl
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_ljfl`;
CREATE TABLE `law_item_jck_ljfl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '监察科(市容环卫)评价项目',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评价类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_jck_ljfl
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_qtaj
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_qtaj`;
CREATE TABLE `law_item_jck_qtaj` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) DEFAULT NULL COMMENT '监察科(备注)',
  `law_item_leibie` varchar(255) DEFAULT NULL COMMENT '评价类别',
  `law_item_pingfen` varchar(255) DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of law_item_jck_qtaj
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_srzx
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_srzx`;
CREATE TABLE `law_item_jck_srzx` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '监察科(市容秩序)',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_jck_srzx
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_szgy
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_szgy`;
CREATE TABLE `law_item_jck_szgy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '监察科(市政公用)',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评价类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_jck_szgy
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_szss
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_szss`;
CREATE TABLE `law_item_jck_szss` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) DEFAULT NULL,
  `law_item_leibie` varchar(255) DEFAULT NULL,
  `law_item_pingfen` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_jck_szss
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_wyzf
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_wyzf`;
CREATE TABLE `law_item_jck_wyzf` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '监察科(物业执法)',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评价类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_jck_wyzf
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_yllh
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_yllh`;
CREATE TABLE `law_item_jck_yllh` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '监察科(园林绿化)',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评价类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_jck_yllh
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_jck_ywld
-- ----------------------------
DROP TABLE IF EXISTS `law_item_jck_ywld`;
CREATE TABLE `law_item_jck_ywld` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) DEFAULT NULL COMMENT '监察科(业务亮点)',
  `law_item_leibie` varchar(255) DEFAULT NULL COMMENT '评分类别',
  `law_item_pingfen` varchar(255) DEFAULT NULL COMMENT '评价内容',
  `description` varchar(255) DEFAULT NULL COMMENT '评价标准',
  `point` varchar(255) DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of law_item_jck_ywld
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_xck
-- ----------------------------
DROP TABLE IF EXISTS `law_item_xck`;
CREATE TABLE `law_item_xck` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '宣传科',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评价类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_xck
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_xfk
-- ----------------------------
DROP TABLE IF EXISTS `law_item_xfk`;
CREATE TABLE `law_item_xfk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '信访科',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_xfk
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_zgk
-- ----------------------------
DROP TABLE IF EXISTS `law_item_zgk`;
CREATE TABLE `law_item_zgk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '政工科',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分类别',
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分内容',
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '评分标准',
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_zgk
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_ztb
-- ----------------------------
DROP TABLE IF EXISTS `law_item_ztb`;
CREATE TABLE `law_item_ztb` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) DEFAULT NULL COMMENT '渣土办',
  `law_item_leibie` varchar(255) DEFAULT NULL,
  `law_item_pingfen` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_ztb
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_ztb_hjzf
-- ----------------------------
DROP TABLE IF EXISTS `law_item_ztb_hjzf`;
CREATE TABLE `law_item_ztb_hjzf` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '渣土办环保执法',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_ztb_hjzf
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for law_item_ztb_jzlj
-- ----------------------------
DROP TABLE IF EXISTS `law_item_ztb_jzlj`;
CREATE TABLE `law_item_ztb_jzlj` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `law_item` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '渣土办建筑垃圾',
  `law_item_leibie` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `law_item_pingfen` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `point` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of law_item_ztb_jzlj
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(140) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(20) NOT NULL,
  `SCHED_TIME` bigint(20) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` tinyint(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` tinyint(1) NOT NULL,
  `IS_NONCONCURRENT` tinyint(1) NOT NULL,
  `IS_UPDATE_DATA` tinyint(1) NOT NULL,
  `REQUESTS_RECOVERY` tinyint(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(20) NOT NULL,
  `CHECKIN_INTERVAL` bigint(20) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(20) NOT NULL,
  `REPEAT_INTERVAL` bigint(20) NOT NULL,
  `TIMES_TRIGGERED` bigint(20) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` tinyint(1) DEFAULT NULL,
  `BOOL_PROP_2` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `QRTZ_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(20) NOT NULL,
  `END_TIME` bigint(20) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(6) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for quartz_task
-- ----------------------------
DROP TABLE IF EXISTS `quartz_task`;
CREATE TABLE `quartz_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '任务名称,input,YES,false,true,true',
  `cron` varchar(255) DEFAULT NULL COMMENT '任务表达式,input,YES,false,true,false',
  `target_bean` varchar(255) DEFAULT NULL COMMENT '执行的类,input,YES,false,true,false',
  `trget_method` varchar(255) DEFAULT NULL COMMENT '执行方法,input,YES,false,true,false',
  `params` varchar(255) DEFAULT NULL COMMENT '执行参数,textarea,YES,false,false,false',
  `status` int(11) DEFAULT NULL COMMENT '任务状态,radio,YES,false,true,true',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='定时任务,1';

-- ----------------------------
-- Records of quartz_task
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for quartz_task_log
-- ----------------------------
DROP TABLE IF EXISTS `quartz_task_log`;
CREATE TABLE `quartz_task_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `job_id` bigint(20) DEFAULT NULL COMMENT '任务ID,0,YES,false,false,false',
  `name` varchar(255) DEFAULT NULL COMMENT '定时任务名称,input,YES,false,true,true',
  `target_bean` varchar(255) DEFAULT NULL COMMENT '定制任务执行类,input,YES,false,true,false',
  `trget_method` varchar(255) DEFAULT NULL COMMENT '定时任务执行方法,input,YES,false,true,false',
  `params` varchar(255) DEFAULT NULL COMMENT '执行参数,input,YES,false,true,false',
  `status` int(11) DEFAULT NULL COMMENT '任务状态,0,YES,false,false,false',
  `error` text COMMENT '异常消息,textarea,YES,false,false,false',
  `times` int(11) DEFAULT NULL COMMENT '执行时间,input,YES,false,true,false',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1059 DEFAULT CHARSET=utf8 COMMENT='任务执行日志,1';

-- ----------------------------
-- Records of quartz_task_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '治违办编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL COMMENT '鎻忚堪',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_bgs
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_bgs`;
CREATE TABLE `sys_dict_bgs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_bgs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_dcs
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_dcs`;
CREATE TABLE `sys_dict_dcs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '指挥中心编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_dcs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_dcs2
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_dcs2`;
CREATE TABLE `sys_dict_dcs2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '指挥中心(派件处置)编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `complete` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '完成数',
  `total` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '总数',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_dcs2
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_dsjk
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_dsjk`;
CREATE TABLE `sys_dict_dsjk` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '督察室编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(10) unsigned DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_dsjk
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_fzk
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_fzk`;
CREATE TABLE `sys_dict_fzk` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '法制科编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_fzk
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_ggb
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_ggb`;
CREATE TABLE `sys_dict_ggb` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '广告办编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_ggb
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_jck_bz
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_jck_bz`;
CREATE TABLE `sys_dict_jck_bz` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '监察科(备注)编号',
  `value` varchar(100) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `type2` varchar(100) DEFAULT NULL,
  `type3` varchar(100) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `parent_id` varchar(64) DEFAULT '0',
  `create_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dict_jck_bz
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_jck_hjws
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_jck_hjws`;
CREATE TABLE `sys_dict_jck_hjws` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '监察科(市容环卫)编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_jck_hjws
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_jck_qtaj
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_jck_qtaj`;
CREATE TABLE `sys_dict_jck_qtaj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '监察科(其他案件)编号',
  `value` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `type2` varchar(255) DEFAULT NULL,
  `type3` varchar(255) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `parent_id` varchar(64) DEFAULT '0',
  `create_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dict_jck_qtaj
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_jck_srzx
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_jck_srzx`;
CREATE TABLE `sys_dict_jck_srzx` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '监察科(市容秩序)编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_jck_srzx
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_jck_szss
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_jck_szss`;
CREATE TABLE `sys_dict_jck_szss` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '监察科(市政公用)编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_jck_szss
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_jck_wyzf
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_jck_wyzf`;
CREATE TABLE `sys_dict_jck_wyzf` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '监察科(物业执法)编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_jck_wyzf
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_jck_yllh
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_jck_yllh`;
CREATE TABLE `sys_dict_jck_yllh` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '监察科(园林绿化)编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_jck_yllh
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_jck_ywld
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_jck_ywld`;
CREATE TABLE `sys_dict_jck_ywld` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '监察科(业务亮点)编号',
  `value` varchar(100) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `type2` varchar(100) DEFAULT NULL,
  `type3` varchar(100) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `parent_id` varchar(64) DEFAULT '1',
  `create_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dict_jck_ywld
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_xck
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_xck`;
CREATE TABLE `sys_dict_xck` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '宣传科编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_xck
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_xfk
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_xfk`;
CREATE TABLE `sys_dict_xfk` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '信访科编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_xfk
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_zgk
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_zgk`;
CREATE TABLE `sys_dict_zgk` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '政工科编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_zgk
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_ztb
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_ztb`;
CREATE TABLE `sys_dict_ztb` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '渣土办编号',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `label` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `type2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '澶囨敞淇℃伅',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `number` int(11) DEFAULT '1',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `visable_flag` int(11) DEFAULT '1',
  `del_flag` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表,测试表';

-- ----------------------------
-- Records of sys_dict_ztb
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_group`;
CREATE TABLE `sys_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL COMMENT '分组名称',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父分组ID',
  `level` bigint(20) DEFAULT NULL,
  `parent_ids` varchar(2000) DEFAULT NULL COMMENT '分组序列号',
  `sort` smallint(6) DEFAULT NULL COMMENT '分组排序值',
  `create_by` bigint(20) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `del_flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_group
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_group_ur
-- ----------------------------
DROP TABLE IF EXISTS `sys_group_ur`;
CREATE TABLE `sys_group_ur` (
  `group_id` bigint(20) NOT NULL COMMENT '分组ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_group_ur
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '请求类型',
  `title` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '日志标题',
  `remote_addr` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作IP地址',
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作用户昵称',
  `request_uri` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '请求URI',
  `http_method` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '操作方式',
  `class_method` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '请求类型.方法',
  `params` text COLLATE utf8_bin COMMENT '操作提交的数据',
  `session_id` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'sessionId',
  `response` longtext COLLATE utf8_bin COMMENT '返回内容',
  `use_time` bigint(20) DEFAULT NULL COMMENT '方法执行时间',
  `browser` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '浏览器信息',
  `area` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '地区',
  `province` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '省',
  `city` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '市',
  `isp` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '网络服务提供商',
  `exception` text COLLATE utf8_bin COMMENT '异常信息',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `del_flag` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sys_log_create_by` (`create_by`) USING BTREE,
  KEY `sys_log_request_uri` (`request_uri`) USING BTREE,
  KEY `sys_log_type` (`type`) USING BTREE,
  KEY `sys_log_create_date` (`create_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13469 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单',
  `level` bigint(20) DEFAULT NULL COMMENT '菜单层级',
  `parent_ids` varchar(2000) DEFAULT NULL COMMENT '父菜单联集',
  `sort` smallint(6) DEFAULT NULL COMMENT '排序',
  `href` varchar(2000) DEFAULT NULL COMMENT '链接地址',
  `target` varchar(20) DEFAULT NULL COMMENT '打开方式',
  `icon` varchar(100) DEFAULT NULL COMMENT '菜单图标',
  `bg_color` varchar(255) DEFAULT NULL COMMENT '显示背景色',
  `is_show` tinyint(4) DEFAULT NULL COMMENT '是否显示',
  `permission` varchar(200) DEFAULT NULL COMMENT '权限标识',
  `create_by` bigint(20) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `del_flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_rescource
-- ----------------------------
DROP TABLE IF EXISTS `sys_rescource`;
CREATE TABLE `sys_rescource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `file_name` varchar(255) DEFAULT NULL COMMENT '文件名称',
  `source` varchar(255) DEFAULT NULL COMMENT '来源',
  `web_url` varchar(500) DEFAULT NULL COMMENT '资源网络地址',
  `hash` varchar(255) DEFAULT NULL COMMENT '文件标识',
  `file_size` varchar(50) DEFAULT NULL COMMENT '文件大小',
  `file_type` varchar(255) DEFAULT NULL COMMENT '文件类型',
  `original_net_url` text,
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='系统资源';

-- ----------------------------
-- Records of sys_rescource
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL COMMENT '角色名称',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `del_flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色编号',
  `menu_id` bigint(20) NOT NULL COMMENT '对应菜单编号',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_site
-- ----------------------------
DROP TABLE IF EXISTS `sys_site`;
CREATE TABLE `sys_site` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL COMMENT '系统网址',
  `open_message` bit(1) DEFAULT NULL COMMENT '是否开放评论',
  `is_no_name` bit(1) DEFAULT NULL COMMENT '是否匿名评论',
  `version` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `author_icon` varchar(255) DEFAULT NULL,
  `file_upload_type` varchar(255) DEFAULT NULL,
  `weibo` varchar(255) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `git` varchar(255) DEFAULT NULL,
  `github` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `server` varchar(255) DEFAULT NULL,
  `database` varchar(255) DEFAULT NULL,
  `max_upload` int(11) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `powerby` varchar(255) DEFAULT NULL,
  `record` varchar(255) DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `remarks` text,
  `del_flag` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_site
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `login_name` varchar(36) DEFAULT NULL COMMENT '登录名',
  `nick_name` varchar(40) DEFAULT NULL COMMENT '昵称',
  `icon` varchar(2000) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL COMMENT '密码',
  `salt` varchar(40) DEFAULT NULL COMMENT 'shiro加密盐',
  `tel` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱地址',
  `locked` tinyint(4) DEFAULT NULL COMMENT '是否锁定',
  `create_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `del_flag` tinyint(4) DEFAULT NULL,
  `login_attempts` int(4) DEFAULT NULL,
  `unlock_time` datetime DEFAULT NULL,
  `clear_attempts_time` datetime DEFAULT NULL,
  `update_password_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `team_name` varchar(50) DEFAULT NULL COMMENT '中队名称',
  `point_zwb` float DEFAULT NULL COMMENT '治违办分值',
  `point_ggb` float DEFAULT NULL COMMENT '广告办分值',
  `point_ztb` float DEFAULT NULL COMMENT '渣土办分值',
  `point_dcs` float DEFAULT NULL COMMENT '指挥中心分值',
  `point_dcs_ajcz` float DEFAULT NULL COMMENT '指挥中心(派件处置)',
  `point_dcs_hs` float DEFAULT NULL COMMENT '指挥中心(诉转案)分值',
  `point_xck` float DEFAULT NULL COMMENT '宣传科分值',
  `point_bgs` float DEFAULT NULL,
  `point_xfk` float DEFAULT NULL COMMENT '信访科分值',
  `point_fzk` float DEFAULT NULL COMMENT '法制科分值',
  `point_zgk` float DEFAULT NULL COMMENT '政工科分值',
  `point_jck_srzx` float DEFAULT NULL COMMENT '监察科(市容秩序)分值',
  `point_jck_hjws` float DEFAULT NULL COMMENT '监察科(市容环卫)分值',
  `point_jck_hjws_zljy` float DEFAULT NULL COMMENT '监察科(市容环卫)(占路经营)分值',
  `point_jck_hjws_xgg` float DEFAULT NULL COMMENT '监察科(市容环卫)(小广告)分值',
  `point_jck_hjws_ws` float DEFAULT NULL COMMENT '监察科(市容环卫)(卫生责任区)分值',
  `point_jck_hjws_ljfl` float DEFAULT NULL COMMENT '监察科(市容环卫)(垃圾分类)分值',
  `point_jck_szss` float DEFAULT NULL COMMENT '监察科(市政公用)分值',
  `point_jck_szss_rq` float DEFAULT NULL COMMENT '监察科(市政公用)(燃气)分值',
  `point_jck_szss_pw` float DEFAULT NULL COMMENT '监察科(市政公用)(排污)分值',
  `point_jck_szss_jl` float DEFAULT NULL COMMENT '监察科(市政公用)(掘路)分值',
  `point_jck_yllh` float DEFAULT NULL COMMENT '监察科(园林绿化)f分值',
  `point_jck_yllh_lh` float DEFAULT NULL COMMENT '监察科(园林绿化)(绿化)分值',
  `point_jck_yllh_yy` float DEFAULT NULL COMMENT '监察科(园林绿化)(油烟噪声扬尘)分值',
  `point_jck_yllh_ztc1` float DEFAULT NULL COMMENT '监察科(园林绿化)(渣土车)分值1',
  `point_jck_yllh_ztc2` float DEFAULT NULL COMMENT '监察科(园林绿化)(渣土车)分值2',
  `point_jck_qtaj` float DEFAULT NULL COMMENT '监察科(其他案件)分值',
  `point_jck_ywld` float DEFAULT NULL COMMENT '监察科(业务亮点)分值',
  `point_jck_bz` float DEFAULT NULL COMMENT '监察科(备注)分值',
  `point_jck_wyzf` float DEFAULT NULL COMMENT '监察科(物业执法)分值',
  `point_jck_wyzf_wgqy` float DEFAULT NULL COMMENT '监察科(物业执法)(物管执法)分值',
  `point_jck_wyzf_qlhl` float DEFAULT NULL COMMENT '监察科(物业执法)(圈绿毁绿)分值',
  `point_jck_wyzf_ldlf` float DEFAULT NULL COMMENT '监察科(物业执法)(地桩地锁乱放)分值',
  `point_jck_wyzf_wyxq` float DEFAULT NULL COMMENT '监察科(物业执法)(物业小区)分值',
  `point_dsjk` float unsigned zerofill DEFAULT NULL COMMENT '督察室分值',
  `point_ldpy` float DEFAULT NULL COMMENT '领导评议分值',
  `time` datetime DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of team
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for upload_info
-- ----------------------------
DROP TABLE IF EXISTS `upload_info`;
CREATE TABLE `upload_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `local_window_url` varchar(255) DEFAULT NULL COMMENT '本地window系统上传路径,input,YES,false,true,false',
  `local_linux_url` varchar(255) DEFAULT NULL COMMENT '本地LINUX系统上传路径,input,YES,false,true,false',
  `qiniu_base_path` varchar(255) DEFAULT NULL COMMENT '七牛前缀路径,input,YES,false,true,false',
  `qiniu_bucket_name` varchar(255) DEFAULT NULL COMMENT '七牛bucket的目录名称,input,YES,false,true,false',
  `qiniu_dir` varchar(255) DEFAULT NULL COMMENT '七牛文件存储目录,input,YES,false,true,false',
  `qiniu_access_key` varchar(255) DEFAULT NULL COMMENT '七牛qiniuAccess值,input,YES,false,true,false',
  `qiniu_secret_key` varchar(255) DEFAULT NULL COMMENT '七牛qiniuKey的值,input,YES,false,true,false',
  `qiniu_test_access` bit(1) DEFAULT NULL COMMENT '七牛上传测试,switch,YES,true,true,false',
  `oss_base_path` varchar(255) DEFAULT NULL COMMENT '阿里云前缀路径,input,YES,false,true,false',
  `oss_bucket_name` varchar(255) DEFAULT NULL COMMENT '阿里云bucket的目录名称,input,YES,false,true,false',
  `oss_dir` varchar(255) DEFAULT NULL COMMENT '阿里云文件上传目录,input,YES,false,true,false',
  `oss_key_id` varchar(255) DEFAULT NULL COMMENT '阿里云ACCESS_KEY_ID值,input,YES,false,true,false',
  `oss_key_secret` varchar(255) DEFAULT NULL COMMENT '阿里云ACCESS_KEY_SECRET,input,YES,false,true,false',
  `oss_endpoint` varchar(255) DEFAULT NULL COMMENT '阿里云ENDPOINT值,input,YES,false,true,false',
  `oss_test_access` bit(1) DEFAULT NULL COMMENT '阿里云上传测试,switch,YES,true,true,false',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文件上传配置,1,switch-qiniuTestAccess-YES-true-qiniu_test_access,switch-ossTestAccess-YES-true-oss_test_access';

-- ----------------------------
-- Records of upload_info
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
