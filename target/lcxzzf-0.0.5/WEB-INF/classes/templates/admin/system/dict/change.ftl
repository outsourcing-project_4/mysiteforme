<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>字典添加--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <!-- 页面描述 &ndash;&gt;
    <meta name="description" content="${site.description}"/>
    <！-- 页面关键词 &ndash;&gt;
    <meta name="keywords" content="${site.keywords}"/>
    <！-- 网页作者 -->
    <meta name="author" content="${site.author}"/>
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <style type="text/css">
        .layui-form-item .layui-inline{ width:33.333%; float:left; margin-right:0; }
        @media(max-width:1240px){
            .layui-form-item .layui-inline{ width:100%; float:none; }
        }
        .layui-form-item .role-box {
            position: relative;
        }
        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
<#--    考核类型下拉框-type-->
    <div class="layui-form-item">
        <label for="type" class="layui-form-label" lay-filter="point">考核类型</label>
        <div class="layui-input-block">
            <select name="type" id="type" lay-verify="required" lay-search="">
                <option value="">搜索选择</option>
            </select>
        </div>
    </div>
    <#--    考核详情下拉框-description-->
    <div class="layui-form-item">
        <label for="description" class="layui-form-label" lay-filter="point">考核详细</label>
        <div class="layui-input-block">
            <select name="description" id="description" lay-verify="required" lay-search="" lay-filter="description">
                <option value="">搜索选择</option>
            </select>
        </div>
    </div>
    <#--&lt;#&ndash;    原本分值-value&ndash;&gt;
    <div class="layui-form-item">
        <label for="value" class="layui-form-label" lay-filter="point">考核原分值</label>
        <div class="layui-input-block">
            <select name="value" id="value" lay-verify="required" lay-search="">
                <option value="">考核原分值</option>
            </select>
        </div>
    </div>-->

     <div class="layui-form-item">
      <label class="layui-form-label">更改分值</label>
      <div class="layui-input-block">
          <input type="text" class="layui-input" name="point" lay-verify="required" placeholder="请输入更改的分值">
      </div>
  </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="changeScore">立即更改</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/jquery.min.js"></script>

<script type="text/javascript">
    window.onload=function(){
        // 初始化内容
        descriptionShow();
        Law_item_Show();

    layui.use(['form','jquery','layer'],function(){
        var form = layui.form,
/*
            $    = layui.jquery,
*/
            layer = layui.layer;
        /*form.on('select(type)','select(description)', function (data) {
            Point_Show(data.value);
        });*/

        //提交表单将分值进行更改 参数考核类型：type 考核详情description 分值value 需要改！
        form.on("submit(changeScore)",function(data){
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            $.get("${base}/item/changePoint",
                data.field,
                function(res){
                layer.close(loadIndex);
                if(res.success){
                    parent.layer.msg("分值更改成功!",{time:1000},function(){
                        //刷新父页面
                        parent.location.reload();
                    });
                }else{
                    console.log(data.field)
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
    }
    //descriptionShow-考核详情下拉框-description
    function descriptionShow() {
        $("#description").html('<option value="">选择考核详情</option>');
        $.ajax({
            url: '${base}/item/queryItemVODescription',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                if (!data.success) {
                    alert(data.msg);
                } else {
                    $.each(data.ItemVO, function (key, value) {
                        var option = new Option(value.description);
                        $('#description').append(option);//往下拉菜单里添加元素
                        renderForm();
                    });
                }
            },
            error: function (data) {
                alert(data.msg);
                console.log(data.msg);
            }
        });
    }
    //Law_item_Show-考核类型下拉框-type
    function Law_item_Show() {
        $("#type").html('<option value="">选择考核类型</option>');
        $.ajax({
            url: '${base}/item/queryItemVO',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                if (!data.success) {
                    alert(data.msg);
                } else {
                    $.each(data.ItemVO, function (key, value) {
                        var option = new Option(value.law_item);
                        $('#type').append(option);//往下拉菜单里添加元素
                        renderForm();
                    });
                }
            },
            error: function (data) {
                alert(data.msg);
                console.log(data.msg);
            }
        });
    }


    function renderForm() {
        layui.use('form', function () {
            var form = layui.form;
            form.render();
        });
    }
</script>
</body>
</html>

