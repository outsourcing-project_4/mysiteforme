<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>历史条目管理列表--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel = "shortcut icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all" />
    <style>
        .detail-body{
            margin: 20px 0 0;
            color: #333;
            word-wrap: break-word;
        }
    </style>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>监察科(市容秩序)历史条目</legend>
    <div class="layui-field-box" >
    <form class="layui-form">
        <#--选择查询日期范围-->
        <div class="layui-inline">
            <input type="text" value="" name="s_date_start" id="s_date_start" placeholder="请输入起始日期" class="layui-input search_input">
        </div>~
        <div class="layui-inline">
            <input type="text" value="" name="s_date_end" id="s_date_end" placeholder="请输入结束日期" class="layui-input search_input">
        </div>

        <div class="layui-inline" style="display: none">
            <input type="text" value="" name="s_type" placeholder="请输入评价项目" class="layui-input search_input">
        </div>
        <div class="layui-inline">
            <input type="text" value="" name="s_label" placeholder="请输入考核中队" class="layui-input search_input">
        </div>
        <div class="layui-inline">
            <a class="layui-btn" lay-submit="" lay-filter="searchForm">查询</a>
        </div>
        <div class="layui-inline" >
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
<#--        <div class="layui-inline">-->
<#--            <a class="layui-btn layui-btn-normal" data-type="addItem">添加条目</a>-->
<#--        </div>-->
<#--        &lt;#&ndash;更改条目&ndash;&gt;-->
<#--        <div class="layui-inline" style="visibility: hidden">-->
<#--            <a class="layui-btn layui-btn-normal" data-type="changeScore">更改条目</a>-->
<#--        </div>-->
        <div class="layui-inline" style="float: right">
            <a class="layui-btn layui-btn-warm" data-type="teamPoint2">中队分数排名(23年.4季度)</a>
        </div>
        <div class="layui-inline" style="float: right">
            <a class="layui-btn layui-btn-warm" data-type="teamPoint1">中队分数排名(23年.3季度)</a>
        </div>
    </form>
        <#--选择查询日期范围的表单-->
<#--        <form class="layui-form" style="width:80%;float: left">-->
<#--            &lt;#&ndash;选择查询日期范围&ndash;&gt;-->
<#--            <div class="layui-form-item" style="float: left">-->
<#--                <div class="layui-input-block">-->
<#--                    <select name="value" id="value" lay-verify="required" lay-search="">-->
<#--                        <option value="">选择查询日期范围</option>-->
<#--                    </select>-->
<#--                </div>-->
<#--            </div>-->
<#--        </form>-->
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>

    <script type="text/html" id="userStatus">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.delFlag == false){ }}
        <span class="layui-badge layui-bg-green">正常</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-gray">停用</span>
        {{#  } }}
    </script>
    <script type="text/html" id="barDemo">
        {{# if(d.description == null || d.description == "" || d.description == undefined || d.description.indexOf('系统')<0){ }}
        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="addOne"><img src="${base}/static/images/addPic.png"></a>
        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="minusOne"><img src="${base}/static/images/minusPic.png"></a>
<#--        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="addType" >新增一个TYPE值</a>-->
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
<#--        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="editType" >编辑TYPE</a>-->
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
        {{# } }}
    </script>
</div>
<div id="page"></div>

<#--这里的type就是评价项目，下面的代码可以修改评价项目，暂时无用-->
<div id="changeTypeDiv" style="display: none" class="detail-body">
    <form  id="changeTypeForm" class="layui-form" style="width: 500px">
        <div class="layui-form-item">
            <label class="layui-form-label">原TYPE值</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input layui-disabled" name="oldType" disabled lay-verify="required" placeholder="请输入原TYPE值">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">新TYPE值</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" name="newType" lay-verify="required" placeholder="请输入新TYPE值">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="changeType">立即提交</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer','form','table','laydate'], function() {
        var layer = layui.layer,
                $ = layui.jquery,
                form = layui.form,
                table = layui.table,
                t,changeIndex;                  //表格数据变量

        t = {
            elem: '#test',
            url:'${base}/admin/system/jck_srzx/histortlist',
            method:'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits:[3,10, 20, 30]
            },
            width: $(parent.window).width(),
            cols: [[
                // {type:'checkbox'},
                {field:'type',title: '评价项目',width: '6%'},
                {field:'type2',title: '类别'},
                {field:'type3',sort:true,title: '评分内容',width: '12%'},
                {field:'label',sort:true,title: '考核中队',width: '8%'},
                {field:'value',sort:true,title: '分值',width: '6%'},
                {field:'description',sort:true,title: '评分标准',width: '19%'},
                {field:'remarks',sort:true,title: '备注',width: '15%'},
                {field:'createDate', sort:true ,width: '12%',title: '创建时间',templet:'<div>{{ layui.laytpl.toDateString(d.createDate) }}</div>'}, //单元格内容水平居中
                {field:'number',sort:true,title: '条数',width: '6%'},
                // {fixed: 'right', title: '操作', align: 'center',toolbar: '#barDemo',unresize: true,width: '17%'}
            ]]
        };
        table.render(t);

        //监听工具条
        table.on('tool(demo)', function(obj){
            var data = obj.data;
            if(obj.event === 'edit'){
                var editIndex = layer.open({
                    title : "编辑条目",
                    type : 2,
                    content : "${base}/admin/system/jck_srzx/edit?id="+data.id,
                    success : function(layero, index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },500);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(editIndex);
                });
                layer.full(editIndex);
            }
            if(obj.event === "del"){
                layer.confirm("确定要删除该条目么？",{btn:['是的,我确定','我再想想']},
                        function(){
                            $.post("${base}/admin/system/jck_srzx/deleteById",{"id":data.id},function (res){
                                if(res.success){
                                    layer.msg("删除成功",{time: 1000},function(){
                                        table.reload('test', t);
                                    });
                                }else{
                                    layer.msg(res.message);
                                }

                            });
                        }
                )
            }
            if(obj.event === "addOne"){
                layui.use(['layer'], function () {
                    $.post("${base}/admin/system/jck_srzx/addOneById",{"id":data.id},function (res){
                        if(res.success){
                            layer.msg("加一成功",{time: 500},function(){
                                table.reload('test', t);
                            });
                        }else{
                            layer.msg(res.message);
                        }

                    });
                })
            }
            if(obj.event === "minusOne"){
                layui.use(['layer'], function () {
                    $.post("${base}/admin/system/jck_srzx/minusOneById",{"id":data.id},function (res){
                        if(res.success){
                            layer.msg("减一成功",{time: 500},function(){
                                table.reload('test', t);
                            });
                        }else{
                            layer.msg(res.message);
                        }

                    });
                })
            }
            if(obj.event === "addType"){
                var addTypeIndex = layer.open({
                    title : "新增TYPE的值",
                    type : 2,
                    content : "${base}/admin/system/dict/add?type="+data.type,
                    success : function(layero, index){
                        setTimeout(function(){
                            layer.tips('点击此处返回条目列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },500);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(addTypeIndex);
                });
                layer.full(addTypeIndex);
            }

            if(obj.event === "editType"){
                $("#changeTypeForm")[0].reset();
                $("input[name='oldType']").val(data.type);
                changeIndex = layer.open({
                    type: 1,
                    title: '编辑TYPE值',
                    closeBtn: 0,
                    area: '516px',
                    shadeClose: true,
                    content: $('#changeTypeDiv')
                });
            }
        });

        //功能按钮添加字典
        var active={
            addItem : function(){
                var addIndex = layer.open({
                    title : "添加条目",
                    type : 2,
                    content : "${base}/admin/system/jck_srzx/add",
                    success : function(layero, addIndex){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(addIndex);
                });
                layer.full(addIndex);
            }

        };
        var active1= {
            //功能按钮更改考核分数
            changeScore: function () {
                var changeIndex = layer.open({
                    title: "更改考核分值",//更改条目
                    type: 2,
                    content: "${base}/admin/system/dict/change",
                    success: function (layero, changeIndex) {
                        setTimeout(function () {
                            layer.tips('点击此处返回会员列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        }, 500);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(changeIndex);
                });
                layer.full(changeIndex);
            }
        };

        //跳转至中队分数排名列表
        var activeTeamPoint={
            teamPoint1 : function(){
                var teamPointList = layer.open({
                    title : "中队分数排名",
                    type : 2,
                    content : "${base}/teamPoint/jck_historySrzxPoint1",
                    success : function(layero, teamPointList){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(teamPointList);
                });
                layer.full(teamPointList);
            }

        };

        //跳转至中队分数排名列表
        var activeTeamPoint2={
            teamPoint2 : function(){
                var teamPointList = layer.open({
                    title : "中队分数排名",
                    type : 2,
                    content : "${base}/teamPoint/jck_historySrzxPoint2",
                    success : function(layero, teamPointList){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(teamPointList);
                });
                layer.full(teamPointList);
            }

        };

        //功能按钮更改

        $('.layui-inline .layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
            active1[type] ? active1[type].call(this) : '';
            activeTeamPoint[type] ? activeTeamPoint[type].call(this) : '';
            activeTeamPoint2[type] ? activeTeamPoint2[type].call(this) : '';
        });

        //搜索
        form.on("submit(searchForm)",function(data){
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

        form.on("submit(changeType)",function (data) {
            if(data.field.oldType == null || data.field.oldType === ""){
                layer.msg("原TYPE值不能为空");
                return false;
            }
            $.post("${base}/admin/system/dict/editType",data.field,function(res){
                if(!res.success){
                    layer.msg(res.message);
                }else{
                    layer.msg("修改成功",{time:1000},function () {
                        table.reload('test', t);
                    })
                }
                layer.close(changeIndex);
            });
            return false;
        });

        //起始日期与结束日期选择框
        var laydate = layui.laydate;
        laydate.render({
            elem: '#s_date_start'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#s_date_end'
            ,type: 'datetime'
        });

    });

</script>
</body>
</html>