<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加条目--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <!-- 页面描述 -->
    <meta name="description" content="${site.description}"/>
    <!-- 页面关键词 -->
    <meta name="keywords" content="${site.keywords}"/>
    <!-- 网页作者 -->
    <meta name="author" content="${site.author}"/>
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <style type="text/css">
        .layui-form-item .layui-inline{ width:33.333%; float:left; margin-right:0; }
        @media(max-width:1240px){
            .layui-form-item .layui-inline{ width:100%; float:none; }
        }
        .layui-form-item .role-box {
            position: relative;
        }
        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
   <#-- <div class="layui-form-item">
        <label class="layui-form-label">考核类型</label>
        <div class="layui-input-block">
            <#if (type != null && type != "")>
                <input type="text" class="layui-input layui-disabled" name="type" value="${type}" disabled lay-verify="required" placeholder="请输入TYPE">
            <#else>
                <input type="text" class="layui-input" name="type" lay-verify="required" placeholder="请输入TYPE">
            </#if>
        </div>
    </div>-->
<#--    考核类型下拉框-type-->
    <div class="layui-form-item">
        <label for="type" class="layui-form-label" lay-filter="type">评价项目</label>
        <div class="layui-input-block">
            <select name="type" id="type" lay-verify="required" lay-search="" lay-filter="type">
                <option value="">选择评价项目</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label for="type2" class="layui-form-label" lay-filter="type2">类别</label>
        <div class="layui-input-block">
            <select name="type2" id="type2" lay-verify="required" lay-search="" lay-filter="type2">
                <option value="">选择类别</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label for="type3" class="layui-form-label" lay-filter="type3">评分内容</label>
        <div class="layui-input-block">
            <select name="type3" id="type3" lay-verify="required" lay-search="" lay-filter="type3">
                <option value="">选择评分内容</option>
            </select>
        </div>
    </div>
<#--    <div class="layui-form-item">-->
<#--        <label class="layui-form-label">考核人员</label>-->
<#--        <div class="layui-input-block">-->
<#--            <input type="text" class="layui-input" name="label" lay-verify="required" placeholder="请输入标签">-->
<#--        </div>-->
<#--    </div>-->

<#--    <div class="layui-form-item">-->
<#--        <label class="layui-form-label">加/扣分值</label>-->
<#--        <div class="layui-input-block">-->
<#--            <input type="text" class="layui-input" name="value" lay-verify="required" placeholder="请输入值">-->
<#--        </div>-->
<#--    </div>-->
   <#-- <div class="layui-form-item">
        <label class="layui-form-label">描述</label>
        <div class="layui-input-block">
            <textarea name="description" class="layui-textarea" placeholder="可以输入该标签描述"></textarea>
        </div>
    </div>-->
    <#--    考核详情下拉框-description-->
    <div class="layui-form-item">
        <label for="description" class="layui-form-label" lay-filter="description">评分标准</label>
        <div class="layui-input-block">
            <select name="description" id="description" lay-verify="required" lay-search="" lay-filter="description">
                <option value="">选择评分标准</option>
            </select>
        </div>
    </div>

    <#--    加扣分-value-->
    <div class="layui-form-item">
        <label for="value" class="layui-form-label" lay-filter="value">加/扣分值</label>
        <div class="layui-input-block">
            <select name="value" id="value" lay-verify="required" lay-search="">
                <option value="">选择加/扣分值</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label for="label" class="layui-form-label" lay-filter="team">考核中队</label>
        <div class="layui-input-block">
            <select name="label" id="label" lay-verify="required" lay-search="">
                <option value="">选择中队</option>
            </select>
        </div>
    </div>

    <#--    条数输入框暂时不给显示-->
    <div class="layui-form-item" hidden="hidden">
        <label class="layui-form-label" lay-filter="number">条数</label>
        <div class="layui-input-block">
            <textarea name="number" class="layui-textarea" placeholder="可以输入该标签描述">1</textarea>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label" lay-filter="remarks">备注</label>
        <div class="layui-input-block">
            <textarea name="remarks" class="layui-textarea" placeholder="可以输入该标签描述"></textarea>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addItem">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/jquery.min.js"></script>

<script type="text/javascript">
    window.onload=function(){
        // 初始化内容
        Law_item_Show();
        Law_item_leibie_Show();
        // Law_item_pingfen_Show();
        // descriptionShow();
        teamShow();

    layui.use(['form','jquery','layer'],function(){
        var form = layui.form,
/*
            $    = layui.jquery,
*/
            layer = layui.layer;

        form.on('select(type2)', function (data) {
            Law_item_pingfen_Show(data.value);
        });

        form.on('select(type3)', function (data) {
            descriptionShow(data.value);
        });

        form.on('select(description)', function (data) {
            Point_Show(data.value);
        });

        form.on("submit(addItem)",function(data){
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            $.post("${base}/admin/system/ggb/add",
                data.field,

                function(res){
                layer.close(loadIndex);
                if(res.success){
                    parent.layer.msg("添加成功!",{time:1000},function(){
                        //刷新父页面
                        parent.location.reload();
                    });
                }else{
                    console.log(data.field)
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
    }
    //Law_item_Show-评价项目下拉框-type
    function Law_item_Show() {
        // $("#type").html('<option value="">选择评价项目</option>');
        $.ajax({
            url: '${base}/item/queryType',
            type: 'get',
            dataType: 'json',
            data: {"user": "ggb"},
            success: function (data) {
                if (!data.success) {
                    alert(data.msg);
                } else {
                    $.each(data.type, function (key, value) {
                        var option = new Option(value.law_item);
                        $('#type').append(option);//往下拉菜单里添加元素
                        renderForm();
                    });
                }
            },
            error: function (data) {
                alert(data.msg);
                console.log(data.msg);
            }
        });
    }
    //Law_item_leibie_Show-类别下拉框-type2
    function Law_item_leibie_Show() {
        // $("#type").html('<option value="">选择类别</option>');
        $.ajax({
            url: '${base}/item/queryType2',
            type: 'get',
            dataType: 'json',
            data: {"user": "ggb"},
            success: function (data) {
                if (!data.success) {
                    alert(data.msg);
                } else {
                    $.each(data.type2, function (key, value) {
                        var option = new Option(value.law_item_leibie);
                        $('#type2').append(option);//往下拉菜单里添加元素
                        renderForm();
                    });
                }
            },
            error: function (data) {
                alert(data.msg);
                console.log(data.msg);
            }
        });
    }
    //Law_item_pingfen_Show-评分内容下拉框-type3
    function Law_item_pingfen_Show(law_item_leibie) {
        // $("#type").html('<option value="">选择评分内容</option>');
        $.ajax({
            url: '${base}/item/queryType3',
            type: 'get',
            dataType: 'json',
            data: {"law_item_leibie": law_item_leibie ,"user": "ggb"},
            success: function (data) {
                if (!data.success) {
                    alert(data.msg);
                } else {
                    $("#type3").empty();
                    $.each(data.type3, function (key, value) {
                        var option = new Option(value.law_item_pingfen);
                        $('#type3').append(option);//往下拉菜单里添加元素
                        renderForm();
                    });
                }
            },
            error: function (data) {
                alert(data.msg);
                console.log(data.msg);
            }
        });
    }
    //teamShow-中队选择下拉框-team
    function teamShow() {
        // $("#team").html('<option value="">选择中队</option>');
        $.ajax({
            url: '${base}/item/queryTeam',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                if (!data.success) {
                    alert(data.msg);
                } else {
                    $.each(data.team, function (key, value) {
                        var option = new Option(value.team);
                        $('#label').append(option);//往下拉菜单里添加元素 select元素的id是label
                        renderForm();
                    });
                }
            },
            error: function (data) {
                alert(data.msg);
                console.log(data.msg);
            }
        });
    }
    //descriptionShow-评分标准下拉框-description
    function descriptionShow(law_item_pingfen) {
        // $("#description").html('<option value="">选择评分标准</option>');
        $.ajax({
            url: '${base}/item/queryDescription',
            type: 'get',
            dataType: 'json',
            data: {"law_item_pingfen": law_item_pingfen , "user" : "ggb"},
            success: function (data) {
                if (!data.success) {
                    alert(data.msg);
                } else {
                    $("#description").empty();
                    $.each(data.description, function (key, value) {
                        var option = new Option(value.description);
                        $('#description').append(option);//往下拉菜单里添加元素
                        renderForm();
                    });
                }
            },
            error: function (data) {
                alert(data.msg);
                console.log(data.msg);
            }
        });
    }
    //Point_Show-加/扣分数下拉框-value
    function Point_Show(description) {
        // $("#value").html('<option value="">选择分值</option>');
        $.ajax({
            url: '${base}/item/queryPoint',
            type: 'get',
            dataType: 'json',
            data: {"description": description , "user" : "ggb"},
            success: function (data) {
                if (!data.success) {
                    alert(data.msg);
                } else {
                    $("#value").empty();
                    $.each(data.point, function (key, value) {
                        var option = new Option(value.point);
                        $('#value').append(option);//往下拉菜单里添加元素
                        renderForm();
                    });
                }
            },
            error: function (data) {
                alert(data.msg);
                console.log(data.msg);
            }
        });
    }

    function renderForm() {
        layui.use('form', function () {
            var form = layui.form;
            form.render();
        });
    }
</script>
</body>
</html>

