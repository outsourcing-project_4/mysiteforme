<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑条目--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <!-- 页面描述 -->
    <meta name="description" content="${site.description}"/>
    <!-- 页面关键词 -->
    <meta name="keywords" content="${site.keywords}"/>
    <!-- 网页作者 -->
    <meta name="author" content="${site.author}"/>
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <style type="text/css">
        .layui-form-item .layui-inline{ width:33.333%; float:left; margin-right:0; }
        @media(max-width:1240px){
            .layui-form-item .layui-inline{ width:100%; float:none; }
        }
        .layui-form-item .role-box {
            position: relative;
        }
        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
    <input type="hidden" name="id" value="${dict_dcs2.id}">
    <div class="layui-form-item">
        <label class="layui-form-label">评价项目</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input " name="type" value="${dict_dcs2.type}" disabled lay-verify="required" placeholder="请输入评价项目">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">类别</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input " name="type2" value="${dict_dcs2.type2}" disabled lay-verify="required" placeholder="请输入类别">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">评价内容</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input " name="type3" value="${dict_dcs2.type3}" disabled lay-verify="required" placeholder="请输入评价内容">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">考核中队</label>
        <div class="layui-input-block">
            <#--            <input type="text" class="layui-input" name="label" value="${dict_dcs2.label}" lay-verify="required" placeholder="请输入标签">-->
            <select name="label" id="label" lay-verify="required" lay-search="" disabled>
                <option >${dict_dcs2.label}</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">完成数</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input " name="value" value="${dict_dcs2.complete}" disabled lay-verify="required" placeholder="请输入值">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">总数</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input " name="value" value="${dict_dcs2.total}" disabled lay-verify="required" placeholder="请输入值">
        </div>
    </div>
    <div class="layui-form-item" style="display: none">
        <label class="layui-form-label">排序</label>
        <div class="layui-input-block">
            <input type="number" class="layui-input" name="sort" value="${dict_dcs2.sort}" lay-verify="required|number" placeholder="请输入排序值">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">评分标准</label>
        <div class="layui-input-block">
            <#--            <textarea name="description" class="layui-textarea" placeholder="可以输入该标签描述">${dict_dcs.description}</textarea>-->
            <input type="text" class="layui-input" name="description" value="${dict_dcs2.description}" disabled lay-verify="required" placeholder="请输入评分标准">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-block">
            <textarea name="remarks" class="layui-textarea" disabled placeholder="标签描述">${dict_dcs.remarks}</textarea>
        </div>
    </div>
<#--    <div class="layui-form-item">-->
<#--        <div class="layui-input-block">-->
<#--            <button class="layui-btn" lay-submit="" lay-filter="addUser">立即提交</button>-->
<#--            <button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
<#--        </div>-->
<#--    </div>-->
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/jquery.min.js"></script>
<script type="text/javascript">
    window.onload=function(){
        // 初始化内容
        teamShow();

        layui.use(['form','jquery','layer'],function(){
            var form = layui.form,
                $    = layui.jquery,
                layer = layui.layer;

            form.on("submit(addUser)",function(data){
                var loadIndex = layer.load(2, {
                    shade: [0.3, '#333']
                });
                $.post("${base}/admin/system/dcs/edit",data.field,function(res){
                    layer.close(loadIndex);
                    if(res.success){
                        parent.layer.msg("条目编辑成功!",{time:1000},function(){
                            //刷新父页面
                            parent.location.reload();
                        });
                    }else{
                        layer.msg(res.message);
                    }
                });
                return false;
            });

        });
    }

    //teamShow-中队选择下拉框-team
    function teamShow() {
        $.ajax({
            url: '${base}/item/queryTeam',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                if (!data.success) {
                    alert(data.msg);
                } else {
                    $.each(data.team, function (key, value) {
                        var option = new Option(value.team);
                        $('#label').append(option);//往下拉菜单里添加元素 select元素的id是label
                        renderForm();
                    });
                }
            },
            error: function (data) {
                alert(data.msg);
                console.log(data.msg);
            }
        });
    }

    function renderForm() {
        layui.use('form', function () {
            var form = layui.form;
            form.render();
        });
    }
</script>
</body>
</html>