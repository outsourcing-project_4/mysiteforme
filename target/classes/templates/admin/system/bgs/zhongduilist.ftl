<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>中队分数排名列表--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel = "shortcut icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all" />
    <style>
        .detail-body{
            margin: 20px 0 0;
            color: #333;
            word-wrap: break-word;
        }
        .layui{
            cursor: pointer;
            font-weight: bold;
        }
    </style>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>中队分数排名列表(总)</legend>
    <div class="layui-field-box" >
<#--        <a class="layui-btn layui-btn-danger" lay-event="settlement">结算</a>-->
<#--        <a class="layui-btn layui-btn-primary" lay-event="exportData">导出数据</a>-->
<#--        <button class="layui-btn layui-btn-danger" id="settlement" lay-filter="settlement">结算</button>-->
<#--        <button class="layui-btn layui-btn-primary" id="exportData" lay-filter="exportData">导出数据</button>-->
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>

    <script type="text/html" id="userStatus">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.delFlag == false){ }}
        <span class="layui-badge layui-bg-green">正常</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-gray">停用</span>
        {{#  } }}
    </script>
    <script type="text/html" id="barDemo">
        {{# if(d.description == null || d.description == "" || d.description == undefined || d.description.indexOf('系统')<0){ }}
<#--        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="addType" >新增一个TYPE值</a>-->
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
<#--        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="editType" >编辑TYPE</a>-->
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
        {{# } }}
    </script>
</div>
<div id="page"></div>

<#--这里的type就是评价项目，下面的代码可以修改评价项目，暂时无用-->
<div id="changeTypeDiv" style="display: none" class="detail-body">
    <form  id="changeTypeForm" class="layui-form" style="width: 500px">
        <div class="layui-form-item">
            <label class="layui-form-label">原TYPE值</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input layui-disabled" name="oldType" disabled lay-verify="required" placeholder="请输入原TYPE值">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">新TYPE值</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" name="newType" lay-verify="required" placeholder="请输入新TYPE值">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="changeType">立即提交</button>
            </div>
        </div>
    </form>
</div>
<#--<script type="text/html" id="toolbarDemo">-->
<#--    <div class="layui-btn-container">-->
<#--        <button class="layui-btn layui-btn-danger layui-btn-lg" lay-event="settlement">结算</button>-->
<#--        <button class="layui-btn layui-btn-primary layui-border-red layui-btn-lg" lay-event="resetScore">重置分数</button>-->
<#--    </div>-->
<#--</script>-->
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script type="text/javascript" src="${base}/static/layui/lay/modules/table.js"></script>
<script>
    layui.use(['layer','form','table'], function() {
        var layer = layui.layer,
                $ = layui.jquery,
                form = layui.form,
                table = layui.table,
                t,changeIndex;                  //表格数据变量
        var slider=layui.slider;

        t = {
            id: 'test',
            elem: '#test',
            url:'${base}/teamPoint/zhongduiPoint',
            toolbar:'#toolbarDemo',
            defaultToolbar: ['filter', 'exports'],
            title:'中队分数排名表',
            method:'post',
            // page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            //     layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
            //     //,curr: 5 //设定初始在第 5 页
            //     groups: 2, //只显示 1 个连续页码
            //     first: "首页", //显示首页
            //     last: "尾页", //显示尾页
            //     limits:[3,10, 20, 30]
            // },
            width: $(parent.window).width()-223,
            cols: [[
                // {type:'checkbox'},
                // {field:'type',title: '评价项目'},
                // {field:'type2',title: '类别'},
                {field:'rank',title: '排名',width:'5%',type:'numbers',rowspan:3,fixed:'left'},
                {field:'team',title: '中队',width:'8%',rowspan:3,fixed:'left'},
                {field:'point_jck_srzx',title: '<a class="layui" data-type="jump1">市容秩序</a>',width:'6%',rowspan:3},
                {field:'point_zwb',title: '<a class="layui" data-type="jump2">违法建设</a>',width:'6%',rowspan:3},
                {field:'point_ggb',title: '<a class="layui" data-type="jump3">广告牌匾</a>',width:'6%',rowspan:3},
                {align: 'center', title: '<a class="layui" data-type="jump4">市容环卫执法</a>', colspan: 4}, //colspan即横跨的单元格数，这种情况下不用设置field和width
                {align: 'center', title: '<a class="layui" data-type="jump5">市容公用执法</a>', colspan: 3},
                {align: 'center', title: '<a class="layui" data-type="jump6">园林与环境执法</a>', colspan: 3},
                {align: 'center', title: '<a class="layui" data-type="jump7">物业执法</a>', colspan: 3},
                {field:'point_jck_qtaj',title: '<a class="layui" data-type="jump8">其他案件</a>',width:'6%',rowspan:3},
                {field:'point_jck_ywld',title: '<a class="layui" data-type="jump9">亮点工作</a>',width:'6%',rowspan:3},
                {align: 'center', title: '非现场执法', colspan: 2},
                {field:'point_fzk', title: '<a class="layui" data-type="jump11">执法监督</a>', width:'6%',rowspan:3},
                {field:'point_dsjk',title: '<a class="layui" data-type="jump12">作风督察</a>',width:'6%',rowspan:3},
                {field:'point_xfk',title: '<a class="layui" data-type="jump13">信访诉求</a>',width:'6%',rowspan:3},
                {field:'point_xck',title: '<a class="layui" data-type="jump14">宣传报道</a>',width:'6%',rowspan:3},
                {field:'point_zgk',title: '<a class="layui" data-type="jump15">队伍建设</a>',width:'6%',rowspan:3},
                {field:'point_ldpy',title: '领导评议',width:'6%',rowspan:3},
                {field:'point_total',title: '总分',rowspan:3}
        ],[
                {field: 'point_jck_hjws_zljy', title: '占路经营', width: '6%',rowspan:2}
                ,{field: 'point_jck_hjws_xgg', title: '小广告', width: '6%',rowspan:2}
                ,{field: 'point_jck_hjws_ws', title: '卫生责任区', width: '8%',rowspan:2}
                ,{field: 'point_jck_hjws_ljfl', title: '垃圾分类', width: '6%',rowspan:2}
                ,{field: 'point_jck_szss_rq', title: '燃气', width: '5%',rowspan:2}
                ,{field: 'point_jck_szss_pw', title: '排污', width: '5%',rowspan:2}
                ,{field: 'point_jck_szss_jl', title: '掘路', width: '5%',rowspan:2}
                ,{field: 'point_jck_yllh_lh', title: '绿化', width: '5%',rowspan:2}
                ,{field: 'point_jck_yllh_yy', title: '油烟噪声扬尘', width: '8%',rowspan:2}
                ,{field: 'point_jck_yllh_ztc', title: '渣土车', width: '6%',rowspan:2}
                ,{field: 'point_jck_wyzf_wgqy', title: '物管处罚', width: '6%',rowspan:2}
                ,{align: 'center', title: '物业小区环境整治', colspan: 2}
                ,{field: 'point_dcs_ajcz', title: '<a class="layui" data-type="jump10">派件处置</a>', width: '6%',rowspan:2}
                ,{field: 'point_dcs_hs', title: '<a class="layui" data-type="jump16">诉转案</a>', width: '8%',rowspan:2}
            ],[
                {field: 'point_jck_wyzf_qlhl', title: '圈绿毁绿', width: '6%'}
                ,{field: 'point_jck_wyzf_ldlf2', title: '地装地锁乱放', width: '8%'}
            ]
            ],
            done: function (res, curr, count) {
                exportData = res.data;

            }
        };
        console.log(layui.table);
        table.render(t);
        // 渲染滑块
        slider.render({
            elem: '#test.slider',
            max: 100, // 滑块最大值
            range: true, // 是否开启范围选择
            change: function(value){ // 滑块值改变时的回调函数
                console.log(value); // 打印滑块的值
            }
        });


        table.on('toolbar(demo)', function(obj){
            if(obj.event === "settlement"){
                layer.confirm("结算操作不可逆，确定要结算么？",{btn:['是的,我确定','我再想想']},
                    function(){
                        table.exportFile('test', exportData, 'xls');
                        $.post("${base}/teamPoint/settlement",function (res){
                            if(res.success){
                                layer.msg("结算成功",{time: 2000},function(){
                                    table.reload('test', t);
                                });
                            }else{
                                layer.msg("结算出错 "+res.message,{time:10000});
                            }

                        });
                    }
                )
            }
            if(obj.event === "resetScore"){
                layer.confirm("重置分数操作不可逆，确定要重置么？",{btn:['是的,我确定','我再想想']},
                    function(){
                        $.post("${base}/teamPoint/resetPoint",function (res){
                            if(res.success){
                                layer.msg("重置成功",{time: 2000},function(){
                                    table.reload('test', t);
                                });
                            }else{
                                layer.msg("重置出错 "+res.message,{time:5000});
                            }

                        });
                    }
                )
            }

        });

        //搜索
        form.on("submit(searchForm)",function(data){
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

        form.on("submit(changeType)",function (data) {
            if(data.field.oldType == null || data.field.oldType === ""){
                layer.msg("原TYPE值不能为空");
                return false;
            }
            $.post("${base}/admin/system/dict/editType",data.field,function(res){
                if(!res.success){
                    layer.msg(res.message);
                }else{
                    layer.msg("修改成功",{time:1000},function () {
                        table.reload('test', t);
                    })
                }
                layer.close(changeIndex);
            });
            return false;
        });

//功能按钮添加字典
        var activeJump1={
            jump1 : function(){
                var jump1Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/jck_srzx/zdList",
                    success : function(layero, jump1Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump1Index);
                });
                layer.full(jump1Index);
            }
        };
        var activeJump2={
            jump2 : function(){
                var jump2Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/dict/zdList",
                    success : function(layero, jump2Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump2Index);
                });
                layer.full(jump2Index);
            }
        };
        var activeJump3={
            jump3 : function(){
                var jump3Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/ggb/zdList",
                    success : function(layero, jump3Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump3Index);
                });
                layer.full(jump3Index);
            }
        };
        var activeJump4={
            jump4 : function(){
                var jump4Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/jck_hjws/zdList",
                    success : function(layero, jump4Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump4Index);
                });
                layer.full(jump4Index);
            }
        };
        var activeJump5={
            jump5 : function(){
                var jump5Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/jck_szss/zdList",
                    success : function(layero, jump5Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump5Index);
                });
                layer.full(jump5Index);
            }
        };
        var activeJump6={
            jump6 : function(){
                var jump6Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/jck_yllh/zdList",
                    success : function(layero, jump6Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump6Index);
                });
                layer.full(jump6Index);
            }
        };
        var activeJump7={
            jump7 : function(){
                var jump7Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/jck_wyzf/zdList",
                    success : function(layero, jump7Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump7Index);
                });
                layer.full(jump7Index);
            }
        };
        var activeJump8={
            jump8 : function(){
                var jump8Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/jck_qtaj/zdList",
                    success : function(layero, jump8Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump8Index);
                });
                layer.full(jump8Index);
            }
        };
        var activeJump9={
            jump9 : function(){
                var jump9Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/jck_ywld/zdList",
                    success : function(layero, jump9Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump9Index);
                });
                layer.full(jump9Index);
            }
        };
        var activeJump10={
            jump10 : function(){
                var jump10Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/dcs/zdList2",
                    success : function(layero, jump10Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump10Index);
                });
                layer.full(jump10Index);
            }
        };
        var activeJump16={
            jump16 : function(){
                var jump16Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/dcs/zdList",
                    success : function(layero, jump16Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump16Index);
                });
                layer.full(jump16Index);
            }
        };
        var activeJump11={
            jump11 : function(){
                var jump11Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/fzk/zdList",
                    success : function(layero, jump11Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump11Index);
                });
                layer.full(jump11Index);
            }
        };
        var activeJump12={
            jump12 : function(){
                var jump12Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/dsjk/zdList",
                    success : function(layero, jump12Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump12Index);
                });
                layer.full(jump12Index);
            }
        };
        var activeJump13={
            jump13 : function(){
                var jump13Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/xfk/zdList",
                    success : function(layero, jump13Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump13Index);
                });
                layer.full(jump13Index);
            }
        };
        var activeJump14={
            jump14 : function(){
                var jump14Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/xck/zdList",
                    success : function(layero, jump14Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump14Index);
                });
                layer.full(jump14Index);
            }
        };
        var activeJump15={
            jump15 : function(){
                var jump15Index = layer.open({
                    title : "中队分数",
                    type : 2,
                    content : "${base}/admin/system/zgk/zdList",
                    success : function(layero, jump15Index){
                        setTimeout(function(){
                            layer.tips('点击此处返回考核条目管理列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },1000);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(jump15Index);
                });
                layer.full(jump15Index);
            }
        };

        //功能按钮更改

        $('.layui').on('click', function(){
            var type = $(this).data('type');
            activeJump1[type] ? activeJump1[type].call(this) : '';
            activeJump2[type] ? activeJump2[type].call(this) : '';
            activeJump3[type] ? activeJump3[type].call(this) : '';
            activeJump4[type] ? activeJump4[type].call(this) : '';
            activeJump5[type] ? activeJump5[type].call(this) : '';
            activeJump6[type] ? activeJump6[type].call(this) : '';
            activeJump7[type] ? activeJump7[type].call(this) : '';
            activeJump8[type] ? activeJump8[type].call(this) : '';
            activeJump9[type] ? activeJump9[type].call(this) : '';
            activeJump10[type] ? activeJump10[type].call(this) : '';
            activeJump11[type] ? activeJump11[type].call(this) : '';
            activeJump12[type] ? activeJump12[type].call(this) : '';
            activeJump13[type] ? activeJump13[type].call(this) : '';
            activeJump14[type] ? activeJump14[type].call(this) : '';
            activeJump15[type] ? activeJump15[type].call(this) : '';
            activeJump16[type] ? activeJump16[type].call(this) : '';
        });

    });
</script>
</body>
</html>