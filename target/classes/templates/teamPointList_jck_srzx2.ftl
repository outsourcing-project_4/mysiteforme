<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>中队分数排名列表_监察科(市容秩序)--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel = "shortcut icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all" />
    <style>
        .detail-body{
            margin: 20px 0 0;
            color: #333;
            word-wrap: break-word;
        }
    </style>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>中队分数排名列表(23.4季度)_监察科(市容秩序)</legend>
    <div class="layui-field-box" >
        <form class="layui-form">
            <#--        <div class="layui-inline">-->
            <#--            <input type="text" value="" name="s_type" placeholder="请输入评价项目" class="layui-input search_input">-->
            <#--        </div>-->
            <#--        <div class="layui-inline">-->
            <#--            <input type="text" value="" name="s_label" placeholder="请输入考核中队" class="layui-input search_input">-->
            <#--        </div>-->
            <#--        <div class="layui-inline">-->
            <#--            <a class="layui-btn" lay-submit="" lay-filter="searchForm">查询</a>-->
            <#--        </div>-->
            <#--        <div class="layui-inline">-->
            <#--            <button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
            <#--        </div>-->
        </form>
        <#--选择查询日期范围的表单-->
        <#--        <form class="layui-form" style="width:80%;float: left">-->
        <#--            &lt;#&ndash;选择查询日期范围&ndash;&gt;-->
        <#--            <div class="layui-form-item" style="float: left">-->
        <#--                <div class="layui-input-block">-->
        <#--                    <select name="value" id="value" lay-verify="required" lay-search="">-->
        <#--                        <option value="">选择查询日期范围</option>-->
        <#--                    </select>-->
        <#--                </div>-->
        <#--            </div>-->
        <#--        </form>-->
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>

    <script type="text/html" id="userStatus">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.delFlag == false){ }}
        <span class="layui-badge layui-bg-green">正常</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-gray">停用</span>
        {{#  } }}
    </script>
    <script type="text/html" id="barDemo">
        {{# if(d.description == null || d.description == "" || d.description == undefined || d.description.indexOf('系统')<0){ }}
        <#--        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="addType" >新增一个TYPE值</a>-->
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <#--        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="editType" >编辑TYPE</a>-->
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
        {{# } }}
    </script>
</div>
<div id="page"></div>

<#--这里的type就是评价项目，下面的代码可以修改评价项目，暂时无用-->
<div id="changeTypeDiv" style="display: none" class="detail-body">
    <form  id="changeTypeForm" class="layui-form" style="width: 500px">
        <div class="layui-form-item">
            <label class="layui-form-label">原TYPE值</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input layui-disabled" name="oldType" disabled lay-verify="required" placeholder="请输入原TYPE值">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">新TYPE值</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" name="newType" lay-verify="required" placeholder="请输入新TYPE值">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="changeType">立即提交</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer','form','table'], function() {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            t,changeIndex;                  //表格数据变量

        t = {
            elem: '#test',
            url:'${base}/teamPoint/jck_historySrzxPoint2',
            method:'post',
            // page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            //     layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
            //     //,curr: 5 //设定初始在第 5 页
            //     groups: 2, //只显示 1 个连续页码
            //     first: "首页", //显示首页
            //     last: "尾页", //显示尾页
            //     limits:[3,10, 20, 30]
            // },
            // width: $(parent.window).width()-223,
            width: $(parent.window).width(),
            cols: [[
                // {type:'checkbox'},
                // {field:'type',title: '评价项目'},
                // {field:'type2',title: '类别'},
                {field:'rank',title: '排名',width:'5%',type:'numbers'},
                {field:'team',title: '中队',width:'45%'},
                {field:'point_jck_srzx',title: '分数',width:'50%'},
                // {field:'description',sort:true,title: '评分标准',width: '22%'},
                // {field:'createDate', sort:true ,width: '11%',title: '创建时间',templet:'<div>{{ layui.laytpl.toDateString(d.createDate) }}</div>',unresize: true}, //单元格内容水平居中
            ]]
        };
        table.render(t);

        //搜索
        form.on("submit(searchForm)",function(data){
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

        form.on("submit(changeType)",function (data) {
            if(data.field.oldType == null || data.field.oldType === ""){
                layer.msg("原TYPE值不能为空");
                return false;
            }
            $.post("${base}/admin/system/dict/editType",data.field,function(res){
                if(!res.success){
                    layer.msg(res.message);
                }else{
                    layer.msg("修改成功",{time:1000},function () {
                        table.reload('test', t);
                    })
                }
                layer.close(changeIndex);
            });
            return false;
        });

    });
</script>
</body>
</html>